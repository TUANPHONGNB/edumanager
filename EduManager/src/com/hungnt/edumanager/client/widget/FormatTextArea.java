package com.hungnt.edumanager.client.widget;

import com.google.gwt.user.client.ui.TextArea;
import com.hungnt.edumanager.shared.Config;

public class FormatTextArea extends TextArea {

	public FormatTextArea() {
		super();
	}
	
	@Override
	public void setText(String text) {
		text = text.replaceAll(Config.HTML_BREAK_LINE, "\n");
		super.setText(text);
	}
	
	@Override
	public String getText() {
		String text = super.getText();
		text = text.replaceAll("\n", Config.HTML_BREAK_LINE);
		return text;
	}
}
