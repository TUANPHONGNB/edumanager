package com.hungnt.edumanager.client.widget;

import com.google.gwt.user.client.ui.ListBox;

public class HListBox extends ListBox {
	public HListBox() {
		super();
	}
	
	public void setSelectedValue(int value) {
		setSelectedValue(value + "");
	}

	public void setSelectedValue(long value) {
		setSelectedValue(value + "");
	}
	
	public void setSelectedValue(String value) {
		for (int i = 0; i < this.getItemCount(); i ++) {
			if (this.getValue(i).equalsIgnoreCase(value)) {
				this.setSelectedIndex(i);
				break;
			}
		}
	}
}
