package com.hungnt.edumanager.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.shared.Teacher;

public class TeacherSelector extends VerticalPanel {
	private HListBox authorList = new HListBox();
	private List<Long> selectedIds = new ArrayList<Long>();
	private List<String> selectedNames = new ArrayList<String>();
	private FlowPanel nameFlow = new FlowPanel();
	
	public TeacherSelector() {
		this.add(authorList);
		this.add(nameFlow);
		authorList.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				if (authorList.getSelectedValue().isEmpty())
					return;
				final Long id = Long.parseLong(authorList.getSelectedValue());
				final String name = authorList.getSelectedItemText();
				if (!selectedIds.contains(id)) {
					selectedIds.add(id);
					selectedNames.add(name);
					addNameFlow(id, authorList.getSelectedItemText());
				}
			}
		});
	}
	
	private void addNameFlow(final Long id, final String name) {
		final Anchor nameAnchor = new Anchor();
		nameAnchor.setHTML(name + "[X]&nbsp;&nbsp;&nbsp;");
		nameAnchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				selectedIds.remove(id);
				selectedNames.remove(name);
				nameAnchor.removeFromParent();
			}
		});
		nameFlow.add(nameAnchor);
	}
	
	public List<Long> getSelectedIds() {
		return this.selectedIds;
	}
	
	public List<String> getSelectedStrings() {
		return this.selectedNames;
	}
	
	public void setupAuthors(List<Teacher> teachers) {
		authorList.clear();
		authorList.addItem("[Chọn]", "");
		for (Teacher s : teachers) {
			authorList.addItem(s.getFullName(), s.getId() + "");
		}
	}
	
	public void setSelectedIds(List<Long> ids) {
		selectedIds = ids;
		nameFlow.clear();
		selectedNames.clear();
		for (int i = 0; i < authorList.getItemCount(); i ++) {
			String val = authorList.getValue(i);
			if (val != null && !val.isEmpty()) {
				Long id = Long.parseLong(val);
				if (selectedIds.contains(id)) {
					selectedNames.add(authorList.getItemText(i));
					addNameFlow(id, authorList.getItemText(i));
				}
			}
		}
	}
}