package com.hungnt.edumanager.client.widget;

import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.TextBox;

public class NumberTextBox extends TextBox {
	public NumberTextBox() {
		super();
		this.addKeyPressHandler(new KeyPressHandler() {
		    @Override
		    public void onKeyPress(KeyPressEvent event) {
		    	if(!Character.isDigit(event.getCharCode()))
	                ((NumberTextBox)event.getSource()).cancelKey();
		    }
		});
	}
}
