package com.hungnt.edumanager.client;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Window;

public class ClientUtils {
	static String EMAIL_VALIDATION_REGEX = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

	public static boolean validateEmail(String email) {
		return email.matches(EMAIL_VALIDATION_REGEX);
	}

	public static void noPermission() {
		Window.alert("Xin lỗi, bạn không có quyền sử dụng chức năng này");
	}

	public static native void log(String message) /*-{
		$wnd.console.log(message);
	}-*/;
	
	public static String getFileSizeInKb(long bytes) {
		return NumberFormat.getFormat("000.00").format(bytes/1000);
	}
}
