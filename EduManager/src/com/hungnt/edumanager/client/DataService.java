package com.hungnt.edumanager.client;

import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.hungnt.edumanager.shared.Category;
import com.hungnt.edumanager.shared.Course;
import com.hungnt.edumanager.shared.Equipment;
import com.hungnt.edumanager.shared.EquipmentHistory;
import com.hungnt.edumanager.shared.Event;
import com.hungnt.edumanager.shared.InternTopic;
import com.hungnt.edumanager.shared.Material;
import com.hungnt.edumanager.shared.MeetingMinutes;
import com.hungnt.edumanager.shared.Publication;
import com.hungnt.edumanager.shared.ServerException;
import com.hungnt.edumanager.shared.Subject;
import com.hungnt.edumanager.shared.Teacher;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("data")
public interface DataService extends RemoteService {

	Teacher updateTeacher(String userId, Teacher teacher);

	Subject updateSubject(String userId, Subject obj) throws ServerException;

	Course updateCourse(String userId, Course obj) throws ServerException;

	List<Teacher> getTeachers(String userId, int scholarYear);

	List<Subject> getSubjects(String userId);

	List<Course> getCourses(String userId, int scholarYear, int semester);

	Teacher login(String sessionId, String userName, String password);

	List<Publication> getPublications(String userId);

	Publication updatePublication(String userId, Publication obj);

	String getBlobstoreUploadUrl();

	Teacher checkLogin(String sessionId);

	void logout(String sessionId);

	Equipment updateEquipment(String userId, Equipment object);

	List<Equipment> getEquipments(String userId, Date now);

	String export(Long userId, Teacher teacher);

	InternTopic updateTopic(String userId, InternTopic obj)
			throws ServerException;

	List<InternTopic> getTopics(String userId, int scholarYear, int semester);

	void updateAB();

	void deleteTopic(String userId, InternTopic obj);

	EquipmentHistory updateEquipmentHistory(EquipmentHistory history) throws ServerException;

	void deleteCourse(Long userId, Course obj);
	
	Long doUpdateCategory(Category category);
	
	List<Category> getCategories(Long parentId, Long ownerId, Long viewerId);
	
	Category getChildCategoriesAndMaterials(Long parentId);
	
	Long doUpdateMaterial(Material material);

	void delete(Long userId, Material material);

	List<Event> getEvents(String userId, Date now);

	Event updateEvent(String userId, Event object);

	Teacher changePassword(String sessionId, Long id,
			String oldPassword, String newPassword);

	void updateStatistics(long userId, int scholarYear, int semester);

	Long doUpdateMeeting(MeetingMinutes meeting);

	List<MeetingMinutes> getMeetings(int scholarYear, int semester);
}
