package com.hungnt.edumanager.client.activities.login;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class LoginView {
	
	private FlexTable flexTable = new FlexTable();
	private TextBox userName = new TextBox();
	private PasswordTextBox password = new PasswordTextBox();
	private Button login = new Button("Đăng nhập");
	private DialogBox dialogBox = null;
	
	public LoginView() {
		userName.setWidth("200px");
		password.setWidth("200px");
		flexTable.setWidth("100%");
		int row = 0, col = 0;
		flexTable.setText(row, col++, "Tên đăng nhập");
		flexTable.setWidget(row, col++, userName);
		row ++; col = 0;
		flexTable.setText(row, col++, "Mật khẩu");
		flexTable.setWidget(row, col++, password);
		row ++; col = 0;
		flexTable.setWidget(row, 1, login);
		row ++; col = 0;
		//password.setText(Config.DEFAULT_PASSWORD);
		//userName.setText("hungnt@soict.hust.edu.vn");
	}
	
	public void view() {
		if (dialogBox == null) {
			dialogBox = new DialogBox(true);
			//dialogBox.setWidth("640px");
		    dialogBox.setGlassEnabled(true);
		    dialogBox.setAnimationEnabled(false);
		    // Create a table to layout the content
		    VerticalPanel dialogContents = new VerticalPanel();
		    dialogContents.setWidth("100%");
		    dialogBox.setWidget(dialogContents);
		    flexTable.setWidth("100%");
		    dialogContents.add(flexTable);
		    dialogContents.setCellHorizontalAlignment(flexTable, HasHorizontalAlignment.ALIGN_CENTER);
		}
	    dialogBox.setText("Đăng nhập");
	    dialogBox.center();
        dialogBox.show();
	}
	
	public DialogBox getDialogBox() {
		return this.dialogBox;
	}
	
	public String getUserName() {
		return this.userName.getText().trim();
	}
	
	public String getPassword() {
		return this.password.getText().trim();
	}
	
	public HasClickHandlers getLoginButton() {
		return this.login;
	}
}