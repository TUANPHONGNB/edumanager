package com.hungnt.edumanager.client.activities.teacher.details;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.BeforeSelectionEvent;
import com.google.gwt.event.logical.shared.BeforeSelectionHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;
import com.hungnt.edumanager.client.activities.course.CourseListViewer;
import com.hungnt.edumanager.client.activities.topic.TopicListViewer;
import com.hungnt.edumanager.shared.Course;
import com.hungnt.edumanager.shared.InternTopic;
import com.hungnt.edumanager.shared.Teacher;

public class TeacherDetailViewImpl extends BasicViewImpl implements TeacherDetailView {
	
	private VerticalPanel teacherViewPanel = new VerticalPanel();
	private FlexTable flexTable = new FlexTable();
	private CourseListViewer courseListViewer = new CourseListViewer();
	private TopicListViewer topicViewer = new TopicListViewer();
	//private HTML teachingHTML = new HTML("<p style=\"font-size:20px\">Các môn đang giảng dạy</p>");
	private HTML teacherNameHtml = new HTML();
	private HorizontalPanel teacherNamePanel = new HorizontalPanel();
	private TabLayoutPanel tabPanel = new TabLayoutPanel(2.5, Unit.EM);
	private Button editButton = new Button("Sửa");
	private Button changePasswordButton = new Button("Đổi mật khẩu");
	private Teacher teacher = null;
	
	public TeacherDetailViewImpl() {	
		super();
		flexTable.setWidth("90%");
		teacherViewPanel.setWidth("100%");
//		teacherViewPanel.add(flexTable);
//		teacherViewPanel.setCellHorizontalAlignment(flexTable, ALIGN_CENTER);
		tabPanel.add(flexTable, "Thông tin", true);
		tabPanel.add(courseListViewer, "Giảng dạy", true);
		tabPanel.add(topicViewer, "Đồ án", true);
		tabPanel.add(documentView, "Tài liệu", true);
		teacherViewPanel.add(new HTML("<b> &nbsp; </b>"));
		tabPanel.setAnimationDuration(500);
	    tabPanel.getElement().getStyle().setMarginBottom(10.0, Unit.PX);
	    tabPanel.selectTab(0);
	    tabPanel.setHeight("500px");
	    tabPanel.addBeforeSelectionHandler(new BeforeSelectionHandler<Integer>() {
			
			@Override
			public void onBeforeSelection(BeforeSelectionEvent<Integer> event) {
				if (event.getItem() > 0 && !ClientData.isLoggedIn(true)) {
					event.cancel();
					return;
				}
			}
		});
	    tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				if (event.getSelectedItem() == 1) {
					courseListViewer.refresh();
				}
				else if (event.getSelectedItem() == 2) {
					topicViewer.refresh();
				}
			}
		});
	    teacherNamePanel.setVerticalAlignment(ALIGN_MIDDLE);
	    this.teacherViewPanel.add(teacherNamePanel);
		this.teacherViewPanel.add(tabPanel);
//		teacherViewPanel.add(teachingHTML);
//		teacherViewPanel.setCellHorizontalAlignment(teachingHTML, ALIGN_CENTER);
//		teacherViewPanel.add(courseListViewer);
//		teacherViewPanel.add(new HTML("<b> &nbsp; </b>"));
//		HTML topic = new HTML("<p style=\"font-size:20px\">Các đề tài Đồ án tốt nghiệp</p>");
//		teacherViewPanel.add(topic);
//		teacherViewPanel.setCellHorizontalAlignment(topic, ALIGN_CENTER);
//		teacherViewPanel.add(topicViewer);
//		teacherViewPanel.add(documentView);
		this.contentPanel.add(teacherViewPanel);
		flexTable.getCellFormatter().setWidth(0, 0, "150px");
	}
	
	@Override
	public void view(Teacher teacher) {
		this.teacher = teacher;
		courseListViewer.setFixedTeacherId(teacher.getId());
		topicViewer.setFixedTeacherId(teacher.getId());
		int row = 0, col = 0;
		teacherNameHtml.setHTML("<font color='blue' size='6'><b>" + teacher.getFullTitle() + "</b></font>");
		teacherNamePanel.clear();
		Image avatar = new Image(teacher.getAvatar());
		avatar.setHeight("100px");
		teacherNamePanel.add(avatar);
		teacherNamePanel.add(teacherNameHtml);
//		flexTable.setWidget(row, col++, new Image(teacher.getAvatar()));
//		flexTable.setWidget(row, col++, new HTML("<font color='blue'><b>" + teacher.getFullTitle() + "</b></font>"));
//		row ++; col = 0;
		flexTable.setText(row, col++, "Email");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + teacher.getWorkEmail() + "; " + teacher.getPersonalEmail() + "</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Phone");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + teacher.getWorkPhoneNumber() + "; " + teacher.getCellPhoneNumber() + "</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Địa chỉ cơ quan");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + teacher.getWorkAddress()));
		row ++; col = 0;
		flexTable.setText(row, col++, "Website");
		flexTable.setWidget(row, col++, new HTML("<a href = '" + teacher.getWebsite() + "'>" + teacher.getWebsite() + "</a>" + "</font>"));
		row ++; col = 0;
		if (ClientData.isLoggedIn()) {
			flexTable.setText(row, col++, "Mã số thuế");
			flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + teacher.getTaxNumber() + "</font>"));
			row ++; col = 0;
			flexTable.setText(row, col++, "Số CMND");
			flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + teacher.getIdNumber() + "</font>"));
			row ++; col = 0;
			flexTable.setText(row, col++, "Số tài khoản");
			flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + teacher.getBankAccount() + " " + teacher.getBankInfo() + "</font>"));
			row ++; col = 0;
			flexTable.setText(row, col++, "Địa chỉ nhà riêng");
			flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + teacher.getHomeAddress()));
			row ++; col = 0;
			flexTable.setText(row, col++, "Ghi chú");
			flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + teacher.getInfo()));
			row ++; col = 0;
		}
		if (ClientData.canEdit(teacher.getId(), false)) {
			HorizontalPanel hPanel = new HorizontalPanel();
			hPanel.setSpacing(5);
			hPanel.add(editButton);
			hPanel.add(changePasswordButton);
			row ++; col = 1;
			flexTable.setWidget(row, col++, hPanel);
		}
		List<Course> courses = new ArrayList<Course>();
		for (Long id : teacher.getCurrentCourseIds()) {
			Course course = ClientData.getCourseMap().get(id);
			if (course != null)
				courses.add(course);
		}
		//teachingHTML.setVisible(ClientData.isLoggedIn());
		if (ClientData.isLoggedIn()) {
			if (courses.size() > 0)
				courseListViewer.view(courses);
			courseListViewer.getDataGrid().setHeight("400px");
		}
		List<InternTopic> topics = new ArrayList<InternTopic>();
		for (Long id : teacher.getTopicIds()) {
			InternTopic topic = ClientData.getTopicMap().get(id);
			if (topic != null)
				topics.add(topic);
		}
		if (topics.size() > 0) {
			topicViewer.view(topics, true);
		}
		topicViewer.getDataGrid().setHeight("400px");
//		flexTable.setText(row, col++, "Các môn đang dạy");
//		VerticalPanel courses = new VerticalPanel();
//		for (Long id : teacher.getCurrentCourseIds()) {
//			Course c = ClientData.getCourseMap().get(id);
//			if (c != null) {
//				Anchor a = new Anchor(c.getSubjectName());
//				courses.add(a);
//			}
//		}
//		flexTable.setWidget(row, col++, courses);
		for (int i = 0; i <= row; i ++) {
			flexTable.getCellFormatter().setHorizontalAlignment(i, 0, ALIGN_RIGHT);
		}
		row ++; col = 0;
		documentView.setReadOnly(!ClientData.isLoggedIn() || ClientData.getCurrentUser().getId().compareTo(teacher.getId()) != 0);
	}
	
	@Override
	public HasClickHandlers getEditButton() {
		return this.editButton;
	}
	
	@Override
	public HasClickHandlers getChangePasswordButton() {
		return this.changePasswordButton;
	}
}