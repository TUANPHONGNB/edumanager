package com.hungnt.edumanager.client.activities.teacher;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.client.CssToken;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;
import com.hungnt.edumanager.shared.Teacher;

public class TeacherViewImpl extends BasicViewImpl implements TeacherView {
	
	private VerticalPanel teacherViewPanel = new VerticalPanel();
	private Button addMore = new Button("Thêm");
	private TeacherListViewer teacherListViewer = new TeacherListViewer();
	
	public TeacherViewImpl() {	
		super();
		teacherViewPanel.setSpacing(5);
		teacherViewPanel.setWidth("100%");
		
		contentPanel.add(teacherViewPanel);
	}
	
	@Override
	public HasClickHandlers getAddMoreButton() {
		return this.addMore;
	}

	@Override
	public void refreshView() {
		super.refreshView();
		teacher.setStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE, true);
	}
	
    @Override
	public  void view(List<Teacher> fullTeachers) {
    	teacherViewPanel.clear();
    	teacherViewPanel.add(teacherListViewer);
    	teacherListViewer.getFunctionPanel().add(addMore);
    	teacherListViewer.getFunctionPanel().setCellHorizontalAlignment(addMore, ALIGN_RIGHT);
    	teacherListViewer.view(fullTeachers);
	}
}
