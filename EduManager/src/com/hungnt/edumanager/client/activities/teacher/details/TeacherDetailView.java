package com.hungnt.edumanager.client.activities.teacher.details;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.IsWidget;
import com.hungnt.edumanager.client.activities.basic.BasicView;
import com.hungnt.edumanager.shared.Teacher;

public interface TeacherDetailView extends BasicView {
	public IsWidget asWidget();
	public void refreshView();
	void view(Teacher teacher);
	HasClickHandlers getEditButton();
	HasClickHandlers getChangePasswordButton();
}
