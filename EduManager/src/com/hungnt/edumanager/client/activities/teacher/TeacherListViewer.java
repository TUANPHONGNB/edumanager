package com.hungnt.edumanager.client.activities.teacher;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.AbstractEditableCell;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ImageCell;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment.HorizontalAlignmentConstant;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.activities.teacher.details.TeacherDetailPlace;
import com.hungnt.edumanager.client.event.EditEvent;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Teacher;

public class TeacherListViewer  extends VerticalPanel {
	
	private FormPanel hiddenUploadForm;
	DataGrid<Teacher> teacherList;// = new DataGrid<Teacher>();
	private HorizontalPanel topFunctionPanel = new HorizontalPanel();
	private ListBox teacherFilter = new ListBox();
	private ListBox scholarYearFilter = new ListBox();
	private ListBox semesterFilter = new ListBox();
	private Long currentSelectedTeacherId = -1L;
	private Long currentSelectedScholarYearId = null;
	private int currentSelectedSemester = 0;
	private TeacherContextMenu contextMenu = new TeacherContextMenu();
	
	public TeacherListViewer() {
		this.setWidth("100%");
		teacherFilter.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedTeacherId = Long.parseLong(teacherFilter.getSelectedValue());
				view(ClientData.getTeachers());
			}
		});
		scholarYearFilter.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
			}
		});
		scholarYearFilter.addItem(Config.getScholorYear(Config.currentScholarYear), 
				Config.currentScholarYear + "");
		semesterFilter.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedSemester = Integer.parseInt(semesterFilter.getSelectedValue());
				view(ClientData.getTeachers());
			}
		});
		semesterFilter.addItem("Cả năm", "0"); 
		semesterFilter.addItem("Kỳ 1", "1"); 
		semesterFilter.addItem("Kỳ 2", "2"); 
		semesterFilter.addItem("Kỳ 3 (Hè)", "3");
		
		teacherFilter.setWidth("200px");
		scholarYearFilter.setWidth("100px");
		semesterFilter.setWidth("100px");
		
		topFunctionPanel.setWidth("100%");
		topFunctionPanel.setSpacing(5);
		HTML filterBy = new HTML("Tìm kiếm:");
		topFunctionPanel.add(filterBy);
		topFunctionPanel.setCellWidth(filterBy, "130px");
		topFunctionPanel.add(teacherFilter);
		topFunctionPanel.add(scholarYearFilter);
		topFunctionPanel.add(semesterFilter);
		topFunctionPanel.setCellHorizontalAlignment(teacherFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(scholarYearFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(semesterFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellWidth(filterBy, "70px");
		topFunctionPanel.setCellWidth(teacherFilter, "100px");
		topFunctionPanel.setCellWidth(scholarYearFilter, "100px");
		topFunctionPanel.setCellWidth(semesterFilter, "100px");
		
		hiddenUploadForm = new FormPanel();
		hiddenUploadForm.setVisible(false);
		hiddenUploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
		hiddenUploadForm.setMethod(FormPanel.METHOD_POST);
		
		contextMenu.getDetailBtn().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				contextMenu.hide();
	    		  EduManager.clientFactory.getPlaceController().goTo(new TeacherDetailPlace(contextMenu.getTeacher()));
			}
		});
		contextMenu.getEditBtn().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				contextMenu.hide();
	    		  EduManager.clientFactory.getEventBus().fireEvent(new EditEvent(contextMenu.getTeacher()));
			}
		});
		contextMenu.getExportBtn().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				contextMenu.hide();
		    	  hiddenUploadForm.setAction(GWT.getHostPageBaseURL()+"export?" 
		    			  + Config.EXPORT_TOKEN + "=" + contextMenu.getTeacher().getId());
		    	  hiddenUploadForm.submit();
			}
		});
	}
	
	/**
	   * The provider that holds the list of contacts in the database.
	   */
	  private ListDataProvider<Teacher> dataProvider = new ListDataProvider<Teacher>();
	
	/**
   * The key provider that provides the unique ID of a contact.
   */
  public static final ProvidesKey<Teacher> KEY_PROVIDER = new ProvidesKey<Teacher>() {
    @Override
    public Object getKey(Teacher item) {
      return item == null ? null : item.getId();
    }
  };
	
	public void setupTeacherFilters(List<Teacher> teachers) {
		teacherFilter.clear();
		teacherFilter.addItem("[Tất cả giáo viên]", "0");
		for (Teacher s : teachers) {
			teacherFilter.addItem(s.getFullName(), s.getId() + "");
		}
	}
	
	public HorizontalPanel getFunctionPanel() {
		return this.topFunctionPanel;
	}
	
	public DataGrid<Teacher> getDataGrid() {
		return this.teacherList;
	}
  
	public  void view(List<Teacher> fullTeachers) {
	    // Create the table.
//		if (teachers == null || teachers.isEmpty()) {
//			teachers = dataProvider.getList();
//			for (int i = 0; i < 10; i ++) {
//				Teacher t = new Teacher("Van " + i, "Nguyen", Random.nextInt() + "", Random.nextInt(2), Random.nextInt(4), Random.nextInt(4), "asdfasfd@gmail.com", Random.nextInt(1000000) + "");
//				teachers.add(t);
//			}
//		}
     	if (teacherFilter.getItemCount() < 2)
     		setupTeacherFilters(ClientData.getTeachers());
     	List<Teacher> teachers = new ArrayList<Teacher>();
		if (currentSelectedTeacherId > 0) {
			for (Teacher teacher : fullTeachers) {
				if (teacher.getId().compareTo(currentSelectedTeacherId) == 0) {
					teachers.add(teacher);
					break;
				}
			}
			for (int i = 0; i < teacherFilter.getItemCount(); i ++) {
				if (teacherFilter.getValue(i).compareToIgnoreCase(currentSelectedTeacherId + "") == 0) {
					teacherFilter.setSelectedIndex(i);
					break;
				}
			}
		}
		if (teachers.size() == 0)
			teachers.addAll(fullTeachers);
     	
     	for (int i = 0; i < teachers.size(); i++) {
			teachers.get(i).setPosIndex(i + 1);
		}
		dataProvider.setList(teachers);
	    editableCells = new ArrayList<AbstractEditableCell<?, ?>>();
	    teacherList = new DataGrid<Teacher>(30, KEY_PROVIDER);
	    
	    teacherList.setWidth("100%");
	    teacherList.setHeight("600px");
	    
	    //teacherList.setMinimumTableWidth(140, Unit.EM);
	    dataProvider.addDataDisplay(teacherList);
	    
	    this.clear();
	    this.add(topFunctionPanel);
	    this.add(teacherList);
	    this.add(hiddenUploadForm);
//	    if (ClientData.isLoggedIn() && ClientData.getCurrentUser().isAdmin()) {
//	    	Button admin = new Button("Admin");
//	    	admin.addClickHandler(new ClickHandler() {
//				
//				@Override
//				public void onClick(ClickEvent event) {
//					new RPCCall<Void>() {
//
//						@Override
//						public void onFailure(Throwable caught) {
//							Window.alert("Failed");
//						}
//
//						@Override
//						public void onSuccess(Void result) {
//							Window.alert("Done");
//						}
//
//						@Override
//						protected void callService(AsyncCallback<Void> cb) {
//							EduManager.dataService.updateAB(cb);
//						}
//					}.retry();
//				}
//			});
//	    	teacherViewPanel.add(admin);
//	    }
	    
	    int bonus = ClientData.isLoggedIn() ? 0 : 30;
	    
	    // NumberCell.
	    //Column<Teacher, Number> numberColumn =
	    addColumn(40, HasHorizontalAlignment.ALIGN_LOCALE_END, new NumberCell(), "#", new GetValue<Number>() {
	          @Override
	          public Number getValue(Teacher contact) {
	            return contact.getPosIndex();
	          }
	        }, null);
	    
	    // ImageCell.
	    //Column<Teacher, String> avatarCol = 
	    addColumn(60, new ImageCell(), "Ảnh", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  if (contact.getAvatar() != null && !contact.getAvatar().isEmpty())
	    		  return contact.getAvatar() + "=s50";
	    	  else if (contact.getGender() == 0)
	    		  return "images/male.png";
	    	  else
	    		  return "images/female.png";
	      }
	    }, null);
	    //teacherList.setColumnWidth(avatarCol, 60, Unit.PX);
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> nameCol = 
	    addColumn(250 + bonus, new ClickableTextCell(), "Họ và tên", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	        return contact.getFullTitle();
	      }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	    	  if (!ClientData.canEdit(object.getId(), false)) {
	    		  EduManager.clientFactory.getPlaceController().goTo(new TeacherDetailPlace(object));
	    		  return;
	    	  }
	    	  contextMenu.open(object);
	      }
	    });
	    //teacherList.setColumnWidth(nameCol, 200 + bonus, Unit.PX);

//	    // TextCell.
//	    addColumn(new TextCell(), "Mã số", new GetValue<String>() {
//	      @Override
//	      public String getValue(Teacher contact) {
//	        return contact.getTeacherId();
//	      }
//	    }, null);
	    
	    // SafeHtmlCell.
	    //Column<Teacher, SafeHtml> emailCol = 
	    addColumn(200 + bonus, new SafeHtmlCell(), "Email/Phone", new GetValue<SafeHtml>() {
	      @Override
	      public SafeHtml getValue(Teacher contact) {
	    	  String info = contact.getWorkEmail();
	    	  if (!contact.getPersonalEmail().isEmpty())
	    		  info += "<br>" + contact.getPersonalEmail();
	    	  info += "<br><font color='red'>" + contact.getWorkPhoneNumber();
	    	  if (!contact.getCellPhoneNumber().isEmpty())
	    		  info += " - " + contact.getCellPhoneNumber();
	    	  info += "</font>";
	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
	    	  builder.appendHtmlConstant(info);
	    	  return builder.toSafeHtml();
	      }
	    }, null);
	    //teacherList.setColumnWidth(emailCol, 200 + bonus, Unit.PX);

	    // SafeHtmlCell.
//	    Column<Teacher, SafeHtml> phoneCol = addColumn(new SafeHtmlCell(), "Phone", new GetValue<SafeHtml>() {
//	      @Override
//	      public SafeHtml getValue(Teacher contact) {
//	    	  String info = contact.getWorkPhoneNumber();
//	    	  if (!contact.getCellPhoneNumber().isEmpty())
//	    		  info += "<br>" + contact.getCellPhoneNumber();
//	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
//	    	  builder.appendHtmlConstant(info);
//	    	  return builder.toSafeHtml();
//	      }
//	    }, null);
//	    teacherList.setColumnWidth(phoneCol, 90, Unit.PX);
	    
	    // ClickableTextCell.
//	    Column<Teacher, String> course1ANumCol = addColumn(new ClickableTextCell(), "Lớp 1A", new GetValue<String>() {
//	      @Override
//	      public String getValue(Teacher contact) {
//	    	  int aNum = 0;
//	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
//	    		  aNum = contact.getStatMap().get(currentSelectedSemester).getCourse1ANum();
//	    	  return aNum + " lớp";
//	      }
//	    }, new FieldUpdater<Teacher, String>() {
//	      @Override
//	      public void update(int index, Teacher object, String value) {
//	    	  //TODO:
//	      }
//	    });
//	    teacherList.setColumnWidth(course1ANumCol, 80, Unit.PX);
//
//	    // ClickableTextCell.
//	    Column<Teacher, String> course1BNumCol = addColumn(new ClickableTextCell(), "Lớp 1B", new GetValue<String>() {
//	      @Override
//	      public String getValue(Teacher contact) {
//	    	  int bNum = 0;
//	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
//	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getCourse1BNum();
//	    	  return bNum + " lớp";
//	     }
//	    }, new FieldUpdater<Teacher, String>() {
//	      @Override
//	      public void update(int index, Teacher object, String value) {
//	    	  //TODO:
//	      }
//	    });
//	    teacherList.setColumnWidth(course1BNumCol, 80, Unit.PX);
	    
	    // ClickableTextCell.
	    Column<Teacher, String> webCol = addColumn(0, new ClickableTextCell(), "Web", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	        return "Link";
	      }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	    	  String website = object.getWebsite();
	    	  if (website == null || website.isEmpty())
	    		  website = Config.DEFAULT_WEB;
	    	  Window.open(website, object.getFullTitle(), "");
	      }
	    });
	    
	    // DateCell.
//	    DateTimeFormat dateFormat = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
//	    Column<Teacher, Date> dateCol = addColumn(new DateCell(dateFormat), "Ngày sinh", new GetValue<Date>() {
//	      @Override
//	      public Date getValue(Teacher contact) {
//	        return contact.getBirthDate();
//	      }
//	    }, null);
//	    teacherList.setColumnWidth(dateCol, 50, Unit.PX);
	    
	    if (ClientData.isLoggedIn()) {
	    	teacherList.setColumnWidth(webCol, 50, Unit.PX);
		    // SafeHtmlCell.
		    //Column<Teacher, SafeHtml> noteCol = 
		    addColumn(0, new SafeHtmlCell(), "Ghi chú", new GetValue<SafeHtml>() {
		      @Override
		      public SafeHtml getValue(Teacher contact) {
		    	  String info = contact.getInfo();
		    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
		    	  builder.appendHtmlConstant(info);
		    	  return builder.toSafeHtml();
		      }
		    }, null);
//	    	teacherList.setColumnWidth(noteCol, 150 + bonus, Unit.PX);
//		    // ActionCell.
//		    Column<Teacher, Teacher> editCol = addColumn(new ActionCell<Teacher>("Sửa", new ActionCell.Delegate<Teacher>() {
//		      @Override
//		      public void execute(Teacher contact) {
//		    	  if (ClientData.canEdit(contact.getId(), true))
//		    		  EduManager.clientFactory.getEventBus().fireEvent(new EditEvent(contact));
//		    		  //edit(contact, false);
//		      }
//		    }), "Sửa", new GetValue<Teacher>() {
//		      @Override
//		      public Teacher getValue(Teacher contact) {
//		        return contact;
//		      }
//		    }, null);
//		    teacherList.setColumnWidth(editCol, 70, Unit.PX);
//	
//		    // ButtonCell.
//		    Column<Teacher, String> delCol = addColumn(new ButtonCell(), "Kê khai", new GetValue<String>() {
//		      @Override
//		      public String getValue(Teacher contact) {
//		    	  return "Kê khai";
//		    	  //return "Click " + contact.getFirstName();
//		      }
//		    }, new FieldUpdater<Teacher, String>() {
//		      @Override
//		      public void update(int index, final Teacher object, String value) {
//		    	  hiddenUploadForm.setAction(GWT.getHostPageBaseURL()+"export?" + Config.EXPORT_TOKEN + "=" + object.getId());
//		    	  hiddenUploadForm.submit();
//		    	  //Window.open(GWT.getHostPageBaseURL()+"export?" + Config.EXPORT_TOKEN + "=" + object.getId(), "Export", "");
////		    	  new RPCCall<String>() {
////
////					@Override
////					public void onFailure(Throwable caught) {
////						Window.alert("Có lỗi xảy ra, không thể export khối lượng giảng dạy");
////					}
////
////					@Override
////					public void onSuccess(String result) {
////						Window.alert(result);
////					}
////
////					@Override
////					protected void callService(AsyncCallback<String> cb) {
////						EduManager.dataService.export(null, object, cb);
////					}
////				}.retry();
//		      }
//		    });
	    }
	    //teacherList.setColumnWidth(delCol, 70, Unit.PX);
	}
	
	/**
	   * The list of cells that are editable.
	   */
	  private List<AbstractEditableCell<?, ?>> editableCells;
	
	 /**
	   * Get a cell value from a record.
	   * 
	   * @param <C> the cell type
	   */
	  private static interface GetValue<C> {
	    C getValue(Teacher contact);
	  }
	  
	  
	  private <C> Column<Teacher, C> addColumn(int width, Cell<C> cell, String headerText,
		      final GetValue<C> getter, FieldUpdater<Teacher, C> fieldUpdater) {
		  return this.addColumn(width, null, cell, headerText, getter, fieldUpdater);
	  }
	
	/**
	   * Add a column with a header.
	   * 
	   * @param <C> the cell type
	   * @param cell the cell used to render the column
	   * @param headerText the header string
	   * @param getter the value getter for the cell
	   */
	  private <C> Column<Teacher, C> addColumn(int width, HorizontalAlignmentConstant align, Cell<C> cell, String headerText,
	      final GetValue<C> getter, FieldUpdater<Teacher, C> fieldUpdater) {
	    Column<Teacher, C> column = new Column<Teacher, C>(cell) {
	      @Override
	      public C getValue(Teacher object) {
	        return getter.getValue(object);
	      }
	    };
	    column.setFieldUpdater(fieldUpdater);
	    if (cell instanceof AbstractEditableCell<?, ?>) {
	      editableCells.add((AbstractEditableCell<?, ?>) cell);
	    }
	    teacherList.addColumn(column, headerText);
	    if (width > 0)
	    	teacherList.setColumnWidth(column, width, Unit.PX);
	    if (align != null)
	    	column.setHorizontalAlignment(align);
	    return column;
	  }
}
