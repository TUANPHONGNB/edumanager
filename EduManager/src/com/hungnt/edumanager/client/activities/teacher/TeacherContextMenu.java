package com.hungnt.edumanager.client.activities.teacher;


import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.hungnt.edumanager.shared.Teacher;

/**
 * @author quantraitreo
 * 
 */
public class TeacherContextMenu extends PopupPanel {
	
	private VerticalPanel panel;
	private HTML title;
	
	private Button detailBtn;
	private Button editBtn;
	private Button deleteBtn;
	private Button exportBtn;
	private Teacher teacher = null;
	/**
	 * 
	 */
	public TeacherContextMenu() {
		panel = new VerticalPanel();
		this.setWidget(panel);
		this.setAnimationEnabled(true);
		this.setAutoHideEnabled(true);
		this.addStyleName("context-Menu");

		title = new HTML();
		panel.add(title);
		panel.setCellHorizontalAlignment(title,
				HasHorizontalAlignment.ALIGN_CENTER);
		
		detailBtn = new Button("Xem chi tiết");
		detailBtn.setType(ButtonType.LINK);
		detailBtn.setIcon(IconType.CAMERA);
		panel.add(detailBtn);
		panel.setCellHorizontalAlignment(detailBtn,
				HasHorizontalAlignment.ALIGN_LEFT);
		
		editBtn = new Button("Sửa");
		editBtn.setType(ButtonType.LINK);
		editBtn.setIcon(IconType.PLUS);
		panel.add(editBtn);
		panel.setCellHorizontalAlignment(editBtn,
				HasHorizontalAlignment.ALIGN_LEFT);
		
		deleteBtn = new Button("Xoá");
		deleteBtn.setType(ButtonType.LINK);
		deleteBtn.setIcon(IconType.REFRESH);
		panel.add(deleteBtn);
		panel.setCellHorizontalAlignment(deleteBtn,
				HasHorizontalAlignment.ALIGN_LEFT);
		
		exportBtn = new Button("Kê khai");
		exportBtn.setType(ButtonType.LINK);
		exportBtn.setIcon(IconType.CAMERA);
		panel.add(exportBtn);
		panel.setCellHorizontalAlignment(exportBtn,
				HasHorizontalAlignment.ALIGN_LEFT);
	}

	public void open(Teacher teacher) {
		this.teacher = teacher;
		title.setHTML(teacher.getFullName());
		center();
	}
	
	public Teacher getTeacher() {
		return this.teacher;
	}

	@Override
	public void show() {
		super.show();
	}
	
	@Override
	public void hide() {
		super.hide();
	}

	public static void refreshParentTreeItem(TreeItem treeItem) {
		TreeItem parent = treeItem.getParentItem();
		if (parent == null) {
			return;
		}
		parent.setState(false, false);
		parent.setState(true, true);
	}
	
	public Button getExportBtn() {
		return exportBtn;
	}

	public Button getEditBtn() {
		return editBtn;
	}
	
	public Button getDetailBtn() {
		return detailBtn;
	}

	public Button getDeleteBtn() {
		return deleteBtn;
	}
}