package com.hungnt.edumanager.client.activities.teacher;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.activities.ClientFactory;
import com.hungnt.edumanager.client.activities.basic.BasicActivity;
import com.hungnt.edumanager.shared.Config;

public class TeacherActivity extends BasicActivity {
	
	private TeacherView view;
	
	public TeacherActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getTeacherView();
		panel.setWidget(view.asWidget());
		super.start(panel, eventBus, view);
		if (ClientData.getTeachers().size() > 0)
			view.view(ClientData.getTeachers());
		if (place != null && place instanceof TeacherPlace) {
			if (((TeacherPlace)place).isRegister())
				view.getTeacherEditor().edit(null, true);
		}
	}
	
	@Override
	protected void bind() {
		super.bind();
		view.getAddMoreButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (!ClientData.isLoggedIn()) {
					view.getLoginView().view();
					return;
				}
				if (ClientData.canEdit(Config.NULL_ID, true))
					view.getTeacherEditor().edit(null, false);
			}
		});
	}
	
	@Override
	protected void onDataLoaded(int dataType) {
		if (dataType == Config.DATA_TEACHER)
			view.view(ClientData.getTeachers());
	}
 }
