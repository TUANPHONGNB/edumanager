package com.hungnt.edumanager.client.activities.teacher;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.hungnt.edumanager.client.activities.basic.BasicPlace;

public class TeacherPlace extends BasicPlace {
	
	private boolean register = false;
	
	public TeacherPlace() {
		super();
	}

	public TeacherPlace(boolean register) {
		this();
		this.register = register;
	}
	
	public boolean isRegister() {
		return this.register;
	}
	
	@Override
	public String getToken() {
		return "teacher";
	}
	
	public static class Tokenizer implements PlaceTokenizer<TeacherPlace> {
        @Override
        public String getToken(TeacherPlace place) {
            return place.getToken();
        }

        @Override
        public TeacherPlace getPlace(String token) {
            return new TeacherPlace();
        }
    }	
}