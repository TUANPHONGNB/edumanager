package com.hungnt.edumanager.client.activities.teacher;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.hungnt.edumanager.client.activities.basic.BasicView;
import com.hungnt.edumanager.shared.Teacher;

public interface TeacherView extends BasicView{
	HasClickHandlers getAddMoreButton();
	void view(List<Teacher> fullTeachers);
}
