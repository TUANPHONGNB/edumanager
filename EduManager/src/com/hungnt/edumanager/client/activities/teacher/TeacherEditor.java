package com.hungnt.edumanager.client.activities.teacher;

import java.util.Date;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.ClientUtils;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.widget.HListBox;
import com.hungnt.edumanager.client.widget.NumberTextBox;
import com.hungnt.edumanager.shared.Teacher;

public class TeacherEditor {
	private Button save = new Button("Lưu");
	private Button cancel = new Button("Huỷ");
	private FlexTable flexTable = new FlexTable();
	private TextBox firstName = new TextBox();
	private TextBox lastName = new TextBox();
	private TextBox teacherId = new TextBox();
	private HListBox gender = new HListBox();
	private HListBox degreeType = new HListBox();
	private HListBox eduType = new HListBox();
	private HListBox teachingType = new HListBox();
	private HListBox position = new HListBox();
	private TextBox email = new TextBox();
	private NumberTextBox phone = new NumberTextBox();
	private TextBox personalEmail = new TextBox();
	private NumberTextBox cellPhone = new NumberTextBox();
	private TextBox taxNumber = new TextBox();
	private TextBox website = new TextBox();
	private TextBox idNumber = new TextBox();
	private TextBox bankAccount = new TextBox();
	private TextBox bankAccountInfo = new TextBox();
	private TextBox workAddress = new TextBox();
	private TextBox homeAddress = new TextBox();
	private TextBox info = new TextBox();
	private DatePicker datePicker = new DatePicker();
	private DateBox dateBox = null;
	private DialogBox dialogBox = null;
	private Teacher currentTeacher = null;
	private FormPanel uploadForm;
	private FormPanel hiddenUploadForm;
	private FileUpload uploadField;
	private Button uploadButton;
	private HorizontalPanel uploadPanel = new HorizontalPanel();
	private Image avatar = new Image();
	private HListBox statusList = new HListBox();
	
	public TeacherEditor() {
		int row = 0, col = 0;
		gender.addItem("Nữ", "0");
		gender.addItem("Nam", "1");
		degreeType.addItem("Kỹ sư", "0");
		degreeType.addItem("Thạc sỹ", "1");
		degreeType.addItem("Tiến sỹ", "2");
		eduType.addItem("[Không]", "0");
		eduType.addItem("Phó Gíao Sư", "1");
		eduType.addItem("Gíao Sư", "2");
		teachingType.addItem("Gỉang Viên", "0");
		teachingType.addItem("Gỉang Viên Chính", "1");
		teachingType.addItem("Cán bộ giảng dạy", "2");
		teachingType.addItem("Nghiên cứu sinh", "3");
		teachingType.addItem("[Khác]", "3");
		position.addItem("[Không]", "0");
		position.addItem("Trưởng Bộ Môn", "1");
		position.addItem("Phó Trưởng Bộ Môn", "2");
		position.addItem("Tổ Trưởng Công Đoàn", "3");
		lastName.setWidth("200px");
		lastName.setWidth("200px");
		firstName.setWidth("200px");
		teacherId.setWidth("200px");
		personalEmail.setWidth("200px");
		cellPhone.setWidth("200px");
		website.setWidth("500px");
		workAddress.setWidth("500px");
		homeAddress.setWidth("500px");
		info.setWidth("500px");
		email.setWidth("200px");
		phone.setWidth("200px");
		taxNumber.setWidth("200px");
		idNumber.setWidth("200px");
		bankAccount.setWidth("200px");
		bankAccountInfo.setWidth("200px");
		datePicker.setValue(new Date(405406800000L));
		datePicker.setYearArrowsVisible(true);
		datePicker.setYearAndMonthDropdownVisible(true);
		datePicker.setVisibleYearCount(50);
		dateBox = new DateBox(datePicker, new Date(405406800000L), new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd/MM/yyyy")));
		dateBox.setWidth("200px");
		
		uploadForm = new FormPanel();
		uploadField = new FileUpload();
		uploadButton = new Button("Tải ảnh");
		uploadForm.setWidget(uploadField);
		//frame.setSize("400px","600px");
		uploadField.setName("image");
		avatar.setWidth("70px");
		uploadPanel.add(avatar);
		uploadPanel.add(uploadField);
		//uploadPanel.add(uploadButton);
		uploadForm.add(uploadPanel);
		uploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
		uploadForm.setMethod(FormPanel.METHOD_POST);
		hiddenUploadForm = new FormPanel();
		hiddenUploadForm.setVisible(false);
		hiddenUploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
		hiddenUploadForm.setMethod(FormPanel.METHOD_POST);
		
		statusList.addItem("Bình thường", "0");
		statusList.addItem("Đi học", "1");
		statusList.addItem("Đi công tác", "2");
		statusList.addItem("Đã nghỉ hưu", "3");
		statusList.addItem("Đơn vị khác", "4");

		flexTable.setWidth("100%");
		flexTable.setText(row, col++, "Họ");
		flexTable.setWidget(row, col++, lastName);
		flexTable.setText(row, col++, "Tên");
		flexTable.setWidget(row, col++, firstName);
		row ++; col = 0;
		flexTable.setText(row, col++, "Giới tính");
		flexTable.setWidget(row, col++, gender);
		flexTable.setText(row, col++, "Ngày sinh");
		flexTable.setWidget(row, col++, dateBox);
//		flexTable.setText(row, col++, "Mã số CB");
//		flexTable.setWidget(row, col++, teacherId);
		row ++; col = 0;
		flexTable.setText(row, col++, "Học vị");
		flexTable.setWidget(row, col++, degreeType);
		flexTable.setText(row, col++, "Học hàm");
		flexTable.setWidget(row, col++, eduType);
		row ++; col = 0;
		flexTable.setText(row, col++, "Vị trí");
		flexTable.setWidget(row, col++, teachingType);
		flexTable.setText(row, col++, "Chức vụ");
		flexTable.setWidget(row, col++, position);
		row ++; col = 0;
		flexTable.setText(row, col++, "Email");
		flexTable.setWidget(row, col++, email);
		flexTable.setText(row, col++, "Số DT");
		flexTable.setWidget(row, col++, phone);
		row ++; col = 0;
		flexTable.setText(row, col++, "Email cá nhân");
		flexTable.setWidget(row, col++, personalEmail);
		flexTable.setText(row, col++, "Số DĐ");
		flexTable.setWidget(row, col++, cellPhone);
		row ++; col = 0;
		flexTable.setText(row, col++, "MST");
		flexTable.setWidget(row, col++, taxNumber);
		flexTable.setText(row, col++, "CMND");
		flexTable.setWidget(row, col++, idNumber);
		row ++; col = 0;
		flexTable.setText(row, col++, "Tài khoản");
		flexTable.setWidget(row, col++, bankAccount);
		flexTable.setText(row, col++, "Chi nhánh");
		flexTable.setWidget(row, col++, bankAccountInfo);
		row ++; col = 0;
		
		flexTable.setText(row, col++, "Website");
		flexTable.setWidget(row, col, website);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		flexTable.setText(row, col++, "Địa chỉ CQ");
		flexTable.setWidget(row, col, workAddress);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		flexTable.setText(row, col++, "Địa chỉ nhà");
		flexTable.setWidget(row, col, homeAddress);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		flexTable.setText(row, col++, "Ghi chú");
		flexTable.setWidget(row, col, info);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		
		flexTable.setText(row, col++, "Trạng thái");
		flexTable.setWidget(row, col, statusList);
		row ++; col = 0;
		uploadForm.setWidth("150px");
		flexTable.setText(row, col++, "Ảnh");
		flexTable.setWidget(row, col++, uploadForm);
//		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		
		flexTable.getCellFormatter().setWidth(row, 0, "100px");
		flexTable.getCellFormatter().setWidth(row, 1, "220px");
		flexTable.getCellFormatter().setWidth(row, 2, "100px");
		flexTable.getCellFormatter().setWidth(row, 3, "220px");
		
		flexTable.setWidget(row, 0, cancel);
		flexTable.setWidget(row, 3, save);
		flexTable.getCellFormatter().setHeight(row, 0, "100px");
		flexTable.getCellFormatter().setHorizontalAlignment(row, 3, HasHorizontalAlignment.ALIGN_CENTER);
	}
	
	public HasClickHandlers getSaveButton() {
		return this.save;
	}
	
	public HasClickHandlers getCancelButton() {
		return this.cancel;
	}
	
	public FormPanel getUploadForm() {
		return uploadForm;
	}

	public FileUpload getUploadField() {
		return uploadField;
	}

	public Button getUploadButton() {
		return uploadButton;
	}
	
	public Teacher getTeacher() {
		if (firstName.getText().trim().isEmpty() || lastName.getText().trim().isEmpty()) {
			Window.alert("Họ tên không được để trống");
			return null;
		}
		if (!ClientUtils.validateEmail(email.getText().trim())) {
			Window.alert("Email không hợp lệ, vui lòng kiểm tra lại");
			return null;
		}
		if (currentTeacher == null)
			currentTeacher = new Teacher();
//		Teacher teacher = new Teacher(firstName.getText().trim(), lastName.getText().trim(), teacherId.getText().trim(),
//				Integer.parseInt(gender.getSelectedValue()), Integer.parseInt(degreeType.getSelectedValue()), 
//				Integer.parseInt(eduType.getSelectedValue()), email.getText().trim(), phone.getText().trim());
		currentTeacher.setFirstName(firstName.getText().trim());
		currentTeacher.setLastName(lastName.getText().trim());
		currentTeacher.setTeacherId(teacherId.getText().trim());
		currentTeacher.setGender(Integer.parseInt(gender.getSelectedValue()));
		currentTeacher.setDegreeType(Integer.parseInt(degreeType.getSelectedValue()));
		currentTeacher.setEduType(Integer.parseInt(eduType.getSelectedValue()));
		currentTeacher.setWorkEmail(email.getText().trim());
		currentTeacher.setWorkPhoneNumber(phone.getText().trim());
		currentTeacher.setBirthDate(dateBox.getValue());
		currentTeacher.setTaxNumber(taxNumber.getText());
		currentTeacher.setIdNumber(idNumber.getText());
		currentTeacher.setBankAccount(bankAccount.getText());
		currentTeacher.setBankInfo(bankAccountInfo.getText());
		currentTeacher.setWebsite(website.getText().trim());
		currentTeacher.setPosition(Integer.parseInt(position.getSelectedValue()));
		if (avatar.getUrl() != null && !avatar.getUrl().isEmpty())
			currentTeacher.setAvatar(avatar.getUrl());
		currentTeacher.setTeachingType(Integer.parseInt(teachingType.getSelectedValue()));
		currentTeacher.setPersonalEmail(personalEmail.getText().trim());
		currentTeacher.setCellPhoneNumber(cellPhone.getText().trim());
		currentTeacher.setWorkAddress(workAddress.getText().trim());
		currentTeacher.setHomeAddress(homeAddress.getText().trim());
		currentTeacher.setInfo(info.getText().trim());
		currentTeacher.setStatus(Integer.parseInt(statusList.getSelectedValue()));
		return currentTeacher;
	}
	
	public DialogBox getDialogBox() {
		return this.dialogBox;
	}
	
	public Image getAvatar() {
		return this.avatar;
	}
	
	public void edit(Teacher teacher, boolean register) {
		if (!register && !ClientData.isLoggedIn()) {
			EduManager.clientFactory.getBasicView().getLoginView().view();
			return;
		}
		if (teacher == null)
			teacher = new Teacher();
		currentTeacher = teacher;
		if (dialogBox == null) {
			dialogBox = new DialogBox(true);
			dialogBox.setWidth("640px");
		    dialogBox.setGlassEnabled(true);
		    dialogBox.setAnimationEnabled(false);
		    // Create a table to layout the content
		    VerticalPanel dialogContents = new VerticalPanel();
		    dialogContents.setWidth("100%");
		    dialogBox.setWidget(dialogContents);
		    flexTable.setWidth("100%");
		    dialogContents.add(flexTable);
		    dialogContents.setCellHorizontalAlignment(flexTable, HasHorizontalAlignment.ALIGN_CENTER);
		}
	    dialogBox.setText(teacher != null ? "Sửa thông tin" : "Tạo mới");
		if (teacher != null) {
			firstName.setText(teacher.getFirstName());
			lastName.setText(teacher.getLastName());
			teacherId.setText(teacher.getTeacherId());
			if (teacher.getId() == null || teacher.getId() < 0) {
				email.setText("");
				phone.setText("");
			}
			else {
				email.setText(teacher.getWorkEmail());
				phone.setText(teacher.getWorkPhoneNumber());
			}
			taxNumber.setText(teacher.getTaxNumber());
			idNumber.setText(teacher.getIdNumber());
			bankAccount.setText(teacher.getBankAccount());
			bankAccountInfo.setText(teacher.getBankInfo());
			degreeType.setSelectedIndex(teacher.getDegreeType());
			eduType.setSelectedIndex(teacher.getEduType());
			gender.setSelectedIndex(teacher.getGender());
			if (teacher.getBirthDate() != null)
				dateBox.setValue(teacher.getBirthDate());
			website.setText(teacher.getWebsite());
			position.setSelectedIndex(teacher.getPosition());
			avatar.setUrl(teacher.getAvatar());
			teachingType.setSelectedIndex(teacher.getTeachingType());
			workAddress.setText(teacher.getWorkAddress());
			homeAddress.setText(teacher.getHomeAddress());
			personalEmail.setText(teacher.getPersonalEmail());
			cellPhone.setText(teacher.getCellPhoneNumber());
			info.setText(teacher.getInfo());
			statusList.setSelectedValue(teacher.getStatus());
		}
	    dialogBox.center();
        dialogBox.show();
	}
}
