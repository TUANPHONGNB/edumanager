package com.hungnt.edumanager.client.activities.teacher.details;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.googlecode.gwt.crypto.client.TripleDesCipher;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.RPCCall;
import com.hungnt.edumanager.client.activities.ClientFactory;
import com.hungnt.edumanager.client.activities.basic.BasicActivity;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Teacher;

public class TeacherDetailActivity extends BasicActivity {
	
	private TeacherDetailView view = null;
	private Teacher teacher = null;
	
	public TeacherDetailActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getTeacherDetailView();
		panel.setWidget(view.asWidget());
		super.start(panel, eventBus, view);
		teacher = ((TeacherDetailPlace)place).getTeacher();
		if (teacher != null) {
			rootId = teacher.getId();
			ownerId = teacher.getId();
			view.view(teacher);
			if (ClientData.isLoggedIn() 
					&& ClientData.getCurrentUser().getId().compareTo(teacher.getId()) == 0)
				this.loadCategoriesData(rootId, ownerId, ClientData.getCurrentUser().getId(), view);
		}
	}
	
	@Override
	public void bind() {
		view.getEditButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				view.getTeacherEditor().edit(teacher, false);
			}
		});
		view.getChangePasswordButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				VerticalPanel vPanel = new VerticalPanel();
				final PasswordTextBox oldPassword = new PasswordTextBox();
				final PasswordTextBox newPassword = new PasswordTextBox();
				final PasswordTextBox newPasswordAgain = new PasswordTextBox();
				Button save = new Button("Lưu");
				Button cancel = new Button("Huỷ");
				FlexTable table = new FlexTable();
				int col = 0, row = 0;
				table.setText(row, col++, "Password cũ");
				table.setWidget(row, col, oldPassword);
				col = 0; row ++;
				table.setText(row, col++, "Password mới");
				table.setWidget(row, col, newPassword);
				col = 0; row ++;
				table.setText(row, col++, "Password mới");
				table.setWidget(row, col, newPasswordAgain);
				col = 0; row ++;
				vPanel.add(table);
				HorizontalPanel hPanel = new HorizontalPanel();
				hPanel.setWidth("100%");
				vPanel.add(hPanel);
				hPanel.add(cancel);
				hPanel.add(save);
				final DialogBox dialogBox = new DialogBox(true);
				dialogBox.setWidth("350px");
			    dialogBox.setGlassEnabled(true);
			    dialogBox.setAnimationEnabled(false);
			    dialogBox.setWidget(vPanel);
				cancel.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						dialogBox.hide();
					}
				});
				save.addClickHandler(new ClickHandler() {
					@Override
					public void onClick(ClickEvent event) {
						if (oldPassword.getText().trim().isEmpty()
								|| newPassword.getText().trim().isEmpty()
								|| newPasswordAgain.getText().trim().isEmpty()) {
							Window.alert("Các trường không được để trống");
							return;
						}
						else if (!newPassword.getText().trim().equalsIgnoreCase(newPasswordAgain.getText().trim())) {
							Window.alert("Cặp password mới không khớp");
							return;
						}
						TripleDesCipher cipher = new TripleDesCipher();
						cipher.setKey(Config.GWT_DES_KEY);
						String encryptedPassword = null;
						String encrypedOldPassword = null;
						try {
							encryptedPassword = cipher.encrypt(newPassword.getText().trim());
							encrypedOldPassword = cipher.encrypt(oldPassword.getText().trim());
						} catch (Exception e) {
						}
						final String fEncryptedPassword = encryptedPassword;
						final String fEncrypedOldPassword = encrypedOldPassword;
						teacher.setPassword(fEncryptedPassword);
						new RPCCall<Teacher>() {

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Không thể đổi mật khẩu, xin hãy thử lại sau!");
							}

							@Override
							public void onSuccess(Teacher result) {
								if (result == null) {
									Window.alert("Mật khẩu cũ không đúng, xin hãy kiểm tra lại!");
									return;
								}
								dialogBox.hide();
							}

							@Override
							protected void callService(AsyncCallback<Teacher> cb) {
								EduManager.dataService.changePassword(ClientData.getCurrentUser().getSessionId(), ClientData.getCurrentUser().getId(),
										fEncrypedOldPassword, fEncryptedPassword, cb);
							}
						}.retry();
					}
				});
				dialogBox.center();
			}
		});
	}
 }