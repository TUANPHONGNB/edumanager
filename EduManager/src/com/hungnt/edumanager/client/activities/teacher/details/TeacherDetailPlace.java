package com.hungnt.edumanager.client.activities.teacher.details;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Teacher;

public class TeacherDetailPlace extends Place {
	
	private Long teacherId = Config.NULL_ID;
	private Teacher teacher = null;
	
	public TeacherDetailPlace() {
		super();
	}

	public TeacherDetailPlace(long teacherId) {
		super();
		this.teacherId = teacherId;
	}
	
	public TeacherDetailPlace(Teacher teacher) {
		super();
		this.teacher = teacher;
	}
	
	public String getToken() {
		return "teacherdetail";
	}
	
	public Long getTeacherId() {
		return this.teacherId;
	}
	
	public Teacher getTeacher() {
		return this.teacher;
	}
	
	public static class Tokenizer implements PlaceTokenizer<TeacherDetailPlace> {
        @Override
        public String getToken(TeacherDetailPlace place) {
            return place.getToken();
        }

        @Override
        public TeacherDetailPlace getPlace(String token) {
            return new TeacherDetailPlace();
        }
    }
}
