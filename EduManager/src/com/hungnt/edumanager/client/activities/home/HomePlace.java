package com.hungnt.edumanager.client.activities.home;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.hungnt.edumanager.client.activities.basic.BasicPlace;

public class HomePlace extends BasicPlace {
	
	public HomePlace() {
		super();
	}
	
	@Override
	public String getToken() {
		return "home";
	}
	
	public static class Tokenizer implements PlaceTokenizer<HomePlace> {
        @Override
        public String getToken(HomePlace place) {
            return place.getToken();
        }

        @Override
        public HomePlace getPlace(String token) {
            return new HomePlace();
        }
    }
	
}
