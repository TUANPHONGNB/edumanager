package com.hungnt.edumanager.client.activities.home;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.client.CssToken;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;

public class HomeViewImpl extends BasicViewImpl implements HomeView {
	
	private VerticalPanel homePanel = new VerticalPanel();
	
	public HomeViewImpl() {	
		super();
	}
	
	@Override
	public void refreshView() {
		super.refreshView();
		home.setStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE, true);
		contentPanel.clear();
		homePanel.clear();
		homePanel.setSpacing(5);
		contentPanel.add(homePanel);
		homePanel.add(new HTML("<h1>Hệ thống quản lý - Bộ môn Công Nghệ Phần Mềm</h1>"));
		homePanel.add(new HTML("<font size='4px'>1. Quản lý cán bộ: </font>"));
		homePanel.add(new HTML("<font size='4px'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp - Quản lý thông tin cán bộ</font>"));
		
		homePanel.add(new HTML("<font size='4px'>2. Quản lý môn học: </font>"));
		homePanel.add(new HTML("<font size='4px'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp - Danh sách các môn đang giảng dạy tại Bộ môn CNPM</font>"));
		homePanel.add(new HTML("<font size='4px'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp - Quản lý tài liệu giảng dạy (bài giảng, tài liệu tham khảo, ...)</font>"));
		
		homePanel.add(new HTML("<font size='4px'>3. Quản lý giảng dạy: </font>"));
		homePanel.add(new HTML("<font size='4px'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp - Thời khoá biểu chi tiết cho tất cả các môn</font>"));
		homePanel.add(new HTML("<font size='4px'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp - Tự động email nhắc nhở trước khi môn học bắt đầu</font>"));
		homePanel.add(new HTML("<font size='4px'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp - Chức năng hỗ trợ nộp bài tự động (đồ án TN, đồ án môn học, ...)</font>"));
		
		homePanel.add(new HTML("<font size='4px'>4. Quản lý bài báo</font>"));
		homePanel.add(new HTML("<font size='4px'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp - Lưu giữ và thống kê tất cả các bài báo theo từng năm từ năm 2010</font>"));

		homePanel.add(new HTML("<font size='4px'>5. Hỗ trợ kê khai giảng dạy: </font>"));
		homePanel.add(new HTML("<font size='4px'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp - Tự động tạo báo cáo Khối lượng giảng dạy cho từng thầy cô vào cuối kỳ</font>"));
		
		homePanel.add(new HTML("<font size='4px'>6. Quản lý thiết bị: </font>"));
		homePanel.add(new HTML("<font size='4px'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp - Thống kê thông tin thiết bị (mã số, trạng thái, người sử dụng, ...)</font>"));
		homePanel.add(new HTML("<font size='4px'>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp - Lịch quản lý, đăng ký sử dụng thiết bị (máy chiếu, máy tính, ...</font>"));

		homePanel.add(new HTML("<font size='4px'>&nbsp</font>"));
		homePanel.add(new HTML("<font size='4px'>&nbsp</font>"));
		homePanel.add(new HTML("<font size='4px'>&nbsp</font>"));
		//		coddntentPanel.add(new HTML("Bộ môn Công nghệ Phần mềm đảm nhiệm chức năng giảng dạy các môn "
//				+ "học chuyên ngành thuộc lĩnh vực Công nghệ phần mềm bậc trên đại học, "
//				+ "đại học (các hệ chính qui, phi chính qui), "
//				+ "cao đẳng thuộc khoa CNTT và nghiên cứu khoa học thuộc các lĩnh vực về lý thuyết cơ bản, "
//				+ "các công cụ và kỹ thuật phát triển của Công nghệ phần mềm."));
//		Image image = new Image("images/CNPM.PNG");
//		contentPanel.add(image);
//		contentPanel.add(new HTML("Phòng 601, B1, trường Đại học Bách Khoa Hà Nội, số 1, Đại Cổ Việt, Hà Nội <br>" + 
//				"Điện thoại : (844)-38682595 <br>" + 
//				"Email:  cnpm@soict.hust.edu.vn"));
	}
}
