package com.hungnt.edumanager.client.activities.basic;

import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.CssToken;
import com.hungnt.edumanager.client.activities.course.CourseEditer;
import com.hungnt.edumanager.client.activities.login.LoginView;
import com.hungnt.edumanager.client.activities.publication.PublicationEditor;
import com.hungnt.edumanager.client.activities.teacher.TeacherEditor;
import com.hungnt.edumanager.client.activities.topic.TopicEditer;

public class BasicViewImpl extends VerticalPanel implements BasicView{
	
	protected FlowPanel headerPanel = new FlowPanel();
	protected FlowPanel contentPanel = new FlowPanel();
	protected FlowPanel footerPanel = new FlowPanel();
	protected Button home = new Button("Trang chủ");
	protected Button teacher = new Button("Cán bộ");
	protected Button subject = new Button("Môn học");
	protected Button course = new Button("Thời khoá biểu");
	protected Button topic = new Button("Đồ án");
	protected Button publication = new Button("Bài báo");
	protected Button equipment = new Button("Thiết bị");
	protected Button material = new Button("Tài liệu");
	protected Button event = new Button("Công việc");
	protected Button meeting = new Button("Biên bản");
	protected HorizontalPanel navigationPanel = new HorizontalPanel();
	protected HorizontalPanel menuPanel = new HorizontalPanel();
	protected VerticalPanel loginPanel = new VerticalPanel();
	protected Anchor login = new Anchor("Đăng nhập");
	protected Anchor logout = new Anchor("Đăng xuất");
	protected Anchor register = new Anchor("Đăng ký");
	private CourseEditer courseEditer = null;
	protected TopicEditer topicEditor = new TopicEditer();
	protected TeacherEditor teacherEditor = new TeacherEditor();
	protected PublicationEditor pubEditor = new PublicationEditor();
	protected DocumentTreeView documentView = new DocumentTreeView();
	protected static LoginView loginView;
	
	protected Anchor userInfo = new Anchor();
	
	public BasicViewImpl() {	
		super();
		this.setWidth("960px");
		this.getElement().getStyle().setProperty("margin", "0px auto");
		this.setHorizontalAlignment(ALIGN_CENTER);
		
		navigationPanel.setWidth("100%");
		navigationPanel.add(menuPanel);
		navigationPanel.setCellHorizontalAlignment(menuPanel, HasHorizontalAlignment.ALIGN_LEFT);
		menuPanel.add(home);
		menuPanel.add(teacher);
		menuPanel.add(subject);
		menuPanel.add(course);
		menuPanel.add(topic);
		menuPanel.add(publication);
		menuPanel.add(event);
		menuPanel.add(meeting);
		//menuPanel.add(equipment);
		menuPanel.add(material);
		home.setWidth("80px");
		teacher.setWidth("70px");
		subject.setWidth("70px");
		course.setWidth("120px");
		topic.setWidth("70px");
		publication.setWidth("80px");
		equipment.setWidth("70px");
		material.setWidth("70px");
		event.setWidth("80px");
		meeting.setWidth("70px");
		material.setVisible(false);
		loginPanel.setSpacing(5);
		navigationPanel.add(loginPanel);
		navigationPanel.setCellHorizontalAlignment(loginPanel, HasHorizontalAlignment.ALIGN_RIGHT);
		home.setStyleName(CssToken.TABBAR_BASE_BUTTON, true);
		teacher.setStyleName(CssToken.TABBAR_BASE_BUTTON, true);
		subject.setStyleName(CssToken.TABBAR_BASE_BUTTON, true);
		course.setStyleName(CssToken.TABBAR_BASE_BUTTON, true);
		topic.setStyleName(CssToken.TABBAR_BASE_BUTTON, true);
		publication.setStyleName(CssToken.TABBAR_BASE_BUTTON, true);
		equipment.setStyleName(CssToken.TABBAR_BASE_BUTTON, true);
		material.setStyleName(CssToken.TABBAR_BASE_BUTTON, true);
		event.setStyleName(CssToken.TABBAR_BASE_BUTTON, true);
		meeting.setStyleName(CssToken.TABBAR_BASE_BUTTON, true);
		this.add(headerPanel);
		this.add(navigationPanel);
		this.add(contentPanel);
		this.add(footerPanel);
		this.setCellHorizontalAlignment(navigationPanel, HasHorizontalAlignment.ALIGN_LEFT);
		contentPanel.setWidth("100%");
		contentPanel.getElement().getStyle().setProperty("border", "1px solid #c7c7c7");
		headerPanel.setHeight("120px");
//		headerPanel.getElement().getStyle().setProperty("background", "url('images/banner_bg.jpg') no-repeat 50% 50%");
		headerPanel.getElement().getStyle().setProperty("background", "url('images/CNPM.PNG') no-repeat 50% 50%");
		footerPanel.setHeight("80px");
		footerPanel.getElement().getStyle().setProperty("background", "url('images/footer_bg.jpg') repeat-x 100% 100%");
		footerPanel.getElement().getStyle().setPosition(Position.RELATIVE);
		HTML htmlFooter = new HTML("<font color='white'>Bản quyền thuộc về Bộ môn Công Nghệ Phần Mềm | Địa chỉ: 601B1, 1, Đại Cồ Việt, Hai Bà Trưng, Hà Nội | Điện thoại: 043 869 2222 - Fax: 043 869 2006</font>");
		htmlFooter.getElement().setAttribute("style", "position: absolute; bottom: 10px; width: 100%; text-align: center;");
		footerPanel.add(htmlFooter);
	}
	
	@Override
	public HasClickHandlers getTeacherButton() {
		return this.teacher;
	}
	
	@Override
	public HasClickHandlers getSubjectButton() {
		return this.subject;
	}
	
	@Override
	public HasClickHandlers getCourseButton() {
		return this.course;
	}
	
	@Override
	public HasClickHandlers getTopicButton() {
		return this.topic;
	}
	
	@Override
	public HasClickHandlers getMeetingButton() {
		return this.meeting;
	}
	
	@Override
	public HasClickHandlers getHomeButton() {
		return this.home;
	}
	
	@Override
	public HasClickHandlers getPublicationButton() {
		return this.publication;
	}

	@Override
	public HasClickHandlers getEquipmentButton() {
		return this.equipment;
	}
	
	@Override
	public HasClickHandlers getEventButton() {
		return this.event;
	}
	
	
	@Override
	public HasClickHandlers getUserInfo() {
		return this.userInfo;
	}
	
	@Override
	public HasClickHandlers getLoginButton() {
		return this.login;
	}
	
	@Override
	public HasClickHandlers getRegisterButton() {
		return this.register;
	}
	
	@Override
	public HasClickHandlers getLogoutButton() {
		return this.logout;
	}
	
	@Override
	public CourseEditer getCourseEditer() {
		if (courseEditer == null)
			courseEditer = new CourseEditer();
		return this.courseEditer;
	}
	
	@Override
	public TopicEditer getTopicEditer() {
		return this.topicEditor;
	}
	
	@Override
	public TeacherEditor getTeacherEditor() {
		return this.teacherEditor;
	}
	
	@Override
	public PublicationEditor getPubEditor() {
		return this.pubEditor;
	}
	
	@Override
	public LoginView getLoginView() {
		if (loginView == null)
			loginView = new LoginView();
		return loginView;
	}

	@Override
	public void refreshView() {
		home.removeStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE);
		teacher.removeStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE);
		subject.removeStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE);
		course.removeStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE);
		equipment.removeStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE);
		material.removeStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE);
		updateUserInfo();
	}
	
	@Override
	public void updateUserInfo() {
		loginPanel.clear();
		if (!ClientData.isLoggedIn()) {
			material.setVisible(false);
			loginPanel.add(login);
			//loginPanel.add(register);
		}
		else {
			material.setVisible(true);
			loginPanel.add(userInfo);
			loginPanel.add(logout);
			userInfo.setHTML(ClientData.getCurrentUser().getFullTitle());
		}
	}
	
	@Override
	public void onDataLoaded(int dataType) {
	}

	@Override
	public HasClickHandlers getMaterialButton() {
		return material;
	}
	
	@Override
	public DocumentTreeView getDocumentView() {
		return this.documentView;
	}
}