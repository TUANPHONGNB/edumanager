package com.hungnt.edumanager.client.activities.basic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.googlecode.gwt.crypto.client.TripleDesCipher;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.ClientUtils;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.RPCCall;
import com.hungnt.edumanager.client.UUID;
import com.hungnt.edumanager.client.activities.ClientFactory;
import com.hungnt.edumanager.client.activities.course.CoursePlace;
import com.hungnt.edumanager.client.activities.document.DocumentPlace;
import com.hungnt.edumanager.client.activities.document.TreeInforItem;
import com.hungnt.edumanager.client.activities.equipment.EquipmentPlace;
import com.hungnt.edumanager.client.activities.event.EventPlace;
import com.hungnt.edumanager.client.activities.home.HomePlace;
import com.hungnt.edumanager.client.activities.meeting.MeetingPlace;
import com.hungnt.edumanager.client.activities.publication.PublicationPlace;
import com.hungnt.edumanager.client.activities.subject.SubjectPlace;
import com.hungnt.edumanager.client.activities.teacher.TeacherPlace;
import com.hungnt.edumanager.client.activities.teacher.details.TeacherDetailPlace;
import com.hungnt.edumanager.client.activities.topic.TopicPlace;
import com.hungnt.edumanager.client.event.DataLoadedEvent;
import com.hungnt.edumanager.client.event.DataLoadedEventHandler;
import com.hungnt.edumanager.client.event.EditEvent;
import com.hungnt.edumanager.client.event.EditEventHandler;
import com.hungnt.edumanager.client.event.LoginEvent;
import com.hungnt.edumanager.client.event.LoginEventHandler;
import com.hungnt.edumanager.client.view.CreateCategoryDialog.ActionCategory;
import com.hungnt.edumanager.client.view.CreateMaterialDialog.ActionMaterial;
import com.hungnt.edumanager.shared.Category;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Course;
import com.hungnt.edumanager.shared.InternTopic;
import com.hungnt.edumanager.shared.Material;
import com.hungnt.edumanager.shared.Publication;
import com.hungnt.edumanager.shared.Teacher;

public class BasicActivity extends AbstractActivity {
	
	protected ClientFactory clientFactory;
	protected EventBus eventBus;
	protected Place place;
	protected long rootId = Config.NULL_ID;
	protected Long ownerId = null;
	protected Category root = null;
	
	public BasicActivity(ClientFactory clientFactory, Place place) {
		super();
		this.clientFactory = clientFactory;
		this.place = place;
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		this.eventBus = eventBus;
		
	}
	
	public void start(AcceptsOneWidget panel, EventBus eventBus, final BasicView basicView) {
		this.eventBus = eventBus;
		ClientData.loadData(Config.currentScholarYear, -1);
		bind();
		if(basicView != null) {
			basicView.refreshView();
			basicView.getHomeButton().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					clientFactory.getPlaceController().goTo(new HomePlace());
				}
			});
			
			basicView.getTeacherButton().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					clientFactory.getPlaceController().goTo(new TeacherPlace());
				}
			});
			
			basicView.getSubjectButton().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					clientFactory.getPlaceController().goTo(new SubjectPlace());
				}
			});
			
			basicView.getCourseButton().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					clientFactory.getPlaceController().goTo(new CoursePlace());
				}
			});
			
			basicView.getTopicButton().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					clientFactory.getPlaceController().goTo(new TopicPlace());
				}
			});
			
			basicView.getPublicationButton().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					clientFactory.getPlaceController().goTo(new PublicationPlace());
				}
			});
			
			basicView.getMeetingButton().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					clientFactory.getPlaceController().goTo(new MeetingPlace());
				}
			});
			
			basicView.getEquipmentButton().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					clientFactory.getPlaceController().goTo(new EquipmentPlace());
				}
			});
			
			basicView.getMaterialButton().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					clientFactory.getPlaceController().goTo(new DocumentPlace());
				}
			});
			
			basicView.getEventButton().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					clientFactory.getPlaceController().goTo(new EventPlace());
				}
			});
			
			basicView.getLoginButton().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					if (!ClientData.isLoggedIn())
						basicView.getLoginView().view();
				}
			});
			basicView.getRegisterButton().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					if (!ClientData.isLoggedIn()) {
						clientFactory.getPlaceController().goTo(new TeacherPlace(true));
					}
				}
			});
			
			basicView.getUserInfo().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					if (ClientData.isLoggedIn()) {
						clientFactory.getPlaceController().goTo(new TeacherDetailPlace(ClientData.getCurrentUser()));
					}
				}
			});
			
			basicView.getLogoutButton().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					if (ClientData.isLoggedIn()) {
						new RPCCall<Void>() {
							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Lỗi đăng xuất");
							}

							@Override
							public void onSuccess(Void result) {
								Cookies.removeCookie(Config.LOGIN_NAME_SPACE);
								ClientData.setCurrentUser(null);
							}

							@Override
							protected void callService(AsyncCallback<Void> cb) {
								EduManager.dataService.logout(ClientData.getCurrentUser().getId() + "", cb);
							}
						}.retry();
					}
				}
			});
			
			basicView.getLoginView().getLoginButton().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					final String userName = basicView.getLoginView().getUserName();
					final String password = basicView.getLoginView().getPassword();
					if (userName.isEmpty()) {
						Window.alert("Tên truy cập không thể để trống");
						return;
					}
					if (password.isEmpty()) {
						Window.alert("Mật khẩu không thể để trống");
						return;
					}
						
					new RPCCall<Teacher>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Lỗi đăng nhập");
						}

						@Override
						public void onSuccess(Teacher result) {
							if (result != null) {
								ClientData.setCurrentUser(result);
								basicView.updateUserInfo();
							}
							else {
								Window.alert("Lỗi đăng nhập");
							}
							basicView.getLoginView().getDialogBox().hide();
						}

						@Override
						protected void callService(AsyncCallback<Teacher> cb) {
							String password = basicView.getLoginView().getPassword();
							TripleDesCipher cipher = new TripleDesCipher();
							cipher.setKey(Config.GWT_DES_KEY);
							String encryptedPassword = password;
							try {
								encryptedPassword = cipher.encrypt(password);
							} catch (Exception e) {
							}
							EduManager.dataService.login(UUID.uuid(), basicView.getLoginView().getUserName(),
									encryptedPassword, cb);
						}
					}.retry();
				}
			});
			eventBus.addHandler(LoginEvent.TYPE, new LoginEventHandler() {
				@Override
				public void onLoggedIn(LoginEvent event) {
					basicView.refreshView();
				}
			});
			eventBus.addHandler(DataLoadedEvent.TYPE, new DataLoadedEventHandler() {
				@Override
				public void onDataLoaded(DataLoadedEvent event) {
					basicView.onDataLoaded(event.getDataType());
				}
			});
			basicView.getCourseEditer().getCancelButton().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					basicView.getCourseEditer().getDialogBox().hide();
				}
			});
			basicView.getCourseEditer().getSaveButton().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					final Course course = basicView.getCourseEditer().getCourse();
					if (course == null)
						return;
					new RPCCall<Course>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Có lỗi xảy ra, không thể lưu được lớp học");
						}

						@Override
						public void onSuccess(Course result) {
							basicView.getCourseEditer().getDialogBox().hide();
							if (!ClientData.getCurrentUser().getWorkEmail().contains("hungnt@soict.hust.edu.vn")) {
								ClientData.getCourses().clear();
								ClientData.loadData(Config.currentScholarYear, -1);
							}
						}

						@Override
						protected void callService(AsyncCallback<Course> cb) {
							EduManager.dataService.updateCourse(null, course, cb);
						}
					}.retry();
				}
			});
			basicView.getTopicEditer().getCancelButton().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					basicView.getTopicEditer().getDialogBox().hide();
				}
			});
			basicView.getTopicEditer().getSaveButton().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					final InternTopic topic = basicView.getTopicEditer().getTopic();
					if (topic == null)
						return;
					new RPCCall<InternTopic>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Có lỗi xảy ra, không thể lưu được đề tài");
						}

						@Override
						public void onSuccess(InternTopic result) {
							basicView.getTopicEditer().getDialogBox().hide();
							ClientData.getTopics().clear();
							ClientData.loadData(Config.currentScholarYear, -1);
						}

						@Override
						protected void callService(AsyncCallback<InternTopic> cb) {
							EduManager.dataService.updateTopic(null, topic, cb);
						}
					}.retry();
				}
			});
			eventBus.addHandler(EditEvent.TYPE, new EditEventHandler() {
				
				@Override
				public void onEdit(EditEvent event) {
					if (event.getObject() != null && event.getObject() instanceof Course)
						basicView.getCourseEditer().edit((Course)event.getObject());
					else if (event.getObject() != null && event.getObject() instanceof InternTopic)
						basicView.getTopicEditer().edit((InternTopic)event.getObject());
					else if (event.getObject() != null && event.getObject() instanceof Teacher)
						basicView.getTeacherEditor().edit((Teacher)event.getObject(), false);
					else if (event.getObject() != null && event.getObject() instanceof Publication)
						basicView.getPubEditor().edit((Publication)event.getObject());
				}
			});
			basicView.getTeacherEditor().getCancelButton().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					basicView.getTeacherEditor().getDialogBox().hide();
				}
			});
//			view.getUploadButton().addClickHandler(new ClickHandler() {
//				
//				@Override
//				public void onClick(ClickEvent event) {
//					startNewBlobstoreSession();
//				}
//			});
			basicView.getTeacherEditor().getUploadForm().addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
				@Override
				public void onSubmitComplete(SubmitCompleteEvent event) {
					//view.getUploadForm().reset();
					EduManager.loadingView.stopProcessing();
					String url = event.getResults();
					if (url != null && url.length() > 10)
						basicView.getTeacherEditor().getAvatar().setUrl(url);
				}
			});
			
			basicView.getTeacherEditor().getUploadField().addChangeHandler(new ChangeHandler() {
				
				@Override
				public void onChange(ChangeEvent event) {
					EduManager.loadingView.startProcessing();
					startNewBlobstoreSession(basicView);
//					if (view.getUploadField().getFilename().length() == 0) {
//						Window.alert("File not found !");
//					} else {
//						view.getUploadForm().setAction(GWT.getHostPageBaseURL()+"gcsupload?folder=avatars");//&bucket=avatars");
//						view.getUploadForm().setEncoding(FormPanel.ENCODING_MULTIPART);
//						view.getUploadForm().setMethod(FormPanel.METHOD_POST);
//						view.getUploadForm().submit();
//					} 
				}
			});
			basicView.getTeacherEditor().getSaveButton().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					final Teacher teacher = basicView.getTeacherEditor().getTeacher();
					if (teacher == null)
						return;
					if (teacher.getPassword() == null || teacher.getPassword().trim().isEmpty()) {
						TripleDesCipher cipher = new TripleDesCipher();
						cipher.setKey(Config.GWT_DES_KEY);
						String encryptedPassword = Config.DEFAULT_PASSWORD;
						try {
							encryptedPassword = cipher.encrypt(Config.DEFAULT_PASSWORD);
						} catch (Exception e) {
						}
						teacher.setPassword(encryptedPassword);
					}
					new RPCCall<Teacher>() {
						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Có lỗi xảy ra");
						}

						@Override
						public void onSuccess(Teacher result) {
							//Window.alert("Đã lưu " + result.getLastName() + " " + result.getFirstName());
							basicView.getTeacherEditor().getDialogBox().hide();
							ClientData.getTeachers().clear();
							ClientData.loadData(Config.currentScholarYear, -1);
						}

						@Override
						protected void callService(AsyncCallback<Teacher> cb) {
							EduManager.dataService.updateTeacher(null, teacher, cb);					}
					}.retry();
				}
			});
			
			//Document Tree
			basicView.getDocumentView().getTreeWrapper().addDomHandler(new ContextMenuHandler() {
				@Override
				public void onContextMenu(ContextMenuEvent event) {
					if (!basicView.getDocumentView().isReadOnly()) {
						event.preventDefault();
						event.stopPropagation();
						basicView.getDocumentView().getPersonDocumentTree().getContextMenu().open(null, 
								event.getNativeEvent().getClientX(),event.getNativeEvent().getClientY(), null);
					}
				}
			}, ContextMenuEvent.getType());
			
			basicView.getDocumentView().getPersonDocumentTree().getContextMenu().getBtnAddChildCategory().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					basicView.getDocumentView().getPersonDocumentTree().getContextMenu().hide();
					basicView.getDocumentView().getCreateCategoryDialog().showCreateDialog(null,
							basicView.getDocumentView().getPersonDocumentTree().getCurrentCategory().getId(), 
							event.getX(), event.getY());
				}
			});
			
			basicView.getDocumentView().getPersonDocumentTree().getContextMenu().getBtnAddChildMaterial().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					basicView.getDocumentView().getPersonDocumentTree().getContextMenu().hide();
					basicView.getDocumentView().getCreateMaterialDialog().showCreateDialog(null, basicView.getDocumentView().getPersonDocumentTree().getCurrentCategory().getId());
				}
			});
			
			basicView.getDocumentView().getCreateMaterialDialog().getBtnSave().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					//TODO
					if(!basicView.getDocumentView().getCreateMaterialDialog().isCheckInfor()){
						Window.alert("Nhập tên và url !");
						return;
					}
					if (basicView.getDocumentView().getCreateMaterialDialog().getCurrentMaterial()==null) {
						Window.alert("Nhập sai privacy !");
						return;
					}
					basicView.getDocumentView().getCreateMaterialDialog().hide();
					doUpdateMaterial(basicView.getDocumentView().getCreateMaterialDialog().getCurrentMaterial(), basicView);
				}
			});
			
			basicView.getDocumentView().getCreateMaterialDialog().getBtnUpload().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					basicView.getDocumentView().getFormUploadDoc().showUploadFileDialog();
				}
			});
			
			basicView.getDocumentView().getFormUploadDoc().getUploadWidget().addChangeHandler(new ChangeHandler() {
				
				@Override
				public void onChange(ChangeEvent event) {
					String fileName = basicView.getDocumentView().getFormUploadDoc().getUploadWidget().getFilename();
					if (fileName.length() == 0) {
						basicView.getDocumentView().getAlertDialog().show("File not found !", null);
						return;
					}
					EduManager.loadingView.show();
					if (fileName.contains("\\")) {
						fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
					}
					if (fileName.contains("/")) {
						fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
					}
					String extension = "";
					if (fileName.contains(".")) {
						extension = fileName.substring(fileName.lastIndexOf(".") + 1);
						fileName = fileName.substring(0, fileName.lastIndexOf("."));
					}
					basicView.getDocumentView().getCreateMaterialDialog().getTbName().setText(fileName);
					basicView.getDocumentView().getCreateMaterialDialog().getTbFileType().setText(extension);
					String folder = Config.FOLDER_DOCUMENT;
					if (rootId > 0)
						folder = rootId + "";
					basicView.getDocumentView().getFormUploadDoc().getUploadForm().setAction("/uploadfile?folder=" + folder);
					basicView.getDocumentView().getFormUploadDoc().getUploadForm().submit();
					return;
				}
			});
			
			basicView.getDocumentView().getFormUploadDoc().getUploadForm().addSubmitCompleteHandler(new SubmitCompleteHandler() {
				
				@Override
				public void onSubmitComplete(SubmitCompleteEvent event) {
					EduManager.loadingView.hide();
					String json = event.getResults();
					if (json == null)
						return;
					JSONObject obj = (JSONObject) JSONParser.parseStrict(json);
					if (obj == null)
						return;
					String url = obj.get("url").isString().stringValue();
					String fileSize = obj.get("filesize").isString().stringValue();
					basicView.getDocumentView().getCreateMaterialDialog().getTbUrl().setText(url);
					basicView.getDocumentView().getCreateMaterialDialog().getTbFileSize().setText(fileSize);
				}
			});
			
			basicView.getDocumentView().getPersonDocumentTree().getContextMenu().getBtnEdit().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					basicView.getDocumentView().getPersonDocumentTree().getContextMenu().hide();
					if(basicView.getDocumentView().getPersonDocumentTree().getTypeFolder()==Config.CATEGORY){
						Category currentCategory = basicView.getDocumentView().getPersonDocumentTree().getCurrentCategory();
						basicView.getDocumentView().getCreateCategoryDialog().showCreateDialog(currentCategory,
								currentCategory.getParentId(), event.getX(), event.getY());
					} else if(basicView.getDocumentView().getPersonDocumentTree().getTypeFolder()==Config.MATERIAL){
						Material material = basicView.getDocumentView().getPersonDocumentTree().getCuMaterial();
						basicView.getDocumentView().getCreateMaterialDialog().showCreateDialog(material, material.getParentId());
					}
					
				}
			});
			
			basicView.getDocumentView().getPersonDocumentTree().getContextMenu().getAddCategoryBtn().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					basicView.getDocumentView().getPersonDocumentTree().getContextMenu().hide();
					basicView.getDocumentView().getCreateCategoryDialog().showCreateDialog(null,
							rootId, event.getX(), event.getY());
				}
			});
			
			basicView.getDocumentView().getPersonDocumentTree().setRightClickCallBack(new AsyncCallback<Void>() {
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(Void result) {
					Category currentCategory = basicView.getDocumentView().getPersonDocumentTree().getCurrentCategory();
					if(currentCategory.getChildren().isEmpty()
					&& currentCategory.getMaterials().isEmpty()){
						loadChildCategoryAndChildMaterial(currentCategory, basicView);
					}
				}
			});
			
			basicView.getDocumentView().getPersonDocumentTree().getContextMenu().getBtnViewCategory().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					basicView.getDocumentView().getPersonDocumentTree().getContextMenu().hide();
					Category currentCategory = basicView.getDocumentView().getPersonDocumentTree().getCurrentCategory();
					if(currentCategory.getChildren().isEmpty()
					&& currentCategory.getMaterials().isEmpty()){
						loadChildCategoryAndChildMaterial(currentCategory, basicView);
					}
				}
			});
			basicView.getDocumentView().getPersonDocumentTree().getContextMenu().getRefreshTreeBtn().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					basicView.getDocumentView().getPersonDocumentTree().getContextMenu().hide();
				}
			});
			
			basicView.getDocumentView().getCreateCategoryDialog().getBtnSave().addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					if (basicView.getDocumentView().getCreateCategoryDialog().getTbName().getText().equals("")) {
						basicView.getDocumentView().getAlertDialog().show("Nhập đầy đủ thông tin!", null);
						return;
					}
					if(basicView.getDocumentView().getCreateCategoryDialog().getCurCategory()==null){
						basicView.getDocumentView().getAlertDialog().show("Thông tin bị lỗi, vui lòng nhập lại!", null);
						return;
					}
					doUpDateCategory(basicView.getDocumentView().getCreateCategoryDialog().getCurCategory(), 
							basicView, rootId);
				}
			});
			
			basicView.getDocumentView().getPersonDocumentTree().getContextMenu().getBtnDelete().addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					if (!ClientData.isLoggedIn())
						return;
					basicView.getDocumentView().getPersonDocumentTree().getContextMenu().hide();
					final Object obj = basicView.getDocumentView().getPersonDocumentTree().getCurrentObject();
					if (obj == null)
						return;
					if (obj instanceof Material) {
						if (!ClientData.getCurrentUser().isAdmin()
								&& ClientData.getCurrentUser().getId().compareTo(((Material)obj).getId()) != 0) {
							Window.alert("Bạn không có quyền xoá tài liệu này");
							return;
						}
						if (!Window.confirm("Bạn có muốn xoá tài liệu này?"))
							return;
						new RPCCall<Void>() {
							@Override
							public void onFailure(Throwable caught) {
							}
							@Override
							public void onSuccess(Void result) {
								getMainCategories().clear();
								loadCategoriesData(rootId, ownerId, ClientData.getCurrentUser().getId(), basicView);
							}
							@Override
							protected void callService(AsyncCallback<Void> cb) {
								EduManager.dataService.delete(null, (Material)obj, cb);
							}
						}.retry();
					}
				}
			});
			bindPubEditor(basicView);
		}
	}
	
	protected void bindPubEditor(final BasicView view) {
		view.getPubEditor().getSaveButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final Publication publication = view.getPubEditor().getPublication();
				if (publication == null)
					return;
				new RPCCall<Publication>() {
					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Có lỗi xảy ra");
					}

					@Override
					public void onSuccess(Publication result) {
						//Window.alert("Đã lưu " + result.getLastName() + " " + result.getFirstName());
						view.getPubEditor().getDialogBox().hide();
						ClientData.getPublications().clear();
						ClientData.loadData(Config.currentScholarYear, -1);
					}

					@Override
					protected void callService(AsyncCallback<Publication> cb) {
						EduManager.dataService.updatePublication(null, publication, cb);					}
				}.retry();
			}
		});
		view.getPubEditor().getAddMoreButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (!ClientData.isLoggedIn()) {
					view.getLoginView().view();
					return;
				}
				view.getPubEditor().edit(null);
			}
		});
		view.getPubEditor().getCancelButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				view.getPubEditor().getDialogBox().hide();
			}
		});
		view.getPubEditor().getUploadForm().addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				//view.getUploadForm().reset();
				EduManager.loadingView.stopProcessing();
				String url = event.getResults();
				String fileName = view.getPubEditor().getUploadField().getFilename();
				if (fileName.contains("/"))
					fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
				if (fileName.contains("\\"))
					fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
				String fileType = "";
				if (fileName.contains("."))
					fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
				Material mat = new Material(ClientData.getCurrentUser().getId(), 
						Config.NULL_ID, fileName, url, "", fileType);
				view.getPubEditor().getCurrentPublication().getMaterials().add(mat);
			}
		});
		
		view.getPubEditor().getUploadField().addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				EduManager.loadingView.startProcessing();
				if (view.getPubEditor().getUploadField().getFilename().length() == 0) {
					Window.alert("File not found !");
				} else {
					view.getPubEditor().getUploadForm().setAction(GWT.getHostPageBaseURL()+"gcsupload?folder=publications");//&bucket=avatars");
					view.getPubEditor().getUploadForm().setEncoding(FormPanel.ENCODING_MULTIPART);
					view.getPubEditor().getUploadForm().setMethod(FormPanel.METHOD_POST);
					view.getPubEditor().getUploadForm().submit();
				} 
			}
		});
	}	
	protected void bind() {
		eventBus.addHandler(DataLoadedEvent.TYPE, new DataLoadedEventHandler() {
			@Override
			public void onDataLoaded(DataLoadedEvent event) {
				BasicActivity.this.onDataLoaded(event.getDataType());
			}
		});
	}
	
	private void startNewBlobstoreSession(final BasicView basicView) {
		EduManager.dataService.getBlobstoreUploadUrl(new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				basicView.getTeacherEditor().getUploadForm().setAction(result);
				basicView.getTeacherEditor().getUploadForm().setEncoding(FormPanel.ENCODING_MULTIPART);
				basicView.getTeacherEditor().getUploadForm().setMethod(FormPanel.METHOD_POST);
				basicView.getTeacherEditor().getUploadForm().submit();
			}
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Không tải được ảnh");
			}
		});
	}

	protected void onDataLoaded(int dataType) {}
	
	private void doUpdateMaterial(final Material material, final BasicView view){
		new RPCCall<Long>() {
			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(Long result) {
				material.setId(result);
				addMaterial(material);
				view.getDocumentView().getCreateCategoryDialog().hide();
				TreeInforItem inforItem = new TreeInforItem(material, material.getId(), 
						material.getName(), material.getParentId(), Config.MATERIAL);
				if(view.getDocumentView().getCreateMaterialDialog().getCurrenAction()==ActionMaterial.CREATE){
					Category parent = findCategory(material.getParentId());
					parent.getMaterials().add(material);
					parent.getMaterialIds().add(result);
					loadChildCategoryAndChildMaterial(parent, view);
					view.getDocumentView().getPersonDocumentTree().addItem(inforItem, 
							view.getDocumentView().getPersonDocumentTree().getCurrentTreeItem());
					view.getDocumentView().getPersonDocumentTree().getCurrentTreeItem().setState(true);
					
				}else if(view.getDocumentView().getCreateMaterialDialog().getCurrenAction()==ActionMaterial.EDIT){
					view.getDocumentView().getPersonDocumentTree().getCurrentTreeItem().setUserObject(inforItem);
					view.getDocumentView().getPersonDocumentTree().setTitleCurrentItem(material.getName());
				}
			}

			@Override
			protected void callService(AsyncCallback<Long> cb) {
				material.setUserId(ClientData.getCurrentUser().getId());
				EduManager.dataService.doUpdateMaterial(material, cb);
			}
		}.retry();
	}
	
	private void loadChildCategoryAndChildMaterial(final Category parent, final BasicView view){
		new RPCCall<Category>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Không thể tải nội dung thư mục. Xin hãy thử lại sau");
			}

			@Override
			public void onSuccess(Category result) {
				parent.setChildren(result.getChildren());
				parent.setMaterials(result.getMaterials());
				view.getDocumentView().getPersonDocumentTree().getCurrentTreeItem().removeItems();
				ClientUtils.log("ChildNum: " + parent.getChildren().size() + " and MatNum: " + parent.getMaterials().size());
				if(!parent.getChildren().isEmpty()){
					List<TreeInforItem> treeInforItemCats = new ArrayList<TreeInforItem>();
					for (Category c : parent.getChildren()) {
						TreeInforItem inforItem = new TreeInforItem(c, c.getId(), 
								c.getName(), c.getParentId(), Config.CATEGORY);
						treeInforItemCats.add(inforItem);
					}
					view.getDocumentView().getPersonDocumentTree().addItems(treeInforItemCats, parent.getId());;
				}
				if(!parent.getMaterials().isEmpty()){
					List<TreeInforItem> treeInforItemMats = new ArrayList<TreeInforItem>();
					for (Material m : parent.getMaterials()) {
						TreeInforItem inforItem = new TreeInforItem(m, m.getId(), 
								m.getName(), m.getParentId(), Config.MATERIAL);
						treeInforItemMats.add(inforItem);
					}
					view.getDocumentView().getPersonDocumentTree().addItems(treeInforItemMats, parent.getId());;
				}
				view.getDocumentView().getPersonDocumentTree().getCurrentTreeItem().setState(true);
			}

			@Override
			protected void callService(AsyncCallback<Category> cb) {
				EduManager.dataService.getChildCategoriesAndMaterials(parent.getId(), cb);
			}
		}.retry();
	}
	
	private void doUpDateCategory(final Category category, final BasicView view, final long rootId){
		if(!ClientData.isLoggedIn()){
			return;
		}
		new RPCCall<Long>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Lỗi khi tạo Category !");
			}

			@Override
			public void onSuccess(Long result) {
				category.setId(result);
				addCategory(category);
				view.getDocumentView().getCreateCategoryDialog().hide();
				TreeInforItem inforItem = new TreeInforItem(category, 
						category.getId(), category.getName(), category.getParentId(), Config.CATEGORY);
				if(view.getDocumentView().getCreateCategoryDialog().getCurrenAction()==ActionCategory.CREATE){
					if(category.getParentId() == rootId){
						getMainCategories().add(category);
						view.getDocumentView().getPersonDocumentTree().addItem(inforItem, 
								view.getDocumentView().getPersonDocumentTree().getTree());
					} else {
						loadChildCategoryAndChildMaterial(category, view);
						view.getDocumentView().getPersonDocumentTree().getCurrentCategory().getChildren().add(category);
						view.getDocumentView().getPersonDocumentTree().addItem(inforItem, 
								view.getDocumentView().getPersonDocumentTree().getCurrentTreeItem());
						view.getDocumentView().getPersonDocumentTree().getCurrentTreeItem().setState(true);
					}
				}else if(view.getDocumentView().getCreateCategoryDialog().getCurrenAction()==ActionCategory.EDIT){
					view.getDocumentView().getPersonDocumentTree().getCurrentTreeItem().setUserObject(inforItem);
					view.getDocumentView().getPersonDocumentTree().setTitleCurrentItem(category.getName());
				}
			}

			@Override
			protected void callService(AsyncCallback<Long> cb) {
				category.setUserId(ClientData.getCurrentUser().getId());
				EduManager.dataService.doUpdateCategory(category, cb);
			}
		}.retry();
	}
	
	protected void loadCategoriesData(final Long rootId, final Long ownerId, final Long viewerId, final BasicView view){
		if (root != null)
			return;
		root = new Category();
		root.setId(rootId);
		if(this.getMainCategories()==null || this.getMainCategories().isEmpty()){
			new RPCCall<List<Category>>() {

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Lỗi khi load maincategory!");
				}

				@Override
				public void onSuccess(List<Category> result) {
					if(!result.isEmpty()){
						sort(result);
						setMainCategories(result);
						root.getChildren().addAll(getMainCategories());
						view.getDocumentView().genTreeView(root);
					}
				}

				@Override
				protected void callService(AsyncCallback<List<Category>> cb) {
					EduManager.dataService.getCategories(rootId, ownerId, viewerId, cb);
				}
			}.retry();
		} else {
			root.getChildren().addAll(getMainCategories());
			view.getDocumentView().genTreeView(root);
		}
	}
	
	protected void sort(List<Category> categories) {
		Collections.sort(categories, new Comparator<Category>() {
			@Override
			public int compare(Category o1, Category o2) {
				return o1.getName().compareToIgnoreCase(o2.getName());
			}
		});
	}
	
	protected Map<Long, Category> mapCategoriesClient = new HashMap<Long,Category>();
	protected Map<Long, Material> mapMaterialsClient = new HashMap<Long,Material>();
	protected List<Category> mainCategories =null;
	
	public void setMainCategories(List<Category> mainCategories) {
		addCategories(mainCategories);
		this.mainCategories = mainCategories;
	}
	
	public List<Category> getMainCategories() {
		if(mainCategories==null){
			mainCategories = new ArrayList<Category>();
		}
		return this.mainCategories;
	}
	
	public void addMaterial(Material material){
		mapMaterialsClient.put(material.getId(), material);
	}
	
	public void addMaterials(List<Material> materials){
		for (Material material : materials) {
			addMaterial(material);
		}
	}
	
	public Material findMaterial(Long id){
		return mapMaterialsClient.get(id);
	}
	
	public void addCategory(Category category){
		mapCategoriesClient.put(category.getId(), category);
		if(category.getChildren().size()>0){
			addCategories(category.getChildren());
		}
		if(category.getMaterials().size()>0){
			addMaterials(category.getMaterials());
		}
	}
	
	public void deleteMaterial(Material material){
		Category parent = findCategory(material.getParentId());
		parent.getMaterialIds().remove(material.getId());
		parent.getMaterials().remove(material);
		mapMaterialsClient.remove(material.getId());
	}
	
	public void deleteCategory(Category category){
		if(category.getParentId()==Config.NULL_ID){
			mainCategories.remove(category);
		} else {
			Category parent = findCategory(category.getParentId());
			parent.getChildren().remove(category);
			parent.getChildrenIds().remove(category.getId());
		}
		mapCategoriesClient.remove(category.getId());
		
	}
	
	public void addCategories(List<Category> categories){
		for (Category category : categories) {
			addCategory(category);
		}
	}
	
	public Category findCategory(Long id){
		return mapCategoriesClient.get(id);
	}
}