package com.hungnt.edumanager.client.activities.basic;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.IsWidget;
import com.hungnt.edumanager.client.activities.course.CourseEditer;
import com.hungnt.edumanager.client.activities.login.LoginView;
import com.hungnt.edumanager.client.activities.publication.PublicationEditor;
import com.hungnt.edumanager.client.activities.teacher.TeacherEditor;
import com.hungnt.edumanager.client.activities.topic.TopicEditer;

public interface BasicView{
	public IsWidget asWidget();
	public void refreshView();
	public HasClickHandlers getTeacherButton();
	public HasClickHandlers getSubjectButton();
	public HasClickHandlers getCourseButton();
	public HasClickHandlers getHomeButton();
	public HasClickHandlers getMaterialButton();
	HasClickHandlers getUserInfo();
	LoginView getLoginView();
	void updateUserInfo();
	HasClickHandlers getPublicationButton();
	HasClickHandlers getRegisterButton();
	HasClickHandlers getLoginButton();
	HasClickHandlers getLogoutButton();
	void onDataLoaded(int dataType);
	HasClickHandlers getEquipmentButton();
	CourseEditer getCourseEditer();
	HasClickHandlers getTopicButton();
	TopicEditer getTopicEditer();
	TeacherEditor getTeacherEditor();
	DocumentTreeView getDocumentView();
	HasClickHandlers getEventButton();
	PublicationEditor getPubEditor();
	HasClickHandlers getMeetingButton();
}
