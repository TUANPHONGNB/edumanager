package com.hungnt.edumanager.client.activities.basic;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.hungnt.edumanager.client.activities.document.PersonDocumentTree;
import com.hungnt.edumanager.client.view.AlertDialog;
import com.hungnt.edumanager.client.view.CreateCategoryDialog;
import com.hungnt.edumanager.client.view.CreateMaterialDialog;
import com.hungnt.edumanager.client.view.FormUploadDoc;
import com.hungnt.edumanager.shared.Category;

public class DocumentTreeView extends VerticalPanel {
	private ScrollPanel treeWrapPanelCategory = new ScrollPanel();
	private PersonDocumentTree personDocumentTree = new PersonDocumentTree();
	private CreateCategoryDialog createCategoryDialog = new CreateCategoryDialog();
	private AlertDialog alertDialog = new AlertDialog();
	private CreateMaterialDialog createMaterialDialog = new CreateMaterialDialog();
	private boolean readOnly = false;

	public DocumentTreeView() {
		this.clear();
		this.setWidth("100%");
		this.setSpacing(5);
		HTML title = new HTML("<p style=\"font-size:20px\">Tài liệu</p>");
		this.add(title);
		this.setCellHorizontalAlignment(title, HasHorizontalAlignment.ALIGN_CENTER);
		this.add(treeWrapPanelCategory);
		this.setWidth("100%");
		treeWrapPanelCategory.setWidth("100%");
		treeWrapPanelCategory.setHeight("400px");
		treeWrapPanelCategory.add(personDocumentTree.getTree());
	}
	
	public Widget getTreeWrapper() {
		return treeWrapPanelCategory;
	}

	public PersonDocumentTree getPersonDocumentTree() {
		return personDocumentTree;
	}

	public void genTreeView(Category root) {
		personDocumentTree.genTreeView(root);
	}

	public CreateCategoryDialog getCreateCategoryDialog() {
		return createCategoryDialog;
	}

	public AlertDialog getAlertDialog() {
		return alertDialog;
	}

	public CreateMaterialDialog getCreateMaterialDialog() {
		return createMaterialDialog;
	}

	public FormUploadDoc getFormUploadDoc() {
		return createMaterialDialog.getFormUpload();
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}
}