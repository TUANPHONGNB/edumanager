package com.hungnt.edumanager.client.activities.course.details;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Course;

public class CourseDetailViewImpl extends BasicViewImpl implements CourseDetailView {
	
	private VerticalPanel courseViewPanel = new VerticalPanel();
	private Button edit = new Button("Sửa");
	private FlexTable flexTable = new FlexTable();
	private Button deleteBtn = new Button("Xoá");
	private Course currentCourse = null;
	
	public CourseDetailViewImpl() {	
		super();
		flexTable.setWidth("90%");
		courseViewPanel.setWidth("100%");
		courseViewPanel.add(flexTable);
		courseViewPanel.setCellHorizontalAlignment(flexTable, ALIGN_CENTER);
		this.contentPanel.add(courseViewPanel);
		this.contentPanel.add(documentView);
		flexTable.getCellFormatter().setWidth(0, 0, "150px");
	}
	
	@Override
	public HasClickHandlers getEditButton() {
		return this.edit;
	}
	
	@Override
	public void view(Course course) {
		if (course == null)
			return;
		currentCourse = course;
		int row = 0, col = 0;
		flexTable.setText(row, col++, "Tên môn");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'><b>" + course.getSubjectName() + " - " + course.getSubjectIdCode() + "</b></font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Giáo viên");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + course.getTeacherName() + "</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Mã lớp");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + course.getCourseIdCode() + "</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Số SV");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + course.getStudentNum() + " Sinh Viên</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Học kỳ");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>Học kỳ " + course.getSemester() + " - Kỳ " + Config.getSemesterType(course.getSemesterType()) + "</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Khối lượng");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + course.getCredits() + " tín chỉ (" + course.getCreditInfo() + ")</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Thông tin lớp");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + Config.getCourseType(course.getCourseType()) + " - " + course.getClassType() + "</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Thời gian/Địa điểm");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + course.getPlaceTimeInfoHtml() + "</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Ghi chú");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + course.getInfo()));
		row ++; col = 0;
		documentView.setReadOnly(!ClientData.isLoggedIn() || !ClientData.canEdit(currentCourse.getTeacherId(), false));
		col ++;
		HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.setSpacing(5);
		hPanel.add(edit);
		hPanel.add(deleteBtn);
		flexTable.setWidget(row, col, hPanel);
	}
	
	@Override
	public HasClickHandlers getDeleteButton() {
		return this.deleteBtn;
	}

	@Override
	public Course getCurrentCourse() {
		return this.currentCourse;
	}
}