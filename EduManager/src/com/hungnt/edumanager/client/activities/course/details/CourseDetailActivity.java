package com.hungnt.edumanager.client.activities.course.details;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.RPCCall;
import com.hungnt.edumanager.client.activities.ClientFactory;
import com.hungnt.edumanager.client.activities.basic.BasicActivity;
import com.hungnt.edumanager.client.activities.course.CoursePlace;
import com.hungnt.edumanager.client.event.EditEvent;
import com.hungnt.edumanager.shared.Course;

public class CourseDetailActivity extends BasicActivity {

	private CourseDetailView view;
	
	public CourseDetailActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getCourseDetailView();
		panel.setWidget(view.asWidget());
		super.start(panel, eventBus, view);
		Course course = ((CourseDetailPlace)place).getCourse();
		if (course != null) {
			rootId = course.getId();
			ownerId = course.getTeacherId();
			view.view(course);
			if (ClientData.isLoggedIn())
				this.loadCategoriesData(rootId, ownerId, ClientData.getCurrentUser().getId(), view);
		}
	}
	
	@Override
	public void bind() {
		super.bind();
		view.getDeleteButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
		    	  if (Window.confirm("Bạn có muốn xoá lớp học này không?"))
			    	  new RPCCall<Void>() {
						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Có lỗi xảy ra, không thể xoá lớp học");
						}
						@Override
						public void onSuccess(Void result) {
							ClientData.getCourses().clear();
							EduManager.clientFactory.getPlaceController().goTo(new CoursePlace());
						}
						@Override
						protected void callService(AsyncCallback<Void> cb) {
							EduManager.dataService.deleteCourse(ClientData.getCurrentUser().getId(), 
									view.getCurrentCourse(), cb);
						}
					}.retry();
			}
		});
		view.getEditButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				EduManager.clientFactory.getEventBus().fireEvent(new EditEvent(view.getCurrentCourse()));
			}
		});
	}
 }
