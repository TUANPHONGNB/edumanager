package com.hungnt.edumanager.client.activities.course;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.hungnt.edumanager.client.activities.basic.BasicPlace;

public class CoursePlace extends BasicPlace {
	
	public CoursePlace() {
		super();
	}
	
	public String getToken() {
		return "course";
	}
	
	public static class Tokenizer implements PlaceTokenizer<CoursePlace> {
        @Override
        public String getToken(CoursePlace place) {
            return place.getToken();
        }

        @Override
        public CoursePlace getPlace(String token) {
            return new CoursePlace();
        }
    }
	
}
