package com.hungnt.edumanager.client.activities.course;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.AbstractEditableCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ImageCell;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.BarChart;
import com.googlecode.gwt.charts.client.corechart.BarChartOptions;
import com.googlecode.gwt.charts.client.options.HAxis;
import com.googlecode.gwt.charts.client.options.VAxis;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.activities.teacher.details.TeacherDetailPlace;
import com.hungnt.edumanager.client.event.EditEvent;
import com.hungnt.edumanager.client.widget.HListBox;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Statistics;
import com.hungnt.edumanager.shared.Teacher;

public class StatViewer  extends VerticalPanel {
	
	private FormPanel hiddenUploadForm;
	DataGrid<Teacher> teacherList;// = new DataGrid<Teacher>();
	private HorizontalPanel topFunctionPanel = new HorizontalPanel();
	private HListBox teacherFilter = new HListBox();
	private HListBox scholarYearFilter = new HListBox();
	private HListBox semesterFilter = new HListBox();
	private HListBox paymentTypeFilter = new HListBox();
	private HListBox courseTypeFilter = new HListBox();

	private Long currentSelectedTeacherId = -1L;
	private Long currentSelectedScholarYearId = null;
	private int currentSelectedSemester = 1;
	private StatContextMenu contextMenu = new StatContextMenu();
	private Button chartButton = new Button("Biểu đồ");
	private VerticalPanel chartPanel = new VerticalPanel();
	private Button closeChartButton = new Button("Đóng");
	
	public StatViewer() {
		this.setWidth("100%");
		teacherFilter.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedTeacherId = Long.parseLong(teacherFilter.getSelectedValue());
				view(ClientData.getTeachers());
			}
		});
		scholarYearFilter.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
			}
		});
		courseTypeFilter.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				view(ClientData.getTeachers());
			}
		});
		scholarYearFilter.addItem(Config.getScholorYear(Config.currentScholarYear), 
				Config.currentScholarYear + "");
		semesterFilter.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedSemester = Integer.parseInt(semesterFilter.getSelectedValue());
				view(ClientData.getTeachers());
			}
		});
		
		paymentTypeFilter.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				view(ClientData.getTeachers());
			}
		});

		paymentTypeFilter.addItem(Config.getPaymentType(0), "0");
		paymentTypeFilter.addItem(Config.getPaymentType(1), "1");
		
		courseTypeFilter.addItem("Giảng dạy", "0");
		courseTypeFilter.addItem("Đồ án", "1");
		
		//semesterFilter.addItem("Cả năm", "0"); 
		semesterFilter.addItem("Kỳ 1", "1"); 
		semesterFilter.addItem("Kỳ 2", "2"); 
		semesterFilter.addItem("Kỳ 3 (Hè)", "3");
		
		teacherFilter.setWidth("200px");
		scholarYearFilter.setWidth("100px");
		courseTypeFilter.setWidth("100px");
		paymentTypeFilter.setWidth("100px");
		semesterFilter.setWidth("100px");
		
		topFunctionPanel.setWidth("100%");
		topFunctionPanel.setSpacing(5);
		HTML filterBy = new HTML("Tìm kiếm:");
		topFunctionPanel.add(filterBy);
		topFunctionPanel.setCellWidth(filterBy, "130px");
		topFunctionPanel.add(teacherFilter);
		topFunctionPanel.add(scholarYearFilter);
		topFunctionPanel.add(courseTypeFilter);
		topFunctionPanel.add(paymentTypeFilter);
		topFunctionPanel.add(semesterFilter);
		
		topFunctionPanel.setCellHorizontalAlignment(teacherFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(scholarYearFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(courseTypeFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(paymentTypeFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(semesterFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellWidth(filterBy, "70px");
		topFunctionPanel.setCellWidth(teacherFilter, "100px");
		topFunctionPanel.setCellWidth(scholarYearFilter, "100px");
		topFunctionPanel.setCellWidth(courseTypeFilter, "50px");
		topFunctionPanel.setCellWidth(paymentTypeFilter, "50px");
		topFunctionPanel.setCellWidth(semesterFilter, "100px");
		
		topFunctionPanel.add(chartButton);
		
		hiddenUploadForm = new FormPanel();
		hiddenUploadForm.setVisible(false);
		hiddenUploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
		hiddenUploadForm.setMethod(FormPanel.METHOD_POST);
		
		contextMenu.getDetailBtn().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				contextMenu.hide();
	    		  EduManager.clientFactory.getPlaceController().goTo(new TeacherDetailPlace(contextMenu.getTeacher()));
			}
		});
		contextMenu.getEditBtn().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				contextMenu.hide();
	    		  EduManager.clientFactory.getEventBus().fireEvent(new EditEvent(contextMenu.getTeacher()));
			}
		});
		contextMenu.getExportBtn().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				contextMenu.hide();
		    	  hiddenUploadForm.setAction(GWT.getHostPageBaseURL()+"export?" 
		    			  + Config.EXPORT_TOKEN + "=" + contextMenu.getTeacher().getId());
		    	  hiddenUploadForm.submit();
			}
		});
		chartPanel.setWidth("100%");
		closeChartButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				chartPanel.clear();
			}
		});
	}

	public HasClickHandlers getChartButton() {
		return this.chartButton;
	}
	
	public int getSemester() {
		return Integer.parseInt(semesterFilter.getSelectedValue());
	}
	
	/**
	   * The provider that holds the list of contacts in the database.
	   */
	  private ListDataProvider<Teacher> dataProvider = new ListDataProvider<Teacher>();
	
	/**
   * The key provider that provides the unique ID of a contact.
   */
  public static final ProvidesKey<Teacher> KEY_PROVIDER = new ProvidesKey<Teacher>() {
    @Override
    public Object getKey(Teacher item) {
      return item == null ? null : item.getId();
    }
  };
	
	public void setupTeacherFilters(List<Teacher> teachers) {
		teacherFilter.clear();
		teacherFilter.addItem("[Tất cả giáo viên]", "0");
		for (Teacher s : teachers) {
			teacherFilter.addItem(s.getFullName(), s.getId() + "");
		}
	}
	
	public HorizontalPanel getFunctionPanel() {
		return this.topFunctionPanel;
	}

	public VerticalPanel getChartPanel() {
		return this.chartPanel;
	}
	
	public DataGrid<Teacher> getDataGrid() {
		return this.teacherList;
	}
  
	public  void view(List<Teacher> fullTeachers) {
     	if (teacherFilter.getItemCount() < 2)
     		setupTeacherFilters(ClientData.getTeachers());
     	List<Teacher> teachers = new ArrayList<Teacher>();
		if (currentSelectedTeacherId > 0) {
			for (Teacher teacher : fullTeachers) {
				if (teacher.getId().compareTo(currentSelectedTeacherId) == 0) {
					teachers.add(teacher);
					break;
				}
			}
			for (int i = 0; i < teacherFilter.getItemCount(); i ++) {
				if (teacherFilter.getValue(i).compareToIgnoreCase(currentSelectedTeacherId + "") == 0) {
					teacherFilter.setSelectedIndex(i);
					break;
				}
			}
		}
		if (teachers.size() == 0)
			teachers.addAll(fullTeachers);
     	
     	for (int i = 0; i < teachers.size(); i++) {
			teachers.get(i).setPosIndex(i + 1);
		}
		dataProvider.setList(teachers);
	    editableCells = new ArrayList<AbstractEditableCell<?, ?>>();
	    teacherList = new DataGrid<Teacher>(30, KEY_PROVIDER);
	    
	    teacherList.setWidth("100%");
	    teacherList.setHeight("600px");
	    
	    //teacherList.setMinimumTableWidth(140, Unit.EM);
	    dataProvider.addDataDisplay(teacherList);
	    
	    this.clear();
	    this.add(topFunctionPanel);
	    this.add(chartPanel);
	    this.add(teacherList);
	    this.add(hiddenUploadForm);
	    
	    // NumberCell.
	    //Column<Teacher, Number> numberColumn =
	    addColumn(40, HasHorizontalAlignment.ALIGN_LOCALE_END, new NumberCell(), "#", new GetValue<Number>() {
	          @Override
	          public Number getValue(Teacher contact) {
	            return contact.getPosIndex();
	          }
	        }, null);
	    
	    // ImageCell.
	    //Column<Teacher, String> avatarCol = 
	    addColumn(60, new ImageCell(), "Ảnh", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  if (contact.getAvatar() != null && !contact.getAvatar().isEmpty())
	    		  return contact.getAvatar() + "=s50";
	    	  else if (contact.getGender() == 0)
	    		  return "images/male.png";
	    	  else
	    		  return "images/female.png";
	      }
	    }, null);
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> nameCol = 
	    addColumn(150, new ClickableTextCell(), "Họ và tên", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	        return contact.getFullName();
	      }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	    	  if (!ClientData.canEdit(object.getId(), false)) {
	    		  EduManager.clientFactory.getPlaceController().goTo(new TeacherDetailPlace(object));
	    		  return;
	    	  }
	    	  contextMenu.open(object);
	      }
	    });
	    
	    currentSelectedSemester = Integer.parseInt(semesterFilter.getSelectedValue());
	    
	    int currentCourseType = Integer.parseInt(courseTypeFilter.getSelectedValue());
	    if (currentCourseType == 0) {
		    if (getSelectedPaymentType() == 0)
		    	gen1AData();
		    else
		    	gen1BData();
	    }
	    else {
		    if (getSelectedPaymentType() == 0)
		    	gen1AProject();
		    else
		    	gen1BProject();
	    }
	}

	private void gen1AData() {
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> cqNumCol = 
	    addColumn(80, new ClickableTextCell(), "CQ", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getKsClassIds().size();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> clcNumCol = 
	    addColumn(80, new ClickableTextCell(), "CLC", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getClcClassIds().size();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> tnNumCol = 
	    addColumn(80, new ClickableTextCell(), "TN", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getKstnClassIds().size();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> ictNumCol = 
	    addColumn(80, new ClickableTextCell(), "ICT", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getCtttClassIds().size();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> vnNumCol = 
	    addColumn(80, new ClickableTextCell(), "VN", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getVnClassIds().size();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> ctttNumCol = 
	    addColumn(80, new ClickableTextCell(), "CTTT", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getCtttClassIds().size();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> sdhNumCol = 
	    addColumn(80, new ClickableTextCell(), "SDH", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getSdhClassIds().size();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> course1ANumCol = 
	    addColumn(80, new ClickableTextCell(), "Tổng 1A", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int aNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  aNum = contact.getStatMap().get(currentSelectedSemester).getCourse1ANum();
	    	  if (aNum > 0)
	    		  return aNum + "";
	    	  return "";
	      }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	}
	
	private void gen1BData() {
	    // ClickableTextCell.
	    //Column<Teacher, String> cqNumCol = 
	    addColumn(80, new ClickableTextCell(), "SIE", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getSieClassIds().size();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> clcNumCol = 
	    addColumn(80, new ClickableTextCell(), "TC", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getTcClassIds().size();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> tnNumCol = 
	    addColumn(80, new ClickableTextCell(), "CD", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getCdClassIds().size();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> ictNumCol = 
	    addColumn(80, new ClickableTextCell(), "KS2", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getKs2ClassIds().size();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> course1ANumCol = 
	    addColumn(80, new ClickableTextCell(), "Tổng 1B", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int aNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  aNum = contact.getStatMap().get(currentSelectedSemester).getCourse1BNum();
	    	  if (aNum > 0)
	    		  return aNum + "";
	    	  return "";
	      }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	}
	
	
	private void gen1AProject() {
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> cqNumCol = 
	    addColumn(80, new ClickableTextCell(), "Project1", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getKsProject1Num();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> clcNumCol = 
	    addColumn(80, new ClickableTextCell(), "Project2", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getKsProject2Num();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> tnNumCol = 
	    addColumn(80, new ClickableTextCell(), "Project3", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getKsProject3Num();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> ictNumCol = 
	    addColumn(80, new ClickableTextCell(), "DATN-KS", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getKsDatnNum();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> vnNumCol = 
	    addColumn(80, new ClickableTextCell(), "DATN-CN", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getCnDatnNum();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> ctttNumCol = 
	    addColumn(80, new ClickableTextCell(), "Project2-KSTN", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getKstnProject2Num();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> sdhNumCol = 
	    addColumn(80, new ClickableTextCell(), "DATN-KSTN", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getKstnDatnNum();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> sdhNumCol = 
	    addColumn(80, new ClickableTextCell(), "DATN-CLC", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getClcDatnNum();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> sdhNumCol = 
	    addColumn(80, new ClickableTextCell(), "TTCN-CLC", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getClcTtcnNum();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> sdhNumCol = 
	    addColumn(80, new ClickableTextCell(), "TTTN-CLC", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getClcTttnNum();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> sdhNumCol = 
	    addColumn(80, new ClickableTextCell(), "GR1-ICT", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getIctGr1Num();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> sdhNumCol = 
	    addColumn(80, new ClickableTextCell(), "GR2-ICT", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getIctGr2Num();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> sdhNumCol = 
	    addColumn(80, new ClickableTextCell(), "GR3-ICT", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getIctGr3Num();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> sdhNumCol = 
	    addColumn(80, new ClickableTextCell(), "DATN-ICT", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getIctDatnNum();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> sdhNumCol = 
	    addColumn(80, new ClickableTextCell(), "GR1-VN", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getVnGr1Num();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> sdhNumCol = 
	    addColumn(80, new ClickableTextCell(), "GR3-VN", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getVnGr3Num();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> sdhNumCol = 
	    addColumn(80, new ClickableTextCell(), "DATN-VN", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getVnDatnNum();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> course1ANumCol = 
	    addColumn(80, new ClickableTextCell(), "Tổng", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int aNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  aNum = contact.getStatMap().get(currentSelectedSemester).getProject1ANum();
	    	  if (aNum > 0)
	    		  return aNum + "";
	    	  return "";
	      }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	}
	
	private void gen1BProject() {
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> cqNumCol = 
	    addColumn(80, new ClickableTextCell(), "SIE", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getSieDatnNum();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> clcNumCol = 
	    addColumn(80, new ClickableTextCell(), "TC", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getTcDatnNum();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> tnNumCol = 
	    addColumn(80, new ClickableTextCell(), "CD", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getCdDatnNum();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> ictNumCol = 
	    addColumn(80, new ClickableTextCell(), "KS2", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int bNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  bNum = contact.getStatMap().get(currentSelectedSemester).getKs2DatnNum();
	    	  if (bNum > 0)
	    		  return bNum + "";
	    	  return "";
	     }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	    
	    // ClickableTextCell.
	    //Column<Teacher, String> course1ANumCol = 
	    addColumn(80, new ClickableTextCell(), "Tổng", new GetValue<String>() {
	      @Override
	      public String getValue(Teacher contact) {
	    	  int aNum = 0;
	    	  if (contact.getStatMap().containsKey(currentSelectedSemester))
	    		  aNum = contact.getStatMap().get(currentSelectedSemester).getProject1BNum();
	    	  if (aNum > 0)
	    		  return aNum + "";
	    	  return "";
	      }
	    }, new FieldUpdater<Teacher, String>() {
	      @Override
	      public void update(int index, Teacher object, String value) {
	      }
	    });
	}	
	
	/**
	   * The list of cells that are editable.
	   */
	  private List<AbstractEditableCell<?, ?>> editableCells;
	
	 /**
	   * Get a cell value from a record.
	   * 
	   * @param <C> the cell type
	   */
	  private static interface GetValue<C> {
	    C getValue(Teacher contact);
	  }
	
	  private <C> Column<Teacher, C> addColumn(int width, Cell<C> cell, String headerText,
		      final GetValue<C> getter, FieldUpdater<Teacher, C> fieldUpdater) {
		  return this.addColumn(width, null, cell, headerText, getter, fieldUpdater);
	  }
	
	/**
	   * Add a column with a header.
	   * 
	   * @param <C> the cell type
	   * @param cell the cell used to render the column
	   * @param headerText the header string
	   * @param getter the value getter for the cell
	   */
	  private <C> Column<Teacher, C> addColumn(int width, HorizontalAlignmentConstant align, Cell<C> cell, String headerText,
	      final GetValue<C> getter, FieldUpdater<Teacher, C> fieldUpdater) {
	    Column<Teacher, C> column = new Column<Teacher, C>(cell) {
	      @Override
	      public C getValue(Teacher object) {
	        return getter.getValue(object);
	      }
	    };
	    column.setFieldUpdater(fieldUpdater);
	    if (cell instanceof AbstractEditableCell<?, ?>) {
	      editableCells.add((AbstractEditableCell<?, ?>) cell);
	    }
	    teacherList.addColumn(column, headerText);
	    if (width > 0)
	    	teacherList.setColumnWidth(column, width, Unit.PX);
	    if (align != null)
	    	column.setHorizontalAlignment(align);
	    return column;
	  }

	  private int getSelectedPaymentType() {
		  return Integer.parseInt(paymentTypeFilter.getSelectedValue());
	  }
	  
	public void showChart() {
		int currentCourseType = Integer.parseInt(courseTypeFilter.getSelectedValue());
		BarChart chart = new BarChart();
		chart.setHeight("500px");
		DataTable dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.STRING, "Name");
		dataTable.addColumn(ColumnType.NUMBER, currentCourseType == 0 ? "Số lớp" : "Số Sinh viên");
		for (Teacher teacher : ClientData.getTeachers()) {
			Statistics stat = teacher.getStatMap().get(getSemester());
			if (stat != null) {
				int num = 0;
				if (currentCourseType == 0)
					num = getSelectedPaymentType() == 0 ? stat.getCourse1ANum() : stat.getCourse1BNum();
				else
					num = getSelectedPaymentType() == 0 ? stat.getProject1ANum() : stat.getProject1BNum();
				if (num > 0)
					dataTable.addRow(teacher.getFullName(), num);
			}
		}
		
		// Set options
		BarChartOptions options = BarChartOptions.create();
		options.setFontName("Tahoma");
		options.setTitle(currentCourseType == 0 ? "Tổng số lớp" : "Tổng số Sinh viên");
		options.setHAxis(HAxis.create(currentCourseType == 0 ? "Số lớp" : "Số Sinh viên"));
		options.setVAxis(VAxis.create("Giáo viên"));

		chartPanel.clear();
		chartPanel.add(closeChartButton);
		chartPanel.setCellHorizontalAlignment(closeChartButton, HasHorizontalAlignment.ALIGN_RIGHT);
		chartPanel.add(chart);
		
		// Draw the chart
		chart.draw(dataTable, options);
		
	}
}
