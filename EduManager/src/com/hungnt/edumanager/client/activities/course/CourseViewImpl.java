package com.hungnt.edumanager.client.activities.course;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.CssToken;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Course;
import com.hungnt.edumanager.shared.Subject;
import com.hungnt.edumanager.shared.Teacher;

public class CourseViewImpl extends BasicViewImpl implements CourseView{
	
	private VerticalPanel courseViewPanel = new VerticalPanel();
	private Button addMore = new Button("Thêm");
	private CourseListViewer courseListViewer = new CourseListViewer();
	private StatViewer statViewer = new StatViewer();

	private Course currentCourse = null;

	public CourseViewImpl() {	
		super();
		courseViewPanel.setSpacing(5);
		courseViewPanel.setWidth("100%");
		contentPanel.add(courseViewPanel);
	}
	
	@Override
	public void refreshView() {
		super.refreshView();
		course.setStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE, true);
	}
	
	@Override
	public HasClickHandlers getAddMoreButton() {
		return this.addMore;
	}
	
	@Override
	public Course getCourse() {
		currentCourse = this.getCourseEditer().getCourse();
		return currentCourse;
	}
	
	@Override
	public void setupSubjects(List<Subject> subjects) {
		this.getCourseEditer().setupSubjects(subjects);
	}
	
	@Override
	public void setupTeachers(List<Teacher> teachers) {
		this.getCourseEditer().setupTeachers(teachers);
	}
	
	@Override
	public void onDataLoaded(int dataType) {
		if (dataType == Config.DATA_TEACHER)
			setupTeacherFilters(ClientData.getTeachers());
		if (dataType == Config.DATA_SUBJECT)
			setupSubjectFilters(ClientData.getSubjects());
	}
	
	@Override
	public void setupTeacherFilters(List<Teacher> teachers) {
		courseListViewer.setupTeacherFilters(teachers);
	}
	
	public void setupSubjectFilters(List<Subject> subjects) {
		courseListViewer.setupSubjectFilters(subjects);
	}
	
	@Override
	public void edit(Course course) {
		this.getCourseEditer().edit(course);
	}
	
    @Override
	public  void view(List<Course> fullCourses) {
    	if (!ClientData.isLoggedIn(true)) {
    		return;
    	}
	    courseViewPanel.clear();
	    courseViewPanel.add(courseListViewer);
	    courseListViewer.getFunctionPanel().add(addMore);
	    courseListViewer.getFunctionPanel().setCellHorizontalAlignment(addMore, ALIGN_RIGHT);
    	courseListViewer.view(fullCourses);
	}
    
    @Override
    public CourseListViewer getCourseListViewer() {
    	return this.courseListViewer;
    }
    
    @Override
    public StatViewer getStatViewer() {
    	return this.statViewer;
    }
    
    @Override
    public VerticalPanel getCourseViewPanel() {
    	return this.courseViewPanel;
    }
}
