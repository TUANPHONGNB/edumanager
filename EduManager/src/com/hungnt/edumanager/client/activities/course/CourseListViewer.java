package com.hungnt.edumanager.client.activities.course;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.AbstractEditableCell;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.activities.course.details.CourseDetailPlace;
import com.hungnt.edumanager.client.event.EditEvent;
import com.hungnt.edumanager.client.widget.HListBox;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Course;
import com.hungnt.edumanager.shared.Subject;
import com.hungnt.edumanager.shared.Teacher;

public class CourseListViewer extends VerticalPanel {
	
	private HListBox teacherFilter = new HListBox();
	private HListBox subjectFilter = new HListBox();
	private HListBox scholarYearFilter = new HListBox();
	private HListBox semesterFilter = new HListBox();
	private HListBox paymentTypeFilter = new HListBox();
	private HListBox statusFilter = new HListBox();
	private TextBox searchBox = new TextBox();
	private Anchor stats = new Anchor("Thống kê");
	private HorizontalPanel topFunctionPanel = new HorizontalPanel();
	private HorizontalPanel searchPanel = new HorizontalPanel();
	private Button searchButton = new Button("Tìm kiếm");
	private Button updateStatButton = new Button("Update stats");

	private Long fixedTeacherId = -1L;
	private Long currentSelectedTeacherId = -1L;
	private Long currentSelectedSubjectId = 0L;
	private int currentSelectedSemester = 0;
	private int currentSelectedPaymentType = -1;
	private int currentSelectedStatus = -1;
	
	DataGrid<Course> courseList;// = new DataGrid<Teacher>();
	
	public CourseListViewer() {	
		super();
		this.setWidth("100%");
		teacherFilter.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedTeacherId = Long.parseLong(teacherFilter.getSelectedValue());
				view(ClientData.getCourses());
			}
		});
		subjectFilter.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedSubjectId = Long.parseLong(subjectFilter.getSelectedValue());
				view(ClientData.getCourses());
			}
		});
		paymentTypeFilter.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedPaymentType = Integer.parseInt(paymentTypeFilter.getSelectedValue());
				view(ClientData.getCourses());
			}
		});
		scholarYearFilter.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				ClientData.getCourses().clear();
				ClientData.loadData(Integer.parseInt(scholarYearFilter.getSelectedValue()), Integer.parseInt(semesterFilter.getSelectedValue()));
			}
		});
		scholarYearFilter.addItem(Config.getScholorYear(Config.currentScholarYear - 1), (Config.currentScholarYear - 1) + "");
		scholarYearFilter.addItem(Config.getScholorYear(Config.currentScholarYear), Config.currentScholarYear + "");
		scholarYearFilter.setSelectedIndex(1);
		semesterFilter.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedSemester = Integer.parseInt(semesterFilter.getSelectedValue());
				view(ClientData.getCourses());
			}
		});
		statusFilter.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedStatus = Integer.parseInt(statusFilter.getSelectedValue());
				view(ClientData.getCourses());
			}
		});
		statusFilter.addItem("[Tất cả]", "-1");
		statusFilter.addItem("Chưa bắt đầu", "0");
		statusFilter.addItem("Đang dạy", "1");
		statusFilter.addItem("Đã kết thúc", "6");
		paymentTypeFilter.addItem("Loại 1A và 1B", "-1");
		paymentTypeFilter.addItem(Config.getPaymentType(0), "0");
		paymentTypeFilter.addItem(Config.getPaymentType(1), "1");
		semesterFilter.addItem("Cả năm", "0"); 
		semesterFilter.addItem("Kỳ 1", "1"); 
		semesterFilter.addItem("Kỳ 2", "2"); 
		semesterFilter.addItem("Kỳ 3 (Hè)", "3");
		
		subjectFilter.setWidth("180px");
		scholarYearFilter.setWidth("100px");
		semesterFilter.setWidth("100px");
		teacherFilter.setWidth("180px");
		paymentTypeFilter.setWidth("130px");
		statusFilter.setWidth("100px");
		
		topFunctionPanel.setWidth("100%");
		topFunctionPanel.setSpacing(5);
		HTML filterBy = new HTML("Lọc theo:");
		topFunctionPanel.add(filterBy);
		topFunctionPanel.add(teacherFilter);
		topFunctionPanel.add(subjectFilter);
		topFunctionPanel.add(scholarYearFilter);
		topFunctionPanel.add(semesterFilter);
		topFunctionPanel.add(paymentTypeFilter);
		topFunctionPanel.add(statusFilter);
		topFunctionPanel.setCellHorizontalAlignment(teacherFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(subjectFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(scholarYearFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(semesterFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(paymentTypeFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellWidth(filterBy, "70px");
		topFunctionPanel.setCellWidth(teacherFilter, "100px");
		topFunctionPanel.setCellWidth(subjectFilter, "100px");
		topFunctionPanel.setCellWidth(scholarYearFilter, "100px");
		topFunctionPanel.setCellWidth(semesterFilter, "100px");
		
		searchPanel.setWidth("100%");
		searchPanel.setSpacing(5);
		HTML searchHtml = new HTML("Tìm kiếm");
		searchPanel.add(searchHtml);
		searchPanel.setCellWidth(searchHtml, "70px");
		searchBox.setWidth("200px");
		searchBox.getElement().setPropertyString("placeholder", "Mã Môn hoặc Mã Lớp");
		searchPanel.add(searchBox);
		searchPanel.setCellWidth(searchBox, "200px");
		searchPanel.add(searchButton);
		searchPanel.add(updateStatButton);
		this.searchButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (searchBox.getText().trim().isEmpty()) {
					view(ClientData.getCourses());
					return;
				}
				List<Course> found = new ArrayList<Course>();
				for (Course c : ClientData.getCourses()) {
					if (c.getSubjectIdCode().equalsIgnoreCase(searchBox.getText().trim())
							|| c.getCourseIdCode().equalsIgnoreCase(searchBox.getText().trim()))
						found.add(c);
				}
				view(found, true);
			}
		});
		
		searchPanel.add(stats);
		searchPanel.setCellHorizontalAlignment(stats, ALIGN_RIGHT);
		searchPanel.setCellVerticalAlignment(stats, ALIGN_MIDDLE);
	}
	
	public HasClickHandlers getSearchButton() {
		return this.searchButton;
	}
	
	public HasClickHandlers getStats() {
		return this.stats;
	}
	
	public TextBox getSearchBox() {
		return this.searchBox;
	}
	
	public void setFixedTeacherId(Long teacherId) {
		this.fixedTeacherId = teacherId;
	}
	
	public HorizontalPanel getFunctionPanel() {
		return this.topFunctionPanel;
	}
	
	public void onDataLoaded(int dataType) {
		if (dataType == Config.DATA_TEACHER)
			setupTeacherFilters(ClientData.getTeachers());
		if (dataType == Config.DATA_SUBJECT)
			setupSubjectFilters(ClientData.getSubjects());
	}
	
	public void setupTeacherFilters(List<Teacher> teachers) {
		teacherFilter.clear();
		if (fixedTeacherId <= 0) {
			teacherFilter.addItem("[Tất cả giáo viên]", "0");
		}
		for (Teacher s : teachers) {
			if (fixedTeacherId > 0) {
				if (s.getId().compareTo(fixedTeacherId) == 0) {
					teacherFilter.addItem(s.getFullName(), s.getId() + "");
					break;
				}
			}
			else {
				teacherFilter.addItem(s.getFullName(), s.getId() + "");
			}
		}
	}

	
	public DataGrid<Course> getDataGrid() {
		return this.courseList;
	}
	
	public void setupSubjectFilters(List<Subject> subjects) {
		subjectFilter.clear();
		subjectFilter.addItem("[Tất cả các môn]", "0");
		if (fixedTeacherId > 0) {
			for (Course c : ClientData.getCourses()) {
				if (c.getTeacherId().compareTo(fixedTeacherId) == 0) {
					subjectFilter.addItem(c.getSubjectName() + " - " + c.getSubjectIdCode(), c.getSubjectId() + "");
				}
			}
		}
		else {
			for (Subject s : subjects)
				subjectFilter.addItem(s.getName() + " - " + s.getSubjectIdCode(), s.getId() + "");
		}
	}
	
	public void refresh() {
		if (courseList != null)
			courseList.redraw();
	}
	
	public HasClickHandlers getUpdateStatsButton() {
		return this.updateStatButton;
	}
	
	public int getScholarYear() {
		return Integer.parseInt(scholarYearFilter.getSelectedValue());
	}

	public int getSemester() {
		return Integer.parseInt(semesterFilter.getSelectedValue());
	}

	
	/**
	   * The provider that holds the list of contacts in the database.
	   */
	  private ListDataProvider<Course> dataProvider = new ListDataProvider<Course>();
	
	/**
     * The key provider that provides the unique ID of a contact.
     */
    public static final ProvidesKey<Course> KEY_PROVIDER = new ProvidesKey<Course>() {
      @Override
      public Object getKey(Course item) {
        return item == null ? null : item.getId();
      }
    };
    
	public  void view(List<Course> fullCourses) {
		this.view(fullCourses, false);
	}
	public  void view(List<Course> fullCourses, boolean showAll) {
		
		updateStatButton.setVisible(ClientData.isLoggedIn());// && ClientData.getCurrentUser().isAdmin());
		
    	if (teacherFilter.getItemCount() < 2)
    		setupTeacherFilters(ClientData.getTeachers());
    	if (subjectFilter.getItemCount() < 2)
    		setupSubjectFilters(ClientData.getSubjects());

    	if (currentSelectedTeacherId < 0 && ClientData.isLoggedIn())
    		currentSelectedTeacherId = ClientData.getCurrentUser().getId();
    	
    	if (fixedTeacherId > 0)
    		currentSelectedTeacherId = fixedTeacherId;
    	
		List<Course> courses = new ArrayList<Course>(fullCourses);
		if (!showAll) {
			if (currentSelectedTeacherId > 0 || currentSelectedSubjectId > 0
					|| currentSelectedSemester > 0
					|| currentSelectedPaymentType >= 0) {
				List<Course> removed = new ArrayList<Course>();
				for (Course course : courses) {
					if ((currentSelectedTeacherId > 0 && course.getTeacherId().compareTo(currentSelectedTeacherId) != 0)
							|| (currentSelectedSubjectId > 0 && course.getSubjectId().compareTo(currentSelectedSubjectId) != 0)
							|| (currentSelectedSemester > 0 && course.getSemester() != currentSelectedSemester)
							|| (currentSelectedPaymentType >= 0 && course.getPaymentType() != currentSelectedPaymentType)
							|| (currentSelectedStatus >= 0 && course.getStatus() != currentSelectedStatus)) {
						removed.add(course);
					}
				}
				courses.removeAll(removed);
				teacherFilter.setSelectedValue(currentSelectedTeacherId);
				subjectFilter.setSelectedValue(currentSelectedSubjectId);
				statusFilter.setSelectedValue(currentSelectedStatus);
			}
		}
		
		List<Course> teachingCourses = new ArrayList<Course>();
		List<Course> projectCourses = new ArrayList<Course>();
		for (Course course : courses) {
			if (course.getCourseType() == Config.COURSE_TYPE_DA
					|| course.getCourseType() >= Config.COURSE_TYPE_PROJECT1) {
				projectCourses.add(course);
			}
			else {
				teachingCourses.add(course);
			}
		}
		
		courses.clear();
		courses.addAll(teachingCourses);
		courses.addAll(projectCourses);

	    // Create the table.
    	//		if (courses == null || courses.isEmpty()) {
    	//			courses = dataProvider.getList();
    	//			for (int i = 0; i < 10; i ++) {
    	//			}
    	//		}
		for (int i = 0; i < courses.size(); i++) {
			courses.get(i).setPosIndex(i + 1);
		}
		dataProvider.setList(courses);
	    editableCells = new ArrayList<AbstractEditableCell<?, ?>>();
	    courseList = new DataGrid<Course>(500, KEY_PROVIDER);
	    
	    courseList.setWidth("100%");
	    courseList.setHeight("600px");
	    
	    //teacherList.setMinimumTableWidth(140, Unit.EM);
	    dataProvider.addDataDisplay(courseList);
	    	    
    	this.clear();
		this.add(topFunctionPanel);
		if (fixedTeacherId <= 0)
			this.add(searchPanel);
		this.add(courseList);

	    
	    // NumberCell.
	    Column<Course, Number> numberColumn =
	        addColumn(40, new NumberCell(), "#", new GetValue<Number>() {
	          @Override
	          public Number getValue(Course contact) {
	            return contact.getPosIndex();
	          }
	        }, null);
	    numberColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LOCALE_END);
	    
	    // ClickableTextCell.
	    addColumn(300, new SafeHtmlCell(), "Học phần", new GetValue<SafeHtml>() {
	      @Override
	      public SafeHtml getValue(Course contact) {
	        String info = "<font color='blue'>" + contact.getSubjectName() + "</font>"
	        		+ "<br><font color='red'>" + contact.getTeacherName() + "</font>"
	        		+ " - " + contact.getStudentNum() + " SV - ";
	        info += contact.getCredits() + " TC";
	    	  if (!contact.getCreditInfo().isEmpty())
	    		  info += " (" + contact.getCreditInfo() + ")";
	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
	    	  builder.appendHtmlConstant(info);
	    	  return builder.toSafeHtml();
	      }
	    }, null);

//	    new FieldUpdater<Course, String>() {
//		      @Override
//		      public void update(int index, Course object, String value) {
//		    	  EduManager.clientFactory.getPlaceController().goTo(new CourseDetailPlace(object));
//		      }
//	    }
	    // TextCell.
//	    Column<Course, String> courseIdCol = addColumn(new TextCell(), "Mã lớp/HP", new GetValue<String>() {
//	      @Override
//	      public String getValue(Course contact) {
//	        return contact.getCourseIdCode() + " / " + contact.getSubjectIdCode();
//	      }
//	    }, null);
//	    courseList.setColumnWidth(courseIdCol, 100, Unit.PX);
	    
	    // SafeHtmlCell.
	    //Column<Course, SafeHtml> courseIdCol = 
	    addColumn(150, new SafeHtmlCell(), "Mã lớp/HP", new GetValue<SafeHtml>() {
	      @Override
	      public SafeHtml getValue(Course contact) {
		      String info = contact.getCourseIdCode() + " - <font color='color:blue'>" + contact.getSubjectIdCode() + "</font>";
		      info += "<br>" + Config.getStudentTypeName(contact.getStudenType())
			  	    	 + " - <font color=\"color:blue\">Kỳ " + contact.getSemester() 
			  	    	 + Config.getSemesterType(contact.getSemesterType()) + "</font>";
	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
	    	  builder.appendHtmlConstant(info);
	    	  return builder.toSafeHtml();
	      }
	    }, null);
	    
//	    // TextCell.
//	    Column<Course, String> subjectIdCol = addColumn(new TextCell(), "Mã HP", new GetValue<String>() {
//	      @Override
//	      public String getValue(Course contact) {
//	        return contact.getSubjectIdCode();
//	      }
//	    }, null);
//	    courseList.setColumnWidth(subjectIdCol, 60, Unit.PX);
	    
	    // TextCell.
//	    Column<Course, String> courseTypeCol = addColumn(new TextCell(), "Hệ", new GetValue<String>() {
//	      @Override
//	      public String getValue(Course contact) {
//	        return Config.getStudentTypeName(contact.getStudenType()) +
////	  	    	  return contact.getSemester() + " (" + Config.getSemesterType(contact.getSemesterType()) + ")";
//
//	      }
//	    }, null);
//	    courseList.setColumnWidth(courseTypeCol, 60, Unit.PX);	 
	    
	    // SafeHtmlCell.
//	    Column<Course, SafeHtml> courseTypeCol = addColumn(80, new SafeHtmlCell(), "Hệ", new GetValue<SafeHtml>() {
//	      @Override
//	      public SafeHtml getValue(Course contact) {
//		      String info = Config.getStudentTypeName(contact.getStudenType())
//			  	    	 + "<br><font color=\"color:blue\">Kỳ " + contact.getSemester() 
//			  	    	 + Config.getSemesterType(contact.getSemesterType()) + "</font>";
//	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
//	    	  builder.appendHtmlConstant(info);
//	    	  return builder.toSafeHtml();
//	      }
//	    }, null);
	    
	    // SafeHtmlCell.
	    //Column<Course, SafeHtml> creditCol = 
//	    addColumn(90, new SafeHtmlCell(), "Khối lượng", new GetValue<SafeHtml>() {
//	      @Override
//	      public SafeHtml getValue(Course contact) {
//	    	  String info = contact.getCredits() + " tín chỉ";
//	    	  if (!contact.getCreditInfo().isEmpty())
//	    		  info += "<br>(" + contact.getCreditInfo() + ")";
//	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
//	    	  builder.appendHtmlConstant(info);
//	    	  return builder.toSafeHtml();
//	      }
//	    }, null);

//	    // ClickableTextCell.
//	    Column<Course, String> teacherCol = addColumn(new ClickableTextCell(), "Giáo viên", new GetValue<String>() {
//	      @Override
//	      public String getValue(Course contact) {
//	        return contact.getTeacherName();
//	      }
//	    }, new FieldUpdater<Course, String>() {
//	      @Override
//	      public void update(int index, Course object, String value) {
//	        Window.alert("You clicked " + object.getName());
//	      }
//	    });
//	    courseList.setColumnWidth(teacherCol, 150, Unit.PX);
	    
	 // SafeHtmlCell.
	    //Column<Course, SafeHtml> placeCol = 
	    addColumn(220, new SafeHtmlCell(), "Thời gian/Địa điểm", new GetValue<SafeHtml>() {
	      @Override
	      public SafeHtml getValue(Course contact) {
	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
	    	  builder.appendHtmlConstant(contact.getPlaceTimeInfoHtml());
	    	  return builder.toSafeHtml();
	      }
	    }, null);
	    
//	    // TextCell.
//	    Column<Course, String> semesterCol = addColumn(new TextCell(), "Kỳ", new GetValue<String>() {
//	      @Override
//	      public String getValue(Course contact) {
//	    	  return contact.getSemester() + " (" + Config.getSemesterType(contact.getSemesterType()) + ")";
//	      }
//	    }, null);
//	    courseList.setColumnWidth(semesterCol, 50 + diff, Unit.PX);
	    
		 // SafeHtmlCell.
	    //Column<Course, SafeHtml> statusCol = 
	    addColumn(100, new SafeHtmlCell(), "Trạng thái", new GetValue<SafeHtml>() {
	      @Override
	      public SafeHtml getValue(Course contact) {
	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
	    	  builder.appendHtmlConstant("<font color='red'>" + Config.getStatus(contact.getStatus()) + "</font>");
	    	  return builder.toSafeHtml();
	      }
	    }, null);
	    
	    // ButtonCell.
	    //Column<Course, String> delCol = 
	    addColumn(ClientData.isLoggedIn() && ClientData.getCurrentUser().isAdmin() ? 50 : 0, new ButtonCell(), "Chi tiết", new GetValue<String>() {
	      @Override
	      public String getValue(Course contact) {
	    	  return "!";
	      }
	    }, new FieldUpdater<Course, String>() {
	      @Override
	      public void update(int index, final Course object, String value) {
	    	  EduManager.clientFactory.getPlaceController().goTo(new CourseDetailPlace(object));
	      }
	    });

	    // DateCell.
//	    DateTimeFormat dateFormat = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
//	    Column<Teacher, Date> dateCol = addColumn(new DateCell(dateFormat), "Ngày sinh", new GetValue<Date>() {
//	      @Override
//	      public Date getValue(Teacher contact) {
//	        return contact.getBirthDate();
//	      }
//	    }, null);
//	    teacherList.setColumnWidth(dateCol, 50, Unit.PX);
	    
//	    if (ClientData.isLoggedIn()) {
//		    courseList.setColumnWidth(statusCol, 100 + diff, Unit.PX);
//		    // ActionCell.
//		    Column<Course, Course> editCol = addColumn(new ActionCell<Course>("Sửa", new ActionCell.Delegate<Course>() {
//		      @Override
//		      public void execute(Course contact) {
//		    	  if (ClientData.canEdit(contact.getUserId(), true))
//		    		  EduManager.clientFactory.getEventBus().fireEvent(new EditEvent(contact));
//		      }
//		    }), "Sửa", new GetValue<Course>() {
//		      @Override
//		      public Course getValue(Course contact) {
//		        return contact;
//		      }
//		    }, null);
	
		    if (ClientData.isLoggedIn() && ClientData.getCurrentUser().isAdmin()) {
			    // ButtonCell.
			    //Column<Course, String> delCol = 
			    addColumn(0, new ButtonCell(), "Sửa", new GetValue<String>() {
			      @Override
			      public String getValue(Course contact) {
			    	  if (contact.getScholarYear() == Config.currentScholarYear)
			    		  return "Sửa";
			    	  else
			    		  return "Copy";
			      }
			    }, new FieldUpdater<Course, String>() {
			      @Override
			      public void update(int index, final Course object, String value) {
			    	  if (object.getScholarYear() < Config.currentScholarYear) {
			    		  object.setId(null);
			    	  }
			    	  EduManager.clientFactory.getEventBus().fireEvent(new EditEvent(object));
			      }
			    });
		    }
		    //teacherList.setColumnWidth(delCol, 70, Unit.PX);
//	    }
	}
	
	/**
	   * The list of cells that are editable.
	   */
	  private List<AbstractEditableCell<?, ?>> editableCells;
	
	 /**
	   * Get a cell value from a record.
	   * 
	   * @param <C> the cell type
	   */
	  private static interface GetValue<C> {
	    C getValue(Course contact);
	  }
	
	  private <C> Column<Course, C> addColumn(int width, Cell<C> cell, String headerText,
		      final GetValue<C> getter, FieldUpdater<Course, C> fieldUpdater) {
		  return this.addColumn(width, null, cell, headerText, getter, fieldUpdater);
	  }
	  
	/**
	   * Add a column with a header.
	   * 
	   * @param <C> the cell type
	   * @param cell the cell used to render the column
	   * @param headerText the header string
	   * @param getter the value getter for the cell
	   */
	  private <C> Column<Course, C> addColumn(int width, HorizontalAlignmentConstant align, Cell<C> cell, String headerText,
	      final GetValue<C> getter, FieldUpdater<Course, C> fieldUpdater) {
	    Column<Course, C> column = new Column<Course, C>(cell) {
	      @Override
	      public C getValue(Course object) {
	        return getter.getValue(object);
	      }
	    };
	    column.setFieldUpdater(fieldUpdater);
	    if (cell instanceof AbstractEditableCell<?, ?>) {
	      editableCells.add((AbstractEditableCell<?, ?>) cell);
	    }
	    courseList.addColumn(column, headerText);
	    if (width > 0)
	    	courseList.setColumnWidth(column, width, Unit.PX);
	    if (align != null)
	    	column.setHorizontalAlignment(align);
	    return column;
	  }
}