package com.hungnt.edumanager.client.activities.course;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.client.activities.basic.BasicView;
import com.hungnt.edumanager.shared.Course;
import com.hungnt.edumanager.shared.Subject;
import com.hungnt.edumanager.shared.Teacher;

public interface CourseView extends BasicView{

	void edit(Course course);

	void view(List<Course> teachers);

	HasClickHandlers getAddMoreButton();

	void setupSubjects(List<Subject> subjects);

	void setupTeachers(List<Teacher> teachers);

	Course getCourse();

	void setupTeacherFilters(List<Teacher> teachers);

	CourseListViewer getCourseListViewer();

	StatViewer getStatViewer();

	VerticalPanel getCourseViewPanel();
	
}
