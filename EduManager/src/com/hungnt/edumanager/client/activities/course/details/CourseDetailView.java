package com.hungnt.edumanager.client.activities.course.details;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.IsWidget;
import com.hungnt.edumanager.client.activities.basic.BasicView;
import com.hungnt.edumanager.shared.Course;

public interface CourseDetailView extends BasicView {
	public IsWidget asWidget();
	public void refreshView();
	HasClickHandlers getEditButton();
	void view(Course course);
	HasClickHandlers getDeleteButton();
	public Course getCurrentCourse();
}
