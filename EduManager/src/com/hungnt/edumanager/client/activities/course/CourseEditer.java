package com.hungnt.edumanager.client.activities.course;

import java.util.Date;
import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.widget.FormatTextArea;
import com.hungnt.edumanager.client.widget.HListBox;
import com.hungnt.edumanager.client.widget.NumberTextBox;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Course;
import com.hungnt.edumanager.shared.CourseInfo;
import com.hungnt.edumanager.shared.ExamInfo;
import com.hungnt.edumanager.shared.Subject;
import com.hungnt.edumanager.shared.Teacher;

public class CourseEditer {
	
	private Button save = new Button("Lưu");
	private Button cancel = new Button("Huỷ");
	private FlexTable flexTable = new FlexTable();
	private FlexTable infoFlexTable = new FlexTable();
	private FlexTable examFlexTable = new FlexTable();
	private HListBox subject = new HListBox();
	private HListBox teacher = new HListBox();
	private TextBox[] places = new TextBox[2];
	private HListBox semesterType = new HListBox();
	private HListBox semester = new HListBox();
	private HListBox courseType = new HListBox();
	private HListBox credits = new HListBox();
	private HListBox[] days = new HListBox[2];
	private HListBox[] from = new HListBox[2];
	private HListBox[] to = new HListBox[2];
	private CheckBox[] checks = new CheckBox[2];
	private TextBox courseIdCode = new TextBox();
	private TextBox subjectIdCode = new TextBox();
	private TextBox creditInfo = new TextBox();
	private FormatTextArea info = new FormatTextArea();
	private NumberTextBox studentNum = new NumberTextBox();
	private TextBox classType = new TextBox();
	private HListBox language = new HListBox();
	private HListBox paymentType = new HListBox();
	private HListBox studentType = new HListBox();
	private HListBox status = new HListBox();
	private HListBox notification = new HListBox();
	private HListBox remindNotification = new HListBox();
	private DatePicker fromDatePicker = new DatePicker();
	private DateBox fromDateBox = null;
	private DatePicker toDatePicker = new DatePicker();
	private DateBox toDateBox = null;
	private CourseListViewer courseListViewer = new CourseListViewer();

	private CheckBox[] examChecks = new CheckBox[2];
	private DatePicker[] examDatePickers = new DatePicker[2];
	private DateBox[] examDateBoxes = new DateBox[2];
	private HListBox[] examHours = new HListBox[2];
	private HListBox[] examMinutes = new HListBox[2];
	private NumberTextBox[] examDurations = new NumberTextBox[2];
	private TextBox[] examPlaces = new TextBox[2];
	
	private DialogBox dialogBox = null;
	private Course currentCourse = null;

	public CourseEditer() {	
		super();
		subject.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				if (subject.getSelectedValue().isEmpty())
					return;
				Subject s = ClientData.getSubjectMap().get(Long.parseLong(subject.getSelectedValue()));
				if (s != null) {
					credits.setSelectedValue(s.getCredits());
					creditInfo.setText(s.getCreditInfo());
					subjectIdCode.setText(s.getSubjectIdCode());
				}
			}
		});

		int row = 0, col = 0;
		for (int i = 1; i <= 3; i ++)
			semesterType.addItem(Config.getSemesterType(i), "" + i);
		semester.addItem("Học kỳ 1", "1");
		semester.addItem("Học kỳ 2", "2");
		semester.addItem("Học kỳ hè", "3");
		for (int i = 0; i <= 20; i++) {
			String name = Config.getCourseType(i);
			if (name != null && !name.isEmpty())
				courseType.addItem(name, "" + i);
		}
		for (int i = 1; i <= 20; i ++) {
			credits.addItem(i + " tín chỉ", i + "");
		}
		language.addItem("Tiếng Việt", "0");
		language.addItem("Tiếng Anh", "1");
		for (int i = 0; i <= 6; i++) {
			if (!Config.getStatus(i).isEmpty())
				status.addItem(Config.getStatus(i), i + "");
		}
		notification.addItem("[Nhắc nhở]", "0");
		for (int i = 0; i <= 8; i++) {
			if (!Config.getNotification(i).isEmpty())
				notification.addItem(Config.getNotification(i), i + "");
		}
		for (int i = 0; i <= 1; i ++)
			paymentType.addItem("Loại " + Config.getPaymentType(i), "" + i);
		paymentType.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				updateStudentType(paymentType.getSelectedIndex());
			}
		});
		teacher.setWidth("220px");
		subject.setWidth("220px");
		courseIdCode.setWidth("200px");
		subjectIdCode.setWidth("200px");
		courseType.setWidth("200px");
		subjectIdCode.setEnabled(false);
		info.setWidth("95%");
		creditInfo.getElement().setPropertyString("placeholder", "Ví dụ: 2-1-0-4, ...");
		creditInfo.setWidth("100px");
		studentNum.setWidth("200px");
		classType.getElement().setPropertyString("placeholder", "K55, K56, ICT, TN, ...");
		classType.setWidth("200px");
		language.setWidth("220px");
		paymentType.setWidth("200px");
		flexTable.setWidth("100%");
		flexTable.setText(row, col++, "Môn học");
		flexTable.setWidget(row, col++, subject);
		flexTable.setText(row, col++, "Giáo viên");
		flexTable.setWidget(row, col++, teacher);
		row ++; col = 0;
		flexTable.setText(row, col++, "Học kỳ");
		HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.setSpacing(5);
		hPanel.setWidth("100px");
		semester.setWidth("100px");
		semesterType.setWidth("80px");
		hPanel.add(semester);
		hPanel.add(new HTML("Kỳ"));
		hPanel.add(semesterType);
		flexTable.setWidget(row, col++, hPanel);
		flexTable.setText(row, col++, "Trạng thái");
		flexTable.setWidget(row, col++, status);
		row ++; col = 0;
		flexTable.setText(row, col++, "Loại môn");
		flexTable.setWidget(row, col++, courseType);
		flexTable.setText(row, col++, "Khối lượng");
		HorizontalPanel creditPanel = new HorizontalPanel();
		creditPanel.setWidth("200px");
		credits.setWidth("100px");
		creditInfo.setWidth("100px");
		creditPanel.add(credits);
		creditPanel.add(creditInfo);
		flexTable.setWidget(row, col++, creditPanel);
		row ++; col = 0;
		flexTable.setText(row, col++, "Loại hình");
		flexTable.setWidget(row, col++, paymentType);
		flexTable.setText(row, col++, "Hệ ĐT");
		flexTable.setWidget(row, col++, studentType);
		row ++; col = 0;
		flexTable.setText(row, col++, "Mã HP");
		flexTable.setWidget(row, col++, subjectIdCode);
		flexTable.setText(row, col++, "Mã lớp");
		flexTable.setWidget(row, col++, courseIdCode);
		row ++; col = 0;
		flexTable.setText(row, col++, "Số SV");
		flexTable.setWidget(row, col++, studentNum);
		flexTable.setText(row, col++, "Nhắc nhở");
		flexTable.setWidget(row, col++, notification);
		row ++; col = 0;
		flexTable.setText(row, col++, "Thông tin lớp");
		flexTable.setWidget(row, col++, classType);
		flexTable.setText(row, col++, "Ngôn ngữ");
		flexTable.setWidget(row, col++, language);
		row ++; col = 0;

		fromDateBox = new DateBox(fromDatePicker, null, new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd/MM/yyyy")));
		fromDateBox.setWidth("200px");
		fromDateBox.getDatePicker().setYearArrowsVisible(true);
		toDateBox = new DateBox(toDatePicker, null, new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd/MM/yyyy")));
		toDateBox.setWidth("200px");
		toDateBox.getDatePicker().setYearArrowsVisible(true);
		flexTable.setText(row, col++, "Bắt đầu");
		flexTable.setWidget(row, col++, fromDateBox);
		flexTable.setText(row, col++, "Kết thúc");
		flexTable.setWidget(row, col++, toDateBox);

		row ++; col = 0;
		flexTable.setText(row, col++, "Thông tin thêm");
		flexTable.setWidget(row, col, info);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		
		if (ClientData.isLoggedIn() && ClientData.getCurrentUser().isAdmin()) {
			remindNotification.addItem("Chưa nhắc nhở", "0");
			remindNotification.addItem("Đã nhắc 1 tuần", "7");
			remindNotification.addItem("Đã nhắc 1 ngày", "1");
			row ++; col = 0;
			flexTable.setText(row, col++, "Gửi email nhắc nhở");
			flexTable.setWidget(row, col, remindNotification);
			flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
			row ++; col = 0;
		}
		
		flexTable.getCellFormatter().setWidth(row, 0, "100px");
		flexTable.getCellFormatter().setWidth(row, 1, "220px");
		flexTable.getCellFormatter().setWidth(row, 2, "100px");
		flexTable.getCellFormatter().setWidth(row, 3, "220px");
		
		row = 0; col = 0;
		for (int i = 0; i < days.length; i ++) {
			days[i] = new HListBox();
			days[i].setWidth("150px");
			days[i].addItem("[Chọn thứ]", "0");
			days[i].addItem("Chủ nhật", "1");
			days[i].addItem("Thứ Hai", "2");
			days[i].addItem("Thứ Ba", "3");
			days[i].addItem("Thứ Tư", "4");
			days[i].addItem("Thứ Năm", "5");
			days[i].addItem("Thứ Sáu", "6");
			days[i].addItem("Thứ Bảy", "7");
			days[i].addItem("Tất cả các ngày", "8");
		}
		for (int i = 0; i < from.length; i ++) {
			from[i] = new HListBox();
			from[i].setWidth("150px");
			from[i].addItem("[Chọn tiết]", "0");
			for (int j = 1; j <= 12; j ++ ) {
				from[i].addItem("Tiết " + j, j + "");
			}
			from[i].addItem("Tiết 13 (tối)", "13");
			from[i].addItem("Tiết 14 (tối)", "14");
		}
		for (int i = 0; i < to.length; i ++) {
			to[i] = new HListBox();
			to[i].setWidth("150px");
			to[i].addItem("[Chọn tiết]", "0");
			for (int j = 1; j <= 12; j ++ ) {
				to[i].addItem("Tiết " + j, j + "");
			}
			to[i].addItem("Tiết 13 (tối)", "13");
			to[i].addItem("Tiết 14 (tối)", "14");
		}
		for (int i = 0; i < days.length; i ++) {
			col = 0;
			final int index = i;
			checks[i] = new CheckBox();
			checks[i].setValue(i == 0);
			checks[i].addValueChangeHandler(new ValueChangeHandler<Boolean>() {
				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					checkTimePlace(index, event.getValue());
				}
			});
			infoFlexTable.setWidget(i, col++, checks[i]);
			infoFlexTable.setText(i, col++, "Thứ");
			infoFlexTable.setWidget(i, col++, days[i]);
			infoFlexTable.setText(i, col++, "Từ");
			infoFlexTable.setWidget(i, col++, from[i]);
			infoFlexTable.setText(i, col++, "Đến");
			infoFlexTable.setWidget(i, col++, to[i]);
			places[i] = new TextBox();
			places[i].getElement().setPropertyString("placeholder", "Ví dụ: 205-D9, ...");
			places[i].setWidth("200px");
			infoFlexTable.setText(i, col++, "Tại");
			infoFlexTable.setWidget(i, col++, places[i]);
		}
		
		for (int i = 0; i < 2; i++) {
			examChecks[i] = new CheckBox();
			final int index = i;
			examChecks[i].addValueChangeHandler(new ValueChangeHandler<Boolean>() {
				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					checkExamInfo(index, event.getValue());
				}
			});
			examDatePickers[i] = new DatePicker();
			examDateBoxes[i] = new DateBox(examDatePickers[i], new Date(), new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd/MM/yyyy")));
			examDateBoxes[i].setWidth("100px");
			examHours[i] = new HListBox();
			examHours[i].setWidth("100px");
			for (int j = 0; j <= 24; j++) {
				examHours[i].addItem(j + " giờ", j + "");
			}
			examMinutes[i] = new HListBox();
			examMinutes[i].setWidth("100px");
			for (int j = 0; j <= 60; j++) {
				examMinutes[i].addItem(j + " phút", j + "");
			}
			examDurations[i] = new NumberTextBox();
			examDurations[i].setWidth("100px");
			examPlaces[i] = new TextBox();
			examPlaces[i].setWidth("100px");
			col = 0;
			examFlexTable.setWidget(i, col++, examChecks[i]);
			examFlexTable.setText(i, col++, "Ngày");
			examFlexTable.setWidget(i, col++, examDateBoxes[i]);
			examFlexTable.setText(i, col++, "Lúc");
			examFlexTable.setWidget(i, col++, examHours[i]);
			examFlexTable.setWidget(i, col++, examMinutes[i]);
			examFlexTable.setText(i, col++, "Thời gian");
			examFlexTable.setWidget(i, col++, examDurations[i]);
			examFlexTable.setText(i, col++, "Tại");
			examFlexTable.setWidget(i, col++, examPlaces[i]);
		}
	}
	
	private void updateStudentType(int type) {
		studentType.clear();
		if (type == 0) {
			for (int i = 0; i < 6; i ++) {
				studentType.addItem(Config.getStudentTypeName(i), "" + i);
			}
		}
		else {
			for (int i = 10; i < 15; i ++) {
				studentType.addItem(Config.getStudentTypeName(i), "" + i);
			}
		}
	}
	
	public HasClickHandlers getCancelButton() {
		return this.cancel;
	}
	
	public HasClickHandlers getSaveButton() {
		return this.save;
	}
	
	public DialogBox getDialogBox() {
		return this.dialogBox;
	}
	
	public Course getCourse() {
		if (currentCourse == null)
			currentCourse = new Course();
		if (subject.getSelectedValue().trim().isEmpty()) {
			Window.alert("Xin hãy chọn môn học");
			return null;
		}
		if (teacher.getSelectedValue().trim().isEmpty()) {
			Window.alert("Xin hãy chọn giáo viên");
			return null;
		}
		Teacher t = ClientData.getTeacherMap().get(Long.parseLong(teacher.getSelectedValue()));
		Subject s = ClientData.getSubjectMap().get(Long.parseLong(subject.getSelectedValue()));
		currentCourse.setCourseIdCode(courseIdCode.getText().trim());
		currentCourse.setCourseLanguage(Integer.parseInt(language.getSelectedValue()));
		currentCourse.setCredits(Integer.parseInt(credits.getSelectedValue()));
		currentCourse.setCreditInfo(creditInfo.getText().trim());
		currentCourse.setSemester(Integer.parseInt(semester.getSelectedValue()));
		currentCourse.setSemesterType(Integer.parseInt(semesterType.getSelectedValue()));
		currentCourse.setSubjectId(s.getId());
		currentCourse.setSubjectIdCode(s.getSubjectIdCode());
		currentCourse.setSubjectName(s.getName().trim());
		currentCourse.setTeacherId(t.getId());
		currentCourse.setTeacherName(t.getFullName());
		currentCourse.setStudentNum(Integer.parseInt(studentNum.getText().trim()));
		currentCourse.setClassType(classType.getText().trim());
		currentCourse.setCourseType(Integer.parseInt(courseType.getSelectedValue()));
		currentCourse.setStartDate(fromDateBox.getValue());
		currentCourse.setEndDate(toDateBox.getValue());
		currentCourse.setUserId(ClientData.getCurrentUser().getId());
		currentCourse.setStatus(Integer.parseInt(status.getSelectedValue()));
		currentCourse.setNotification(Integer.parseInt(notification.getSelectedValue()));
		currentCourse.setStudenType(Integer.parseInt(studentType.getSelectedValue()));
		currentCourse.setPaymentType(Integer.parseInt(paymentType.getSelectedValue()));
		currentCourse.setScholarYear(Config.currentScholarYear);
		currentCourse.getTimePlaces().clear();
		for (int i = 0; i < checks.length; i ++) {
			if (checks[i].getValue() && days[i].getSelectedIndex() > 0) {
				CourseInfo info = new CourseInfo();
				info.setDay(Integer.parseInt(days[i].getSelectedValue()));
				info.setFrom(Integer.parseInt(from[i].getSelectedValue()));
				info.setTo(Integer.parseInt(to[i].getSelectedValue()));
				info.setPlace(places[i].getText().trim());
				currentCourse.getTimePlaces().add(info);
			}
		}
		for (int i = 0; i < examChecks.length; i ++) {
			if (examChecks[i].getValue()) {
				int duration = 0;
				try {
					duration = Integer.parseInt(examDurations[i].getText().trim());
				}
				catch (Exception e) {}
				ExamInfo info = new ExamInfo(examDatePickers[i].getValue(),
						Integer.parseInt(examHours[i].getSelectedValue()),
						Integer.parseInt(examMinutes[i].getSelectedValue()),
						duration, examPlaces[i].getText().trim());
				currentCourse.getExams().add(info);
			}
		}
		if (ClientData.isLoggedIn() && ClientData.getCurrentUser().isAdmin()) {
			currentCourse.setDoneNotification(Integer.parseInt(remindNotification.getSelectedValue()));
		}
		return currentCourse;
	}
	
	public void setupSubjects(List<Subject> subjects) {
		subject.clear();
		subject.addItem("[Chọn môn]", "");
		for (Subject s : subjects) {
			subject.addItem(s.getName() + " - " + s.getSubjectIdCode(), s.getId() + "");
		}
	}
	
	public void setupTeachers(List<Teacher> teachers) {
		teacher.clear();
		teacher.addItem("[Chọn giáo viên]", "");
		for (Teacher s : teachers) {
			teacher.addItem(s.getFullName(), s.getId() + "");
		}
	}
	
	public void onDataLoaded(int dataType) {
		if (dataType == Config.DATA_TEACHER)
			setupTeacherFilters(ClientData.getTeachers());
		if (dataType == Config.DATA_SUBJECT)
			setupSubjectFilters(ClientData.getSubjects());
	}
	
	public void setupTeacherFilters(List<Teacher> teachers) {
		courseListViewer.setupTeacherFilters(teachers);
	}
	
	public void setupSubjectFilters(List<Subject> subjects) {
		courseListViewer.setupSubjectFilters(subjects);
	}
	
	public void edit(Course course) {
		if (!ClientData.isLoggedIn()) {
			EduManager.clientFactory.getBasicView().getLoginView().view();
			return;
		}
		if (course == null)
			course = new Course();
		setupSubjects(ClientData.getSubjects());
		setupTeachers(ClientData.getTeachers());
		currentCourse = course;
		if (dialogBox == null) {
			dialogBox = new DialogBox(true);
			dialogBox.setWidth("700px");
		    dialogBox.setGlassEnabled(true);
		    dialogBox.setAnimationEnabled(false);
		    // Create a table to layout the content
		    VerticalPanel dialogContents = new VerticalPanel();
		    dialogContents.setSpacing(5);
		    dialogContents.setWidth("100%");
		    dialogBox.setWidget(dialogContents);
		    flexTable.setWidth("100%");
		    dialogContents.add(flexTable);
		    dialogContents.setCellHorizontalAlignment(flexTable, HasHorizontalAlignment.ALIGN_CENTER);
		    HTML placeInfo = new HTML("<font size='4'>Thời gian / Địa điểm</font>");
		    dialogContents.add(placeInfo);
		    dialogContents.setCellHorizontalAlignment(placeInfo, HasHorizontalAlignment.ALIGN_CENTER);
		    dialogContents.add(infoFlexTable);
		    dialogContents.setCellHorizontalAlignment(infoFlexTable, HasHorizontalAlignment.ALIGN_CENTER);
		    HTML exam = new HTML("<font size='4'>Lịch thi</font>");
		    dialogContents.add(exam);
		    dialogContents.setCellHorizontalAlignment(exam, HasHorizontalAlignment.ALIGN_CENTER);
		    dialogContents.add(examFlexTable);
		    dialogContents.setCellHorizontalAlignment(examFlexTable, HasHorizontalAlignment.ALIGN_CENTER);
		    
		    HorizontalPanel hPanel = new HorizontalPanel();
		    hPanel.setWidth("100%");
		    dialogContents.add(hPanel);
		    hPanel.add(cancel);
		    hPanel.setCellHorizontalAlignment(cancel, HasHorizontalAlignment.ALIGN_LEFT);
		    hPanel.add(save);
		    hPanel.setCellHorizontalAlignment(save, HasHorizontalAlignment.ALIGN_RIGHT);
		}
	    dialogBox.setText(course != null ? "Sửa thông tin" : "Tạo mới");
	    for (int i = 0; i < days.length; i ++) {
	    	checkTimePlace(i, false);
	    }
	    for (int i = 0; i < examChecks.length; i ++) {
	    	checkExamInfo(i, false);
	    }
		if (course != null) {
			for (int i = 0; i < course.getTimePlaces().size(); i ++) {
				if (i < 2) {
					checkTimePlace(i, true);
					CourseInfo info = course.getTimePlaces().get(i);
					days[i].setSelectedIndex(info.getDay());
					from[i].setSelectedIndex(info.getFrom());
					to[i].setSelectedIndex(info.getTo());
					places[i].setText(info.getPlace());
				}
			}
			for (int i = 0; i < course.getExams().size(); i ++) {
				if (i < 2) {
					checkExamInfo(i, true);
					ExamInfo info = course.getExams().get(i);
					examDateBoxes[i].setValue(info.getDate());
					examHours[i].setSelectedIndex(info.getHour());
					examMinutes[i].setSelectedIndex(info.getMinute());
					examDurations[i].setText(info.getDuration() + "");
					examPlaces[i].setText(info.getPlace());
				}
			}
			for (int i = 0; i < subject.getItemCount(); i ++) {
				if (subject.getValue(i).equalsIgnoreCase(course.getSubjectId() + "")) {
					subject.setSelectedIndex(i);
					Subject s = ClientData.getSubjectMap().get(course.getSubjectId());
					if (s != null)
						subjectIdCode.setText(s.getSubjectIdCode());
					break;
				}
			}
			teacher.setSelectedValue(course.getTeacherId());
			if (course.getSemester() > 0)
				semester.setSelectedIndex(course.getSemester() - 1);
			else
				semester.setSelectedIndex(Config.currentSemester - 1);

			semesterType.setSelectedIndex(course.getSemesterType() - 1);
			studentNum.setText(course.getStudentNum() + "");
			courseType.setSelectedIndex(course.getCourseType());
			paymentType.setSelectedIndex(course.getPaymentType());
			updateStudentType(paymentType.getSelectedIndex());
			credits.setSelectedIndex(course.getCredits() - 1);
			creditInfo.setText(course.getCreditInfo());
			courseIdCode.setText(course.getCourseIdCode());
			if (!course.getClassType().isEmpty())
				classType.setText(course.getClassType());
			if (course.getStartDate() != null)
				fromDateBox.setValue(course.getStartDate());
			if (course.getEndDate() != null)
				toDateBox.setValue(course.getEndDate());
			status.setSelectedValue(course.getStatus());
			notification.setSelectedValue(course.getNotification());
			studentType.setSelectedValue(course.getStudenType());
			if (ClientData.isLoggedIn() && ClientData.getCurrentUser().isAdmin()) {
				remindNotification.setSelectedValue(course.getDoneNotification());
			}
		}
	    dialogBox.center();
	}
	
	private void checkTimePlace(int index, boolean val) {
    	checks[index].setValue(val);
    	days[index].setEnabled(val);
		from[index].setEnabled(val);
		to[index].setEnabled(val);
		places[index].setEnabled(val);
	}
	
	private void checkExamInfo(int index, boolean val) {
    	examChecks[index].setValue(val);
    	examDateBoxes[index].setEnabled(val);
		examHours[index].setEnabled(val);
		examMinutes[index].setEnabled(val);
		examDurations[index].setEnabled(val);
		examPlaces[index].setEnabled(val);
	}
}
