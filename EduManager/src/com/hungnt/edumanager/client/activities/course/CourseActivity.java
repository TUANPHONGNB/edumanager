package com.hungnt.edumanager.client.activities.course;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.BarChart;
import com.googlecode.gwt.charts.client.corechart.BarChartOptions;
import com.googlecode.gwt.charts.client.options.HAxis;
import com.googlecode.gwt.charts.client.options.Interval;
import com.googlecode.gwt.charts.client.options.VAxis;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.RPCCall;
import com.hungnt.edumanager.client.activities.ClientFactory;
import com.hungnt.edumanager.client.activities.basic.BasicActivity;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Statistics;
import com.hungnt.edumanager.shared.Teacher;

public class CourseActivity extends BasicActivity {
	
	private CourseView view;
	
	public CourseActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getCourseView();
		panel.setWidget(view.asWidget());
		super.start(panel, eventBus, view);
		if (ClientData.getCourses().size() > 0) {
			view.view(ClientData.getCourses());
		}
	}
	
	@Override
	protected void bind() {
		super.bind();
		view.getAddMoreButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (!ClientData.isLoggedIn()) {
					view.getLoginView().view();
					return;
				}
				if (ClientData.canEdit(Config.NULL_ID, true))
					view.edit(null);
			}
		});
		view.getCourseListViewer().getStats().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				view.getCourseViewPanel().clear();
				view.getCourseViewPanel().add(view.getStatViewer());
				view.getStatViewer().view(ClientData.getTeachers());
			}
		});
		
		view.getStatViewer().getChartButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				view.getStatViewer().showChart();
			}
		});
		
		view.getCourseListViewer().getUpdateStatsButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				new RPCCall<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Failed to update Statistics");
					}

					@Override
					public void onSuccess(Void result) {
						Window.alert("Done updating Statistics");
					}

					@Override
					protected void callService(AsyncCallback<Void> cb) {
						EduManager.dataService.updateStatistics(ClientData.getCurrentUser().getId(), 
								view.getCourseListViewer().getScholarYear(), view.getCourseListViewer().getSemester(), cb);
					}
				}.retry();
			}
		});
	}
	
	@Override
	protected void onDataLoaded(int dataType) {
		if (dataType == Config.DATA_COURSE)
			view.view(ClientData.getCourses());
	}
 }
