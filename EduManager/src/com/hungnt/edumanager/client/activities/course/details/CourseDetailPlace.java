package com.hungnt.edumanager.client.activities.course.details;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Course;

public class CourseDetailPlace extends Place {
	
	private Long courseId = Config.NULL_ID;
	private Course course = null;
	
	public CourseDetailPlace() {
		super();
	}

	public CourseDetailPlace(long courseId) {
		super();
		this.courseId = courseId;
	}

	public CourseDetailPlace(Course course) {
		super();
		this.course = course;
	}
	
	public String getToken() {
		return "coursedetail";
	}
	
	public Long getCourseId() {
		return this.courseId;
	}
	
	public Course getCourse() {
		return this.course;
	}
	
	public static class Tokenizer implements PlaceTokenizer<CourseDetailPlace> {
        @Override
        public String getToken(CourseDetailPlace place) {
            return place.getToken();
        }

        @Override
        public CourseDetailPlace getPlace(String token) {
            return new CourseDetailPlace();
        }
    }
}
