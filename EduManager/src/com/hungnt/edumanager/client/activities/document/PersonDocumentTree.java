package com.hungnt.edumanager.client.activities.document;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;
import com.hungnt.edumanager.client.view.ContextMenu;
import com.hungnt.edumanager.shared.Category;
import com.hungnt.edumanager.shared.Material;

/**
 * 	@author zoro
 *	@email nguyenhongquanbkit@gmail.com
 * 
 */
public class PersonDocumentTree extends DocumentTree {
	
	private ContextMenu contextMenu = new ContextMenu();
	private AsyncCallback<Void> rightClickCallBack = null;
	
	public PersonDocumentTree() {
		super();
	}
	
	@Override
	public void onRightClickItem(ContextMenuEvent event, TreeItem treeItem, Widget target) {
		super.onRightClickItem(event, treeItem, target);
		contextMenu.open(treeItem, event.getNativeEvent().getClientX(),  
				event.getNativeEvent().getClientY(), target);
	}
	
	@Override
	public void onClickItem(ClickEvent event, TreeItem treeItem) {
		super.onClickItem(event, treeItem);
		if (rightClickCallBack != null) {
			rightClickCallBack.onSuccess(null);
		}
	}
	
	public void setRightClickCallBack(AsyncCallback<Void> callBack) {
		rightClickCallBack = callBack;
	}
	
	public ContextMenu getContextMenu() {
		return contextMenu;
	}

	public Object getCurrentObject(){
		Object obj = ((TreeInforItem)currentTreeItem.getUserObject()).getObject();
		return obj;
	}

	
	public Category getCurrentCategory(){
		Object obj = ((TreeInforItem)currentTreeItem.getUserObject()).getObject();
		if (obj instanceof Category)
			return (Category)obj;
		return null;
	}
	
	public int getTypeFolder(){
		return ((TreeInforItem)currentTreeItem.getUserObject()).getType();
	}
	
	public Material getCuMaterial(){
		Object obj = ((TreeInforItem)currentTreeItem.getUserObject()).getObject();
		if (obj instanceof Material)
			return (Material)obj;
		return null;
	}
}
