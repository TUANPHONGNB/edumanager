package com.hungnt.edumanager.client.activities.document;

import com.hungnt.edumanager.shared.Config;

/**
 * 	@author zoro
 *	@email nguyenhongquanbkit@gmail.com
 * 
 */
public class TreeInforItem {
	
	private long id = Config.NULL_ID;
	private String name = Config.NULL_TXT;
	private long idCurrentParent = Config.NULL_ID;
	private int type = 0;//0 is Category,1 is material
	private Object object = null;
	
	public TreeInforItem(Object obj, long id, String name, long idCurrentParent,int type) {
		this.id = id;
		this.name = name;
		this.idCurrentParent = idCurrentParent;
		this.type = type;
		this.object = obj;
	}
	
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public long getIdCurrentParent() {
		return idCurrentParent;
	}
	public int getType() {
		return type;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
}
