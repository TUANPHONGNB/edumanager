package com.hungnt.edumanager.client.activities.document;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.hungnt.edumanager.client.activities.basic.BasicPlace;

public class DocumentPlace extends BasicPlace {
	
	public DocumentPlace() {
		super();
	}
	
	@Override
	public String getToken() {
		return "material";
	}
	
	public static class Tokenizer implements PlaceTokenizer<DocumentPlace> {
        @Override
        public String getToken(DocumentPlace place) {
            return place.getToken();
        }

        @Override
        public DocumentPlace getPlace(String token) {
            return new DocumentPlace();
        }
    }
	
}
