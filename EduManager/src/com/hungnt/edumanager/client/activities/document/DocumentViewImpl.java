package com.hungnt.edumanager.client.activities.document;

import com.hungnt.edumanager.client.CssToken;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;
import com.hungnt.edumanager.client.activities.basic.DocumentTreeView;

public class DocumentViewImpl extends BasicViewImpl implements DocumentView {
		
	public DocumentViewImpl() {	
		super();
	}
	
	@Override
	public void refreshView() {
		super.refreshView();
		material.setStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE, true);
		contentPanel.clear();
		if (documentView == null)
			documentView = new DocumentTreeView();
		contentPanel.add(documentView);
	}
}