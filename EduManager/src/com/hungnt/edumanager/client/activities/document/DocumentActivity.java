package com.hungnt.edumanager.client.activities.document;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.activities.ClientFactory;
import com.hungnt.edumanager.client.activities.basic.BasicActivity;

public class DocumentActivity extends BasicActivity {
	
	private DocumentView view;
	
	public DocumentActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getMaterialView();
		panel.setWidget(view.asWidget());
		if (root == null) {
			super.start(panel, eventBus, view);
			Long viewerId = null;
			if (ClientData.isLoggedIn())
				viewerId = ClientData.getCurrentUser().getId();
			loadCategoriesData(rootId, viewerId, viewerId, view);
		}
	}
 }