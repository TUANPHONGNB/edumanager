package com.hungnt.edumanager.client.activities.document;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasTreeItems;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.ClientUtils;
import com.hungnt.edumanager.shared.Category;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Material;

public class DocumentTree extends VerticalPanel{
	
	protected Tree tree;
	protected TreeItem currentTreeItem;
	protected Button currentTitle;
	protected Map<Long, TreeItem> mapItem = new HashMap<Long, TreeItem>();
	protected PopupPanel toolTip = null;
	
	public DocumentTree() {
		tree = new Tree();
		this.add(tree);
	}
	
	public void genTreeView(Category root) {
		tree.clear();
		mapItem.clear();
		if(root!=null){
			addItems(root,tree);
		}
	}
	
	private void addItems(Category parent,HasTreeItems treeItem){
		for (Category child : parent.getChildren()) {
			ClientUtils.log("Cat name: " + child.getName() + " Privacy: " + child.getPrivacy());
			if (child.getPrivacy() == 0
					|| (ClientData.isLoggedIn() 
							&& (child.getPrivacy() == 1 || (child.getPrivacy() == 2 
							&& ClientData.getCurrentUser().getId().compareTo(child.getUserId()) == 0)))) {
				TreeInforItem inforItem = new TreeInforItem(child, child.getId(), child.getName(),parent.getId(),Config.CATEGORY);
				addItem(inforItem, treeItem);
			}
		}
		for (Material child : parent.getMaterials()) {
			ClientUtils.log("Mat name: " + child.getName() + " Privacy: " + child.getPrivacy());
			if (child.getPrivacy() == 0
					|| (ClientData.isLoggedIn() 
							&& (child.getPrivacy() == 1 || (child.getPrivacy() == 2 
							&& ClientData.getCurrentUser().getId().compareTo(child.getUserId()) == 0)))) {
				TreeInforItem inforItem = new TreeInforItem(child, child.getId(), child.getName(),parent.getId(),Config.MATERIAL);
				addItem(inforItem, treeItem);
			}
		}
	}
	
	public void onRightClickItem(ContextMenuEvent event,TreeItem treeItem, Widget target){
		event.preventDefault();
		event.stopPropagation();
	}
	
	public void onClickItem(ClickEvent event,TreeItem treeItem){
		if (event.getNativeButton() == NativeEvent.BUTTON_LEFT) {
			treeItem.setState(!treeItem.getState());
		}
	}
	
	public void addItem(final TreeInforItem treeInforItem,final HasTreeItems treeItem) {
		String name = treeInforItem.getName();
		int privacy = 0;
		if (treeInforItem.getObject() != null) {
			if (treeInforItem.getObject() instanceof Material) {
				privacy = ((Material)treeInforItem.getObject()).getPrivacy();
				name += " (" + ClientUtils.getFileSizeInKb(((Material)treeInforItem.getObject()).getFileSize()) + "Kb)";
			}
			else if (treeInforItem.getObject() instanceof Category) {
				privacy = ((Category)treeInforItem.getObject()).getPrivacy();
			}
		}
		final Button title = new Button(name);
		title.setType(ButtonType.LINK);
		if(treeInforItem.getType()==Config.CATEGORY){
			title.setIcon(privacy == 0 ? IconType.FOLDER_CLOSE_ALT : privacy == 1 ? IconType.FOLDER_CLOSE : IconType.LOCK);
		}else if(treeInforItem.getType()==Config.MATERIAL){
			title.setIcon(privacy == 0 ? IconType.FILE_ALT : privacy == 0 ? IconType.FILE : IconType.LOCK);
		}
		title.addStyleName("folder-treeitem");
		title.addMouseOverHandler(new MouseOverHandler() {
			
			@Override
			public void onMouseOver(MouseOverEvent event) {
				showToolTip(treeInforItem.getObject(), event.getClientX(), event.getClientY());
			}
		});
//		title.addMouseMoveHandler(new MouseMoveHandler() {
//
//			@Override
//			public void onMouseMove(MouseMoveEvent event) {
//				//view.getToolTip().show(f, event.getClientX(),event.getClientY());
//				showToolTip(treeInforItem.getObject());
//			}
//		});
		title.addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				//view.getToolTip().hide();
				hideToolTip();
			}
		});
		final TreeItem child = treeItem.addItem(title);
		mapItem.put(treeInforItem.getId(), child);
		child.setUserObject(treeInforItem);
		
		child.getWidget().addDomHandler(new ContextMenuHandler() {
			@Override
			public void onContextMenu(ContextMenuEvent event) {
				currentTreeItem = child;
				currentTitle = title;
				onRightClickItem(event,child, child.getWidget());
			}
		}, ContextMenuEvent.getType());

		child.getWidget().addDomHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				currentTreeItem = child;
				currentTitle = title;
				if (treeInforItem.getObject() instanceof Material)
					Window.open(((Material)treeInforItem.getObject()).getUrl(), "", "");
				onClickItem(event, child);
			}
		},ClickEvent.getType());

		// reopen after added
		if (treeItem instanceof TreeItem) {
			((TreeItem) treeItem).setState(false, false);
		}
	}
	
	private void showToolTip(Object obj, int left, int top) {
		if (obj instanceof Material) {
			Material mat = (Material) obj;
			if (toolTip == null)
				toolTip = new PopupPanel();
			toolTip.setPopupPosition(left, top);
			toolTip.clear();
			FlexTable fTable = new FlexTable();
			toolTip.setWidget(fTable);
			int row = 0, col = 0;
			fTable.setText(row, col++, "Tên file");
			fTable.setText(row, col++, ":");
			fTable.setWidget(row, col++, new HTML("<b>" + mat.getName() + "</b>"));
			row ++; col = 0;
			fTable.setText(row, col++, "Mô tả");
			fTable.setText(row, col++, ":");
			fTable.setWidget(row, col++, new HTML("<b>" + mat.getDescription() + "</b>"));
			row ++; col = 0;
			fTable.setText(row, col++, "Định dạng");
			fTable.setText(row, col++, ":");
			fTable.setWidget(row, col++, new HTML("<b>" + mat.getFileType() + "</b>"));
			row ++; col = 0;
			fTable.setText(row, col++, "Kích thước");
			fTable.setText(row, col++, ":");
			fTable.setWidget(row, col++, new HTML("<b>" 
					+ ClientUtils.getFileSizeInKb(mat.getFileSize()) + " Kb</b>"));
			row ++; col = 0;
			fTable.setText(row, col++, "Quyền truy cập");
			fTable.setText(row, col++, ":");
			fTable.setWidget(row, col++, new HTML("<b>" 
					+ Config.getPrivacyValue(mat.getPrivacy()) + "</b>"));
			toolTip.show();
		}
		else if (obj instanceof Category) {
//			Category mat = (Category) obj;
//			if (toolTip == null)
//				toolTip = new PopupPanel();
//			toolTip.setPopupPosition(left, top);
//			toolTip.clear();
//			VerticalPanel vPanel = new VerticalPanel();
//			toolTip.setWidget(vPanel);
//			vPanel.setSpacing(3);
//			vPanel.add(new HTML(mat.getName()));
//			vPanel.add(new HTML("Hello there"));
//			toolTip.show();
		}
	}
	
	private void hideToolTip() {
		if (toolTip != null)
			toolTip.hide();
	}
	
	public void setTitleCurrentItem(String title){
		currentTitle.setText(title);
	}
	
	public Tree getTree() {
		return tree;
	}
	
	public TreeItem getItem(Long id){
		return mapItem.get(id);
	}
	
	public void addItem(TreeInforItem child,Long idParent){
		TreeItem parentItem = getItem(idParent);
		addItem(child, parentItem);
	}
	
	public void addItems(List<TreeInforItem> treeInforItems,Long idParent){
		TreeItem parentItem = getItem(idParent);
		for (TreeInforItem treeInforItem : treeInforItems) {
			addItem(treeInforItem, parentItem);
		}
	}
	
	public void moveItem(Long idResource,Long idNewParent,Long idOldParent){
		TreeItem itemResource = getItem(idResource);
		TreeItem itemNewParent= getItem(idNewParent);
		TreeItem itemOldParent = getItem(idOldParent);
		itemNewParent.addItem(itemResource);
		itemOldParent.removeItem(itemResource);
	}
	
	public void removeItem(Long id){
		TreeItem item = getItem(id);
		item.remove();
		mapItem.remove(id);
	}
	
	public void removeCurrentItem(){
		currentTreeItem.remove();
	}
	
	public TreeItem getCurrentTreeItem() {
		return currentTreeItem;
	}
	public Map<Long, TreeItem> getMapItem() {
		return mapItem;
	}
}
