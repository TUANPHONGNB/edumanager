package com.hungnt.edumanager.client.activities.publication;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.hungnt.edumanager.client.activities.basic.BasicView;
import com.hungnt.edumanager.shared.Publication;

public interface PublicationView extends BasicView{
	HasClickHandlers getAddMoreButton();
	void view(List<Publication> publications);
}
