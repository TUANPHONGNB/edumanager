package com.hungnt.edumanager.client.activities.publication;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.widget.TeacherSelector;
import com.hungnt.edumanager.shared.Publication;
import com.hungnt.edumanager.shared.Teacher;

public class PublicationEditor {
	private VerticalPanel publicationViewPanel = new VerticalPanel();
	private Button save = new Button("Lưu");
	private Button cancel = new Button("Huỷ");
	private Button addMore = new Button("Thêm");
	private FlexTable flexTable = new FlexTable();
	private TextBox paperName = new TextBox();
	private TextBox authors = new TextBox();
	private TextBox bookName = new TextBox();
	private TextBox issn = new TextBox();
	private TextBox publishName = new TextBox();
	private TextBox publishPlace = new TextBox();
	private TextBox volume = new TextBox();
	private TextBox pages = new TextBox();
	private TextArea info = new TextArea();
	private ListBox month = new ListBox();
	private ListBox paperType = new ListBox();
	private ListBox year = new ListBox();
	private TeacherSelector authorList = new TeacherSelector();
	private DialogBox dialogBox = null;
	private Publication currentPublication = null;
	private FormPanel uploadForm;
	private FileUpload uploadField;
	private Button uploadButton;
	private HorizontalPanel uploadPanel = new HorizontalPanel();
	
	public PublicationEditor() {
		publicationViewPanel.setSpacing(5);
		publicationViewPanel.setWidth("100%");
		
		uploadForm = new FormPanel();
		uploadField = new FileUpload();
		uploadButton = new Button("Tải ảnh");
		uploadForm.setWidget(uploadField);
		//frame.setSize("400px","600px");
		uploadField.setName("material");
		uploadPanel.add(uploadField);
		//uploadPanel.add(uploadButton);
		uploadForm.add(uploadPanel);
		uploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
		uploadForm.setMethod(FormPanel.METHOD_POST);
		
		for (int i = 1; i <= 12; i++) {
			month.addItem(i + "", i + "");
		}
		for (int i = 2010; i < 2016; i ++)
			year.addItem(i + "", i + "");
				
		//0: other, 1: int. conf, 2: int. journal, 3: int. book, 4: national conf. 5: national journal, 6: national book
		paperType.addItem("[Chọn]", "0");
		paperType.addItem("Hội nghị quốc tế", "1");
		paperType.addItem("Tạp chí quốc tế", "2");
		paperType.addItem("Hội nghị quốc gia", "3");
		paperType.addItem("Tạp chí quốc gia", "4");
		paperType.addItem("Sách", "5");
		paperType.addItem("Báo cáo khoa học", "6");
		authors.setWidth("95%");
		paperName.setWidth("95%");
		bookName.setWidth("95%");
		info.setWidth("95%");
		volume.setWidth("200px");
		issn.setWidth("150px");
		month.setWidth("100px");
		year.setWidth("100px");
		publishName.setWidth("200px");
		publishPlace.setWidth("200px");
		pages.setWidth("200px");
		flexTable.setWidth("100%");
		int row = 0, col = 0;
		flexTable.setText(row, col++, "Tên bài báo");
		flexTable.setWidget(row, col, paperName);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		flexTable.setText(row, col++, "Tác giả");
		flexTable.setWidget(row, col, authors);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		flexTable.setText(row, col++, "Liên quan");
		flexTable.setWidget(row, col, authorList);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		flexTable.setText(row, col++, "Tên hội nghị");
		flexTable.setWidget(row, col, bookName);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.add(issn);
		hPanel.add(new HTML("&nbsp;&nbsp; Tháng"));
		hPanel.add(month);
		hPanel.add(new HTML("&nbsp;&nbsp; Năm"));
		hPanel.add(year);
		flexTable.setText(row, col++, "Loại xuất bản");
		flexTable.setWidget(row, col, paperType);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		flexTable.setText(row, col++, "ISSN");
		flexTable.setWidget(row, col, hPanel);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		flexTable.setText(row, col++, "Tên xuất bản");
		flexTable.setWidget(row, col++, publishName);
		flexTable.setText(row, col++, "Địa điểm");
		flexTable.setWidget(row, col++, publishPlace);
		row ++; col = 0;
		flexTable.setText(row, col++, "Số trang");
		flexTable.setWidget(row, col++, pages);
		flexTable.setText(row, col++, "Volume");
		flexTable.setWidget(row, col++, volume);
		row ++; col = 0;
		flexTable.setText(row, col++, "Thông tin");
		flexTable.setWidget(row, col, info);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
				
		flexTable.setText(row, col++, "File đính kèm");
		flexTable.setWidget(row, col, uploadForm);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		
		flexTable.getCellFormatter().setWidth(row, 0, "100px");
//		flexTable.getCellFormatter().setWidth(row, 1, "220px");
//		flexTable.getCellFormatter().setWidth(row, 2, "100px");
//		flexTable.getCellFormatter().setWidth(row, 3, "220px");
		
		flexTable.setWidget(row, 0, cancel);
		flexTable.setWidget(row, 3, save);
		flexTable.getCellFormatter().setHeight(row, 0, "100px");
		flexTable.getCellFormatter().setHorizontalAlignment(row, 3, HasHorizontalAlignment.ALIGN_RIGHT);
		
//		authorList.addChangeHandler(new ChangeHandler() {
//			@Override
//			public void onChange(ChangeEvent event) {
//				if (authorList.getSelectedValue().isEmpty())
//					return;
//				Long id = Long.parseLong(authorList.getSelectedValue());
//				Teacher author = ClientData.getTeacherMap().get(id);
//				if (author != null) {
//					String txt = authors.getText().trim();
//					if (!txt.isEmpty())
//						txt += ", ";
//					txt += author.getFullName();
//					authors.setText(txt);
//					if (currentPublication != null && !currentPublication.getAuthorIds().contains(id)) {
//						currentPublication.getAuthorIds().add(id);
//					}
//				}
//			}
//		});
	}
	
	public Publication getCurrentPublication() {
		return this.currentPublication;
	}
	
	public HasClickHandlers getSaveButton() {
		return this.save;
	}
	
	public HasClickHandlers getCancelButton() {
		return this.cancel;
	}
	
	public HasClickHandlers getAddMoreButton() {
		return this.addMore;
	}
	
	public FormPanel getUploadForm() {
		return uploadForm;
	}

	public FileUpload getUploadField() {
		return uploadField;
	}

	public Button getUploadButton() {
		return uploadButton;
	}

	public Publication getPublication() {
		if (paperName.getText().trim().isEmpty() || authors.getText().trim().isEmpty()) {
			Window.alert("Tên bài báo và tác giả không được để trống");
			return null;
		}
		if (currentPublication == null)
			currentPublication = new Publication();
		currentPublication.setBookName(bookName.getText().trim());
		currentPublication.setTitle(paperName.getText().trim());
		currentPublication.setAuthors(authors.getText().trim());
		currentPublication.setInfo(info.getText().trim());
		currentPublication.setIssn(issn.getText().trim());
		currentPublication.setMonth(Integer.parseInt(month.getSelectedValue()));
		currentPublication.setYear(Integer.parseInt(year.getSelectedValue()));
		currentPublication.setPageRange(pages.getText().trim());
		currentPublication.setPaperType(Integer.parseInt(paperType.getSelectedValue()));
		currentPublication.setPublishName(publishName.getText().trim());
		currentPublication.setPublishPlace(publishPlace.getText().trim());
		currentPublication.setVolume(volume.getText().trim());
		currentPublication.setUserId(ClientData.getCurrentUser().getId());
		currentPublication.setAuthorIds(authorList.getSelectedIds());
		return currentPublication;
	}
	
	public void setupAuthors(List<Teacher> teachers) {
		authorList.setupAuthors(teachers);
//		authorList.clear();
//		authorList.addItem("[Chọn tác giả]", "");
//		for (Teacher s : teachers) {
//			authorList.addItem(s.getFullName(), s.getId() + "");
//		}
	}
	
	public DialogBox getDialogBox() {
		return this.dialogBox;
	}
	
	public void edit(Publication publication) {
		
		authorList.setupAuthors(ClientData.getTeachers());
		
		if (!ClientData.isLoggedIn()) {
			EduManager.clientFactory.getBasicView().getLoginView().view();
			return;
		}
		if (publication == null)
			publication = new Publication();
		currentPublication = publication;
		if (dialogBox == null) {
			dialogBox = new DialogBox(true);
			dialogBox.setWidth("640px");
		    dialogBox.setGlassEnabled(true);
		    dialogBox.setAnimationEnabled(false);
		    // Create a table to layout the content
		    VerticalPanel dialogContents = new VerticalPanel();
		    dialogContents.setWidth("100%");
		    dialogBox.setWidget(dialogContents);
		    flexTable.setWidth("100%");
		    dialogContents.add(flexTable);
		    dialogContents.setCellHorizontalAlignment(flexTable, HasHorizontalAlignment.ALIGN_RIGHT);
		}
	    dialogBox.setText((publication.getId() != null && publication.getId() > 0) ? "Sửa thông tin" : "Tạo mới");
		if (publication != null) {
			bookName.setText(publication.getBookName());
			paperName.setText(publication.getTitle());
			authors.setText(publication.getAuthors());
			info.setText(publication.getInfo());
			issn.setText(publication.getIssn());
			month.setSelectedIndex(publication.getMonth());
			year.setSelectedIndex(publication.getYear());
			pages.setText(publication.getPageRange());
			paperType.setSelectedIndex(publication.getPaperType());
			publishName.setText(publication.getPublishName());
			publishPlace.setText(publication.getPublishPlace());
			currentPublication.setVolume(publication.getVolume());
			authorList.setSelectedIds(publication.getAuthorIds());
		}
	    dialogBox.center();
        dialogBox.show();
	}
}
