package com.hungnt.edumanager.client.activities.publication;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.hungnt.edumanager.client.activities.basic.BasicPlace;

public class PublicationPlace extends BasicPlace {
	
	public PublicationPlace() {
		super();
	}
	
	@Override
	public String getToken() {
		return "publication";
	}
	
	public static class Tokenizer implements PlaceTokenizer<PublicationPlace> {
        @Override
        public String getToken(PublicationPlace place) {
            return place.getToken();
        }

        @Override
        public PublicationPlace getPlace(String token) {
            return new PublicationPlace();
        }
    }
	
}
