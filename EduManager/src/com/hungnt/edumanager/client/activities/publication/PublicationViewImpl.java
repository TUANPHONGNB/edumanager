package com.hungnt.edumanager.client.activities.publication;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.client.CssToken;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;
import com.hungnt.edumanager.shared.Publication;

public class PublicationViewImpl extends BasicViewImpl implements PublicationView {
	
	private VerticalPanel publicationViewPanel = new VerticalPanel();
	private Button addMore = new Button("Thêm");
	PublicationListViewer listViewer = new PublicationListViewer();

	public PublicationViewImpl() {	
		super();
		publicationViewPanel.setSpacing(5);
		publicationViewPanel.setWidth("100%");
		contentPanel.add(publicationViewPanel);
	}
	
	@Override
	public HasClickHandlers getAddMoreButton() {
		return this.addMore;
	}
		
	@Override
	public void refreshView() {
		super.refreshView();
		publication.setStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE, true);
	}
	
    @Override
	public  void view(List<Publication> publications) {
    	publicationViewPanel.clear();
    	publicationViewPanel.add(listViewer);
    	listViewer.getFunctionPanel().add(addMore);
    	listViewer.getFunctionPanel().setCellHorizontalAlignment(addMore, ALIGN_RIGHT);
    	listViewer.view(publications, false);
	}
}
