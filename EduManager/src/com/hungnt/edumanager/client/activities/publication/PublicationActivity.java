package com.hungnt.edumanager.client.activities.publication;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.RPCCall;
import com.hungnt.edumanager.client.activities.ClientFactory;
import com.hungnt.edumanager.client.activities.basic.BasicActivity;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Publication;

public class PublicationActivity extends BasicActivity {
	
	private PublicationView view;
	
	public PublicationActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getPublicationView();
		panel.setWidget(view.asWidget());
		super.start(panel, eventBus, view);
		if (ClientData.getPublications().size() > 0)
			view.view(ClientData.getPublications());
		else
			loadData();
	}
	
	@Override
	protected void bind() {
		super.bind();
		view.getAddMoreButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (!ClientData.isLoggedIn()) {
					view.getLoginView().view();
					return;
				}
				view.getPubEditor().edit(null);
			}
		});
	}
	
	private void loadData() {
		new RPCCall<List<Publication>>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Có lỗi xảy ra, không thể tải dữ liệu");
			}

			@Override
			public void onSuccess(List<Publication> result) {
				ClientData.setPublications(result);
				view.view(result);
			}

			@Override
			protected void callService(AsyncCallback<List<Publication>> cb) {
				EduManager.dataService.getPublications(null, cb);
				
			}}.retry();
	}
	
	@Override
	protected void onDataLoaded(int dataType) {
		if (dataType == Config.DATA_PUBLICATION)
			view.view(ClientData.getPublications());
	}
 }
