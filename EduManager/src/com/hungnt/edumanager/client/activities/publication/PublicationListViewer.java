package com.hungnt.edumanager.client.activities.publication;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.AbstractEditableCell;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.event.EditEvent;
import com.hungnt.edumanager.client.widget.HListBox;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Material;
import com.hungnt.edumanager.shared.Publication;
import com.hungnt.edumanager.shared.Teacher;

public class PublicationListViewer extends VerticalPanel{
	private HListBox teacherFilter = new HListBox();
	private HListBox paperTypeFilter = new HListBox();
	private HListBox scholarYearFilter = new HListBox();
	
	private Long fixedTeacherId = -1L;
	private Long currentSelectedTeacherId = -1L;
	private int currentSelectedPaperTypeId = 0;
	private int currentSelectedYear = 0;
	private HorizontalPanel topFunctionPanel = new HorizontalPanel();

	DataGrid<Publication> dataGrid;// = new DataGrid<Publication>();
	
	public PublicationListViewer() {
		this.setWidth("100%");
		teacherFilter.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedTeacherId = Long.parseLong(teacherFilter.getSelectedValue());
				view(ClientData.getPublications(), false);
			}
		});
		paperTypeFilter.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedPaperTypeId = Integer.parseInt(paperTypeFilter.getSelectedValue());
				view(ClientData.getPublications(), false);
			}
		});
		scholarYearFilter.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedYear = Integer.parseInt(scholarYearFilter.getSelectedValue());
				view(ClientData.getPublications(), false);
			}
		});
		teacherFilter.setWidth("200px");
		paperTypeFilter.setWidth("150px");
		scholarYearFilter.setWidth("150px");
		
		topFunctionPanel.setWidth("100%");
		topFunctionPanel.setSpacing(5);
		HTML filterBy = new HTML("Lọc theo");
		topFunctionPanel.add(filterBy);
		topFunctionPanel.add(teacherFilter);
		topFunctionPanel.add(paperTypeFilter);
		topFunctionPanel.add(scholarYearFilter);
		topFunctionPanel.setCellHorizontalAlignment(teacherFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(paperTypeFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(scholarYearFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellWidth(filterBy, "70px");
		topFunctionPanel.setCellWidth(teacherFilter, "100px");
		topFunctionPanel.setCellWidth(paperTypeFilter, "100px");
		topFunctionPanel.setCellWidth(scholarYearFilter, "100px");
		paperTypeFilter.addItem("[Tất cả bài báo]", "0");
		for (int i = 0; i <= 6; i ++) {
			String paperType = Config.getPaperType(i);
			if (paperType != null && !paperType.isEmpty()) {
				paperTypeFilter.addItem(paperType, i + "");
			}
		}
		
		scholarYearFilter.addItem("[Tất cả các năm]", "0");
		for (int i = 2010; i < 2016; i ++)
			scholarYearFilter.addItem(i + "", i + "");
	}
	
	public HorizontalPanel getFunctionPanel() {
		return this.topFunctionPanel;
	}
	
	/**
	   * The provider that holds the list of contacts in the database.
	   */
	  private ListDataProvider<Publication> dataProvider = new ListDataProvider<Publication>();
	
	/**
   * The key provider that provides the unique ID of a contact.
   */
  public static final ProvidesKey<Publication> KEY_PROVIDER = new ProvidesKey<Publication>() {
    @Override
    public Object getKey(Publication item) {
      return item == null ? null : item.getId();
    }
  };
	
  
	public void setupTeacherFilters(List<Teacher> teachers) {
		teacherFilter.clear();
		if (fixedTeacherId <= 0) {
			teacherFilter.addItem("[Tất cả giáo viên]", "0");
		}
		for (Teacher s : teachers) {
			if (fixedTeacherId > 0) {
				if (s.getId().compareTo(fixedTeacherId) == 0) {
					teacherFilter.addItem(s.getFullName(), s.getId() + "");
					break;
				}
			}
			else {
				teacherFilter.addItem(s.getFullName(), s.getId() + "");
			}
		}
	}
	public  void view(List<Publication> fullPublications, boolean showAll) {
	   	if (teacherFilter.getItemCount() < 2)
    		setupTeacherFilters(ClientData.getTeachers());
		
    	if (currentSelectedTeacherId < 0 && ClientData.isLoggedIn())
    		currentSelectedTeacherId = ClientData.getCurrentUser().getId();
    	
    	if (fixedTeacherId > 0)
    		currentSelectedTeacherId = fixedTeacherId;
    	
		List<Publication> publications = new ArrayList<Publication>(fullPublications);
		if (!showAll) {
			if (currentSelectedTeacherId > 0 || currentSelectedPaperTypeId > 0
					|| currentSelectedYear > 0) {
				List<Publication> removed = new ArrayList<Publication>();
				for (Publication pub : publications) {
					if ((currentSelectedTeacherId > 0 && !pub.getAuthorIds().contains(currentSelectedTeacherId))
							|| (currentSelectedPaperTypeId > 0 && pub.getPaperType().compareTo(currentSelectedPaperTypeId) != 0)
							|| (currentSelectedYear > 0 && pub.getYear() != currentSelectedYear)) {
						removed.add(pub);
					}
				}
				publications.removeAll(removed);
				teacherFilter.setSelectedValue(currentSelectedTeacherId);
				paperTypeFilter.setSelectedValue(currentSelectedPaperTypeId);
				scholarYearFilter.setSelectedValue(currentSelectedYear);
			}
		}
		
		for (int i = 0; i < publications.size(); i++) {
			publications.get(i).setPosIndex(i + 1);
		}
		
		dataProvider.setList(publications);
	    editableCells = new ArrayList<AbstractEditableCell<?, ?>>();
	    dataGrid = new DataGrid<Publication>(200, KEY_PROVIDER);
	    
	    dataGrid.setWidth("100%");
	    dataGrid.setHeight("600px");
	    
	    //PublicationList.setMinimumTableWidth(140, Unit.EM);
	    dataProvider.addDataDisplay(dataGrid);
	    
	    this.clear();
		this.add(topFunctionPanel);
		this.add(dataGrid);
	    
//	    publicationViewPanel.clear();
//	    publicationViewPanel.add(addMore);
//	    publicationViewPanel.add(dataGrid);
//	    publicationViewPanel.setCellHorizontalAlignment(addMore, ALIGN_CENTER);
	    
	    int diff = ClientData.isLoggedIn() ? 0 : 30;
	    
	    // NumberCell.
	    Column<Publication, Number> numberColumn =
	        addColumn(new NumberCell(), "#", new GetValue<Number>() {
	          @Override
	          public Number getValue(Publication contact) {
	            return contact.getPosIndex();
	          }
	        }, null);
	    dataGrid.setColumnWidth(numberColumn, 30, Unit.PX);
	    numberColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LOCALE_END);
	    
	    
	    // SafeHtmlCell.
	    Column<Publication, SafeHtml> infoCol = addColumn(new SafeHtmlCell(), "Thông tin", new GetValue<SafeHtml>() {
	      @Override
	      public SafeHtml getValue(Publication object) {
	    	  String info = "";
	    	  info += "<font color='blue'>" + object.getTitle() + "</font><br>";
	    	  info += "<i>" + object.getAuthors() + "</i><br>";
	    	  info += object.getPublishInfo();
	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
	    	  builder.appendHtmlConstant(info);
	    	  return builder.toSafeHtml();
	      }
	    }, null);
	    dataGrid.setColumnWidth(infoCol, 700, Unit.PX);
	    
//	    // ClickableTextCell.
//	    Column<Publication, String> nameCol = addColumn(new ClickableTextCell(), "Tên bài báo/Tên sách", new GetValue<String>() {
//	      @Override
//	      public String getValue(Publication contact) {
//	        return contact.getTitle();
//	      }
//	    }, new FieldUpdater<Publication, String>() {
//	      @Override
//	      public void update(int index, Publication object, String value) {
//	        Window.alert("You clicked " + object.getTitle());
//	      }
//	    });
//	    dataGrid.setColumnWidth(nameCol, 250 + diff, Unit.PX);
//
////	    // TextCell.
////	    addColumn(new TextCell(), "Mã số", new GetValue<String>() {
////	      @Override
////	      public String getValue(Publication contact) {
////	        return contact.getPublicationId();
////	      }
////	    }, null);
//	    
//	    // TextCell.
//	    Column<Publication, String> emailCol = addColumn(new TextCell(), "Tác giả", new GetValue<String>() {
//	      @Override
//	      public String getValue(Publication contact) {
//	    	 return contact.getAuthors();
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(emailCol, 250 + diff, Unit.PX);
//	    
//	    // TextCell.
//	    Column<Publication, String> phoneCol = addColumn(new TextCell(), "Tên hội nghị/tạp chí", new GetValue<String>() {
//	      @Override
//	      public String getValue(Publication contact) {
//	    	 return contact.getBookName() + " - Tháng " + contact.getMonth() + "/" + contact.getYear();
//	      }
//	    }, null);
	    
	    // SafeHtmlCell.
	    Column<Publication, SafeHtml> fileCol = addColumn(new SafeHtmlCell(), "Files", new GetValue<SafeHtml>() {
	      @Override
	      public SafeHtml getValue(Publication contact) {
	    	  String info = "";
	    	  int index = 0;
	    	  if (contact.getMaterials().size() > 0) {
	    		  for (Material mat : contact.getMaterials()) {
	    			  info += "<a href='" + mat.getUrl() + "'>" + mat.getName() + "</a>";
	    			  if (index < contact.getMaterials().size() - 1)
	    				  info += "<br>";
	    		  }
	    	  }
	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
	    	  builder.appendHtmlConstant(info);
	    	  return builder.toSafeHtml();
	      }
	    }, null);

	    // DateCell.
//	    DateTimeFormat dateFormat = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
//	    Column<Publication, Date> dateCol = addColumn(new DateCell(dateFormat), "Ngày sinh", new GetValue<Date>() {
//	      @Override
//	      public Date getValue(Publication contact) {
//	        return contact.getBirthDate();
//	      }
//	    }, null);
//	    PublicationList.setColumnWidth(dateCol, 50, Unit.PX);
	    
	    if (ClientData.isLoggedIn()) {
	    	 dataGrid.setColumnWidth(fileCol, 70, Unit.PX);
		    // ActionCell.
		    Column<Publication, Publication> editCol = addColumn(new ActionCell<Publication>("Sửa", new ActionCell.Delegate<Publication>() {
		      @Override
		      public void execute(Publication contact) {
		    	  if (ClientData.canEdit(contact.getUserId(), true))
			    	EduManager.clientFactory.getEventBus().fireEvent(new EditEvent(contact));
		    		  //edit(contact);
		      }
		    }), "Sửa", new GetValue<Publication>() {
		      @Override
		      public Publication getValue(Publication contact) {
		        return contact;
		      }
		    }, null);
		    dataGrid.setColumnWidth(editCol, 70, Unit.PX);
	
		    // ButtonCell.
		    Column<Publication, String> delCol = addColumn(new ButtonCell(), "Xoá", new GetValue<String>() {
		      @Override
		      public String getValue(Publication contact) {
		    	  return "Xoá";
		    	  //return "Click " + contact.getFirstName();
		      }
		    }, new FieldUpdater<Publication, String>() {
		      @Override
		      public void update(int index, Publication object, String value) {
		        Window.alert("You clicked " + object.getTitle());
		      }
		    });
		    //PublicationList.setColumnWidth(delCol, 70, Unit.PX);
	    }
	}
	
	/**
	   * The list of cells that are editable.
	   */
	  private List<AbstractEditableCell<?, ?>> editableCells;
	
	 /**
	   * Get a cell value from a record.
	   * 
	   * @param <C> the cell type
	   */
	  private static interface GetValue<C> {
	    C getValue(Publication contact);
	  }
	
	/**
	   * Add a column with a header.
	   * 
	   * @param <C> the cell type
	   * @param cell the cell used to render the column
	   * @param headerText the header string
	   * @param getter the value getter for the cell
	   */
	  private <C> Column<Publication, C> addColumn(Cell<C> cell, String headerText,
	      final GetValue<C> getter, FieldUpdater<Publication, C> fieldUpdater) {
	    Column<Publication, C> column = new Column<Publication, C>(cell) {
	      @Override
	      public C getValue(Publication object) {
	        return getter.getValue(object);
	      }
	    };
	    column.setFieldUpdater(fieldUpdater);
	    if (cell instanceof AbstractEditableCell<?, ?>) {
	      editableCells.add((AbstractEditableCell<?, ?>) cell);
	    }
	    dataGrid.addColumn(column, headerText);
	    return column;
	  }
}
