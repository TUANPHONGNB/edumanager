package com.hungnt.edumanager.client.activities.subject.details;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Subject;

public class SubjectDetailViewImpl extends BasicViewImpl implements SubjectDetailView{
	
	private VerticalPanel viewPanel = new VerticalPanel();
	private Button edit = new Button("Sửa");
	private FlexTable flexTable = new FlexTable();
	private Subject currentSubject = null;
	
	public SubjectDetailViewImpl() {
		super();
		flexTable.setWidth("90%");
		viewPanel.setWidth("100%");
		viewPanel.add(flexTable);
		viewPanel.setCellHorizontalAlignment(flexTable, ALIGN_CENTER);
		this.contentPanel.add(viewPanel);
		this.contentPanel.add(documentView);
		flexTable.getCellFormatter().setWidth(0, 0, "150px");
	}
	
	@Override
	public HasClickHandlers getEditButton() {
		return this.edit;
	}
	
	@Override
	public void view(Subject subject) {
		if (subject == null)
			return;
		currentSubject = subject;
		int row = 0, col = 0;
		flexTable.setText(row, col++, "Tên môn");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'><b>" + subject.getName() + " - " + subject.getSubjectIdCode() + "</b></font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Mã môn");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + subject.getSubjectIdCode() + "</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Số tín chỉ");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + subject.getCredits() + " tín chỉ (" + subject.getCreditInfo() + ")</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Loại môn");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + Config.getCourseType(subject.getCourseType()) + "</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Ngôn ngữ");
		String language = subject.getCourseLanguage() == 0 ? "Tiếng Việt" : "Tiếng Anh";
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + language + "</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Mô tả");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + subject.getDescription() + "</font>"));
		row ++; col = 0;
		flexTable.setText(row, col++, "Đề cương");
		flexTable.setWidget(row, col++, new HTML("<font color='blue'>" + subject.getSyllabus() + "</font>"));
		row ++; col = 0;
		documentView.setVisible(ClientData.isLoggedIn());
	}
	
	@Override
	public void refreshView() {
	}
}
