package com.hungnt.edumanager.client.activities.subject;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.RPCCall;
import com.hungnt.edumanager.client.activities.ClientFactory;
import com.hungnt.edumanager.client.activities.basic.BasicActivity;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Subject;

public class SubjectActivity extends BasicActivity {
	
	private SubjectView view;
	
	public SubjectActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getSubjectView();
		panel.setWidget(view.asWidget());
		super.start(panel, eventBus, view);
		if (ClientData.getSubjects().size() > 0)
			view.view(ClientData.getSubjects());
	}
	
	@Override
	protected void bind() {
		super.bind();
		view.getSaveButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final Subject subject = view.getSubject();
				if (subject == null)
					return;
				new RPCCall<Subject>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Lỗi xảy ra, không lưu được môn");
					}

					@Override
					public void onSuccess(Subject result) {
						view.getDialogBox().hide();
						ClientData.getSubjects().clear();
						ClientData.loadData(Config.currentScholarYear, -1);
					}

					@Override
					protected void callService(AsyncCallback<Subject> cb) {
						EduManager.dataService.updateSubject(null, subject, cb);
					}
				}.retry();
			}
		});
		view.getAddMoreButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (!ClientData.isLoggedIn()) {
					view.getLoginView().view();
					return;
				}
				if (ClientData.canEdit(Config.NULL_ID, true))
					view.edit(null);
			}
		});
		view.getCancelButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				view.getDialogBox().hide();
			}
		});
	}
	
	@Override
	protected void onDataLoaded(int dataType) {
		if (dataType == Config.DATA_SUBJECT)
			view.view(ClientData.getSubjects());
	}
 }
