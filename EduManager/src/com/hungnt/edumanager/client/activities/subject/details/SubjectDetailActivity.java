package com.hungnt.edumanager.client.activities.subject.details;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.activities.ClientFactory;
import com.hungnt.edumanager.client.activities.basic.BasicActivity;
import com.hungnt.edumanager.shared.Subject;

public class SubjectDetailActivity extends BasicActivity {
	
	private SubjectDetailView view;
	
	public SubjectDetailActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getSubjectDetailView();
		panel.setWidget(view.asWidget());
		super.start(panel, eventBus, view);
		Subject subject = ((SubjectDetailPlace)place).getSubject();
		if (subject != null) {
			rootId = subject.getId();
			view.view(subject);
			if (ClientData.isLoggedIn())
				this.loadCategoriesData(rootId, ClientData.getCurrentUser().getId(), ClientData.getCurrentUser().getId(), view);
		}
	}
}