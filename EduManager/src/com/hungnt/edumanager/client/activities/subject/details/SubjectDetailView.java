package com.hungnt.edumanager.client.activities.subject.details;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.hungnt.edumanager.client.activities.basic.BasicView;
import com.hungnt.edumanager.shared.Subject;

public interface SubjectDetailView extends BasicView {

	void view(Subject subject);

	HasClickHandlers getEditButton();
}
