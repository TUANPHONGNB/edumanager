package com.hungnt.edumanager.client.activities.subject;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.hungnt.edumanager.client.activities.basic.BasicPlace;

public class SubjectPlace extends BasicPlace {
	
	public SubjectPlace() {
		super();
	}
	
	public String getToken() {
		return "subject";
	}
	
	public static class Tokenizer implements PlaceTokenizer<SubjectPlace> {
        @Override
        public String getToken(SubjectPlace place) {
            return place.getToken();
        }

        @Override
        public SubjectPlace getPlace(String token) {
            return new SubjectPlace();
        }
    }
	
}
