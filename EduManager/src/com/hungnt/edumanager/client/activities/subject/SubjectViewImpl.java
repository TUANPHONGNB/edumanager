package com.hungnt.edumanager.client.activities.subject;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.AbstractEditableCell;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ImageCell;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.CssToken;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;
import com.hungnt.edumanager.client.activities.subject.details.SubjectDetailPlace;
import com.hungnt.edumanager.client.widget.FormatTextArea;
import com.hungnt.edumanager.client.widget.HListBox;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Subject;

public class SubjectViewImpl extends BasicViewImpl implements SubjectView{
	
	private VerticalPanel subjectContentPanel = new VerticalPanel();
	private Button save = new Button("Lưu");
	private Button cancel = new Button("Huỷ");
	private Button addMore = new Button("Thêm");
	private FlexTable flexTable = new FlexTable();
	private TextBox subjectName = new TextBox();
	private TextBox subjectIdCode = new TextBox();
	private FormatTextArea description = new FormatTextArea();
	private FormatTextArea syllabus = new FormatTextArea();
	private HListBox credits = new HListBox();
	private HListBox courseType = new HListBox();
	private TextBox creditInfo = new TextBox();
	private HListBox language = new HListBox();
	private DialogBox dialogBox = null;
	private Subject currentSubject = null;
	
	DataGrid<Subject> dataGrid;// = new DataGrid<Teacher>();
	
	public SubjectViewImpl() {	
		super();
		subjectContentPanel.setWidth("100%");
		subjectContentPanel.setSpacing(5);
		int row = 0, col = 0;
		for (int i = 1; i <= 20; i ++) {
			credits.addItem(i + " tín chỉ", i + "");
		}
		for (int i = 0; i <= 20; i ++) {
			String name = Config.getCourseType(i);
			if (name != null && !name.isEmpty())
				courseType.addItem(name, "" + i);
		}
		language.addItem("Tiếng Việt", "0");
		language.addItem("Tiếng Anh", "1");
		subjectName.setWidth("200px");
		subjectIdCode.setWidth("200px");
		language.setWidth("200px");
		description.setWidth("96%");
		syllabus.setWidth("96%");
		creditInfo.getElement().setPropertyString("placeholder", "Ví dụ: 2-1-0-4, ...");
		creditInfo.setWidth("200px");
		flexTable.setText(row, col++, "Tên môn");
		flexTable.setWidget(row, col++, subjectName);
		flexTable.setText(row, col++, "Mã môn");
		flexTable.setWidget(row, col++, subjectIdCode);
		row ++; col = 0;
		flexTable.setText(row, col++, "Số tín chỉ");
		flexTable.setWidget(row, col++, credits);
		flexTable.setText(row, col++, "Phân bố");
		flexTable.setWidget(row, col++, creditInfo);
		row ++; col = 0;
		flexTable.setText(row, col++, "Loại môn");
		flexTable.setWidget(row, col++, courseType);
		flexTable.setText(row, col++, "Ngôn ngữ");
		flexTable.setWidget(row, col++, language);
		row ++; col = 0;
		flexTable.setText(row, col++, "Mô tả môn");
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		flexTable.setWidget(row, col++, description);
		row ++; col = 0;
		flexTable.setText(row, col++, "Đề cương");
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		flexTable.setWidget(row, col++, syllabus);
		row ++; col = 0;
		
		flexTable.getCellFormatter().setWidth(row, 0, "100px");
		flexTable.getCellFormatter().setWidth(row, 1, "220px");
		flexTable.getCellFormatter().setWidth(row, 2, "100px");
		flexTable.getCellFormatter().setWidth(row, 3, "220px");
		
		flexTable.setWidget(row, 0, cancel);
		flexTable.setWidget(row, 3, save);
		flexTable.getCellFormatter().setHorizontalAlignment(row, 3, ALIGN_RIGHT);
		flexTable.getCellFormatter().setHeight(row, 0, "100px");

		contentPanel.add(subjectContentPanel);
	}
	
	@Override
	public void refreshView() {
		super.refreshView();
		subject.setStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE, true);
	}
	
	@Override
	public Subject getSubject() {
		if (this.subjectName.getText().trim().isEmpty() || this.subjectIdCode.getText().trim().isEmpty()) {
			Window.alert("Tên môn và mã môn không được để trống");
			return null;
		}
		if (currentSubject == null)
			currentSubject = new Subject();
		currentSubject.setSubjectIdCode(subjectIdCode.getText().trim());
		currentSubject.setName(subjectName.getText().trim());
		currentSubject.setDescription(description.getText().trim());
		currentSubject.setSyllabus(syllabus.getText().trim());
		currentSubject.setCredits(Integer.parseInt(credits.getSelectedValue()));
		currentSubject.setCreditInfo(creditInfo.getText().trim());
		currentSubject.setUserId(ClientData.getCurrentUser().getId());
		currentSubject.setCourseLanguage(Integer.parseInt(language.getSelectedValue()));
		currentSubject.setCourseType(Integer.parseInt(courseType.getSelectedValue()));
		return currentSubject;
	}
	
	@Override
	public void edit(Subject obj) {
		if (!ClientData.isLoggedIn()) {
			this.getLoginView().view();
			return;
		}
		if (obj == null)
			obj = new Subject();
		currentSubject = obj;
		if (dialogBox == null) {
			dialogBox = new DialogBox(true);
			dialogBox.setWidth("640px");
		    dialogBox.setGlassEnabled(true);
		    dialogBox.setAnimationEnabled(false);
		    // Create a table to layout the content
		    VerticalPanel dialogContents = new VerticalPanel();
		    dialogContents.setWidth("100%");
		    dialogBox.setWidget(dialogContents);
		    flexTable.setWidth("100%");
		    dialogContents.add(flexTable);
		    dialogContents.setCellHorizontalAlignment(flexTable, ALIGN_CENTER);
		}
	    dialogBox.setText(obj != null ? "Sửa thông tin" : "Tạo mới");
		if (obj != null) {
			subjectName.setText(obj.getName());
			subjectIdCode.setText(obj.getSubjectIdCode());
			description.setText(obj.getDescription());
			syllabus.setText(obj.getSyllabus());
			creditInfo.setText(obj.getCreditInfo());
			credits.setSelectedIndex(obj.getCredits() - 1);
			language.setSelectedIndex(obj.getCourseLanguage());
			courseType.setSelectedValue(obj.getCourseType());
		}
	    dialogBox.center();
        dialogBox.show();
	}
	
	@Override
	public HasClickHandlers getCancelButton() {
		return this.cancel;
	}
	
	@Override
	public HasClickHandlers getSaveButton() {
		return this.save;
	}
	
	@Override
	public HasClickHandlers getAddMoreButton() {
		return this.addMore;
	}
	
	@Override
	public DialogBox getDialogBox() {
		return this.dialogBox;
	}

	/**
	   * The provider that holds the list of contacts in the database.
	   */
	  private ListDataProvider<Subject> dataProvider = new ListDataProvider<Subject>();
	
	/**
     * The key provider that provides the unique ID of a contact.
     */
    public static final ProvidesKey<Subject> KEY_PROVIDER = new ProvidesKey<Subject>() {
      @Override
      public Object getKey(Subject item) {
        return item == null ? null : item.getId();
      }
    };
	
    @Override
	public void view(List<Subject> subjects) {
	    // Create the table.
//		if (subjects == null || subjects.size() == 0) {
//			subjects = dataProvider.getList();
//			for (int i = 0; i < 10; i ++) {
//				Subject t = new Subject(Random.nextInt(100000) + "", 
//						"Môn " + i, "Description " + i, "", Random.nextInt(5) + 1, "2-0-3-1");
//				subjects.add(t);
//			}
//		}
		for (int i = 0; i < subjects.size(); i ++) {
			subjects.get(i).setPosIndex(i + 1);
		}
		dataProvider.setList(subjects);
	    editableCells = new ArrayList<AbstractEditableCell<?, ?>>();
	    dataGrid = new DataGrid<Subject>(200, KEY_PROVIDER);
	    
	    dataGrid.setWidth("100%");
	    dataGrid.setHeight("600px");
	    
	    //dataGrid.setMinimumTableWidth(140, Unit.EM);
	    dataProvider.addDataDisplay(dataGrid);
	    
	    subjectContentPanel.clear();
	    subjectContentPanel.add(addMore);
	    subjectContentPanel.add(dataGrid);
	    subjectContentPanel.setCellHorizontalAlignment(addMore, ALIGN_CENTER);
	    
	    int diff = ClientData.isLoggedIn() ? 0 : 30;
	    
	    // NumberCell.
	    Column<Subject, Number> numberColumn =
	        addColumn(new NumberCell(), "#", new GetValue<Number>() {
	          @Override
	          public Number getValue(Subject contact) {
	            return contact.getPosIndex();
	          }
	        }, null);
	    dataGrid.setColumnWidth(numberColumn, 50, Unit.PX);
	    numberColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LOCALE_END);
	    
	    // ImageCell.
	    Column<Subject, String> avatarCol = addColumn(new ImageCell(), "Ảnh", new GetValue<String>() {
	      @Override
	      public String getValue(Subject contact) {
	    	  if (contact.getAvatar() == null || contact.getAvatar().isEmpty())
	    		  return "images/book.png";
	    	  else
	    		  return contact.getAvatar();
	      }
	    }, null);
	    dataGrid.setColumnWidth(avatarCol, 60, Unit.PX);
	    
	    // ClickableTextCell.
	    Column<Subject, String> nameCol = addColumn(new ClickableTextCell(), "Tên môn", new GetValue<String>() {
	      @Override
	      public String getValue(Subject contact) {
	        return contact.getName();
	      }
	    }, new FieldUpdater<Subject, String>() {
	      @Override
	      public void update(int index, Subject object, String value) {
	    	  EduManager.clientFactory.getPlaceController().goTo(new SubjectDetailPlace(object));
	      }
	    });
	    dataGrid.setColumnWidth(nameCol, 300 + diff, Unit.PX);

//	    // TextCell.
//	    addColumn(new TextCell(), "Mã số", new GetValue<String>() {
//	      @Override
//	      public String getValue(Subject contact) {
//	        return contact.getSubjectId();
//	      }
//	    }, null);
	    
	    // SafeHtmlCell.
	    Column<Subject, SafeHtml> creditCol = addColumn(new SafeHtmlCell(), "Thời lượng", new GetValue<SafeHtml>() {
	      @Override
	      public SafeHtml getValue(Subject contact) {
	    	  String info = contact.getCredits() + " tín chỉ";
	    	  if (!contact.getCreditInfo().isEmpty())
	    		  info += "<br>(" + contact.getCreditInfo() + ")";
	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
	    	  builder.appendHtmlConstant(info);
	    	  return builder.toSafeHtml();
	      }
	    }, null);
	    dataGrid.setColumnWidth(creditCol, 100 + diff, Unit.PX);
	    
	 // TextCell.
	    Column<Subject, String> idCodeCol = addColumn(new TextCell(), "Mã HP", new GetValue<String>() {
	      @Override
	      public String getValue(Subject contact) {
	    	  return contact.getSubjectIdCode();
	      }
	    }, null);
	    dataGrid.setColumnWidth(idCodeCol, 70 + diff, Unit.PX);
	    
	    // TextCell.
	    Column<Subject, String> phoneCol = addColumn(new TextCell(), "Mô tả", new GetValue<String>() {
	      @Override
	      public String getValue(Subject contact) {
	    	  return contact.getDescription();
	      }
	    }, null);

	    // DateCell.
//	    DateTimeFormat dateFormat = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
//	    Column<Subject, Date> dateCol = addColumn(new DateCell(dateFormat), "Ngày sinh", new GetValue<Date>() {
//	      @Override
//	      public Date getValue(Subject contact) {
//	        return contact.getBirthDate();
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(dateCol, 50, Unit.PX);
	    if (ClientData.isLoggedIn()) {
		    dataGrid.setColumnWidth(phoneCol, 200 + diff, Unit.PX);

		    // ActionCell.
		    Column<Subject, Subject> editCol = addColumn(new ActionCell<Subject>("Sửa", new ActionCell.Delegate<Subject>() {
		      @Override
		      public void execute(Subject contact) {
		    	  if (ClientData.canEdit(contact.getUserId(), true))
		    		  edit(contact);
		      }
		    }), "Sửa", new GetValue<Subject>() {
		      @Override
		      public Subject getValue(Subject contact) {
		        return contact;
		      }
		    }, null);
//		    dataGrid.setColumnWidth(editCol, 70, Unit.PX);
//	
//		    // ButtonCell.
//		    Column<Subject, String> delCol = addColumn(new ButtonCell(), "Xoá", new GetValue<String>() {
//		      @Override
//		      public String getValue(Subject contact) {
//		    	  return "Xoá";
//		    	  //return "Click " + contact.getFirstName();
//		      }
//		    }, new FieldUpdater<Subject, String>() {
//		      @Override
//		      public void update(int index, Subject object, String value) {
//		        Window.alert("You clicked " + object.getName());
//		      }
//		    });
		    //dataGrid.setColumnWidth(delCol, 70, Unit.PX);
	    }
	}
	
	/**
	   * The list of cells that are editable.
	   */
	  private List<AbstractEditableCell<?, ?>> editableCells;
	
	 /**
	   * Get a cell value from a record.
	   * 
	   * @param <C> the cell type
	   */
	  private static interface GetValue<C> {
	    C getValue(Subject contact);
	  }
	
	/**
	   * Add a column with a header.
	   * 
	   * @param <C> the cell type
	   * @param cell the cell used to render the column
	   * @param headerText the header string
	   * @param getter the value getter for the cell
	   */
	  private <C> Column<Subject, C> addColumn(Cell<C> cell, String headerText,
	      final GetValue<C> getter, FieldUpdater<Subject, C> fieldUpdater) {
	    Column<Subject, C> column = new Column<Subject, C>(cell) {
	      @Override
	      public C getValue(Subject object) {
	        return getter.getValue(object);
	      }
	    };
	    column.setFieldUpdater(fieldUpdater);
	    if (cell instanceof AbstractEditableCell<?, ?>) {
	      editableCells.add((AbstractEditableCell<?, ?>) cell);
	    }
	    dataGrid.addColumn(column, headerText);
	    return column;
	  }
}