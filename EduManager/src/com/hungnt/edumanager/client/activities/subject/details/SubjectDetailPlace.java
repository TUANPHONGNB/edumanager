package com.hungnt.edumanager.client.activities.subject.details;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Subject;

public class SubjectDetailPlace extends Place {
	
	private Long subjectId = Config.NULL_ID;
	private Subject subject = null;
	
	public SubjectDetailPlace() {
		super();
	}

	public SubjectDetailPlace(long subjectId) {
		super();
		this.subjectId = subjectId;
	}

	public SubjectDetailPlace(Subject subject) {
		super();
		this.subject = subject;
	}
	
	public String getToken() {
		return "subjectdetail";
	}
	
	public Long getSubjectId() {
		return this.subjectId;
	}

	public Subject getSubject() {
		return this.subject;
	}
	
	public static class Tokenizer implements PlaceTokenizer<SubjectDetailPlace> {
        @Override
        public String getToken(SubjectDetailPlace place) {
            return place.getToken();
        }

        @Override
        public SubjectDetailPlace getPlace(String token) {
            return new SubjectDetailPlace();
        }
    }
}
