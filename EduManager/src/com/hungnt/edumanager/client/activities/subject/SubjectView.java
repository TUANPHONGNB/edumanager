package com.hungnt.edumanager.client.activities.subject;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.DialogBox;
import com.hungnt.edumanager.client.activities.basic.BasicView;
import com.hungnt.edumanager.shared.Subject;

public interface SubjectView extends BasicView{
	
	void edit(Subject teacher);

	HasClickHandlers getSaveButton();

	HasClickHandlers getAddMoreButton();

	Subject getSubject();

	void view(List<Subject> subjects);

	DialogBox getDialogBox();

	HasClickHandlers getCancelButton();

}
