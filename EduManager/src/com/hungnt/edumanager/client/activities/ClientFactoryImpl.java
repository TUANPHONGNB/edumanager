package com.hungnt.edumanager.client.activities;

import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.hungnt.edumanager.client.activities.basic.BasicView;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;
import com.hungnt.edumanager.client.activities.course.CourseView;
import com.hungnt.edumanager.client.activities.course.CourseViewImpl;
import com.hungnt.edumanager.client.activities.course.details.CourseDetailView;
import com.hungnt.edumanager.client.activities.course.details.CourseDetailViewImpl;
import com.hungnt.edumanager.client.activities.document.DocumentView;
import com.hungnt.edumanager.client.activities.document.DocumentViewImpl;
import com.hungnt.edumanager.client.activities.equipment.EquipmentView;
import com.hungnt.edumanager.client.activities.equipment.EquipmentViewImpl;
import com.hungnt.edumanager.client.activities.event.EventView;
import com.hungnt.edumanager.client.activities.event.EventViewImpl;
import com.hungnt.edumanager.client.activities.home.HomeView;
import com.hungnt.edumanager.client.activities.home.HomeViewImpl;
import com.hungnt.edumanager.client.activities.meeting.MeetingView;
import com.hungnt.edumanager.client.activities.meeting.MeetingViewImpl;
import com.hungnt.edumanager.client.activities.publication.PublicationView;
import com.hungnt.edumanager.client.activities.publication.PublicationViewImpl;
import com.hungnt.edumanager.client.activities.subject.SubjectView;
import com.hungnt.edumanager.client.activities.subject.SubjectViewImpl;
import com.hungnt.edumanager.client.activities.subject.details.SubjectDetailView;
import com.hungnt.edumanager.client.activities.subject.details.SubjectDetailViewImpl;
import com.hungnt.edumanager.client.activities.teacher.TeacherView;
import com.hungnt.edumanager.client.activities.teacher.TeacherViewImpl;
import com.hungnt.edumanager.client.activities.teacher.details.TeacherDetailView;
import com.hungnt.edumanager.client.activities.teacher.details.TeacherDetailViewImpl;
import com.hungnt.edumanager.client.activities.topic.TopicView;
import com.hungnt.edumanager.client.activities.topic.TopicViewImpl;

public class ClientFactoryImpl implements ClientFactory {
	private SimpleEventBus eventBus;
	private PlaceController placeController;
	private BasicView basicView;
	private DocumentView documentView = null;
	
	public ClientFactoryImpl() {
		eventBus = new SimpleEventBus();
		placeController = new PlaceController(eventBus);
	}
	
	@Override
	public PlaceController getPlaceController() {
		return placeController;
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public BasicView getBasicView() {
		if(basicView==null){
			basicView= new BasicViewImpl();
		}
		return basicView;
	}
	
	@Override
	public SubjectView getSubjectView() {
		basicView = new SubjectViewImpl();
		return (SubjectView) basicView;
	}
	
	@Override
	public SubjectDetailView getSubjectDetailView() {
		basicView = new SubjectDetailViewImpl();
		return (SubjectDetailView) basicView;
	}

	@Override
	public CourseDetailView getCourseDetailView() {
		basicView = new CourseDetailViewImpl();
		return (CourseDetailView) basicView;
	}
	
	@Override
	public TeacherView getTeacherView() {
		basicView = new TeacherViewImpl();
		return (TeacherView) basicView;
	}

	@Override
	public TeacherDetailView getTeacherDetailView() {
		basicView = new TeacherDetailViewImpl();
		return (TeacherDetailView) basicView;
	}
	
	@Override
	public CourseView getCourseView() {
		basicView = new CourseViewImpl();
		return (CourseView) basicView;
	}
	
	@Override
	public TopicView getTopicView() {
		basicView = new TopicViewImpl();
		return (TopicView) basicView;
	}
	
	@Override
	public HomeView getHomeView() {
		basicView = new HomeViewImpl();
		return (HomeView) basicView;
	}
	
	@Override
	public PublicationView getPublicationView() {
		basicView = new PublicationViewImpl();
		return (PublicationView) basicView;
	}
	
	@Override
	public EquipmentView getEquipmentView() {
		basicView = new EquipmentViewImpl();
		return (EquipmentView) basicView;
	}
	
	@Override
	public EventView getEventView() {
		basicView = new EventViewImpl();
		return (EventView) basicView;
	}
	
	@Override
	public MeetingView getMeetingView() {
		basicView = new MeetingViewImpl();
		return (MeetingView) basicView;
	}

	@Override
	public DocumentView getMaterialView() {
		if (documentView == null)
			documentView = new DocumentViewImpl();
		basicView = documentView;
		return documentView;
	}	
}
