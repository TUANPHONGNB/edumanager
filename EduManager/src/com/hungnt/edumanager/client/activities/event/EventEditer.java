package com.hungnt.edumanager.client.activities.event;

import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.widget.HListBox;
import com.hungnt.edumanager.client.widget.TeacherSelector;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Event;
import com.hungnt.edumanager.shared.Teacher;

public class EventEditer {
	
	private Button save = new Button("Lưu");
	private Button cancel = new Button("Huỷ");
	private FlexTable flexTable = new FlexTable();
	private TextBox title = new TextBox();
	private TextArea description = new TextArea();
	private TextBox place = new TextBox();
	private DatePicker fromDatePicker = new DatePicker();
	private DateBox fromDateBox = null;
	private TeacherSelector teacherSelector = new TeacherSelector();
	private HListBox eventTypes = new  HListBox();
	
	private HListBox hour = new HListBox();
	private HListBox minute = new HListBox();
	private HTML timeInfo = new HTML("Thời gian");
	
	private DialogBox dialogBox = null;
	private Event currentEvent = null;

	public EventEditer() {	
		super();
		flexTable.setWidth("100%");
		title.setWidth("500px");
		description.setWidth("500px");
		place.setWidth("500px");
		teacherSelector.setWidth("500px");
		
		eventTypes.addItem("Sự kiện", Config.TYPE_EVENT + "");
		eventTypes.addItem("Công việc", Config.TYPE_TASK + "");
		
		eventTypes.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				timeInfo.setHTML(eventTypes.getSelectedIndex() == 0 ? "Thời gian" : "Deadline");
			}
		});
		
		int row = 0, col = 0;
		flexTable.setText(row, col++, "Loại");
		flexTable.setWidget(row, col, eventTypes);
		row ++; col = 0;
		flexTable.setText(row, col++, "Tiêu đề");
		flexTable.setWidget(row, col, title);
		row ++; col = 0;
		flexTable.setText(row, col++, "Mô tả");
		flexTable.setWidget(row, col++, description);
		row ++; col = 0;
		
		fromDateBox = new DateBox(fromDatePicker, null, new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd/MM/yyyy")));
		fromDateBox.setWidth("100px");
		fromDateBox.getDatePicker().setYearArrowsVisible(true);
		
		hour.setWidth("80px");
		minute.setWidth("80px");
		for (int j = 0; j <= 24; j++) {
			hour.addItem(j + " giờ", j + "");
		}
		for (int j = 0; j <= 60; j++) {
			minute.addItem(j + " phút", j + "");
		}
		
		HorizontalPanel timePanel = new HorizontalPanel();
		timePanel.add(fromDateBox);
		timePanel.add(hour);
		timePanel.add(minute);
		
		flexTable.setWidget(row, col++, timeInfo);
		flexTable.setWidget(row, col++, timePanel);
		row ++; col = 0;
		flexTable.setText(row, col++, "Địa điểm");
		flexTable.setWidget(row, col++, place);
		
		row ++; col = 0;
		flexTable.setText(row, col++, "Tham gia");
		flexTable.setWidget(row, col++, teacherSelector);
		
//		flexTable.getCellFormatter().setWidth(row, 0, "100px");
//		flexTable.getCellFormatter().setWidth(row, 1, "400px");
		
	}
	
	public void setupAuthors(List<Teacher> teachers) {
		teacherSelector.setupAuthors(teachers);
	}
	
	public HasClickHandlers getCancelButton() {
		return this.cancel;
	}
	
	public HasClickHandlers getSaveButton() {
		return this.save;
	}
	
	public DialogBox getDialogBox() {
		return this.dialogBox;
	}
	
	public Event getEvent() {
		if (currentEvent == null)
			currentEvent = new Event();
		currentEvent.setName(title.getText().trim());
		currentEvent.setDescription(description.getText().trim());
		currentEvent.setDate(fromDateBox.getValue());
		currentEvent.setHour(Integer.parseInt(hour.getSelectedValue()));
		currentEvent.setMinute(Integer.parseInt(minute.getSelectedValue()));
		currentEvent.setInvolvedUsers(teacherSelector.getSelectedIds());
		currentEvent.setInvolvedUserNames(teacherSelector.getSelectedStrings());
		currentEvent.setPlace(place.getText().trim());
		currentEvent.setUserId(ClientData.getCurrentUser().getId());
		currentEvent.setEventType(Integer.parseInt(eventTypes.getSelectedValue()));
		return currentEvent;
	}
	
	public void setupTeachers(List<Teacher> teacherList) {
		teacherSelector.setupAuthors(teacherList);
	}
	
	public void edit(Event event) {
		if (!ClientData.isLoggedIn()) {
			EduManager.clientFactory.getBasicView().getLoginView().view();
			return;
		}
		if (event == null)
			event = new Event();
		setupTeachers(ClientData.getTeachers());
		currentEvent = event;
		if (dialogBox == null) {
			dialogBox = new DialogBox(true);
			dialogBox.setWidth("700px");
		    dialogBox.setGlassEnabled(true);
		    dialogBox.setAnimationEnabled(false);
		    // Create a table to layout the content
		    VerticalPanel dialogContents = new VerticalPanel();
		    dialogContents.setSpacing(5);
		    dialogContents.setWidth("100%");
		    dialogBox.setWidget(dialogContents);
		    flexTable.setWidth("100%");
		    dialogContents.add(flexTable);
		    dialogContents.setCellHorizontalAlignment(flexTable, HasHorizontalAlignment.ALIGN_CENTER);
		    
		    HorizontalPanel hPanel = new HorizontalPanel();
		    hPanel.setWidth("100%");
		    dialogContents.add(hPanel);
		    hPanel.add(cancel);
		    hPanel.setCellHorizontalAlignment(cancel, HasHorizontalAlignment.ALIGN_LEFT);
		    hPanel.add(save);
		    hPanel.setCellHorizontalAlignment(save, HasHorizontalAlignment.ALIGN_RIGHT);
		}
	    dialogBox.setText(event != null ? "Sửa thông tin" : "Tạo mới");
	    eventTypes.setSelectedValue(currentEvent.getEventType());
	    title.setText(currentEvent.getName());
	    description.setText(currentEvent.getDescription());
	    fromDateBox.setValue(currentEvent.getDate());
	    hour.setSelectedValue(currentEvent.getHour());
	    minute.setSelectedValue(currentEvent.getMinute());
	    place.setText(currentEvent.getPlace());
	    teacherSelector.setSelectedIds(currentEvent.getInvolvedUsers());
	    dialogBox.center();
	}
}