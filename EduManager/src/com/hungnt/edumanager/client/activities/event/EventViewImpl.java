package com.hungnt.edumanager.client.activities.event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.AbstractEditableCell;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.ClientUtils;
import com.hungnt.edumanager.client.CssToken;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;
import com.hungnt.edumanager.shared.Event;
import com.hungnt.edumanager.shared.Teacher;

public class EventViewImpl extends BasicViewImpl implements EventView {
	
	private VerticalPanel eventViewPanel = new VerticalPanel();
	private EventEditer eventEditor = new EventEditer();
	private Button addMore = new Button("Thêm");
	
	DataGrid<Event> dataGrid;// = new DataGrid<Event>();

	public EventViewImpl() {	
		super();
		eventViewPanel.setSpacing(5);
		eventViewPanel.setWidth("100%");
		eventViewPanel.add(addMore);
		contentPanel.add(eventViewPanel);
	}
	
	@Override
	public HasClickHandlers getSaveButton() {
		return this.eventEditor.getSaveButton();
	}
	
	@Override
	public HasClickHandlers getCancelButton() {
		return this.eventEditor.getCancelButton();
	}
	
	@Override
	public HasClickHandlers getAddMoreButton() {
		return this.addMore;
	}

	@Override
	public Event getEvent() {
		return this.eventEditor.getEvent();
	}
	
	@Override
	public void refreshView() {
		super.refreshView();
		event.setStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE, true);
	}
	
	@Override
	public DialogBox getDialogBox() {
		return this.eventEditor.getDialogBox();
	}
	
	@Override
	public void edit(Event event) {
		eventEditor.edit(event);
	}
	
	@Override
	public void setupAuthors(List<Teacher> teachers) {
		eventEditor.setupAuthors(teachers);
	}
	
	/**
	   * The provider that holds the list of contacts in the database.
	   */
	  private ListDataProvider<Event> dataProvider = new ListDataProvider<Event>();
	
	/**
     * The key provider that provides the unique ID of a contact.
     */
    public static final ProvidesKey<Event> KEY_PROVIDER = new ProvidesKey<Event>() {
      @Override
      public Object getKey(Event item) {
        return item == null ? null : item.getId();
      }
    };
	
    @Override
	public  void view(List<Event> events) {
		ClientUtils.log("Event y1");
    	Collections.sort(events, new Comparator<Event>() {
			@Override
			public int compare(Event o1, Event o2) {
				if (o1.getDate() == null || o2.getDate() == null)
					return 0;
				if (o1.getDate().after(o2.getDate()))
					return -1;
				else if (o1.getDate().before(o2.getDate()))
					return 1;
				else
					return 0;
			}
		});
		ClientUtils.log("Event y2");
		for (int i = 0; i < events.size(); i++) {
			events.get(i).setPosIndex(i + 1);
		}
		ClientUtils.log("Event y3");

		dataProvider.setList(events);
	    editableCells = new ArrayList<AbstractEditableCell<?, ?>>();
	    dataGrid = new DataGrid<Event>(25, KEY_PROVIDER);
	    
	    dataGrid.setWidth("100%");
	    dataGrid.setHeight("600px");
	    
	    //teacherList.setMinimumTableWidth(140, Unit.EM);
	    dataProvider.addDataDisplay(dataGrid);
	    
	    eventViewPanel.clear();
	    eventViewPanel.add(addMore);
	    eventViewPanel.add(dataGrid);
	    eventViewPanel.setCellHorizontalAlignment(addMore, ALIGN_CENTER);
		ClientUtils.log("Event y5");

	    // NumberCell.
	    Column<Event, Number> numberColumn =
	        addColumn(new NumberCell(), "#", new GetValue<Number>() {
	          @Override
	          public Number getValue(Event contact) {
	            return contact.getPosIndex();
	          }
	        }, null);
	    dataGrid.setColumnWidth(numberColumn, 40, Unit.PX);
	    numberColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LOCALE_END);
		ClientUtils.log("Event y6");

	    // SafeHtmlCell.
	    Column<Event, SafeHtml> infoCol = addColumn(new SafeHtmlCell(), "Sự kiện", new GetValue<SafeHtml>() {
	      @Override
	      public SafeHtml getValue(Event object) {
	  		ClientUtils.log("Event z1");

	    	  String info = "";
	    	  String color = object.getStatus() == 0 ? "blue" : "red";
	    	  String title = object.getName();
	    	  if (object.getStatus() > 0)
	    		  title += " (Đã diễn ra)";
		  		ClientUtils.log("Event z2");
		  	try {
	    	  info += "<font color='" + color + "'>" + title + "</font><br>";
	    	  info += "<i>Mô tả: " + object.getDescription() + "</i><br>";
	    	  info += "Thời gian: " + object.getTime() + " tại " + object.getPlace() + "<br>";
	    	  String users = "";
	    	  for (String name : object.getInvolvedUserNames())
	    		  users += name + ", ";
		  		ClientUtils.log("Event z3");
	    	  info += "<i>Tham gia: " + users + "</i><br>";
		  	}
		  	catch (Exception e) {
		  		ClientUtils.log("Exception: " + e.toString());
		  	}
	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
	    	  builder.appendHtmlConstant(info);
		  		ClientUtils.log("Event z4");
	    	  return builder.toSafeHtml();
	      }
	    }, null);
		ClientUtils.log("Event y7");

//	    // ClickableTextCell.
//	    Column<Event, String> nameCol = addColumn(new ClickableTextCell(), "Sự kiện", new GetValue<String>() {
//	      @Override
//	      public String getValue(Event contact) {
//	        return contact.getName();
//	      }
//	    }, new FieldUpdater<Event, String>() {
//	      @Override
//	      public void update(int index, Event object, String value) {
//	      }
//	    });
//	    dataGrid.setColumnWidth(nameCol, 100, Unit.PX);
//
//	    // TextCell.
//	    Column<Event, String> quantityCol = addColumn(new TextCell(), "Mô tả", new GetValue<String>() {
//	      @Override
//	      public String getValue(Event contact) {
//	        return contact.getDescription();
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(quantityCol, 150, Unit.PX);
//
//	    // TextCell.
//	    Column<Event, SafeHtml> placeTimeCol = addColumn(new SafeHtmlCell(), "Thời gian/Địa điểm", new GetValue<SafeHtml>() {
//	      @Override
//	      public SafeHtml getValue(Event contact) {
//	    	  String info = contact.getTime() + "<br>" + contact.getPlace();
//	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
//	    	  builder.appendHtmlConstant(info);
//	    	  return builder.toSafeHtml();
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(placeTimeCol, 100, Unit.PX);
//	    
//	    // TextCell.
//	    Column<Event, SafeHtml> involvedUsers = addColumn(new SafeHtmlCell(), "Tham gia", new GetValue<SafeHtml>() {
//	      @Override
//	      public SafeHtml getValue(Event contact) {
//	    	  String info = "";
//	    	  for (String name : contact.getInvolvedUserNames())
//	    		  info += name + "<br>";
//	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
//	    	  builder.appendHtmlConstant(info);
//	        return builder.toSafeHtml();
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(involvedUsers, 150, Unit.PX);
//	    
//	    // TextCell.
//	    Column<Event, String> notesCol = addColumn(new TextCell(), "Ghi chú", new GetValue<String>() {
//	      @Override
//	      public String getValue(Event contact) {
//	        return contact.getNotes();
//	      }
//	    }, null);
		ClientUtils.log("Event y8");

	    if (ClientData.isLoggedIn()) {
		    dataGrid.setColumnWidth(infoCol, 700, Unit.PX);

	    	if (ClientData.getCurrentUser().isAdmin()) {
		    	// ActionCell.
			    Column<Event, Event> editCol = addColumn(new ActionCell<Event>("Sửa", new ActionCell.Delegate<Event>() {
			      @Override
			      public void execute(Event contact) {
			        edit(contact);
			      }
			    }), "Sửa", new GetValue<Event>() {
			      @Override
			      public Event getValue(Event contact) {
			        return contact;
			      }
			    }, null);
			    //dataGrid.setColumnWidth(editCol, 70, Unit.PX);
	    	}
	    }
		ClientUtils.log("Event y9");

	    //teacherList.setColumnWidth(delCol, 70, Unit.PX);
	}
	
	/**
	   * The list of cells that are editable.
	   */
	  private List<AbstractEditableCell<?, ?>> editableCells;
	
	 /**
	   * Get a cell value from a record.
	   * 
	   * @param <C> the cell type
	   */
	  private static interface GetValue<C> {
	    C getValue(Event contact);
	  }
	
	/**
	   * Add a column with a header.
	   * 
	   * @param <C> the cell type
	   * @param cell the cell used to render the column
	   * @param headerText the header string
	   * @param getter the value getter for the cell
	   */
	  private <C> Column<Event, C> addColumn(Cell<C> cell, String headerText,
	      final GetValue<C> getter, FieldUpdater<Event, C> fieldUpdater) {
	    Column<Event, C> column = new Column<Event, C>(cell) {
	      @Override
	      public C getValue(Event object) {
	        return getter.getValue(object);
	      }
	    };
	    column.setFieldUpdater(fieldUpdater);
	    if (cell instanceof AbstractEditableCell<?, ?>) {
	      editableCells.add((AbstractEditableCell<?, ?>) cell);
	    }
	    dataGrid.addColumn(column, headerText);
	    return column;
	  }
}
