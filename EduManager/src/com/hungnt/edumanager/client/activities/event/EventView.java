package com.hungnt.edumanager.client.activities.event;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.DialogBox;
import com.hungnt.edumanager.client.activities.basic.BasicView;
import com.hungnt.edumanager.shared.Event;
import com.hungnt.edumanager.shared.Teacher;

public interface EventView extends BasicView{
	HasClickHandlers getSaveButton();
	HasClickHandlers getAddMoreButton();
	void edit(Event equipment);
	void view(List<Event> equipments);
	DialogBox getDialogBox();
	HasClickHandlers getCancelButton();
	Event getEvent();
	void setupAuthors(List<Teacher> teachers);
}
