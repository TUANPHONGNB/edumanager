package com.hungnt.edumanager.client.activities.event;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.hungnt.edumanager.client.activities.basic.BasicPlace;

public class EventPlace extends BasicPlace {
	
	public EventPlace() {
		super();
	}

	@Override
	public String getToken() {
		return "event";
	}
	
	public static class Tokenizer implements PlaceTokenizer<EventPlace> {
        @Override
        public String getToken(EventPlace place) {
            return place.getToken();
        }

        @Override
        public EventPlace getPlace(String token) {
            return new EventPlace();
        }
    }	
}