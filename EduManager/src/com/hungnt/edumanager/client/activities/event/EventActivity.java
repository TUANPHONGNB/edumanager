package com.hungnt.edumanager.client.activities.event;

import java.util.Date;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.ClientUtils;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.RPCCall;
import com.hungnt.edumanager.client.activities.ClientFactory;
import com.hungnt.edumanager.client.activities.basic.BasicActivity;
import com.hungnt.edumanager.shared.Event;

public class EventActivity extends BasicActivity {
	
	private EventView view;
	
	public EventActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getEventView();
		panel.setWidget(view.asWidget());
		super.start(panel, eventBus, view);
		view.setupAuthors(ClientData.getTeachers());
		if (ClientData.getEvents().size() > 0)
			view.view(ClientData.getEvents());
		else
			loadData();
	}
	
	@Override
	protected void bind() {
		super.bind();
		view.getSaveButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final Event object = view.getEvent();
				if (object == null)
					return;
				new RPCCall<Event>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Có lỗi xảy ra, không thể lưu được thiết bị");
					}

					@Override
					public void onSuccess(Event result) {
						view.getDialogBox().hide();
						loadData();
					}

					@Override
					protected void callService(AsyncCallback<Event> cb) {
						EduManager.dataService.updateEvent(ClientData.getCurrentUser().getId() + "", object, cb);
					}
				}.retry();
			}
		});
		view.getAddMoreButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (!ClientData.isLoggedIn()) {
					view.getLoginView().view();
					return;
				}
				view.edit(new Event());
			}
		});
		view.getCancelButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				view.getDialogBox().hide();
			}
		});
	}
		
	private void loadData() {
		new RPCCall<List<Event>>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Có lỗi xảy ra, không thể tải dữ liệu");
			}

			@Override
			public void onSuccess(List<Event> result) {
				ClientUtils.log("Event x1");
				ClientData.setEvents(result);
				ClientUtils.log("Event x2");
				view.view(result);
				ClientUtils.log("Event x3");
			}

			@Override
			protected void callService(AsyncCallback<List<Event>> cb) {
				EduManager.dataService.getEvents(null, new Date(), cb);
				
			}}.retry();
	}
 }
