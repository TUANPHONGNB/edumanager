package com.hungnt.edumanager.client.activities.equipment;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.AbstractEditableCell;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.CssToken;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;
import com.hungnt.edumanager.client.widget.NumberTextBox;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Equipment;
import com.hungnt.edumanager.shared.EquipmentHistory;
import com.hungnt.edumanager.shared.Teacher;

public class EquipmentViewImpl extends BasicViewImpl implements EquipmentView {
	
	private VerticalPanel teacherViewPanel = new VerticalPanel();
	private Button save = new Button("Lưu");
	private Button bookingSave = new Button("Lưu");
	private Button cancel = new Button("Huỷ");
	private Button addMore = new Button("Thêm");
	private FlexTable flexTable = new FlexTable();
	private TextBox name = new TextBox();
	private TextBox serialNumber = new TextBox();
	private TextBox idCode = new TextBox();
	private NumberTextBox price = new NumberTextBox();
	private ListBox quantity = new ListBox();
	private ListBox madeFrom = new ListBox();
	private ListBox fromMonth = new ListBox();
	private ListBox fromYear = new ListBox();
	private ListBox status = new ListBox();
	private ListBox financeBy = new ListBox();
	private TextBox notes = new TextBox();
	private TextBox description = new TextBox();
	private DialogBox dialogBox = null;
	private Equipment currentEquipment = null;
	private EquipmentHistory currentBooking = null;
	private static boolean isBooking = false;
	
	private FlexTable bookingFlexTable = new FlexTable();
	private ListBox teacherList = new ListBox();
	private DatePicker[] datePicker = new DatePicker[2];
	private DateBox[] dateBox = new DateBox[2];
	private ListBox[] hour = new ListBox[2];
	private ListBox[] minute = new ListBox[2];
	private TextBox place = new TextBox();
	private TextBox reason = new TextBox();
	private TextBox bookingNotes = new TextBox();
	
	DataGrid<Equipment> dataGrid;// = new DataGrid<Equipment>();

	public EquipmentViewImpl() {	
		super();
		teacherViewPanel.setSpacing(5);
		teacherViewPanel.setWidth("100%");
		int row = 0, col = 0;
		for (int i = 0; i <= 2; i++) {
			madeFrom.addItem(Config.getMadeFrom(i), "" + i);
		}
		for (int i = 1; i <= 12; i++)
			fromMonth.addItem("Tháng " + i, "" + i);
		for (int i = 2000; i < 2015; i ++)
			fromYear.addItem("Năm " + i, "" + i);
		for (int i = 0; i <= 10; i ++)
			status.addItem(i*10 + "%", "" + i*10);
		for (int i = 0; i <= 2; i ++)
			financeBy.addItem(Config.getFinanceBy(i), "" + i);
		for (int i = 1; i <= 30; i ++)
			quantity.addItem("" + i, "" + i);
		serialNumber.setWidth("200px");
		serialNumber.setWidth("200px");
		name.setWidth("200px");
		idCode.setWidth("200px");
		price.setWidth("200px");
		description.setWidth("95%");
		notes.setWidth("95%");
		
		flexTable.setWidth("100%");
		flexTable.setText(row, col++, "Tên thiết bị");
		flexTable.setWidget(row, col++, name);
		flexTable.setText(row, col++, "Số Serial");
		flexTable.setWidget(row, col++, serialNumber);
		row ++; col = 0;
		flexTable.setText(row, col++, "Mã số");
		flexTable.setWidget(row, col++, idCode);
		flexTable.setText(row, col++, "Số lượng");
		flexTable.setWidget(row, col++, quantity);
		row ++; col = 0;
		flexTable.setText(row, col++, "Xuất sứ");
		flexTable.setWidget(row, col++, madeFrom);
		flexTable.setText(row, col++, "Nguồn tiền");
		flexTable.setWidget(row, col++, financeBy);
		row ++; col = 0;
		flexTable.setText(row, col++, "Thời gian");
		HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.setWidth("200px");
		hPanel.add(fromMonth);
		hPanel.add(fromYear);
		flexTable.setWidget(row, col++, hPanel);
		flexTable.setText(row, col++, "Tình trạng");
		flexTable.setWidget(row, col++, status);
		row ++; col = 0;
		flexTable.setText(row, col++, "Giá tiền");
		flexTable.setWidget(row, col++, price);
		row ++; col = 0;
		flexTable.setText(row, col++, "Mô tả");
		flexTable.setWidget(row, col, description);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		flexTable.setText(row, col++, "Ghi chú");
		flexTable.setWidget(row, col, notes);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		
		flexTable.getCellFormatter().setWidth(row, 0, "100px");
		flexTable.getCellFormatter().setWidth(row, 1, "220px");
		flexTable.getCellFormatter().setWidth(row, 2, "100px");
		flexTable.getCellFormatter().setWidth(row, 3, "220px");
		
		//flexTable.setWidget(row, 0, cancel);
		//flexTable.setWidget(row, 3, save);
		//flexTable.getCellFormatter().setHeight(row, 0, "100px");
		//flexTable.getCellFormatter().setHorizontalAlignment(row, 3, ALIGN_RIGHT);
		
		row = 0; col = 0;
		reason.setWidth("200px");
		place.setWidth("200px");
		bookingNotes.setWidth("200px");

		bookingFlexTable.setText(row, col++, "Người mượn");
		bookingFlexTable.setWidget(row, col++, teacherList);
		bookingFlexTable.setText(row, col++, "Địa điểm");
		bookingFlexTable.setWidget(row, col++, place);
		row ++; col = 0;
		HorizontalPanel[] timePanel = new HorizontalPanel[2];
		for (int i = 0; i <= 1; i ++) {
			datePicker[i] = new DatePicker();
			dateBox[i] = new DateBox(datePicker[i], null, new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd/MM/yyyy")));
			dateBox[i].setWidth("80px");
			dateBox[i].getDatePicker().setYearArrowsVisible(true);
			hour[i] = new ListBox();
			for (int j = 0; j <= 24; j ++)
				hour[i].addItem(j + "h", "" + j);
			minute[i] = new ListBox();
			for (int j = 0; j <= 60; j ++)
				minute[i].addItem(j + "m", "" + j);
			timePanel[i] = new HorizontalPanel();
			timePanel[i].add(dateBox[i]);
			timePanel[i].add(hour[i]);
			timePanel[i].add(minute[i]);
		}
		
		bookingFlexTable.setText(row, col++, "Từ");
		bookingFlexTable.setWidget(row, col++, timePanel[0]);
		bookingFlexTable.setText(row, col++, "Đến");
		bookingFlexTable.setWidget(row, col++, timePanel[1]);
		row ++; col = 0;
		bookingFlexTable.setText(row, col++, "Lí do");
		bookingFlexTable.setWidget(row, col++, reason);
		bookingFlexTable.setText(row, col++, "Ghi chú");
		bookingFlexTable.setWidget(row, col++, bookingNotes);
		row ++; col = 0;
		contentPanel.add(teacherViewPanel);
	}
	
	@Override
	public HasClickHandlers getSaveButton() {
		return this.save;
	}
	
	@Override
	public HasClickHandlers getBookingSaveButton() {
		return this.bookingSave;
	}
	
	@Override
	public HasClickHandlers getCancelButton() {
		return this.cancel;
	}
	
	@Override
	public HasClickHandlers getAddMoreButton() {
		return this.addMore;
	}

	@Override
	public Equipment getEquipment() {
		if (name.getText().trim().isEmpty()) {
			Window.alert("Tên thiết bị không được để trống");
			return null;
		}
		if (currentEquipment == null)
			currentEquipment = new Equipment();
		currentEquipment.setName(name.getText().trim());
		currentEquipment.setSerialNumber(serialNumber.getText().trim());
		currentEquipment.setIdCode(idCode.getText().trim());
		currentEquipment.setMadeFrom(Integer.parseInt(madeFrom.getSelectedValue()));
		currentEquipment.setFromMonth(Integer.parseInt(fromMonth.getSelectedValue()));
		currentEquipment.setFromYear(Integer.parseInt(fromYear.getSelectedValue()));
		currentEquipment.setNotes(notes.getText().trim());
		currentEquipment.setFinanceBy(Integer.parseInt(financeBy.getSelectedValue()));
		currentEquipment.setQuantity(Integer.parseInt(quantity.getSelectedValue()));
		currentEquipment.setStatus(Integer.parseInt(status.getSelectedValue()));
		currentEquipment.setDescription(description.getText().trim());
		try {
			currentEquipment.setPrice(Long.parseLong(price.getText().trim()));
		}
		catch (Exception e) {
			Window.alert("Kiểm tra lại giá tiền");
		}
		if (dateBox[0].getValue() != null && dateBox[1].getValue() != null) {
			EquipmentHistory history = new EquipmentHistory();
			Teacher t = ClientData.getTeacherMap().get(Long.parseLong(teacherList.getSelectedValue()));
			if (t != null) {
				history.setUserId(t.getId());
				history.setUserName(t.getFullName());
				history.setFromDate(dateBox[0].getValue());
				history.setToDate(dateBox[1].getValue());
				history.setFromHour(Integer.parseInt(hour[0].getSelectedValue()));
				history.setFromMinute(Integer.parseInt(minute[0].getSelectedValue()));
				history.setToHour(Integer.parseInt(hour[1].getSelectedValue()));
				history.setToMinute(Integer.parseInt(minute[1].getSelectedValue()));
				history.setPlace(place.getText().trim());
				history.setReason(reason.getText().trim());
				history.setNotes(bookingNotes.getText().trim());
				history.setEquipmentId(currentEquipment.getId());
				currentEquipment.setCurrentHistory(history);
			}
		}
		return currentEquipment;
	}
	
	@Override
	public EquipmentHistory getBooking() {
		if (dateBox[0].getValue() == null || dateBox[1].getValue() == null) {
			Window.alert("Xin hãy chọn thời gian");
			return null;
		}
		if (currentBooking == null)
			currentBooking = new EquipmentHistory();
		Long id = Long.parseLong(teacherList.getSelectedValue());
		Teacher t = ClientData.getTeacherMap().get(id);
		if (t == null) {
			Window.alert("Xin hãy chọn giáo viên");
			return null;
		}
		currentBooking.setUserId(t.getId());
		currentBooking.setUserName(t.getFullName());
		currentBooking.setFromDate(dateBox[0].getValue());
		currentBooking.setToDate(dateBox[1].getValue());
		currentBooking.setFromHour(Integer.parseInt(hour[0].getSelectedValue()));
		currentBooking.setFromMinute(Integer.parseInt(minute[0].getSelectedValue()));
		currentBooking.setToHour(Integer.parseInt(hour[1].getSelectedValue()));
		currentBooking.setToMinute(Integer.parseInt(minute[1].getSelectedValue()));
		currentBooking.setPlace(place.getText().trim());
		currentBooking.setReason(reason.getText().trim());
		currentBooking.setNotes(bookingNotes.getText().trim());
		currentBooking.setEquipmentId(currentEquipment.getId());
		return currentBooking;
	}
	
	@Override
	public void refreshView() {
		super.refreshView();
		equipment.setStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE, true);
	}
	
	@Override
	public DialogBox getDialogBox() {
		return this.dialogBox;
	}
	
	@Override
	public void edit(Equipment equipment) {
		if (equipment == null)
			equipment = new Equipment();
		currentEquipment = equipment;
		if (dialogBox == null) {
			dialogBox = new DialogBox(true);
			dialogBox.setWidth("640px");
		    dialogBox.setGlassEnabled(true);
		    dialogBox.setAnimationEnabled(false);
		}
	    // Create a table to layout the content
	    VerticalPanel dialogContents = new VerticalPanel();
	    dialogContents.setWidth("100%");
	    dialogContents.setSpacing(5);
	    dialogBox.setWidget(dialogContents);
	    dialogBox.setText(equipment != null ? "Sửa thông tin" : "Tạo mới");
	    flexTable.setWidth("100%");
	    dialogContents.add(flexTable);
	    dialogContents.setCellHorizontalAlignment(flexTable, ALIGN_CENTER);
	    HorizontalPanel bottomPanel = new HorizontalPanel();
	    bottomPanel.setWidth("100%");
	    bottomPanel.add(cancel);
	    bottomPanel.add(save);
	    bottomPanel.setCellHorizontalAlignment(cancel, ALIGN_LEFT);
	    bottomPanel.setCellHorizontalAlignment(save, ALIGN_RIGHT);
	    dialogContents.add(bottomPanel);
	    
		name.setText(equipment.getName());
		serialNumber.setText(equipment.getSerialNumber());
		idCode.setText(equipment.getIdCode());
		notes.setText(equipment.getNotes());
		for (int i = 0; i < fromMonth.getItemCount(); i ++) {
			if (fromMonth.getValue(i).equalsIgnoreCase(equipment.getFromMonth() + "")) {
				fromMonth.setSelectedIndex(i);
			}
		}
		for (int i = 0; i < fromYear.getItemCount(); i ++) {
			if (fromYear.getValue(i).equalsIgnoreCase(equipment.getFromYear() + "")) {
				fromYear.setSelectedIndex(i);
			}
		}
		for (int i = 0; i < status.getItemCount(); i ++) {
			if (status.getValue(i).equalsIgnoreCase(equipment.getStatus() + "")) {
				status.setSelectedIndex(i);
			}
		}
		madeFrom.setSelectedIndex(equipment.getMadeFrom());
		financeBy.setSelectedIndex(equipment.getFinanceBy());
		description.setText(equipment.getDescription());
		price.setText(equipment.getPrice() + "");
		quantity.setSelectedIndex(equipment.getQuantity() - 1);
	    dialogBox.center();
        dialogBox.show();
	}
	
	@Override
	public void edit(EquipmentHistory history) {
		if (history == null)
			history = new EquipmentHistory();
		currentBooking = history;
		if (dialogBox == null) {
			dialogBox = new DialogBox(true);
			dialogBox.setWidth("640px");
		    dialogBox.setGlassEnabled(true);
		    dialogBox.setAnimationEnabled(false);
		}
	    // Create a table to layout the content
	    VerticalPanel dialogContents = new VerticalPanel();
	    dialogContents.setWidth("100%");
	    dialogContents.setSpacing(5);
	    dialogBox.setWidget(dialogContents);
	    dialogBox.setText("Đăng ký sử dụng");
    	dialogContents.add(bookingFlexTable);
    	if (ClientData.getCurrentUser().isAdmin()) {
	    	teacherList.addItem("[Chọn giáo viên]", "0");
		    for (Teacher tc : ClientData.getTeachers())
		    	teacherList.addItem(tc.getFullName(), tc.getId() + "");
    	}
    	else
    		teacherList.addItem(ClientData.getCurrentUser().getFullName(), ClientData.getCurrentUser().getId() + "");
	    HorizontalPanel bottomPanel = new HorizontalPanel();
	    bottomPanel.setWidth("100%");
	    bottomPanel.add(cancel);
	    bottomPanel.add(bookingSave);
	    bottomPanel.setCellHorizontalAlignment(cancel, ALIGN_LEFT);
	    bottomPanel.setCellHorizontalAlignment(bookingSave, ALIGN_RIGHT);
	    dialogContents.add(bottomPanel);
	    dialogBox.center();
        dialogBox.show();
	}
	
	/**
	   * The provider that holds the list of contacts in the database.
	   */
	  private ListDataProvider<Equipment> dataProvider = new ListDataProvider<Equipment>();
	
	/**
     * The key provider that provides the unique ID of a contact.
     */
    public static final ProvidesKey<Equipment> KEY_PROVIDER = new ProvidesKey<Equipment>() {
      @Override
      public Object getKey(Equipment item) {
        return item == null ? null : item.getId();
      }
    };
	
    @Override
	public  void view(List<Equipment> equipments) {
	    // Create the table.
//		if (teachers == null || teachers.isEmpty()) {
//			teachers = dataProvider.getList();
//			for (int i = 0; i < 10; i ++) {
//				Equipment t = new Equipment("Van " + i, "Nguyen", Random.nextInt() + "", Random.nextInt(2), Random.nextInt(4), Random.nextInt(4), "asdfasfd@gmail.com", Random.nextInt(1000000) + "");
//				teachers.add(t);
//			}
//		}
		for (int i = 0; i < equipments.size(); i++) {
			equipments.get(i).setPosIndex(i + 1);
		}
		dataProvider.setList(equipments);
	    editableCells = new ArrayList<AbstractEditableCell<?, ?>>();
	    dataGrid = new DataGrid<Equipment>(25, KEY_PROVIDER);
	    
	    dataGrid.setWidth("100%");
	    dataGrid.setHeight("600px");
	    
	    //teacherList.setMinimumTableWidth(140, Unit.EM);
	    dataProvider.addDataDisplay(dataGrid);
	    
	    teacherViewPanel.clear();
	    teacherViewPanel.add(addMore);
	    teacherViewPanel.add(dataGrid);
	    teacherViewPanel.setCellHorizontalAlignment(addMore, ALIGN_CENTER);
	    
	    // NumberCell.
	    Column<Equipment, Number> numberColumn =
	        addColumn(new NumberCell(), "#", new GetValue<Number>() {
	          @Override
	          public Number getValue(Equipment contact) {
	            return contact.getPosIndex();
	          }
	        }, null);
	    dataGrid.setColumnWidth(numberColumn, 40, Unit.PX);
	    numberColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LOCALE_END);
	    
	    // ClickableTextCell.
	    Column<Equipment, String> nameCol = addColumn(new ClickableTextCell(), "Tên thiết bị", new GetValue<String>() {
	      @Override
	      public String getValue(Equipment contact) {
	        return contact.getName();
	      }
	    }, new FieldUpdater<Equipment, String>() {
	      @Override
	      public void update(int index, Equipment object, String value) {
	      }
	    });
	    dataGrid.setColumnWidth(nameCol, 150, Unit.PX);

	    // TextCell.
	    Column<Equipment, String> quantityCol = addColumn(new TextCell(), "SL", new GetValue<String>() {
	      @Override
	      public String getValue(Equipment contact) {
	        return contact.getQuantity() + "";
	      }
	    }, null);
	    dataGrid.setColumnWidth(quantityCol, 50, Unit.PX);

	    // TextCell.
	    Column<Equipment, String> serialCol = addColumn(new TextCell(), "Số Serial", new GetValue<String>() {
	      @Override
	      public String getValue(Equipment contact) {
	        return contact.getSerialNumber();
	      }
	    }, null);
	    dataGrid.setColumnWidth(serialCol, 100, Unit.PX);
	    
	    // TextCell.
	    Column<Equipment, String> idCodeCol = addColumn(new TextCell(), "Mã số", new GetValue<String>() {
	      @Override
	      public String getValue(Equipment contact) {
	        return contact.getIdCode();
	      }
	    }, null);
	    dataGrid.setColumnWidth(idCodeCol, 100, Unit.PX);
	    
	    // TextCell.
	    Column<Equipment, String> madeFromCol = addColumn(new TextCell(), "Xuất sứ", new GetValue<String>() {
	      @Override
	      public String getValue(Equipment contact) {
	        return Config.getMadeFrom(contact.getMadeFrom());
	      }
	    }, null);
	    dataGrid.setColumnWidth(madeFromCol, 70, Unit.PX);
	    
//	    // TextCell.
//	    Column<Equipment, String> fromYearCol = addColumn(new TextCell(), "Ngày nhập", new GetValue<String>() {
//	      @Override
//	      public String getValue(Equipment contact) {
//	        return contact.getFromYear() + "";
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(fromYearCol, 90, Unit.PX);
//	    
//	    // TextCell.
//	    Column<Equipment, String> priceCol = addColumn(new TextCell(), "Giá tiền", new GetValue<String>() {
//	      @Override
//	      public String getValue(Equipment contact) {
//	        return contact.getPrice() + "";
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(priceCol, 70, Unit.PX);
//
//	    
//	    // SafeHtmlCell.
//	    Column<Equipment, SafeHtml> statusCol = addColumn(new SafeHtmlCell(), "Tình trạng", new GetValue<SafeHtml>() {
//	      @Override
//	      public SafeHtml getValue(Equipment contact) {
//	    	  String info = contact.getStatus() + "%";
//	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
//	    	  builder.appendHtmlConstant(info);
//	    	  return builder.toSafeHtml();
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(statusCol, 90, Unit.PX);
//	    
//	    // TextCell.
//	    Column<Equipment, String> financeByCol = addColumn(new TextCell(), "Nguồn tiền", new GetValue<String>() {
//	      @Override
//	      public String getValue(Equipment contact) {
//	        return Config.getFinanceBy(contact.getFinanceBy());
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(financeByCol, 100, Unit.PX);
	    
	    // TextCell.
	    Column<Equipment, String> notesCol = addColumn(new TextCell(), "Ghi chú", new GetValue<String>() {
	      @Override
	      public String getValue(Equipment contact) {
	        return contact.getNotes();
	      }
	    }, null);

	    // ClickableTextCell.
	    Column<Equipment, String> statusCol = addColumn(new ClickableTextCell(), "Trạng thái", new GetValue<String>() {
	      @Override
	      public String getValue(Equipment contact) {
	    	  if (contact.getCurrentHistory() != null) {
	    		  return "Occupied";
	    	  }
	    	  else {
	    		  return "Available";
	    	  }
	      }
	    }, new FieldUpdater<Equipment, String>() {
	      @Override
	      public void update(int index, Equipment object, String value) {
	      }
	    });
	    dataGrid.setColumnWidth(statusCol, 100, Unit.PX);
	    
	    // DateCell.
//	    DateTimeFormat dateFormat = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
//	    Column<Equipment, Date> dateCol = addColumn(new DateCell(dateFormat), "Ngày sinh", new GetValue<Date>() {
//	      @Override
//	      public Date getValue(Equipment contact) {
//	        return contact.getBirthDate();
//	      }
//	    }, null);
//	    teacherList.setColumnWidth(dateCol, 50, Unit.PX);
	    
	    if (ClientData.isLoggedIn()) {
	    	dataGrid.setColumnWidth(notesCol, 150, Unit.PX);
	    	
	    	if (ClientData.getCurrentUser().isAdmin()) {
		    	// ActionCell.
			    Column<Equipment, Equipment> editCol = addColumn(new ActionCell<Equipment>("Sửa", new ActionCell.Delegate<Equipment>() {
			      @Override
			      public void execute(Equipment contact) {
			        edit(contact);
			      }
			    }), "Sửa", new GetValue<Equipment>() {
			      @Override
			      public Equipment getValue(Equipment contact) {
			        return contact;
			      }
			    }, null);
			    dataGrid.setColumnWidth(editCol, 70, Unit.PX);
	    	}
//	
		    // ButtonCell.
		    Column<Equipment, String> delCol = addColumn(new ButtonCell(), "Đăng ký", new GetValue<String>() {
		      @Override
		      public String getValue(Equipment contact) {
		    	  return "Đăng ký";
		    	  //return "Click " + contact.getFirstName();
		      }
		    }, new FieldUpdater<Equipment, String>() {
		      @Override
		      public void update(int index, Equipment object, String value) {
		        edit(new EquipmentHistory());
		      }
		    });
	    }
	    //teacherList.setColumnWidth(delCol, 70, Unit.PX);
	}
	
	/**
	   * The list of cells that are editable.
	   */
	  private List<AbstractEditableCell<?, ?>> editableCells;
	
	 /**
	   * Get a cell value from a record.
	   * 
	   * @param <C> the cell type
	   */
	  private static interface GetValue<C> {
	    C getValue(Equipment contact);
	  }
	
	/**
	   * Add a column with a header.
	   * 
	   * @param <C> the cell type
	   * @param cell the cell used to render the column
	   * @param headerText the header string
	   * @param getter the value getter for the cell
	   */
	  private <C> Column<Equipment, C> addColumn(Cell<C> cell, String headerText,
	      final GetValue<C> getter, FieldUpdater<Equipment, C> fieldUpdater) {
	    Column<Equipment, C> column = new Column<Equipment, C>(cell) {
	      @Override
	      public C getValue(Equipment object) {
	        return getter.getValue(object);
	      }
	    };
	    column.setFieldUpdater(fieldUpdater);
	    if (cell instanceof AbstractEditableCell<?, ?>) {
	      editableCells.add((AbstractEditableCell<?, ?>) cell);
	    }
	    dataGrid.addColumn(column, headerText);
	    return column;
	  }
}
