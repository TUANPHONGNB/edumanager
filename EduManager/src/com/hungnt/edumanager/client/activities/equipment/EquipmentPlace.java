package com.hungnt.edumanager.client.activities.equipment;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.hungnt.edumanager.client.activities.basic.BasicPlace;

public class EquipmentPlace extends BasicPlace {
	
	public EquipmentPlace() {
		super();
	}

	@Override
	public String getToken() {
		return "teacher";
	}
	
	public static class Tokenizer implements PlaceTokenizer<EquipmentPlace> {
        @Override
        public String getToken(EquipmentPlace place) {
            return place.getToken();
        }

        @Override
        public EquipmentPlace getPlace(String token) {
            return new EquipmentPlace();
        }
    }	
}