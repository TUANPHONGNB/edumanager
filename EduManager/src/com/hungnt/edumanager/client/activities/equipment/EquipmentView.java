package com.hungnt.edumanager.client.activities.equipment;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.DialogBox;
import com.hungnt.edumanager.client.activities.basic.BasicView;
import com.hungnt.edumanager.shared.Equipment;
import com.hungnt.edumanager.shared.EquipmentHistory;

public interface EquipmentView extends BasicView{
	HasClickHandlers getSaveButton();
	HasClickHandlers getAddMoreButton();
	void edit(Equipment equipment);
	void view(List<Equipment> equipments);
	DialogBox getDialogBox();
	HasClickHandlers getCancelButton();
	Equipment getEquipment();
	void edit(EquipmentHistory history);
	EquipmentHistory getBooking();
	HasClickHandlers getBookingSaveButton();
}
