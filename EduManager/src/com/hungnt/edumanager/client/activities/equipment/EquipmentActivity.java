package com.hungnt.edumanager.client.activities.equipment;

import java.util.Date;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.RPCCall;
import com.hungnt.edumanager.client.activities.ClientFactory;
import com.hungnt.edumanager.client.activities.basic.BasicActivity;
import com.hungnt.edumanager.shared.Equipment;
import com.hungnt.edumanager.shared.EquipmentHistory;

public class EquipmentActivity extends BasicActivity {
	
	private EquipmentView view;
	
	public EquipmentActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getEquipmentView();
		panel.setWidget(view.asWidget());
		super.start(panel, eventBus, view);
		if (ClientData.getEquipments().size() > 0)
			view.view(ClientData.getEquipments());
		else
			loadData();
	}
	
	@Override
	protected void bind() {
		super.bind();
		view.getSaveButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final Equipment object = view.getEquipment();
				if (object == null)
					return;
				new RPCCall<Equipment>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Có lỗi xảy ra, không thể lưu được thiết bị");
					}

					@Override
					public void onSuccess(Equipment result) {
						view.getDialogBox().hide();
						loadData();
					}

					@Override
					protected void callService(AsyncCallback<Equipment> cb) {
						EduManager.dataService.updateEquipment(ClientData.getCurrentUser().getId() + "",
								object, cb);
					}
				}.retry();
			}
		});
		view.getBookingSaveButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final EquipmentHistory object = view.getBooking();
				if (object == null)
					return;
				new RPCCall<EquipmentHistory>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Có lỗi xảy ra, không thể đăng ký sử dụng thiết bị");
					}

					@Override
					public void onSuccess(EquipmentHistory result) {
						view.getDialogBox().hide();
						loadData();
					}

					@Override
					protected void callService(AsyncCallback<EquipmentHistory> cb) {
						EduManager.dataService.updateEquipmentHistory(object, cb);
					}
				}.retry();
			}
		});
		view.getAddMoreButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (!ClientData.isLoggedIn()) {
					view.getLoginView().view();
					return;
				}
				view.edit(new Equipment());
			}
		});
		view.getCancelButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				view.getDialogBox().hide();
			}
		});
	}
		
	private void loadData() {
		new RPCCall<List<Equipment>>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Có lỗi xảy ra, không thể tải dữ liệu");
			}

			@Override
			public void onSuccess(List<Equipment> result) {
				ClientData.setEquipments(result);
				view.view(result);
			}

			@Override
			protected void callService(AsyncCallback<List<Equipment>> cb) {
				EduManager.dataService.getEquipments(null, new Date(), cb);
				
			}}.retry();
	}
 }
