package com.hungnt.edumanager.client.activities.meeting;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.hungnt.edumanager.shared.Teacher;

public class AbsentPanel extends HorizontalPanel {
	
	private Anchor nameAnchor = new Anchor();
	private TextBox reason = new TextBox();
	private Teacher teacher = null;
	
	public AbsentPanel(Teacher teacher) {
		this.teacher = teacher;
		this.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		nameAnchor.setHTML(teacher.getFullName() + "[X]&nbsp;&nbsp;&nbsp;");
		this.add(nameAnchor);
		this.add(reason);
		reason.getElement().setPropertyString("placeholder", "Lý do vắng mặt");
	}
	
	public Teacher getTeacher() {
		return this.teacher;
	}
	
	public HasClickHandlers getAnchor() {
		return this.nameAnchor;
	}
	
	public TextBox getReason() {
		return this.reason;
	}
	
	
}
