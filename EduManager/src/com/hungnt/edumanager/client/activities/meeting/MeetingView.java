package com.hungnt.edumanager.client.activities.meeting;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.hungnt.edumanager.client.activities.basic.BasicView;
import com.hungnt.edumanager.shared.MeetingMinutes;

public interface MeetingView extends BasicView{
	HasClickHandlers getSaveButton();
	HasClickHandlers getAddMoreButton();
	void edit(MeetingMinutes equipment);
	void view(List<MeetingMinutes> equipments);
	HasClickHandlers getCancelButton();
	MeetingMinutes getMeetingMinute();
	MeetingEditer getEditor();
}
