package com.hungnt.edumanager.client.activities.meeting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.cell.client.AbstractEditableCell;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.CssToken;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;
import com.hungnt.edumanager.shared.Category;
import com.hungnt.edumanager.shared.MeetingMinutes;

public class MeetingViewImpl extends BasicViewImpl implements MeetingView {
	
	private VerticalPanel eventViewPanel = new VerticalPanel();
	private MeetingEditer eventEditor = new MeetingEditer();
	private MeetingViewer viewer = new MeetingViewer();
	private Button addMore = new Button("Thêm");
	
	DataGrid<MeetingMinutes> dataGrid;// = new DataGrid<Event>();

	public MeetingViewImpl() {	
		super();
		eventViewPanel.setSpacing(5);
		eventViewPanel.setWidth("100%");
		eventViewPanel.add(addMore);
		contentPanel.add(eventViewPanel);
		viewer.getCancelButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				contentPanel.clear();
				contentPanel.add(addMore);
				contentPanel.add(eventViewPanel);
			}
		});
	}
	
	@Override
	public HasClickHandlers getSaveButton() {
		return this.eventEditor.getSaveButton();
	}
	
	@Override
	public HasClickHandlers getCancelButton() {
		return this.eventEditor.getCancelButton();
	}
	
	@Override
	public HasClickHandlers getAddMoreButton() {
		return this.addMore;
	}

	@Override
	public MeetingMinutes getMeetingMinute() {
		return this.eventEditor.getMeetingMinute();
	}
	
	@Override
	public MeetingEditer getEditor() {
		return this.eventEditor;
	}
	
	@Override
	public void refreshView() {
		super.refreshView();
		meeting.setStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE, true);
	}
	
	@Override
	public void edit(MeetingMinutes event) {
		contentPanel.clear();
		contentPanel.add(eventEditor.getContentPanel());
		eventEditor.edit(event);
	}
	
	/**
	   * The provider that holds the list of contacts in the database.
	   */
	  private ListDataProvider<MeetingMinutes> dataProvider = new ListDataProvider<MeetingMinutes>();
	
	/**
     * The key provider that provides the unique ID of a contact.
     */
    public static final ProvidesKey<MeetingMinutes> KEY_PROVIDER = new ProvidesKey<MeetingMinutes>() {
      @Override
      public Object getKey(MeetingMinutes item) {
        return item == null ? null : item.getId();
      }
    };
	
    @Override
	public  void view(List<MeetingMinutes> events) {
    	contentPanel.clear();
    	contentPanel.add(addMore);
    	contentPanel.add(eventViewPanel);
    	Collections.sort(events, new Comparator<MeetingMinutes>() {
			@Override
			public int compare(MeetingMinutes o1, MeetingMinutes o2) {
				if (o1.getDate() == null || o2.getDate() == null)
					return 0;
				if (o1.getDate().after(o2.getDate()))
					return -1;
				else if (o1.getDate().before(o2.getDate()))
					return 1;
				else
					return 0;
			}
		});
//		ClientUtils.log("Event y2");
//		for (int i = 0; i < events.size(); i++) {
//			events.get(i).setPosIndex(i + 1);
//		}
//		ClientUtils.log("Event y3");

		dataProvider.setList(events);
	    editableCells = new ArrayList<AbstractEditableCell<?, ?>>();
	    dataGrid = new DataGrid<MeetingMinutes>(25, KEY_PROVIDER);
	    
	    dataGrid.setWidth("100%");
	    dataGrid.setHeight("600px");
	    
	    //teacherList.setMinimumTableWidth(140, Unit.EM);
	    dataProvider.addDataDisplay(dataGrid);
	    
	    eventViewPanel.clear();
	    eventViewPanel.add(addMore);
	    eventViewPanel.add(dataGrid);
	    eventViewPanel.setCellHorizontalAlignment(addMore, ALIGN_CENTER);

	    // SafeHtmlCell.
	    Column<MeetingMinutes, SafeHtml> infoCol = addColumn(new SafeHtmlCell(), "Thời gian", new GetValue<SafeHtml>() {
	      @Override
	      public SafeHtml getValue(MeetingMinutes object) {
	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
	    	  builder.appendHtmlConstant(DateTimeFormat.getFormat("dd-MM-yyyy").format(object.getDate()));
	    	  return builder.toSafeHtml();
	      }
	    }, null);
	    dataGrid.setColumnWidth(infoCol, 100, Unit.PX);

	    // ClickableTextCell.
	    Column<MeetingMinutes, String> nameCol = addColumn(new ClickableTextCell(), "Tiêu đề", new GetValue<String>() {
	      @Override
	      public String getValue(MeetingMinutes contact) {
	        return contact.getTitle();
	      }
	    }, new FieldUpdater<MeetingMinutes, String>() {
	      @Override
	      public void update(int index, MeetingMinutes object, String value) {
	    	  contentPanel.clear();
	    	  contentPanel.add(viewer.getContentPanel());
	    	  viewer.setMeetingMinute(object);
	      }
	    });
//
//	    // TextCell.
//	    Column<Event, String> quantityCol = addColumn(new TextCell(), "Mô tả", new GetValue<String>() {
//	      @Override
//	      public String getValue(Event contact) {
//	        return contact.getDescription();
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(quantityCol, 150, Unit.PX);
//
//	    // TextCell.
//	    Column<Event, SafeHtml> placeTimeCol = addColumn(new SafeHtmlCell(), "Thời gian/Địa điểm", new GetValue<SafeHtml>() {
//	      @Override
//	      public SafeHtml getValue(Event contact) {
//	    	  String info = contact.getTime() + "<br>" + contact.getPlace();
//	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
//	    	  builder.appendHtmlConstant(info);
//	    	  return builder.toSafeHtml();
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(placeTimeCol, 100, Unit.PX);
//	    
//	    // TextCell.
//	    Column<Event, SafeHtml> involvedUsers = addColumn(new SafeHtmlCell(), "Tham gia", new GetValue<SafeHtml>() {
//	      @Override
//	      public SafeHtml getValue(Event contact) {
//	    	  String info = "";
//	    	  for (String name : contact.getInvolvedUserNames())
//	    		  info += name + "<br>";
//	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
//	    	  builder.appendHtmlConstant(info);
//	        return builder.toSafeHtml();
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(involvedUsers, 150, Unit.PX);
//	    
//	    // TextCell.
//	    Column<Event, String> notesCol = addColumn(new TextCell(), "Ghi chú", new GetValue<String>() {
//	      @Override
//	      public String getValue(Event contact) {
//	        return contact.getNotes();
//	      }
//	    }, null);

//	    if (ClientData.isLoggedIn()) {
//	    	if (ClientData.getCurrentUser().isAdmin()) {
//	    		 dataGrid.setColumnWidth(nameCol, 700, Unit.PX);
//		    	// ActionCell.
//			    Column<MeetingMinutes, MeetingMinutes> editCol = addColumn(new ActionCell<MeetingMinutes>("Sửa", 
//			    		new ActionCell.Delegate<MeetingMinutes>() {
//			      @Override
//			      public void execute(MeetingMinutes contact) {
//			        edit(contact);
//			      }
//			    }), "Sửa", new GetValue<MeetingMinutes>() {
//			      @Override
//			      public MeetingMinutes getValue(MeetingMinutes contact) {
//			        return contact;
//			      }
//			    }, null);
//			    //dataGrid.setColumnWidth(editCol, 70, Unit.PX);
//	    	}
//	    }
	}
	
	/**
	   * The list of cells that are editable.
	   */
	  private List<AbstractEditableCell<?, ?>> editableCells;
	
	 /**
	   * Get a cell value from a record.
	   * 
	   * @param <C> the cell type
	   */
	  private static interface GetValue<C> {
	    C getValue(MeetingMinutes contact);
	  }
	
	/**
	   * Add a column with a header.
	   * 
	   * @param <C> the cell type
	   * @param cell the cell used to render the column
	   * @param headerText the header string
	   * @param getter the value getter for the cell
	   */
	  private <C> Column<MeetingMinutes, C> addColumn(Cell<C> cell, String headerText,
	      final GetValue<C> getter, FieldUpdater<MeetingMinutes, C> fieldUpdater) {
	    Column<MeetingMinutes, C> column = new Column<MeetingMinutes, C>(cell) {
	      @Override
	      public C getValue(MeetingMinutes object) {
	        return getter.getValue(object);
	      }
	    };
	    column.setFieldUpdater(fieldUpdater);
	    if (cell instanceof AbstractEditableCell<?, ?>) {
	      editableCells.add((AbstractEditableCell<?, ?>) cell);
	    }
	    dataGrid.addColumn(column, headerText);
	    return column;
	  }
}
