package com.hungnt.edumanager.client.activities.meeting;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.widget.HListBox;
import com.hungnt.edumanager.shared.MeetingAbsent;
import com.hungnt.edumanager.shared.Teacher;

public class ParticipantSelector extends VerticalPanel {
	private HListBox authorList = new HListBox();
	private List<Long> selectedIds = new ArrayList<Long>();
	private List<String> selectedNames = new ArrayList<String>();
	private List<AbsentPanel> absentPanels = new ArrayList<AbsentPanel>();
	
	public ParticipantSelector() {
		this.add(authorList);
		authorList.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				if (authorList.getSelectedValue().isEmpty())
					return;
				final Long id = Long.parseLong(authorList.getSelectedValue());
				final String name = authorList.getSelectedItemText();
				if (!selectedIds.contains(id)) {
					selectedIds.add(id);
					selectedNames.add(name);
					final AbsentPanel panel = new AbsentPanel(ClientData.getTeacherMap().get(id));
					panel.getAnchor().addClickHandler(new ClickHandler() {
						
						@Override
						public void onClick(ClickEvent event) {
							selectedIds.remove(id);
							selectedNames.remove(name);
							panel.removeFromParent();
							absentPanels.remove(panel);
						}
					});
					absentPanels.add(panel);
					ParticipantSelector.this.add(panel);
				}
			}
		});
	}
	
	public List<Long> getSelectedIds() {
		return this.selectedIds;
	}
	
	public List<Long> getUnselectedIds() {
		List<Long> list = new ArrayList<Long>();
		for (int i = 0; i < authorList.getItemCount(); i ++) {
			Long id = Long.parseLong(authorList.getSelectedValue());
			if (id > 0 && !selectedIds.contains(id))
				list.add(id);
		}
		return list;
	}
	
	public List<String> getSelectedStrings() {
		return this.selectedNames;
	}
	
	public List<MeetingAbsent> getAbsentReasons() {
		List<MeetingAbsent> absents = new ArrayList<MeetingAbsent>();
		for (AbsentPanel panel : absentPanels) {
			MeetingAbsent absent = new MeetingAbsent();
			absent.setTeacherId(panel.getTeacher().getId());
			absent.setTeacherName(panel.getTeacher().getFullName());
			absent.setReason(panel.getReason().getText().trim());
			absents.add(absent);
		}
		return absents;
	}
	
	public void setupAuthors(List<Teacher> teachers) {
		authorList.clear();
		authorList.addItem("[Chọn]", "");
		for (Teacher s : teachers) {
			if (s.getStatus() == 0) {
				authorList.addItem(s.getFullName(), s.getId() + "");
			}
		}
	}
	
//	public void setSelectedIds(List<Long> ids) {
//		selectedIds = ids;
//		selectedNames.clear();
//		for (int i = 0; i < authorList.getItemCount(); i ++) {
//			String val = authorList.getValue(i);
//			if (val != null && !val.isEmpty()) {
//				Long id = Long.parseLong(val);
//				if (selectedIds.contains(id)) {
//					selectedNames.add(authorList.getItemText(i));
//					addNameFlow(id, authorList.getItemText(i));
//				}
//			}
//		}
//	}
}