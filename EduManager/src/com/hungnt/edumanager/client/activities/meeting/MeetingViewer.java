package com.hungnt.edumanager.client.activities.meeting;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.hungnt.edumanager.shared.MeetingMinutes;

public class MeetingViewer {
	
	private Button cancel = new Button("Huỷ");
	private HTML title = new HTML();
	private HTML content = new HTML();
	private HTML place = new HTML();
	private HTML date = new HTML();
	private VerticalPanel contentPanel = new VerticalPanel();
	private HTML chairListBox = new HTML();
	private HTML secListBox = new HTML();
	private ScrollPanel scrollPanel = new ScrollPanel();
	private VerticalPanel filePanel = new VerticalPanel();
	
	private MeetingMinutes currentEvent = null;

	public MeetingViewer() {	
		super();
		contentPanel.setSpacing(5);
		contentPanel.setWidth("100%");
	    contentPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		contentPanel.add(title);
		
		HorizontalPanel timePanel = new HorizontalPanel();
		timePanel.add(date);
		timePanel.add(place);
		contentPanel.add(timePanel);
		
		HorizontalPanel hPanel0 = new HorizontalPanel();
		hPanel0.setSpacing(10);
		hPanel0.add(chairListBox);
		hPanel0.add(secListBox);
		contentPanel.add(hPanel0);
		
		scrollPanel.setWidget(content);
		contentPanel.add(scrollPanel);
		contentPanel.setCellHorizontalAlignment(scrollPanel, HasHorizontalAlignment.ALIGN_LEFT);
	    contentPanel.add(cancel);
	}
	
	public HasClickHandlers getCancelButton() {
		return this.cancel;
	}
	
	public void setMeetingMinute(MeetingMinutes meeting) {
		currentEvent = meeting;
		filePanel.clear();
		content.setHTML(currentEvent.getContent());
		title.setHTML("<h1>" + currentEvent.getTitle() + "</h1>");
		date.setHTML("Lúc: " + DateTimeFormat.getFormat("dd-MM-yyyy").format(meeting.getDate()) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		place.setHTML("Tại: " + currentEvent.getPlace());
		chairListBox.setHTML("Chủ trì: <b>" + currentEvent.getMeetingChair() + "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		secListBox.setHTML("Thư ký: <b>" + currentEvent.getReporter() + "</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		if (meeting.getAttacheds() != null && !meeting.getAttacheds().isEmpty()) {
			for (String url : meeting.getAttacheds()) {
				filePanel.add(new HTML("<a href='" + url + "'>" + url + "</a>"));
			}
		}
	}
	
	public Widget getContentPanel() {
		return this.contentPanel;
	}
}