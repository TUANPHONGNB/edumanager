package com.hungnt.edumanager.client.activities.meeting;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.hungnt.edumanager.client.activities.basic.BasicPlace;

public class MeetingPlace extends BasicPlace {
	
	public MeetingPlace() {
		super();
	}

	@Override
	public String getToken() {
		return "event";
	}
	
	public static class Tokenizer implements PlaceTokenizer<MeetingPlace> {
        @Override
        public String getToken(MeetingPlace place) {
            return place.getToken();
        }

        @Override
        public MeetingPlace getPlace(String token) {
            return new MeetingPlace();
        }
    }	
}