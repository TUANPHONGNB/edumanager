package com.hungnt.edumanager.client.activities.meeting;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.RPCCall;
import com.hungnt.edumanager.client.activities.ClientFactory;
import com.hungnt.edumanager.client.activities.basic.BasicActivity;
import com.hungnt.edumanager.shared.Category;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.MeetingMinutes;

public class MeetingActivity extends BasicActivity {
	
	private MeetingView view;
	
	public MeetingActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}
	
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getMeetingView();
		panel.setWidget(view.asWidget());
		super.start(panel, eventBus, view);
		root = new Category();
		root.setName("Tài liệu đính kèm");
		if (ClientData.getMeetingMinutes().size() > 0)
			view.view(ClientData.getMeetingMinutes());
		else
			loadData();
	}
	
	@Override
	protected void bind() {
		super.bind();
		view.getSaveButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final MeetingMinutes object = view.getMeetingMinute();
				if (object == null)
					return;
				new RPCCall<Long>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Có lỗi xảy ra, không thể lưu được thiết bị");
					}

					@Override
					public void onSuccess(Long result) {
						loadData();
					}

					@Override
					protected void callService(AsyncCallback<Long> cb) {
						EduManager.dataService.doUpdateMeeting(object, cb);
					}
				}.retry();
			}
		});
		view.getAddMoreButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (!ClientData.isLoggedIn()) {
					view.getLoginView().view();
					return;
				}
				view.edit(new MeetingMinutes());
			}
		});
		view.getCancelButton().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				view.view(ClientData.getMeetingMinutes());
			}
		});
		view.getEditor().getUploadButton().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				String fileName = view.getEditor().getUploadField().getFilename();
				if (fileName.length() == 0) {
					return;
				}
				EduManager.loadingView.show();
				view.getEditor().getUploadForm().setAction("/uploadfile?folder=meeting");
				view.getEditor().getUploadForm().submit();
			}
		});
		view.getEditor().getUploadForm().addSubmitCompleteHandler(new SubmitCompleteHandler() {
			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				EduManager.loadingView.hide();
				String json = event.getResults();
				if (json == null)
					return;
				JSONObject obj = (JSONObject) JSONParser.parseStrict(json);
				if (obj == null)
					return;
				String url = obj.get("url").isString().stringValue();
				view.getEditor().onFileAttached(url);
			}
		});
	}
	
	private void loadData() {
		new RPCCall<List<MeetingMinutes>>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Có lỗi xảy ra, không thể tải dữ liệu");
			}

			@Override
			public void onSuccess(List<MeetingMinutes> result) {
				ClientData.setMeetingMinutes(result);
				view.view(result);
			}

			@Override
			protected void callService(AsyncCallback<List<MeetingMinutes>> cb) {
				EduManager.dataService.getMeetings(Config.currentScholarYear, Config.currentSemester, cb);
				
			}}.retry();
	}
 }
