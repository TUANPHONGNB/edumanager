package com.hungnt.edumanager.client.activities.meeting;

import java.util.Date;
import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.widget.HListBox;
import com.hungnt.edumanager.shared.MeetingMinutes;
import com.hungnt.edumanager.shared.Teacher;

public class MeetingEditer {
	
	private Button save = new Button("Lưu");
	private Button cancel = new Button("Huỷ");
	private FlexTable flexTable = new FlexTable();
	private TextBox title = new TextBox();
	private RichTextArea description = new RichTextArea();
	private RichTextToolbar toolbar = new RichTextToolbar(description);
	private TextBox place = new TextBox();
	private DatePicker fromDatePicker = new DatePicker();
	private DateBox fromDateBox = null;
	private ParticipantSelector teacherSelector = new ParticipantSelector();
	private VerticalPanel contentPanel = new VerticalPanel();
	private HListBox chairListBox = new HListBox();
	private HListBox secListBox = new HListBox();
	private FormPanel uploadForm;
	private FormPanel hiddenUploadForm;
	private FileUpload uploadField;
	private Button uploadButton;
	private HorizontalPanel uploadPanel = new HorizontalPanel();
	private VerticalPanel filePanel = new VerticalPanel();
	
	private HListBox hour = new HListBox();
	private HListBox minute = new HListBox();
	
	private MeetingMinutes currentEvent = null;

	public MeetingEditer() {	
		super();
		uploadForm = new FormPanel();
		uploadField = new FileUpload();
		uploadButton = new Button("Tải file");
		uploadForm.setWidget(uploadField);
		//frame.setSize("400px","600px");
		uploadField.setName("image");
		filePanel.setWidth("100%");
		filePanel.add(uploadPanel);
		uploadPanel.add(uploadField);
		uploadPanel.add(uploadButton);
		uploadForm.add(filePanel);
		uploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
		uploadForm.setMethod(FormPanel.METHOD_POST);
		hiddenUploadForm = new FormPanel();
		hiddenUploadForm.setVisible(false);
		hiddenUploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
		hiddenUploadForm.setMethod(FormPanel.METHOD_POST);
		
		flexTable.setWidth("100%");
		title.setWidth("500px");
		description.setWidth("100%");
		description.setHeight("300px");
		place.setWidth("200px");
		teacherSelector.setWidth("500px");
		
		int row = 0, col = 0;
		flexTable.setText(row, col++, "Tiêu đề");
		flexTable.setWidget(row, col, title);
		row ++; col = 0;
		flexTable.setText(row, col++, "Thời gian");
		HorizontalPanel timePanel = new HorizontalPanel();
		fromDateBox = new DateBox(fromDatePicker, null, new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd/MM/yyyy")));
		fromDateBox.setWidth("100px");
		fromDateBox.getDatePicker().setYearArrowsVisible(true);
		timePanel.add(fromDateBox);
		timePanel.add(hour);
		timePanel.add(minute);
		timePanel.add(new HTML(" Tại "));
		timePanel.add(place);
		flexTable.setWidget(row, col++, timePanel);
		fromDateBox.setValue(new Date());
		row ++; col = 0;
		
		flexTable.setText(row, col++, "Chủ trì");
		HorizontalPanel hPanel0 = new HorizontalPanel();
		hPanel0.setSpacing(10);
		hPanel0.add(chairListBox);
		hPanel0.add(new HTML("Thư ký"));
		hPanel0.add(secListBox);
		flexTable.setWidget(row, col, hPanel0);
		row ++; col = 0;
		
		flexTable.setText(row, col++, "");
		flexTable.setWidget(row, col++, toolbar);
		row ++; col = 0;
		flexTable.setText(row, col++, "Nội dung");
		flexTable.setWidget(row, col++, description);
		row ++; col = 0;
		
		hour.setWidth("80px");
		minute.setWidth("80px");
		for (int j = 1; j <= 24; j++) {
			hour.addItem(j + " giờ", j + "");
		}
		for (int j = 1; j <= 60; j++) {
			minute.addItem(j + " phút", j + "");
		}
		
		row ++; col = 0;
		flexTable.setText(row, col++, "Vắng mặt");
		flexTable.setWidget(row, col++, teacherSelector);
		
		row ++; col = 0;
		flexTable.setText(row, col++, "File");
		flexTable.setWidget(row, col++, uploadForm);
		
		contentPanel.setSpacing(5);
		contentPanel.setWidth("100%");
	    flexTable.setWidth("100%");
	    contentPanel.add(flexTable);
	    contentPanel.setCellHorizontalAlignment(flexTable, HasHorizontalAlignment.ALIGN_CENTER);
	    
	    HorizontalPanel hPanel = new HorizontalPanel();
	    hPanel.setWidth("100%");
	    contentPanel.add(hPanel);
	    hPanel.add(cancel);
	    hPanel.setCellHorizontalAlignment(cancel, HasHorizontalAlignment.ALIGN_LEFT);
	    hPanel.add(save);
	    hPanel.setCellHorizontalAlignment(save, HasHorizontalAlignment.ALIGN_RIGHT);
	}
	
	public void setupAuthors(List<Teacher> teachers) {
		teacherSelector.setupAuthors(teachers);
	}
	
	public HasClickHandlers getCancelButton() {
		return this.cancel;
	}
	
	public HasClickHandlers getSaveButton() {
		return this.save;
	}
	
	public MeetingMinutes getMeetingMinute() {
		if (currentEvent == null) {
			currentEvent = new MeetingMinutes();
		}
		currentEvent.setContent(description.getHTML());
		currentEvent.setTitle(title.getText());
		currentEvent.setPlace(place.getText());
		currentEvent.setDate(fromDatePicker.getValue());
		currentEvent.setMeetingChair(chairListBox.getSelectedItemText());
		currentEvent.setMeetingChairId(Long.parseLong(chairListBox.getSelectedValue()));
		currentEvent.setReporter(secListBox.getSelectedItemText());
		currentEvent.setReporterId(Long.parseLong(secListBox.getSelectedValue()));
		currentEvent.setParticipantIds(teacherSelector.getSelectedIds());
		currentEvent.setAbsentIds(teacherSelector.getSelectedIds());
		currentEvent.setAbsentReasons(teacherSelector.getAbsentReasons());
		return currentEvent;
	}
	
	public void setupTeachers(List<Teacher> teacherList) {
		teacherSelector.setupAuthors(teacherList);
		for (Teacher teacher : teacherList) {
			if (teacher.getStatus() == 0) {
				chairListBox.addItem(teacher.getFullName(), teacher.getId() + "");
				secListBox.addItem(teacher.getFullName(), teacher.getId() + "");
			}
		}
	}
	
	public Widget getContentPanel() {
		return this.contentPanel;
	}
	
	public void edit(MeetingMinutes event) {
		if (!ClientData.isLoggedIn()) {
			EduManager.clientFactory.getBasicView().getLoginView().view();
			return;
		}
		if (event == null)
			event = new MeetingMinutes();
		setupTeachers(ClientData.getTeachers());
		currentEvent = event;
//		if (dialogBox == null) {
//			dialogBox = new DialogBox(true);
//			dialogBox.setWidth("900px");
//		    dialogBox.setGlassEnabled(true);
//		    dialogBox.setAnimationEnabled(false);
//		    // Create a table to layout the content
//		    VerticalPanel dialogContents = new VerticalPanel();
//		    dialogContents.setSpacing(5);
//		    dialogContents.setWidth("100%");
//		    dialogBox.setWidget(dialogContents);
//		    flexTable.setWidth("100%");
//		    dialogContents.add(flexTable);
//		    dialogContents.setCellHorizontalAlignment(flexTable, HasHorizontalAlignment.ALIGN_CENTER);
//		    
//		    HorizontalPanel hPanel = new HorizontalPanel();
//		    hPanel.setWidth("100%");
//		    dialogContents.add(hPanel);
//		    hPanel.add(cancel);
//		    hPanel.setCellHorizontalAlignment(cancel, HasHorizontalAlignment.ALIGN_LEFT);
//		    hPanel.add(save);
//		    hPanel.setCellHorizontalAlignment(save, HasHorizontalAlignment.ALIGN_RIGHT);
//		}
//	    dialogBox.setText(event != null ? "Sửa thông tin" : "Tạo mới");
//	    dialogBox.center();
	}

	public FormPanel getUploadForm() {
		return uploadForm;
	}

	public FileUpload getUploadField() {
		return uploadField;
	}

	public Button getUploadButton() {
		return uploadButton;
	}

	public HorizontalPanel getUploadPanel() {
		return uploadPanel;
	}
	
	public void onFileAttached(String url) {
		if (!currentEvent.getAttacheds().contains(url)) {
			currentEvent.getAttacheds().add(url);
			filePanel.insert(new HTML("<a href='" + url + "'>" + url + "</a>"), 0);
		}
	}
}