package com.hungnt.edumanager.client.activities.topic;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DatePicker;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.widget.FormatTextArea;
import com.hungnt.edumanager.client.widget.HListBox;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.InternTopic;
import com.hungnt.edumanager.shared.Teacher;

public class TopicEditer {
	
	private Button save = new Button("Lưu");
	private Button cancel = new Button("Huỷ");
	private FlexTable flexTable = new FlexTable();
	private HListBox topicType = new HListBox();
	private ListBox teacher = new ListBox();
	private ListBox reviewer = new ListBox();
	private ListBox scholarYear = new ListBox();
	private ListBox semester = new ListBox();
	private ListBox studentNum = new ListBox();
	private TextBox title = new TextBox();
	private TextBox courseIdCode = new TextBox();
	private TextBox subjectIdCode = new TextBox();
	private TextBox studentName = new TextBox();
	private TextBox studentId = new TextBox();
	private TextBox studentEmail = new TextBox();
	private TextBox studentPhone = new TextBox();
	private FormatTextArea description = new FormatTextArea();
	private ListBox studentType = new ListBox();
	private ListBox status = new ListBox();
	private DatePicker submittedDatePicker = new DatePicker();
	private DateBox submittedDateBox = null;

	private DialogBox dialogBox = null;
	private InternTopic currentTopic= null;

	public TopicEditer() {	
		super();
		int row = 0, col = 0;
		scholarYear.addItem(Config.getScholorYear(Config.currentScholarYear), Config.currentScholarYear + "");
		semester.addItem("Học kỳ 1", "1");
		semester.addItem("Học kỳ 2", "2");
		semester.addItem("Học kỳ hè", "3");
		for (int i = 1; i <= 5; i++)
			studentNum.addItem(i + "", "" + i);
		for (int i = 0; i <= 2; i++) {
			status.addItem(Config.getTopicStatus(i), i + "");
		}
		for (int i = 0; i <= 6; i ++) {
			studentType.addItem(Config.getStudentTypeName(i), "" + i);
		}
		for (int i = 10; i < 15; i ++) {
			studentType.addItem(Config.getStudentTypeName(i), "" + i);
		}
		for (int i = 0; i <= 3; i ++) {
			String topicType = Config.getTopicType(i);
			if (topicType != null && !topicType.isEmpty()) {
				this.topicType.addItem(topicType, i + "");
			}
		}
		teacher.setWidth("200px");
		reviewer.setWidth("200px");
		title.setWidth("95%");
		description.setWidth("95%");
		studentNum.setWidth("200px");
		studentName.setWidth("200px");
		studentId.setWidth("200px");
		studentPhone.setWidth("200px");
		studentEmail.setWidth("200px");
		flexTable.setWidth("100%");
		flexTable.setText(row, col++, "Loại đồ án");
		flexTable.setWidget(row, col++, topicType);
		flexTable.setText(row, col++, "Giáo viên");
		flexTable.setWidget(row, col++, teacher);
		row ++; col = 0;
//		flexTable.setText(row, col++, "Trạng thái");
//		flexTable.setWidget(row, col++, status);
//		row ++; col = 0;
		flexTable.setText(row, col++, "Học kỳ");
		flexTable.setWidget(row, col++, semester);
		flexTable.setText(row, col++, "Năm học");
		flexTable.setWidget(row, col++, scholarYear);
		row ++; col = 0;
		flexTable.setText(row, col++, "Số SV");
		flexTable.setWidget(row, col++, studentNum);
		flexTable.setText(row, col++, "Hệ ĐT");
		flexTable.setWidget(row, col++, studentType);
		row ++; col = 0;
		flexTable.setText(row, col++, "Mã học phần");
		flexTable.setWidget(row, col++, subjectIdCode);
		flexTable.setText(row, col++, "Mã lớp");
		flexTable.setWidget(row, col++, courseIdCode);
		row ++; col = 0;
		flexTable.setText(row, col++, "Tên SV");
		flexTable.setWidget(row, col++, studentName);
		flexTable.setText(row, col++, "Mã số SV");
		flexTable.setWidget(row, col++, studentId);
		row ++; col = 0;
		flexTable.setText(row, col++, "Email SV");
		flexTable.setWidget(row, col++, studentEmail);
		flexTable.setText(row, col++, "Phone SV");
		flexTable.setWidget(row, col++, studentPhone);
		row ++; col = 0;
		flexTable.setText(row, col++, "Tên đề tài");
		flexTable.setWidget(row, col, title);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		flexTable.setText(row, col++, "Mô tả đề tài");
		flexTable.setWidget(row, col, description);
		flexTable.getFlexCellFormatter().setColSpan(row, col, 3);
		row ++; col = 0;
		
		
		submittedDateBox = new DateBox(submittedDatePicker, null, 
				new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd/MM/yyyy")));
		submittedDateBox.setWidth("200px");
		submittedDateBox.getDatePicker().setYearArrowsVisible(true);
		flexTable.setText(row, col++, "Ngày nộp");
		flexTable.setWidget(row, col++, submittedDateBox);
		flexTable.setText(row, col++, "Phản biện");
		flexTable.setWidget(row, col++, reviewer);
		row ++; col = 0;
		flexTable.getCellFormatter().setWidth(row, 0, "80px");
		flexTable.getCellFormatter().setWidth(row, 1, "220px");
		flexTable.getCellFormatter().setWidth(row, 2, "70px");
		flexTable.getCellFormatter().setWidth(row, 3, "220px");
	}
	
	public HasClickHandlers getCancelButton() {
		return this.cancel;
	}
	
	public HasClickHandlers getSaveButton() {
		return this.save;
	}
	
	public DialogBox getDialogBox() {
		return this.dialogBox;
	}
	
	public InternTopic getTopic() {
		if (currentTopic == null)
			currentTopic = new InternTopic();
		if (teacher.getSelectedValue().trim().isEmpty()) {
			Window.alert("Xin hãy chọn giáo viên");
			return null;
		}
//		if (title.getText().trim().isEmpty()) {
//			Window.alert("Xin hãy điền tên đề tài");
//			return null;
//		}
		Teacher t = ClientData.getTeacherMap().get(Long.parseLong(teacher.getSelectedValue()));
		currentTopic.setTopicType(Integer.parseInt(topicType.getSelectedValue()));
		currentTopic.setDescription(description.getText().trim());
		currentTopic.setScholarYear(Integer.parseInt(scholarYear.getSelectedValue()));
		currentTopic.setSemester(Integer.parseInt(semester.getSelectedValue()));
		currentTopic.setStatus(Integer.parseInt(status.getSelectedValue()));
		currentTopic.setStudentEmail(studentEmail.getText().trim());
		currentTopic.setStudentName(studentName.getText().trim());
		currentTopic.setStudentId(studentId.getText().trim());
		currentTopic.setStudentNum(Integer.parseInt(studentNum.getSelectedValue()));
		currentTopic.setStudentPhone(studentPhone.getText().trim());
		currentTopic.setTeacherName(t.getFullName());
		currentTopic.setTeacherId(t.getId());
		currentTopic.setTitle(title.getText().trim());
		currentTopic.setStudentType(Integer.parseInt(studentType.getSelectedValue()));
		currentTopic.setSubjectIdCode(subjectIdCode.getText().trim());
		currentTopic.setCourseIdCode(courseIdCode.getText().trim());
		if (submittedDateBox.getValue() != null) {
			currentTopic.setSubmittedDate(submittedDateBox.getValue());
		}
		if (!reviewer.getSelectedValue().isEmpty()) {
			Teacher review = ClientData.getTeacherMap().get(Long.parseLong(reviewer.getSelectedValue()));
			if (review != null) {
				currentTopic.setReviewerId(review.getId());
				currentTopic.setReviewerName(review.getFullName());
			}
		}
		return currentTopic;
	}
	
	public void setupTeachers(List<Teacher> teachers) {
		teacher.clear();
		teacher.addItem("[Chọn giáo viên]", "");
		for (Teacher s : teachers) {
			teacher.addItem(s.getFullName(), s.getId() + "");
		}
		reviewer.addItem("[Chọn giáo viên]", "");
		for (Teacher s : teachers) {
			reviewer.addItem(s.getFullName(), s.getId() + "");
		}
	}
	
	public void edit(InternTopic topic) {
		if (!ClientData.isLoggedIn()) {
			EduManager.clientFactory.getBasicView().getLoginView().view();
			return;
		}
		if (teacher.getItemCount() < 2) {
			setupTeachers(ClientData.getTeachers());
		}
		if (dialogBox == null) {
			dialogBox = new DialogBox(true);
			dialogBox.setWidth("700px");
		    dialogBox.setGlassEnabled(true);
		    dialogBox.setAnimationEnabled(false);
		    // Create a table to layout the content
		    VerticalPanel dialogContents = new VerticalPanel();
		    dialogContents.setSpacing(5);
		    dialogContents.setWidth("100%");
		    dialogBox.setWidget(dialogContents);
		    flexTable.setWidth("100%");
		    dialogContents.add(flexTable);
		    dialogContents.setCellHorizontalAlignment(flexTable, HasHorizontalAlignment.ALIGN_CENTER);
		    
		    HorizontalPanel hPanel = new HorizontalPanel();
		    hPanel.setWidth("100%");
		    dialogContents.add(hPanel);
		    hPanel.add(cancel);
		    hPanel.setCellHorizontalAlignment(cancel, HasHorizontalAlignment.ALIGN_LEFT);
		    hPanel.add(save);
		    hPanel.setCellHorizontalAlignment(save, HasHorizontalAlignment.ALIGN_RIGHT);
		}
		if (topic == null) {
			topic = new InternTopic();
		}
		currentTopic = topic;
		dialogBox.setText(currentTopic.getId() != null && currentTopic.getId() > 0 ? "Sửa thông tin" : "Tạo mới");
		if (currentTopic.getId() == null || currentTopic.getId() <= 0) {
			dialogBox.center();
	        dialogBox.show();
			return;
		}
	    
	    for (int i = 0; i < teacher.getItemCount(); i ++) {
			if (teacher.getValue(i).equalsIgnoreCase(topic.getTeacherId() + "")) {
				teacher.setSelectedIndex(i);
				break;
			}
		}
	    this.status.setSelectedIndex(topic.getStatus());
	    if (topic.getSemester() > 0)
	    	this.semester.setSelectedIndex(topic.getSemester() - 1);
	    else
	    	this.semester.setSelectedIndex(Config.currentSemester - 1);
	    this.topicType.setSelectedValue(topic.getTopicType());
	    this.studentNum.setSelectedIndex(topic.getStudentNum() - 1);
	    this.studentType.setSelectedIndex(topic.getStudentType());
	    this.studentName.setText(topic.getStudentName());
	    this.studentId.setText(topic.getStudentId());
	    this.studentEmail.setText(topic.getStudentEmail());
	    this.studentPhone.setText(topic.getStudentPhone());
	    this.title.setText(topic.getTitle());
	    this.description.setText(topic.getDescription());
	    this.subjectIdCode.setText(topic.getSubjectIdCode());
	    this.courseIdCode.setText(topic.getCourseIdCode());
	    if (topic.getSubmittedDate() != null)
	    	this.submittedDateBox.setValue(topic.getSubmittedDate());
	    else
	    	this.submittedDateBox.setValue(null);
	    if (topic.getReviewerId() > 0) {
	    	for (int i = 0; i < teacher.getItemCount(); i ++) {
				if (reviewer.getValue(i).equalsIgnoreCase(topic.getReviewerId() + "")) {
					reviewer.setSelectedIndex(i);
					break;
				}
			}
	    }
	    else {
	    	reviewer.setSelectedIndex(0);
	    }
	    dialogBox.center();
        dialogBox.show();
	}
}
