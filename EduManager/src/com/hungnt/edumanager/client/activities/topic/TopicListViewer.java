package com.hungnt.edumanager.client.activities.topic;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.AbstractEditableCell;
import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.hungnt.edumanager.client.ClientData;
import com.hungnt.edumanager.client.EduManager;
import com.hungnt.edumanager.client.RPCCall;
import com.hungnt.edumanager.client.event.EditEvent;
import com.hungnt.edumanager.client.widget.HListBox;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.InternTopic;
import com.hungnt.edumanager.shared.Teacher;

public class TopicListViewer extends VerticalPanel {
	
	private HListBox teacherFilter = new HListBox();
	private HListBox subjectFilter = new HListBox();
	private HListBox scholarYearFilter = new HListBox();
	private HListBox semesterFilter = new HListBox();
	private HListBox statusFilter = new HListBox();
	private HListBox topicTypeFilter = new HListBox();

	private HorizontalPanel topFunctionPanel = new HorizontalPanel();

	private Long fixedTeacherId = -1L;
	private Long currentSelectedTeacherId = -1L;
	private Long currentSelectedSubjectId = 0L;
	private int currentSelectedSemester = 0;
	private int currentSelectedStatus = -1;
	private int currentSelectedTopicType = -1;
	
	DataGrid<InternTopic> dataGrid;// = new DataGrid<Teacher>();
	
	public TopicListViewer() {	
		super();
		this.setWidth("100%");
		teacherFilter.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedTeacherId = Long.parseLong(teacherFilter.getSelectedValue());
				view(ClientData.getTopics(), false);
			}
		});
		subjectFilter.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedSubjectId = Long.parseLong(subjectFilter.getSelectedValue());
				view(ClientData.getTopics(), false);
			}
		});
		scholarYearFilter.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				ClientData.getTopics().clear();
				ClientData.loadData(Integer.parseInt(scholarYearFilter.getSelectedValue()), Integer.parseInt(semesterFilter.getSelectedValue()));
			}
		});
		scholarYearFilter.addItem(Config.getScholorYear(Config.currentScholarYear - 1), (Config.currentScholarYear - 1) + "");
		scholarYearFilter.addItem(Config.getScholorYear(Config.currentScholarYear), Config.currentScholarYear + "");
		scholarYearFilter.setSelectedIndex(1);
		semesterFilter.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedSemester = Integer.parseInt(semesterFilter.getSelectedValue());
				view(ClientData.getTopics(), false);
			}
		});
		statusFilter.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedStatus = Integer.parseInt(statusFilter.getSelectedValue());
				view(ClientData.getTopics(), false);
			}
		});
		topicTypeFilter.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				currentSelectedTopicType = Integer.parseInt(topicTypeFilter.getSelectedValue());
				view(ClientData.getTopics(), false);
			}
		});

		statusFilter.addItem("[Tất cả]", "-1");
		for (int i = 0; i <= 2; i ++) {
			String val = Config.getTopicStatus(i);
			if (!val.isEmpty())
				statusFilter.addItem(val, i + "");
		}
		
		topicTypeFilter.addItem("[Tất cả]", "-1");
		for (int i = 0; i <= 4; i ++) {
			String topicType = Config.getTopicType(i);
			if (topicType != null && !topicType.isEmpty()) {
				topicTypeFilter.addItem(topicType, i + "");
			}
		}
		
		semesterFilter.addItem("Cả năm", "0"); 
		semesterFilter.addItem("Kỳ 1", "1"); 
		semesterFilter.addItem("Kỳ 2", "2"); 
		semesterFilter.addItem("Kỳ 3 (Hè)", "3");
		
		subjectFilter.setWidth("200px");
		scholarYearFilter.setWidth("100px");
		semesterFilter.setWidth("100px");
		teacherFilter.setWidth("200px");
		statusFilter.setWidth("100px");
		topicTypeFilter.setWidth("150px");
		
		topFunctionPanel.setWidth("100%");
		topFunctionPanel.setSpacing(5);
		HTML filterBy = new HTML("Tìm kiếm:");
		topFunctionPanel.add(filterBy);
		topFunctionPanel.setCellWidth(filterBy, "130px");
		topFunctionPanel.add(teacherFilter);
		topFunctionPanel.add(subjectFilter);
		topFunctionPanel.add(scholarYearFilter);
		topFunctionPanel.add(semesterFilter);
		//topFunctionPanel.add(statusFilter);
		topFunctionPanel.add(topicTypeFilter);
		topFunctionPanel.setCellHorizontalAlignment(teacherFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(subjectFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(scholarYearFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellHorizontalAlignment(semesterFilter, HasHorizontalAlignment.ALIGN_LEFT);
		topFunctionPanel.setCellWidth(filterBy, "70px");
		topFunctionPanel.setCellWidth(teacherFilter, "100px");
		topFunctionPanel.setCellWidth(subjectFilter, "100px");
		topFunctionPanel.setCellWidth(scholarYearFilter, "100px");
		topFunctionPanel.setCellWidth(semesterFilter, "100px");
	}
	
	public void refresh() {
		if (dataGrid != null)
			dataGrid.redraw();
	}
	
	public void setFixedTeacherId(Long teacherId) {
		this.fixedTeacherId = teacherId;
	}
	
	public HorizontalPanel getFunctionPanel() {
		return this.topFunctionPanel;
	}
	
	public void onDataLoaded(int dataType) {
		if (dataType == Config.DATA_TEACHER)
			setupTeacherFilters(ClientData.getTeachers());
		if (dataType == Config.DATA_TOPIC)
			setupSubjectFilters(ClientData.getTopics());
	}
	
	public void setupTeacherFilters(List<Teacher> teachers) {
		teacherFilter.clear();
		if (fixedTeacherId <= 0) {
			teacherFilter.addItem("[Tất cả giáo viên]", "0");
		}
		for (Teacher s : teachers) {
			if (fixedTeacherId > 0) {
				if (s.getId().compareTo(fixedTeacherId) == 0) {
					teacherFilter.addItem(s.getFullName(), s.getId() + "");
					break;
				}
			}
			else {
				teacherFilter.addItem(s.getFullName(), s.getId() + "");
			}
		}
	}
	
	public void setupSubjectFilters(List<InternTopic> subjects) {
		subjectFilter.clear();
		subjectFilter.addItem("[Tất cả đề tài]", "0");
		for (InternTopic s : subjects) {
			if (fixedTeacherId > 0) {
				if (s.getTeacherId().compareTo(fixedTeacherId) == 0
						&& s.getTitle() != null && !s.getTitle().isEmpty())
					subjectFilter.addItem(s.getTitle(), s.getId() + "");
			}
			else if (s.getTitle() != null && !s.getTitle().isEmpty())
				subjectFilter.addItem(s.getTitle(), s.getId() + "");
		}
		subjectFilter.setWidth("200px");
	}
	
	public DataGrid<InternTopic> getDataGrid() {
		return this.dataGrid;
	}
	
	/**
	   * The provider that holds the list of contacts in the database.
	   */
	  private ListDataProvider<InternTopic> dataProvider = new ListDataProvider<InternTopic>();
	
	/**
   * The key provider that provides the unique ID of a contact.
   */
  public static final ProvidesKey<InternTopic> KEY_PROVIDER = new ProvidesKey<InternTopic>() {
    @Override
    public Object getKey(InternTopic item) {
      return item == null ? null : item.getId();
    }
  };
	
	public  void view(List<InternTopic> fullTopics, boolean showAll) {
		if (teacherFilter.getItemCount() < 2)
    		setupTeacherFilters(ClientData.getTeachers());
    	if (subjectFilter.getItemCount() < 2)
    		setupSubjectFilters(ClientData.getTopics());
    	
    	if (currentSelectedTeacherId < 0 && ClientData.isLoggedIn())
    		currentSelectedTeacherId = ClientData.getCurrentUser().getId();
    	
    	if (fixedTeacherId > 0)
    		currentSelectedTeacherId = fixedTeacherId;
    	
		List<InternTopic> topics = new ArrayList<InternTopic>(fullTopics);
		if (currentSelectedTeacherId > 0 || currentSelectedSubjectId > 0
				|| currentSelectedSemester > 0
				|| currentSelectedTopicType >= 0) {
			List<InternTopic> removed = new ArrayList<InternTopic>();
			for (InternTopic topic : topics) {
				if ((currentSelectedTeacherId > 0 && topic.getTeacherId().compareTo(currentSelectedTeacherId) != 0)
						|| (currentSelectedSubjectId > 0 && topic.getId().compareTo(currentSelectedSubjectId) != 0)
						|| (currentSelectedSemester > 0 && topic.getSemester() != currentSelectedSemester)
						|| (currentSelectedStatus >= 0 && topic.getStatus() != currentSelectedStatus)
						|| (currentSelectedTopicType >= 0 && topic.getTopicType() != currentSelectedTopicType)) {
					removed.add(topic);
				}
			}
			topics.removeAll(removed);
			teacherFilter.setSelectedValue(currentSelectedTeacherId);
			subjectFilter.setSelectedValue(currentSelectedSubjectId);
			semesterFilter.setSelectedValue(currentSelectedSemester);
			statusFilter.setSelectedValue(currentSelectedStatus);
			topicTypeFilter.setSelectedValue(currentSelectedTopicType);
		}
    	
	    // Create the table.
//		if (topics == null || topics.isEmpty()) {
//			topics = dataProvider.getList();
//		}
		for (int i = 0; i < topics.size(); i++) {
			topics.get(i).setPosIndex(i + 1);
		}
		dataProvider.setList(topics);
	    editableCells = new ArrayList<AbstractEditableCell<?, ?>>();
	    dataGrid = new DataGrid<InternTopic>(200, KEY_PROVIDER);
	    
	    dataGrid.setWidth("100%");
	    dataGrid.setHeight("600px");
	    
	    //InternTopicList.setMinimumTableWidth(140, Unit.EM);
	    dataProvider.addDataDisplay(dataGrid);
	    
	    this.clear();
	    this.add(topFunctionPanel);
	    this.add(dataGrid);
	    
	    int diff = ClientData.isLoggedIn() ? 0 : 30;
	    
	    // NumberCell.
	    Column<InternTopic, Number> numberColumn =
	        addColumn(new NumberCell(), "#", new GetValue<Number>() {
	          @Override
	          public Number getValue(InternTopic contact) {
	            return contact.getPosIndex();
	          }
	        }, null);
	    dataGrid.setColumnWidth(numberColumn, 40, Unit.PX);
	    numberColumn.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LOCALE_END);
	    
	    Column<InternTopic, SafeHtml> infoCol = addColumn(new SafeHtmlCell(), "Thông tin", new GetValue<SafeHtml>() {
		      @Override
		      public SafeHtml getValue(InternTopic contact) {
		    	  String info = "";
		    	  if (contact.getTopicType() > 0)
		    		  info += Config.getTopicType(contact.getTopicType());
		    	  else
		    		  info += "DATN: " + contact.getTitle();
		    	  info += "<br><i>GVHD: " + contact.getTeacherName() + " - SV: " + contact.getStudentName() + "</i>";
		    	  if (contact.getDescription() != null && !contact.getDescription().isEmpty())
		    		  info += "<br>Mô tả: " + contact.getDescription();
		    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
		    	  builder.appendHtmlConstant(info);
		    	  return builder.toSafeHtml();
		      }
		    }, null);
	    dataGrid.setColumnWidth(infoCol, 650 + diff, Unit.PX);
	    
//	    // ClickableTextCell.
//	    Column<InternTopic, String> nameCol = addColumn(new ClickableTextCell(), "Tên thầy cô", new GetValue<String>() {
//	      @Override
//	      public String getValue(InternTopic contact) {
//	        return contact.getTeacherName();
//	      }
//	    }, new FieldUpdater<InternTopic, String>() {
//	      @Override
//	      public void update(int index, InternTopic object, String value) {
//	        Window.alert("You clicked " + object.getTitle());
//	      }
//	    });
//	    dataGrid.setColumnWidth(nameCol, 150 + diff, Unit.PX);
//
////	    // TextCell.
////	    addColumn(new TextCell(), "Mã số", new GetValue<String>() {
////	      @Override
////	      public String getValue(InternTopic contact) {
////	        return contact.getInternTopicId();
////	      }
////	    }, null);
//	    
//	    // TextCell.
//	    Column<InternTopic, String> emailCol = addColumn(new TextCell(), "Tên đề tài", new GetValue<String>() {
//	      @Override
//	      public String getValue(InternTopic contact) {
//	    	  if (contact.getTopicType() > 0)
//	    		  return Config.getTopicType(contact.getTopicType());
//	    	  else
//	    		  return "DATN: " + contact.getTitle();
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(emailCol, 150 + diff, Unit.PX);
//	    
//	    // TextCell.
//	    Column<InternTopic, String> studentCol = addColumn(new TextCell(), "Tên SV", new GetValue<String>() {
//	      @Override
//	      public String getValue(InternTopic contact) {
//	    	 return contact.getStudentName();
//	      }
//	    }, null);
//	    dataGrid.setColumnWidth(studentCol, 150, Unit.PX);
//	    
	    // TextCell.
	    Column<InternTopic, String> studentTypeCol = addColumn(new TextCell(), "Hệ ĐT", new GetValue<String>() {
	      @Override
	      public String getValue(InternTopic contact) {
	    	 return Config.getStudentTypeName(contact.getStudentType());
	      }
	    }, null);
	    //dataGrid.setColumnWidth(studentTypeCol, 70, Unit.PX);
//	    
//	    // SafeHtmlCell.
//	    Column<InternTopic, SafeHtml> desCol = addColumn(new SafeHtmlCell(), "Mô tả", new GetValue<SafeHtml>() {
//	      @Override
//	      public SafeHtml getValue(InternTopic contact) {
//	    	  String info = contact.getDescription();
//	    	  SafeHtmlBuilder builder = new SafeHtmlBuilder();
//	    	  builder.appendHtmlConstant(info);
//	    	  return builder.toSafeHtml();
//	      }
//	    }, null);
	    
	   

	    // DateCell.
//	    DateTimeFormat dateFormat = DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT);
//	    Column<InternTopic, Date> dateCol = addColumn(new DateCell(dateFormat), "Ngày sinh", new GetValue<Date>() {
//	      @Override
//	      public Date getValue(InternTopic contact) {
//	        return contact.getBirthDate();
//	      }
//	    }, null);
//	    InternTopicList.setColumnWidth(dateCol, 50, Unit.PX);
	    
	    if (ClientData.isLoggedIn()) {
	    	dataGrid.setColumnWidth(studentTypeCol, 150, Unit.PX);
		    // ActionCell.
		    Column<InternTopic, InternTopic> editCol = addColumn(new ActionCell<InternTopic>("Sửa", new ActionCell.Delegate<InternTopic>() {
		      @Override
		      public void execute(InternTopic contact) {
		    	  if (ClientData.canEdit(contact.getTeacherId(), true))
		    		  EduManager.clientFactory.getEventBus().fireEvent(new EditEvent(contact));
		      }
		    }), "Sửa", new GetValue<InternTopic>() {
		      @Override
		      public InternTopic getValue(InternTopic contact) {
		        return contact;
		      }
		    }, null);
		    dataGrid.setColumnWidth(editCol, 70, Unit.PX);
	
		    // ButtonCell.
		    Column<InternTopic, String> delCol = addColumn(new ButtonCell(), "Xoá", new GetValue<String>() {
		      @Override
		      public String getValue(InternTopic contact) {
		    	  return "Xoá";
		    	  //return "Click " + contact.getFirstName();
		      }
		    }, new FieldUpdater<InternTopic, String>() {
		      @Override
		      public void update(int index, final InternTopic object, String value) {
		    	  if (ClientData.canEdit(object.getTeacherId(), true)) {
		    		  if (Window.confirm("Bạn có chắc chắn muốn xoá không?")) {
		    			  new RPCCall<Void>() {

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Lỗi xảy ra, không thể xoá được chủ đề");
							}

							@Override
							public void onSuccess(Void result) {
								Window.alert("Xoá thành công");
								ClientData.getTopics().clear();
								ClientData.loadData(Config.currentScholarYear, -1);
							}

							@Override
							protected void callService(AsyncCallback<Void> cb) {
				    			  EduManager.dataService.deleteTopic(null, object, cb);
							}
						}.retry();
		    		  }
		    	  }
		      }
		    });
		    //InternTopicList.setColumnWidth(delCol, 70, Unit.PX);
	    }
	}
	
	/**
	   * The list of cells that are editable.
	   */
	  private List<AbstractEditableCell<?, ?>> editableCells;
	
	 /**
	   * Get a cell value from a record.
	   * 
	   * @param <C> the cell type
	   */
	  private static interface GetValue<C> {
	    C getValue(InternTopic contact);
	  }
	
	/**
	   * Add a column with a header.
	   * 
	   * @param <C> the cell type
	   * @param cell the cell used to render the column
	   * @param headerText the header string
	   * @param getter the value getter for the cell
	   */
	  private <C> Column<InternTopic, C> addColumn(Cell<C> cell, String headerText,
	      final GetValue<C> getter, FieldUpdater<InternTopic, C> fieldUpdater) {
	    Column<InternTopic, C> column = new Column<InternTopic, C>(cell) {
	      @Override
	      public C getValue(InternTopic object) {
	        return getter.getValue(object);
	      }
	    };
	    column.setFieldUpdater(fieldUpdater);
	    if (cell instanceof AbstractEditableCell<?, ?>) {
	      editableCells.add((AbstractEditableCell<?, ?>) cell);
	    }
	    dataGrid.addColumn(column, headerText);
	    return column;
	  }
}
