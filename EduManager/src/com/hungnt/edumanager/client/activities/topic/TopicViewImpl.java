package com.hungnt.edumanager.client.activities.topic;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.client.CssToken;
import com.hungnt.edumanager.client.activities.basic.BasicViewImpl;
import com.hungnt.edumanager.shared.InternTopic;

public class TopicViewImpl extends BasicViewImpl implements TopicView {
	
	private VerticalPanel topicViewPanel = new VerticalPanel();
	private Button addMore = new Button("Thêm");
	private InternTopic currentTopic = null;
	private TopicListViewer topicViewer = new TopicListViewer();

	public TopicViewImpl() {	
		super();
		topicViewPanel.setSpacing(5);
		topicViewPanel.setWidth("100%");
		topicViewPanel.add(topicViewer);
		contentPanel.add(topicViewPanel);
	}
	
	@Override
	public InternTopic getCurrentTopic() {
		return this.currentTopic;
	}
	
	@Override
	public HasClickHandlers getSaveButton() {
		return this.topicEditor.getSaveButton();
	}
	
	@Override
	public HasClickHandlers getCancelButton() {
		return this.topicEditor.getCancelButton();
	}
	
	@Override
	public HasClickHandlers getAddMoreButton() {
		return this.addMore;
	}

	@Override
	public InternTopic getTopic() {
		return this.topicEditor.getTopic();
	}
	
	@Override
	public void refreshView() {
		super.refreshView();
		topic.setStyleName(CssToken.TABBAR_BASE_BUTTON_ACTIVE, true);
	}
	
	@Override
	public DialogBox getDialogBox() {
		return this.topicEditor.getDialogBox();
	}
	
	@Override
	public void edit(InternTopic topic) {
		this.topicEditor.edit(topic);
	}
	
	@Override
	public void view(List<InternTopic> topics) {
		topicViewPanel.clear();
		topicViewPanel.add(topicViewer);
		topicViewer.getFunctionPanel().add(addMore);
		topicViewer.getFunctionPanel().setCellHorizontalAlignment(addMore, ALIGN_RIGHT);
		topicViewer.view(topics, false);
	}
}
