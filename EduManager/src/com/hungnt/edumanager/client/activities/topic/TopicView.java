package com.hungnt.edumanager.client.activities.topic;

import java.util.List;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.hungnt.edumanager.client.activities.basic.BasicView;
import com.hungnt.edumanager.shared.InternTopic;
import com.hungnt.edumanager.shared.Publication;
import com.hungnt.edumanager.shared.Teacher;

public interface TopicView extends BasicView{
	HasClickHandlers getSaveButton();
	InternTopic getTopic();
	HasClickHandlers getAddMoreButton();
	void edit(InternTopic topic);
	void view(List<InternTopic> topics);
	DialogBox getDialogBox();
	HasClickHandlers getCancelButton();
	InternTopic getCurrentTopic();
}
