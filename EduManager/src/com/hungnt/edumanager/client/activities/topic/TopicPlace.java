package com.hungnt.edumanager.client.activities.topic;

import com.google.gwt.place.shared.PlaceTokenizer;
import com.hungnt.edumanager.client.activities.basic.BasicPlace;

public class TopicPlace extends BasicPlace {
	
	public TopicPlace() {
		super();
	}
	
	@Override
	public String getToken() {
		return "topic";
	}
	
	public static class Tokenizer implements PlaceTokenizer<TopicPlace> {
        @Override
        public String getToken(TopicPlace place) {
            return place.getToken();
        }

        @Override
        public TopicPlace getPlace(String token) {
            return new TopicPlace();
        }
    }
	
}
