package com.hungnt.edumanager.client.activities;

import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;
import com.hungnt.edumanager.client.activities.basic.BasicView;
import com.hungnt.edumanager.client.activities.course.CourseView;
import com.hungnt.edumanager.client.activities.course.details.CourseDetailView;
import com.hungnt.edumanager.client.activities.document.DocumentView;
import com.hungnt.edumanager.client.activities.equipment.EquipmentView;
import com.hungnt.edumanager.client.activities.event.EventView;
import com.hungnt.edumanager.client.activities.home.HomeView;
import com.hungnt.edumanager.client.activities.meeting.MeetingView;
import com.hungnt.edumanager.client.activities.publication.PublicationView;
import com.hungnt.edumanager.client.activities.subject.SubjectView;
import com.hungnt.edumanager.client.activities.subject.details.SubjectDetailView;
import com.hungnt.edumanager.client.activities.teacher.TeacherView;
import com.hungnt.edumanager.client.activities.teacher.details.TeacherDetailView;
import com.hungnt.edumanager.client.activities.topic.TopicView;

public interface ClientFactory {
	PlaceController getPlaceController();
	EventBus getEventBus();
	BasicView getBasicView();
	SubjectView getSubjectView();
	TeacherView getTeacherView();
	CourseView getCourseView();
	HomeView getHomeView();
	DocumentView getMaterialView();
	PublicationView getPublicationView();
	CourseDetailView getCourseDetailView();
	TeacherDetailView getTeacherDetailView();
	EquipmentView getEquipmentView();
	TopicView getTopicView();
	SubjectDetailView getSubjectDetailView();
	EventView getEventView();
	MeetingView getMeetingView();
}
