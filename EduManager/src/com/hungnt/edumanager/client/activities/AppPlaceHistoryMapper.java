package com.hungnt.edumanager.client.activities;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;
import com.hungnt.edumanager.client.activities.course.CoursePlace;
import com.hungnt.edumanager.client.activities.course.details.CourseDetailPlace;
import com.hungnt.edumanager.client.activities.document.DocumentPlace;
import com.hungnt.edumanager.client.activities.equipment.EquipmentPlace;
import com.hungnt.edumanager.client.activities.event.EventPlace;
import com.hungnt.edumanager.client.activities.home.HomePlace;
import com.hungnt.edumanager.client.activities.meeting.MeetingPlace;
import com.hungnt.edumanager.client.activities.publication.PublicationPlace;
import com.hungnt.edumanager.client.activities.subject.SubjectPlace;
import com.hungnt.edumanager.client.activities.subject.details.SubjectDetailPlace;
import com.hungnt.edumanager.client.activities.teacher.TeacherPlace;
import com.hungnt.edumanager.client.activities.teacher.details.TeacherDetailPlace;
import com.hungnt.edumanager.client.activities.topic.TopicPlace;

@WithTokenizers({HomePlace.Tokenizer.class, TeacherPlace.Tokenizer.class, 
	CoursePlace.Tokenizer.class, SubjectPlace.Tokenizer.class,
	PublicationPlace.Tokenizer.class, CourseDetailPlace.Tokenizer.class,
	TeacherDetailPlace.Tokenizer.class, SubjectDetailPlace.Tokenizer.class,
	EquipmentPlace.Tokenizer.class, TopicPlace.Tokenizer.class,
	DocumentPlace.Tokenizer.class, EventPlace.Tokenizer.class,
	MeetingPlace.Tokenizer.class})
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {
}
