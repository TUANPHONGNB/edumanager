package com.hungnt.edumanager.client.activities;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.hungnt.edumanager.client.activities.course.CourseActivity;
import com.hungnt.edumanager.client.activities.course.CoursePlace;
import com.hungnt.edumanager.client.activities.course.details.CourseDetailActivity;
import com.hungnt.edumanager.client.activities.course.details.CourseDetailPlace;
import com.hungnt.edumanager.client.activities.document.DocumentActivity;
import com.hungnt.edumanager.client.activities.document.DocumentPlace;
import com.hungnt.edumanager.client.activities.equipment.EquipmentActivity;
import com.hungnt.edumanager.client.activities.equipment.EquipmentPlace;
import com.hungnt.edumanager.client.activities.event.EventActivity;
import com.hungnt.edumanager.client.activities.event.EventPlace;
import com.hungnt.edumanager.client.activities.home.HomeActivity;
import com.hungnt.edumanager.client.activities.home.HomePlace;
import com.hungnt.edumanager.client.activities.meeting.MeetingActivity;
import com.hungnt.edumanager.client.activities.meeting.MeetingPlace;
import com.hungnt.edumanager.client.activities.publication.PublicationActivity;
import com.hungnt.edumanager.client.activities.publication.PublicationPlace;
import com.hungnt.edumanager.client.activities.subject.SubjectActivity;
import com.hungnt.edumanager.client.activities.subject.SubjectPlace;
import com.hungnt.edumanager.client.activities.subject.details.SubjectDetailActivity;
import com.hungnt.edumanager.client.activities.subject.details.SubjectDetailPlace;
import com.hungnt.edumanager.client.activities.teacher.TeacherActivity;
import com.hungnt.edumanager.client.activities.teacher.TeacherPlace;
import com.hungnt.edumanager.client.activities.teacher.details.TeacherDetailActivity;
import com.hungnt.edumanager.client.activities.teacher.details.TeacherDetailPlace;
import com.hungnt.edumanager.client.activities.topic.TopicActivity;
import com.hungnt.edumanager.client.activities.topic.TopicPlace;

public class AppActivityMapper implements ActivityMapper {
	
	private ClientFactory clientFactory;
	private DocumentActivity documentActivity = null;
	
	public AppActivityMapper(ClientFactory clientFactory){
		this.clientFactory = clientFactory;
	}

	@Override
	public Activity getActivity(Place place) {
		if (place instanceof TeacherPlace){
			return new TeacherActivity(clientFactory, place);
		}
		else if (place instanceof SubjectPlace){
			return new SubjectActivity(clientFactory, place);
		}
		else if (place instanceof CoursePlace){
			return new CourseActivity(clientFactory, place);
		}
		else if (place instanceof HomePlace){
			return new HomeActivity(clientFactory, place);
		}
		else if (place instanceof PublicationPlace){
			return new PublicationActivity(clientFactory, place);
		}
		else if (place instanceof CourseDetailPlace){
			return new CourseDetailActivity(clientFactory, place);
		}
		else if (place instanceof SubjectDetailPlace){
			return new SubjectDetailActivity(clientFactory, place);
		}
		else if (place instanceof TeacherDetailPlace){
			return new TeacherDetailActivity(clientFactory, place);
		}
		else if (place instanceof EquipmentPlace){
			return new EquipmentActivity(clientFactory, place);
		}
		else if (place instanceof EventPlace){
			return new EventActivity(clientFactory, place);
		}
		else if (place instanceof MeetingPlace){
			return new MeetingActivity(clientFactory, place);
		}
		else if (place instanceof TopicPlace){
			return new TopicActivity(clientFactory, place);
		} else if (place instanceof DocumentPlace){
			if (documentActivity == null)
				documentActivity = new DocumentActivity(clientFactory, place);
			return documentActivity;
		}
		return null;
	}

}
