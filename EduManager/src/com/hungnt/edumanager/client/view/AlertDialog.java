package com.hungnt.edumanager.client.view;

import com.github.gwtbootstrap.client.ui.Button;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class AlertDialog extends PopupPanel {
	
	private AlertCallback callback;
	private Button okBtn;
	private HTML message;

	public static interface AlertCallback {
		void onOk();
	}

	/**
	 * 
	 */
	public AlertDialog() {
		VerticalPanel panel = new VerticalPanel();
		panel.setWidth("100%");
		this.setWidget(panel);
		this.setAutoHideEnabled(false);
		this.setAnimationEnabled(true);
		this.setModal(true);
		this.setStyleName("context-Menu");
		
		message = new HTML();
		panel.add(message);
		panel.setCellHorizontalAlignment(message, HasHorizontalAlignment.ALIGN_CENTER);
		
		okBtn = new Button("OK");
		panel.add(okBtn);
		panel.setCellHorizontalAlignment(okBtn, HasHorizontalAlignment.ALIGN_CENTER);
		
		initHandlers();
		
	}
	
	private void initHandlers() {
		okBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				hide();
				if (callback != null) {
					callback.onOk();
				}
			}
		});
	}
	
	public void show(String msg, AlertCallback callback) {
		this.callback = callback;
		message.setHTML(msg);
		center();
	}

}
