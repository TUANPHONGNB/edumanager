package com.hungnt.edumanager.client.view;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

public class FormUploadDoc {
	
	private FormPanel uploadForm;
	private FileUpload uploadWidget;
	
	public FormUploadDoc() {
		// TODO Auto-generated constructor stub
	}
	
	public void showUploadFileDialog() {
		clickElement(uploadWidget.getElement());
	}
	

	public static native void clickElement(Element elem) /*-{
		elem.click();
	}-*/;
	
	public Widget getUploadPanel() {
		uploadForm = new FormPanel();
		uploadForm.setVisible(false);
		uploadForm.setWidth("100%");

		uploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
		uploadForm.setMethod(FormPanel.METHOD_POST);

		HorizontalPanel panel = new HorizontalPanel();
		panel.setWidth("100%");
		panel.setSpacing(10);
		uploadForm.setWidget(panel);
		initUploadWidget();
		panel.add(uploadWidget);
		
		panel.setCellHorizontalAlignment(uploadWidget,
				HasHorizontalAlignment.ALIGN_LEFT);
		panel.setCellWidth(uploadWidget, "300px");
		return uploadForm;
	}
	
	public void initUploadWidget() {
		// FileUpload Widget
		uploadWidget = new FileUpload();
		uploadWidget.setWidth("300px");
		uploadWidget.setName("uploadWidget");
	}
	
	public FileUpload getUploadWidget() {
		return uploadWidget;
	}
	
	public FormPanel getUploadForm() {
		return uploadForm;
	}
	
}
