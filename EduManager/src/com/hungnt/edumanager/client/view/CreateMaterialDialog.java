package com.hungnt.edumanager.client.view;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.base.TextBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Material;

public class CreateMaterialDialog extends PopupPanel {
	
	public enum ActionMaterial{
		EDIT,CREATE,
	}
	
	private VerticalPanel mainPanel;
	private Material material;
	private Button btnSave;
	private TextBox tbName;
	private ListBox tbPrivacy;
	private TextBox tbDescription;
	private TextBox tbFileType;
	private TextBox tbUrl;
	private TextBox tbFileSize;
	private ActionMaterial currenAction;
	private Button btnUpload;
	private FormUploadDoc formUpload = new FormUploadDoc();
	private FlexTable flexTable = new FlexTable();
	
	public CreateMaterialDialog() {
		mainPanel = new VerticalPanel();
		mainPanel.setWidth("100%");
		mainPanel.setSpacing(10);
		mainPanel.add(formUpload.getUploadPanel());
		this.setWidget(mainPanel);
		this.setWidth("100%");
		this.setAutoHideEnabled(true);
		this.setAnimationEnabled(true);
		this.setModal(false);
		this.addStyleName("context-Menu");

		tbName = new TextBox();
		tbPrivacy = new ListBox();
		for (int i = 0; i <= 2; i ++) {
			if (!Config.getPrivacyValue(i).isEmpty())
				tbPrivacy.addItem(Config.getPrivacyValue(i), "" + i);
		}

		tbDescription = new TextBox();
		tbFileType = new TextBox();
		tbUrl = new TextBox();
		tbFileSize = new TextBox();
		btnSave = new Button("Lưu");
		btnUpload = new Button("Tải file");

		int col = 0, row = 0;
		flexTable.setText(row, col++, "");
		flexTable.setWidget(row, col, btnUpload);
		row ++; col = 0;
		flexTable.setText(row, col++, "Tên tài liệu");
		flexTable.setWidget(row, col, tbName);
		row ++; col = 0;
		flexTable.setText(row, col++, "Mô tả");
		flexTable.setWidget(row, col, tbDescription);
		row ++; col = 0;
		flexTable.setText(row, col++, "Định dạng");
		flexTable.setWidget(row, col, tbFileType);
		row ++; col = 0;
		flexTable.setText(row, col++, "Kích thước");
		flexTable.setWidget(row, col, tbFileSize);
		row ++; col = 0;
		flexTable.setText(row, col++, "Url");
		flexTable.setWidget(row, col, tbUrl);
		row ++; col = 0;
		flexTable.setText(row, col++, "Quyền truy cập");
		flexTable.setWidget(row, col, tbPrivacy);
		row ++; col = 0;
		flexTable.setText(row, col++, "");
		flexTable.setWidget(row, col, btnSave);
		row ++; col = 0;
		tbFileSize.setEnabled(false);
		tbUrl.setEnabled(false);
		
//		HorizontalPanel uploadPanel = new HorizontalPanel();
//		uploadPanel.add(genForm(new Label("URL"), tbUrl));
//		uploadPanel.add(btnUpload);
//		mainPanel.add(uploadPanel);
//		HorizontalPanel btnPanel = new HorizontalPanel();
//		btnPanel.setWidth("100%");
//		btnPanel.add(btnSave);
		mainPanel.add(flexTable);
	}

	public void showCreateDialog(Object obj,Long parentId) {
		if (obj == null) {
			tbName.setText("");
			tbPrivacy.setSelectedIndex(0);
			tbFileType.setText("");
			tbDescription.setText("");
			tbUrl.setText("");
			tbFileSize.setText("");
			currenAction = ActionMaterial.CREATE;
			material = new Material();
			material.setParentId(parentId);
		}else if(obj instanceof Material) {
			material = (Material)obj;
			tbName.setText(material.getName());
			tbPrivacy.setSelectedIndex(material.getPrivacy());
			tbFileType.setText(material.getFileType());
			tbDescription.setText(material.getDescription());
			tbUrl.setText(material.getUrl());
			tbFileSize.setText(material.getFileSize() + "");
			currenAction = ActionMaterial.EDIT;
		}
		center();
	}

//	private Widget genForm(Label label, Widget widget) {
//		HorizontalPanel hPanel = new HorizontalPanel();
//		hPanel.setWidth("100%");
//		hPanel.setSpacing(10);
//		hPanel.add(label);
//		hPanel.setCellWidth(label, "100px");
//		hPanel.add(widget);
//		hPanel.getElement().getStyle().setMarginTop(5, Unit.PX);
//		return hPanel;
//	}
	
	public Button getBtnSave() {
		return btnSave;
	}
	
	public TextBox getTbName() {
		return tbName;
	}
	
	public TextBox getTbDescription() {
		return tbDescription;
	}
	public TextBox getTbUrl() {
		return tbUrl;
	}
	public TextBox getTbFileType() {
		return tbFileType;
	}
	
	public TextBox getTbFileSize() {
		return tbFileSize;
	}
	public Button getBtnUpload() {
		return btnUpload;
	}
	
	public ActionMaterial getCurrenAction() {
		return currenAction;
	}
	
	public boolean isCheckInfor(){
		if(tbName.getText().trim().isEmpty()&& tbUrl.getText().trim().isEmpty()){
			return false;
		}
		return true;
	}
	
	public Material getCurrentMaterial(){
		try {
			material.setName(tbName.getText().trim());
			material.setDescription(tbDescription.getText().trim());
			material.setFileType(tbFileType.getText().trim());
			material.setUrl(tbUrl.getText().trim());
			material.setPrivacy(Integer.parseInt(tbPrivacy.getSelectedValue().trim()));
			try {
				Long fileSize = Long.parseLong(tbFileSize.getText().trim());
				material.setFileSize(fileSize);
			}catch (Exception e) {}
			return material;
		} catch (Exception e) {
			return null;
		}
	}
	public FormUploadDoc getFormUpload() {
		return formUpload;
	}
	
}
