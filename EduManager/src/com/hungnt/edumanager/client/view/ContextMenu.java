package com.hungnt.edumanager.client.view;


import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.hungnt.edumanager.client.activities.document.TreeInforItem;
import com.hungnt.edumanager.shared.Config;

/**
 * @author quantraitreo
 * 
 */
public class ContextMenu extends PopupPanel {
	
	public static enum TestScenarioType{
		EDIT,VIEW
	}

	private VerticalPanel panel;
	private HTML title;
	private TreeItem treeItem;
	private TreeInforItem curentTreeInforItem;
	private VerticalPanel rootPanel;
	
	private Button addCategoryBtn;
	//private Button addMaterialBtn;
	private Button refreshTreeBtn;
	
	private Button btnViewCategory;
	private Button btnAddChildCategory;
	private Button btnAddChildMaterial;
	private Button btnEdit;
	private Button btnMove;
	private Button btnCopy;
	private Button btnDelete;
	/**
	 * 
	 */
	public ContextMenu() {
		panel = new VerticalPanel();
		this.setWidget(panel);
		this.setAnimationEnabled(true);
		this.setAutoHideEnabled(true);
		this.addStyleName("context-Menu");

		title = new HTML();
		panel.add(title);
		panel.setCellHorizontalAlignment(title,
				HasHorizontalAlignment.ALIGN_CENTER);
		
		btnViewCategory = new Button("Xem thư mục");
		btnViewCategory.setType(ButtonType.LINK);
		btnViewCategory.setIcon(IconType.CAMERA);
		panel.add(btnViewCategory);
		panel.setCellHorizontalAlignment(btnViewCategory,
				HasHorizontalAlignment.ALIGN_LEFT);
		
		btnAddChildCategory = new Button("Tạo thư mục");
		btnAddChildCategory.setType(ButtonType.LINK);
		btnAddChildCategory.setIcon(IconType.PLUS);
		panel.add(btnAddChildCategory);
		panel.setCellHorizontalAlignment(btnAddChildCategory,
				HasHorizontalAlignment.ALIGN_LEFT);
		
		btnAddChildMaterial = new Button("Thêm tài liệu");
		btnAddChildMaterial.setType(ButtonType.LINK);
		btnAddChildMaterial.setIcon(IconType.PLUS);
		panel.add(btnAddChildMaterial);
		panel.setCellHorizontalAlignment(btnAddChildMaterial,
				HasHorizontalAlignment.ALIGN_LEFT);
		
		btnMove = new Button("Chuyển đến ...");
		btnMove.setType(ButtonType.LINK);
		btnMove.setIcon(IconType.SHARE_ALT);
		panel.add(btnMove);
		panel.setCellHorizontalAlignment(btnMove,
				HasHorizontalAlignment.ALIGN_LEFT);
		btnMove.setVisible(false);
		
		btnCopy = new Button("Copy đến ...");
		btnCopy.setType(ButtonType.LINK);
		btnCopy.setIcon(IconType.COPY);
		panel.add(btnCopy);
		panel.setCellHorizontalAlignment(btnCopy,
				HasHorizontalAlignment.ALIGN_LEFT);
		btnCopy.setVisible(false);

		btnEdit = new Button("Sửa");
		btnEdit.setType(ButtonType.LINK);
		btnEdit.setIcon(IconType.EDIT);
		panel.add(btnEdit);
		panel.setCellHorizontalAlignment(btnEdit,
				HasHorizontalAlignment.ALIGN_LEFT);
		
		btnDelete = new Button("Xoá");
		btnDelete.setType(ButtonType.LINK);
		btnDelete.setIcon(IconType.MINUS_SIGN);
		panel.add(btnDelete);
		panel.setCellHorizontalAlignment(btnDelete,
				HasHorizontalAlignment.ALIGN_LEFT);

		rootPanel = new VerticalPanel();
		
		addCategoryBtn = new Button("Tạo thư mục chính");
		addCategoryBtn.setType(ButtonType.LINK);
		addCategoryBtn.setIcon(IconType.PLUS);
		rootPanel.add(addCategoryBtn);
		rootPanel.setCellHorizontalAlignment(addCategoryBtn,
				HasHorizontalAlignment.ALIGN_LEFT);
		
//		addMaterialBtn = new Button("Add Material");
//		addMaterialBtn.setType(ButtonType.LINK);
//		addMaterialBtn.setIcon(IconType.PLUS);
//		rootPanel.add(addMaterialBtn);
//		rootPanel.setCellHorizontalAlignment(addMaterialBtn,
//				HasHorizontalAlignment.ALIGN_LEFT);
		
		refreshTreeBtn = new Button("Refresh");
		refreshTreeBtn.setType(ButtonType.LINK);
		refreshTreeBtn.setIcon(IconType.REFRESH);
		rootPanel.add(refreshTreeBtn);
		rootPanel.setCellHorizontalAlignment(refreshTreeBtn,
				HasHorizontalAlignment.ALIGN_LEFT);
	}

	public void open(Object obj, final int left, final int top, final Widget target) {
		if (obj != null) {
			this.treeItem = (TreeItem) obj;
			if (treeItem.getUserObject() instanceof TreeInforItem) {
				this.curentTreeInforItem = (TreeInforItem) treeItem.getUserObject();
				title.setHTML("<b>.::" + curentTreeInforItem.getName() + "::.</b>");
				this.setWidget(panel);
				if(curentTreeInforItem.getType()==Config.CATEGORY){
					btnAddChildCategory.setVisible(true);
					btnAddChildMaterial.setVisible(true);
					btnViewCategory.setVisible(true);
				}else if(curentTreeInforItem.getType()==Config.MATERIAL){
					btnAddChildCategory.setVisible(false);
					btnAddChildMaterial.setVisible(false);
					btnViewCategory.setVisible(false);
				}
			} 
		} else if (obj == null) {
			this.setWidget(rootPanel);
		}
		if (target != null) {
			this.setPopupPositionAndShow(new PositionCallback() {

                public void setPosition(int offsetWidth, int offsetHeight) {
                    ContextMenu.this.showRelativeTo(target);
                }
            });
		}
		else {
			center();
		}
		//show();
		//setPopupPosition(left, top);
	}

	@Override
	public void show() {
		super.show();
	}
	
	@Override
	public void hide() {
		super.hide();
	}

	public static void refreshParentTreeItem(TreeItem treeItem) {
		TreeItem parent = treeItem.getParentItem();
		if (parent == null) {
			return;
		}
		parent.setState(false, false);
		parent.setState(true, true);
	}
	
	public Button getBtnViewCategory() {
		return btnViewCategory;
	}

	public Button getAddCategoryBtn() {
		return addCategoryBtn;
	}

//	public Button getAddMaterialBtn() {
//		return addMaterialBtn;
//	}


	public Button getRefreshTreeBtn() {
		return refreshTreeBtn;
	}

	public Button getBtnAddChildCategory() {
		return btnAddChildCategory;
	}

	public Button getBtnAddChildMaterial() {
		return btnAddChildMaterial;
	}

	public Button getBtnEdit() {
		return btnEdit;
	}

	public Button getBtnMove() {
		return btnMove;
	}

	public Button getBtnCopy() {
		return btnCopy;
	}

	public Button getBtnDelete() {
		return btnDelete;
	}

	public TreeInforItem getCurentTreeInforItem() {
		return curentTreeInforItem;
	}
}
