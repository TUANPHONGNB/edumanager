package com.hungnt.edumanager.client.view;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.base.TextBox;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.hungnt.edumanager.shared.Category;
import com.hungnt.edumanager.shared.Config;

public class CreateCategoryDialog extends PopupPanel {
	
	public enum ActionCategory{
		EDIT,CREATE,
	}
	
	private VerticalPanel mainPanel;
	private Category curCategory;
	private Button btnSave;
	private TextBox tbName;
	private ListBox tbPrivacy;
	private ActionCategory currenAction;
	
	public CreateCategoryDialog() {
		mainPanel = new VerticalPanel();
		mainPanel.setWidth("100%");
		mainPanel.setSpacing(10);
		this.setWidget(mainPanel);
		this.setWidth("100%");
		this.setAutoHideEnabled(true);
		this.setAnimationEnabled(true);
		this.setModal(false);
		this.addStyleName("context-Menu");

		tbName = new TextBox();
		tbPrivacy = new ListBox();
		for (int i = 0; i <= 2; i ++) {
			if (!Config.getPrivacyValue(i).isEmpty())
				tbPrivacy.addItem(Config.getPrivacyValue(i), "" + i);
		}
		btnSave = new Button("Lưu");
		mainPanel.add(genForm(new Label("Tên thư mục"), tbName));
		mainPanel.add(genForm(new Label("Quyền truy cập"), tbPrivacy));
		
		HorizontalPanel btnPanel = new HorizontalPanel();
		btnPanel.setWidth("100%");
		btnPanel.add(btnSave);
		mainPanel.add(btnPanel);
	}

	public void showCreateDialog(Object obj,Long parentId, int left, int top) {
		if (obj == null) {
			tbName.setText("");
			tbPrivacy.setSelectedIndex(0);
			currenAction = ActionCategory.CREATE;
			curCategory = new Category();
			curCategory.setParentId(parentId);
		}else if(obj instanceof Category) {
			curCategory = (Category)obj;
			tbName.setText(curCategory.getName());
			tbPrivacy.setSelectedIndex(curCategory.getPrivacy());
			currenAction = ActionCategory.EDIT;
		}
		center();
//		this.setPopupPosition(left, top);
//		this.show();
	}

	private Widget genForm(Label label, Widget widget) {
		HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.setWidth("100%");
		hPanel.setSpacing(10);
		hPanel.add(label);
		hPanel.setCellWidth(label, "100px");
		hPanel.add(widget);
		hPanel.getElement().getStyle().setMarginTop(5, Unit.PX);
		return hPanel;
	}
	
	public Button getBtnSave() {
		return btnSave;
	}
	
	public TextBox getTbName() {
		return tbName;
	}
	
	public ListBox getTbPrivacy() {
		return tbPrivacy;
	}
	
	public ActionCategory getCurrenAction() {
		return currenAction;
	}
	
	public Category getCurCategory() {
		try {
			curCategory.setName(tbName.getText().trim());
			curCategory.setPrivacy(Integer.parseInt(tbPrivacy.getSelectedValue().trim()));
			return curCategory;
		} catch (Exception e) {
			return null;
		}
	}
}
