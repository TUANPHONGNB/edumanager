package com.hungnt.edumanager.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class LoginEvent extends GwtEvent<LoginEventHandler>{

	public static Type<LoginEventHandler> TYPE = new Type<LoginEventHandler>();
	
	public boolean isLoggedIn = false;
	
	public LoginEvent(boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<LoginEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(LoginEventHandler handler) {
		handler.onLoggedIn(this);
	}

	public boolean isLoggedIn() {
		return this.isLoggedIn;
	}
}
