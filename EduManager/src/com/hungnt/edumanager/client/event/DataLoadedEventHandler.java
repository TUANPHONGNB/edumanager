package com.hungnt.edumanager.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface DataLoadedEventHandler extends EventHandler {
	void onDataLoaded(DataLoadedEvent event);
}
