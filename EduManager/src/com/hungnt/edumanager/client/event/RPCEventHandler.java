package com.hungnt.edumanager.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface RPCEventHandler extends EventHandler {
	void onRPC(RPCEvent event);
}
