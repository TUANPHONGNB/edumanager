package com.hungnt.edumanager.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface EditEventHandler extends EventHandler {
	void onEdit(EditEvent event);
}
