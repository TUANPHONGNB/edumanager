package com.hungnt.edumanager.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class EditEvent extends GwtEvent<EditEventHandler>{

	public static Type<EditEventHandler> TYPE = new Type<EditEventHandler>();
	
	public Object object = 0;
	
	public EditEvent(Object obj) {
		this.object = obj;
	}
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<EditEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(EditEventHandler handler) {
		handler.onEdit(this);
	}

	public Object getObject() {
		return this.object;
	}
}
