package com.hungnt.edumanager.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface LoginEventHandler extends EventHandler {
	void onLoggedIn(LoginEvent event);
}
