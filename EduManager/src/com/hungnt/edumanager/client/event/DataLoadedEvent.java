package com.hungnt.edumanager.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class DataLoadedEvent extends GwtEvent<DataLoadedEventHandler>{

	public static Type<DataLoadedEventHandler> TYPE = new Type<DataLoadedEventHandler>();
	
	public int dataType = 0;
	
	public DataLoadedEvent(int dataType) {
		this.dataType = dataType;
	}
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<DataLoadedEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(DataLoadedEventHandler handler) {
		handler.onDataLoaded(this);
	}

	public int getDataType() {
		return this.dataType;
	}
}
