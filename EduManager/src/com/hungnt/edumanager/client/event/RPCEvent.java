package com.hungnt.edumanager.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class RPCEvent extends GwtEvent<RPCEventHandler>{

	public static Type<RPCEventHandler> TYPE = new Type<RPCEventHandler>();
	
	public static final int LOADING_IN = 1;
	public static final int LOADING_OUT = 2;
	public static final int SAVING_IN = 3;
	public static final int SAVING_OUT = 4;
	public static final int PLAYING = 5;
	
	private int rpcType = 0;
	
	public RPCEvent(int rpcType) {
		this.rpcType = rpcType;
	}
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<RPCEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(RPCEventHandler handler) {
		handler.onRPC(this);
	}

	public int getRpcType() {
		return rpcType;
	}
}
