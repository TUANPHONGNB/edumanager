package com.hungnt.edumanager.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.RequestTimeoutException;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.SerializationException;
import com.hungnt.edumanager.client.event.RPCEvent;
import com.hungnt.edumanager.shared.ServerException;

public abstract class RPCCall<T> implements AsyncCallback<T> {
	protected abstract void callService(AsyncCallback<T> cb);

	private void call(final int retriesLeft) {
		onRPCOut();

		callService(new AsyncCallback<T>() {
			@Override
			public void onFailure(Throwable caught) {
				onRPCIn();
				GWT.log(caught.toString(), caught);
				try {
					throw caught;
				} catch (InvocationException invocationException) {
					if (retriesLeft <= 0) {
						RPCCall.this.onFailure(invocationException);
					} else {
						call(retriesLeft - 1); // retry call
					}
				} catch (IncompatibleRemoteServiceException remoteServiceException) {
					Window.alert("The app maybe out of date. Reload this page in your browser.");
					History.fireCurrentHistoryState();
				} catch (SerializationException serializationException) {
					Window.alert("A serialization error occurred. Try again.");
				} catch (RequestTimeoutException e) {
					Window.alert("This is taking too long, try again");
				} catch (ServerException e) {
					Window.alert(e.getMessage());
				} catch (Throwable e) {// application exception
					RPCCall.this.onFailure(e);
				}
			}

			@Override
			public void onSuccess(T result) {
				onRPCIn();
				RPCCall.this.onSuccess(result);
			}
		});
	}

	private void onRPCIn() {
		EduManager.loadingView.stopProcessing();
		EduManager.clientFactory.getEventBus().fireEvent(
				new RPCEvent(RPCEvent.LOADING_IN));
	}

	private void onRPCOut() {
		EduManager.loadingView.startProcessing();
		EduManager.clientFactory.getEventBus().fireEvent(
				new RPCEvent(RPCEvent.LOADING_OUT));
	}

	public void retry(int retryCount) {
		call(retryCount);
	}

	public void retry() {
		call(0);
	}
}