package com.hungnt.edumanager.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.hungnt.edumanager.client.event.DataLoadedEvent;
import com.hungnt.edumanager.client.event.LoginEvent;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Course;
import com.hungnt.edumanager.shared.Equipment;
import com.hungnt.edumanager.shared.Event;
import com.hungnt.edumanager.shared.InternTopic;
import com.hungnt.edumanager.shared.MeetingMinutes;
import com.hungnt.edumanager.shared.Publication;
import com.hungnt.edumanager.shared.Subject;
import com.hungnt.edumanager.shared.Teacher;

public class ClientData {
	private static Teacher currentUser;
	
	private static List<Teacher> teachers = new ArrayList<Teacher>();
	private static List<Subject> subjects = new ArrayList<Subject>();
	private static List<Course> courses = new ArrayList<Course>();
	private static List<InternTopic> topics = new ArrayList<InternTopic>();
	private static List<Equipment> equipments = new ArrayList<Equipment>();
	private static List<Event> events = new ArrayList<Event>();
	private static List<MeetingMinutes> meetingMinutes = new ArrayList<MeetingMinutes>();
	private static List<Publication> publications = new ArrayList<Publication>();
	private static Map<Long, Teacher> teacherMap = new HashMap<Long, Teacher>();
	private static Map<Long, Subject> subjectMap = new HashMap<Long, Subject>();
	private static Map<Long, Course> courseMap = new HashMap<Long, Course>();
	private static Map<Long, InternTopic> topicMap = new HashMap<Long, InternTopic>();
	private static Map<Long, Publication> publicationMap = new HashMap<Long, Publication>();
	
	public static Teacher getCurrentUser() {
		return currentUser;
	}
	
	public static void setCurrentUser(Teacher user) {
		currentUser = user;
		if (user != null) {
			Cookies.setCookie(Config.LOGIN_NAME_SPACE, currentUser.getSessionId(), new Date(new Date().getTime() + 10000*60*60*24*7));
		}
		EduManager.clientFactory.getEventBus().fireEvent(new LoginEvent(user != null));
	}
	
	public static boolean isLoggedIn() {
		return currentUser != null;
	}
	
	public static boolean isLoggedIn(boolean mustLogin) {
		boolean loggedIn = isLoggedIn();
		if (mustLogin && !loggedIn)
			EduManager.clientFactory.getBasicView().getLoginView().view();
		return loggedIn;
	}

	public static List<Teacher> getTeachers() {
		return teachers;
	}
	
	public static List<Publication> getPublications() {
		return publications;
	}
	
	public static List<Equipment> getEquipments() {
		return equipments;
	}
	
	public static List<Event> getEvents() {
		return events;
	}
	
	public static List<MeetingMinutes> getMeetingMinutes() {
		return meetingMinutes;
	}
	
	public static void setMeetingMinutes(List<MeetingMinutes> list) {
		meetingMinutes = list;
	}
	
	public static boolean canEdit(Long teacherId, boolean warning) {
		boolean can = isLoggedIn() 
				&& (currentUser.isAdmin() || currentUser.getId().compareTo(teacherId) == 0);
		if (!can && warning)
			ClientUtils.noPermission();
		return can;
	}

	public static void setTeachers(List<Teacher> teachers) {
		ClientData.teachers = teachers;
		Collections.sort(ClientData.teachers, new Comparator<Teacher>() {
			@Override
			public int compare(Teacher o1, Teacher o2) {
				int c = o1.getLastName().compareToIgnoreCase(o2.getLastName());
				if (c == 0)
					c = o1.getFirstName().compareToIgnoreCase(o2.getFirstName());
				return c;
			}
		});
		teacherMap.clear();
		for (Teacher s : ClientData.teachers)
			teacherMap.put(s.getId(), s);
		EduManager.clientFactory.getEventBus().fireEvent(new DataLoadedEvent(Config.DATA_TEACHER));
	}

	public static List<Subject> getSubjects() {
		return subjects;
	}

	public static void setSubjects(List<Subject> subjects) {
		ClientData.subjects = subjects;
		Collections.sort(ClientData.subjects, new Comparator<Subject>() {
			@Override
			public int compare(Subject o1, Subject o2) {
				return o1.getName().compareToIgnoreCase(o2.getName());
			}
		});
		subjectMap.clear();
		for (Subject s : ClientData.subjects)
			subjectMap.put(s.getId(), s);
		EduManager.clientFactory.getEventBus().fireEvent(new DataLoadedEvent(Config.DATA_SUBJECT));
	}
	
	public static void setPublications(List<Publication> publications) {
		ClientData.publications = publications;
		Collections.sort(ClientData.publications, new Comparator<Publication>() {
			@Override
			public int compare(Publication o1, Publication o2) {
				return o1.getTitle().compareToIgnoreCase(o2.getTitle());
			}
		});
		publicationMap.clear();
		for (Publication s : ClientData.publications)
			publicationMap.put(s.getId(), s);
		EduManager.clientFactory.getEventBus().fireEvent(new DataLoadedEvent(Config.DATA_PUBLICATION));
	}

	public static List<Course> getCourses() {
		return courses;
	}
	
	public static List<InternTopic> getTopics() {
		return topics;
	}
	
	public static void setTopics(List<InternTopic> topics) {
		ClientData.topics = topics;
		topicMap.clear();
		for (InternTopic course : topics)
			topicMap.put(course.getId(), course);
	}

	public static void setCourses(List<Course> courses) {
		ClientData.courses = courses;
		courseMap.clear();
		for (Course course : courses)
			courseMap.put(course.getId(), course);
	}
	
	public static void setEquipments(List<Equipment> equipments) {
		ClientData.equipments = equipments;
	}
	
	public static void setEvents(List<Event> events) {
		ClientData.events = events;
	}

	public static Map<Long, Teacher> getTeacherMap() {
		return teacherMap;
	}

	public static Map<Long, Subject> getSubjectMap() {
		return subjectMap;
	}

	public static Map<Long, Course> getCourseMap() {
		return courseMap;
	}
	
	public static Map<Long, InternTopic> getTopicMap() {
		return topicMap;
	}
	
	public static Map<Long, Publication> getPublciationMap() {
		return publicationMap;
	}
	
	public static void loadData(final int scholarYear, final int semester) {
		if (teachers.size() == 0) {
			new RPCCall<List<Teacher>>(){

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Có lỗi xảy ra, không thể tải dữ liệu");
				}

				@Override
				public void onSuccess(List<Teacher> result) {
					ClientData.setTeachers(result);
					EduManager.clientFactory.getEventBus().fireEvent(new DataLoadedEvent(Config.DATA_TEACHER));
				}

				@Override
				protected void callService(AsyncCallback<List<Teacher>> cb) {
					EduManager.dataService.getTeachers(null, Config.currentScholarYear, cb);
					
			}}.retry();
		}
		if (subjects.size() == 0) {
			new RPCCall<List<Subject>>() {

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Có lỗi xảy ra, không thể tải danh sách môn học");
				}

				@Override
				public void onSuccess(List<Subject> result) {
					ClientData.setSubjects(result);
					EduManager.clientFactory.getEventBus().fireEvent(new DataLoadedEvent(Config.DATA_SUBJECT));
				}

				@Override
				protected void callService(AsyncCallback<List<Subject>> cb) {
					EduManager.dataService.getSubjects(null, cb);
				}
			}.retry();
		}
		if (courses.size() == 0) {
			new RPCCall<List<Course>>() {

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Có lỗi xảy ra, không thể tải được các lớp học");
				}

				@Override
				public void onSuccess(List<Course> result) {
					ClientData.setCourses(result);
					EduManager.clientFactory.getEventBus().fireEvent(new DataLoadedEvent(Config.DATA_COURSE));
				}

				@Override
				protected void callService(AsyncCallback<List<Course>> cb) {
					EduManager.dataService.getCourses(null, scholarYear, semester, cb);
				}
			}.retry();
		}
		
		if (topics.size() == 0) {
			new RPCCall<List<InternTopic>>() {

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Có lỗi xảy ra, không thể tải được đề tài");
				}

				@Override
				public void onSuccess(List<InternTopic> result) {
					ClientData.setTopics(result);
					EduManager.clientFactory.getEventBus().fireEvent(new DataLoadedEvent(Config.DATA_TOPIC));
				}

				@Override
				protected void callService(AsyncCallback<List<InternTopic>> cb) {
					EduManager.dataService.getTopics(null, scholarYear, semester, cb);
				}
			}.retry();
		}
		
		if (publications.size() == 0) {
			new RPCCall<List<Publication>>() {

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Có lỗi xảy ra, không thể tải được đề tài");
				}

				@Override
				public void onSuccess(List<Publication> result) {
					ClientData.setPublications(result);
					EduManager.clientFactory.getEventBus().fireEvent(new DataLoadedEvent(Config.DATA_PUBLICATION));
				}

				@Override
				protected void callService(AsyncCallback<List<Publication>> cb) {
					EduManager.dataService.getPublications(null, cb);
				}
			}.retry();
		}
	}
}
