package com.hungnt.edumanager.client;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.hungnt.edumanager.client.activities.AppActivityMapper;
import com.hungnt.edumanager.client.activities.AppPlaceHistoryMapper;
import com.hungnt.edumanager.client.activities.ClientFactory;
import com.hungnt.edumanager.client.activities.ClientFactoryImpl;
import com.hungnt.edumanager.client.activities.home.HomePlace;
import com.hungnt.edumanager.client.widget.AppLoadingView;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Teacher;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class EduManager implements EntryPoint {
	public static final DataServiceAsync dataService = GWT
			.create(DataService.class);
	
	private SimplePanel appWidget = new SimplePanel();
	public static ClientFactory clientFactory = new ClientFactoryImpl();
	public static AppLoadingView loadingView = new AppLoadingView();

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		EventBus eventBus = clientFactory.getEventBus();
		PlaceController placeController = clientFactory.getPlaceController();

		// Start ActivityManager for the main widget with our ActivityMapper
		ActivityMapper activityMapper = new AppActivityMapper(clientFactory);
		ActivityManager activityManager = new ActivityManager(activityMapper, eventBus);
		activityManager.setDisplay(appWidget);

		// Start PlaceHistoryHandler with our PlaceHistoryMapper
		try {
			
		AppPlaceHistoryMapper historyMapper= GWT.create(AppPlaceHistoryMapper.class);
		PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper);
		historyHandler.register(placeController, eventBus, new HomePlace());

		RootPanel.get().add(appWidget);
		historyHandler.handleCurrentHistory();
		} catch (Exception e) {
			Window.alert(e.toString());
		}
		
		final String sessionId = Cookies.getCookie(Config.LOGIN_NAME_SPACE);
		ClientUtils.log("SessionId: " + sessionId);
		if (sessionId != null && !sessionId.isEmpty()) {
			new RPCCall<Teacher>() {
				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(Teacher result) {
					ClientData.setCurrentUser(result);
				}

				@Override
				protected void callService(AsyncCallback<Teacher> cb) {
					dataService.checkLogin(sessionId, cb);
				}
			}.retry();
		}
		ChartLoader chartLoader = new ChartLoader(ChartPackage.CORECHART);
		 chartLoader.loadApi(new Runnable() {
		        @Override
		        public void run() {
		                // call method to create and show the chart
		        }
		 });
	}
}
