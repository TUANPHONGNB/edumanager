package com.hungnt.edumanager.client;

import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.hungnt.edumanager.shared.Category;
import com.hungnt.edumanager.shared.Course;
import com.hungnt.edumanager.shared.Equipment;
import com.hungnt.edumanager.shared.EquipmentHistory;
import com.hungnt.edumanager.shared.Event;
import com.hungnt.edumanager.shared.InternTopic;
import com.hungnt.edumanager.shared.Material;
import com.hungnt.edumanager.shared.MeetingMinutes;
import com.hungnt.edumanager.shared.Publication;
import com.hungnt.edumanager.shared.Subject;
import com.hungnt.edumanager.shared.Teacher;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface DataServiceAsync {

	void updateTeacher(String userId, Teacher teacher, AsyncCallback<Teacher> callback);
	void updateSubject(String userId, Subject obj, AsyncCallback<Subject> callback);
	void updateCourse(String userId, Course obj, AsyncCallback<Course> callback);
	void getTeachers(String userId, int scholarYear, AsyncCallback<List<Teacher>> callback);
	void getSubjects(String userId, AsyncCallback<List<Subject>> callback);
	void getCourses(String userId, int scholarYear, int semester, AsyncCallback<List<Course>> callback);
	void login(String sessionId, String userName, String password, AsyncCallback<Teacher> callback);
	void getPublications(String userId,
			AsyncCallback<List<Publication>> callback);
	void updatePublication(String userId, Publication obj,
			AsyncCallback<Publication> callback);
	void getBlobstoreUploadUrl(AsyncCallback<String> callback);
	void checkLogin(String sessionId, AsyncCallback<Teacher> callback);
	void logout(String sessionId, AsyncCallback<Void> callback);
	void updateEquipment(String userId, Equipment object,
			AsyncCallback<Equipment> callback);
	void getEquipments(String userId, Date now, AsyncCallback<List<Equipment>> callback);
	void export(Long userId, Teacher teacher, AsyncCallback<String> callback);
	void updateTopic(String userId, InternTopic obj,
			AsyncCallback<InternTopic> callback);
	void getTopics(String userId, int scholarYear, int semester,
			AsyncCallback<List<InternTopic>> callback);
	void updateAB(AsyncCallback<Void> callback);
	void deleteTopic(String userId, InternTopic obj,
			AsyncCallback<Void> callback);
	void updateEquipmentHistory(EquipmentHistory history,
			AsyncCallback<EquipmentHistory> callback);
	void deleteCourse(Long userId, Course obj, AsyncCallback<Void> callback);
	void doUpdateCategory(Category category, AsyncCallback<Long> callback);
	void getCategories(Long parentId, Long ownerId, Long viewerId,
			AsyncCallback<List<Category>> callback);
	void getChildCategoriesAndMaterials(Long parentId,
			AsyncCallback<Category> callback);
	void doUpdateMaterial(Material material, AsyncCallback<Long> callback);
	void delete(Long userId, Material material, AsyncCallback<Void> callback);
	void getEvents(String userId, Date now, AsyncCallback<List<Event>> callback);
	void updateEvent(String userId, Event object, AsyncCallback<Event> callback);
	void changePassword(String sessionId, Long id, String oldPassword,
			String newPassword, AsyncCallback<Teacher> callback);
	void updateStatistics(long userId, int scholarYear, int semester,
			AsyncCallback<Void> callback);
	void doUpdateMeeting(MeetingMinutes meeting, AsyncCallback<Long> callback);
	void getMeetings(int scholarYear, int semester,
			AsyncCallback<List<MeetingMinutes>> callback);
}
