package com.hungnt.edumanager.shared;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Publication implements IBasic {
	@Id private Long id;
	@Index private Long userId = Config.NULL_ID;
	private String title = Config.NULL_TXT;
	private String authors = Config.NULL_TXT;
	private String bookName = Config.NULL_TXT;		//Conference name, paper name, ...
	@Index private Integer paperType = 0;			//0: other, 1: int. conf, 2: int. journal, 3: int. book, 4: national conf. 5: national journal, 6: national book
	private String publishName = Config.NULL_TXT;	//ACM, IEEE, ...
	@Index private Integer rankingType = 0;			//0: non-isi, 1: isi, ...
	private String pageRange = Config.NULL_TXT;		//page 100-200, ...
	private String publishPlace = Config.NULL_TXT;	//New York, Paris, ...
	private String info = Config.NULL_TXT;			//Additional info
	@Index private Integer month = 0;
	@Index private Integer year = 0;
	@Index private Integer departmentId = 0;		//0: CNPM, 1: HTTT, 2: KHMT
	private String issn = Config.NULL_TXT;
	private String volume = Config.NULL_TXT;
	private List<Long> authorIds = new ArrayList<Long>();
	@Ignore private Integer posIndex = 0;
	
	private List<Long> materialIds = new ArrayList<Long>();
	@Ignore private List<Material> materials = new ArrayList<Material>();
	
	public Publication() {}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthors() {
		return authors;
	}
	public void setAuthors(String authors) {
		this.authors = authors;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public Integer getPaperType() {
		return paperType;
	}
	public void setPaperType(Integer paperType) {
		this.paperType = paperType;
	}
	public String getPublishName() {
		return publishName;
	}
	public void setPublishName(String publishName) {
		this.publishName = publishName;
	}
	public Integer getRankingType() {
		return rankingType;
	}
	public void setRankingType(Integer rankingType) {
		this.rankingType = rankingType;
	}
	public String getPageRange() {
		return pageRange;
	}
	public void setPageRange(String pageRange) {
		this.pageRange = pageRange;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getIssn() {
		return issn;
	}
	public void setIssn(String issn) {
		this.issn = issn;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public List<Long> getAuthorIds() {
		return authorIds;
	}
	public void setAuthorIds(List<Long> authorIds) {
		this.authorIds = authorIds;
	}

	public Integer getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	public String getPublishPlace() {
		return publishPlace;
	}

	public void setPublishPlace(String place) {
		this.publishPlace = place;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Integer getPosIndex() {
		return posIndex;
	}

	public void setPosIndex(Integer posIndex) {
		this.posIndex = posIndex;
	}

	public List<Long> getMaterialIds() {
		return materialIds;
	}

	public void setMaterialIds(List<Long> materialIds) {
		this.materialIds = materialIds;
	}

	public List<Material> getMaterials() {
		return materials;
	}

	public void setMaterials(List<Material> materials) {
		this.materials = materials;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getPublishInfo() {
		String info = this.bookName + " - " + this.month + "/" + this.year;
		if (this.issn != null && !this.issn.isEmpty())
			info += " - ISSN: " + this.issn;
		if (this.publishName != null && !this.publishName.isEmpty())
			info += " - " + this.publishName;
		if (this.pageRange != null && !this.pageRange.isEmpty())
			info += " - Page " + this.pageRange;
		return info;
	}
}
