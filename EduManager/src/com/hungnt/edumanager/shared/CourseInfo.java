package com.hungnt.edumanager.shared;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class CourseInfo implements IBasic {
	@Id private Long id;
	private String place;
	@Index private Integer day;	//Monday, Tuesday, ...
	@Index private Integer from;	//Tiet 1,2,3,4....
	@Index private Integer to;		//Tiet 4,5,6,7....
	
	public CourseInfo() {}
	public CourseInfo(int day, int from, int to, String place) {
		this.setDay(day);
		this.setFrom(from);
		this.setTo(to);
		this.setPlace(place);
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public Integer getDay() {
		return day;
	}
	public void setDay(Integer day) {
		this.day = day;
	}
	public Integer getFrom() {
		return from;
	}
	public void setFrom(Integer from) {
		this.from = from;
	}
	public Integer getTo() {
		return to;
	}
	public void setTo(Integer to) {
		this.to = to;
	}
	
	public String toHtml() {
		String time = (from >= 13) ? "</b><b>Buổi tối</b>" :  "</b>Tiết <b>" + from + "-" + to + "</b>";
		String str = "<b>" + Config.getDay(day) + "</b> " + time + " tại <b>" + place + "</b>";
		return str;
	}
	
	public String toString() {
		String time = (from >= 13) ? "Buổi tối" :  "Tiết " + from + "-" + to;
		String str = Config.getDay(day) + " " + time + " " + " tại " + place;
		return str;
	}
}
