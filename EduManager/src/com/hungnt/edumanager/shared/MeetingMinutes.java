package com.hungnt.edumanager.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class MeetingMinutes implements IBasic {
	@Id private Long id;
	@Index private Long userId = Config.NULL_ID;
	private String title = Config.NULL_TXT;
	private String content = Config.NULL_TXT;
	@Index private Integer semester = 0;						//1, 2, 3
	@Index private Integer status = 0;							//0: not yet, 1: on-going, 2: done
	@Index private Integer notification = 0;							//0: not yet, 1: on-going, 2: done
	@Index private int scholarYear = Config.currentScholarYear;		//0: 2010-2011, 1: 2011-2012, 2: 2012-2013, 3: 2013-2014, 4: 2014-2015
	private String place = Config.NULL_TXT;
	private String time = Config.NULL_TXT;
	private Long meetingChairId = Config.NULL_ID;
	private String meetingChair = Config.NULL_TXT;
	private Long reporterId = Config.NULL_ID; 
	private String reporter = Config.NULL_TXT; 
	@Index private Date date = null;
	private List<Long> participantIds = new ArrayList<Long>();
	private List<Long> absentIds = new ArrayList<Long>();
	private List<String> attacheds = new ArrayList<String>();
	@Ignore private List<MeetingAbsent> absentReasons = new ArrayList<MeetingAbsent>();
	
	public MeetingMinutes() {
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Integer getSemester() {
		return semester;
	}
	public void setSemester(Integer semester) {
		this.semester = semester;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getNotification() {
		return notification;
	}
	public void setNotification(Integer notification) {
		this.notification = notification;
	}
	public int getScholarYear() {
		return scholarYear;
	}
	public void setScholarYear(int scholarYear) {
		this.scholarYear = scholarYear;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public List<Long> getParticipantIds() {
		return participantIds;
	}
	public void setParticipantIds(List<Long> participantIds) {
		this.participantIds = participantIds;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Long getMeetingChairId() {
		return meetingChairId;
	}
	public void setMeetingChairId(Long meetingChairId) {
		this.meetingChairId = meetingChairId;
	}
	public String getMeetingChair() {
		return meetingChair;
	}
	public void setMeetingChair(String meetingChair) {
		this.meetingChair = meetingChair;
	}
	public Long getReporterId() {
		return reporterId;
	}
	public void setReporterId(Long reporterId) {
		this.reporterId = reporterId;
	}
	public String getReporter() {
		return reporter;
	}
	public void setReporter(String reporter) {
		this.reporter = reporter;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<Long> getAbsentIds() {
		return absentIds;
	}
	public void setAbsentIds(List<Long> absentIds) {
		this.absentIds = absentIds;
	}

	public List<MeetingAbsent> getAbsentReasons() {
		return absentReasons;
	}

	public void setAbsentReasons(List<MeetingAbsent> absentReasons) {
		this.absentReasons = absentReasons;
	}

	public List<String> getAttacheds() {
		return attacheds;
	}

	public void setAttacheds(List<String> attacheds) {
		this.attacheds = attacheds;
	}
}
