package com.hungnt.edumanager.shared;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Statistics implements IBasic {
	@Id private String id;
	@Index private Long teacherId = Config.NULL_ID;
	private String teacherName = Config.NULL_TXT;
	@Index private Integer scholarYear = 0;
	@Index private Integer semester = 0;
	@Index private Integer course1ANum = 0;
	@Index private Integer course1BNum = 0;
	@Index private Integer project1ANum = 0;
	@Index private Integer project1BNum = 0;
	@Index private Integer totalCourseNum = 0;
	@Index private Integer publicationNum = 0;
	@Index private Integer studentNum = 0;
	@Index private Integer topicNum = 0;
	
	private List<Long> ksClassIds = new ArrayList<Long>();
	private List<Long> cncnClassIds = new ArrayList<Long>();
	private List<Long> clcClassIds = new ArrayList<Long>();
	private List<Long> kstnClassIds = new ArrayList<Long>();
	private List<Long> ictClassIds = new ArrayList<Long>();
	private List<Long> vnClassIds = new ArrayList<Long>();
	private List<Long> ctttClassIds = new ArrayList<Long>();
	private List<Long> sdhClassIds = new ArrayList<Long>();
	private List<Long> sieClassIds = new ArrayList<Long>();
	private List<Long> tcClassIds = new ArrayList<Long>();
	private List<Long> cdClassIds = new ArrayList<Long>();
	private List<Long> ks2ClassIds = new ArrayList<Long>();
	private List<Long> dhlkClassIds = new ArrayList<Long>();
	
	private Integer project1Num = 0;
	private Integer project2Num = 0;
	private Integer project3Num = 0;
	private Integer cnDatnNum = 0;
	private Integer ksDatnNum = 0;
	private Integer ksProject1Num = 0;
	private Integer ksProject2Num = 0;
	private Integer ksProject3Num = 0;
	private Integer kstnProject2Num = 0;
	private Integer kstnDatnNum = 0;
	private Integer clcTtcnNum = 0;
	private Integer clcTttnNum = 0;
	private Integer clcDatnNum = 0;
	private Integer ictGr1Num = 0;
	private Integer ictGr2Num = 0;
	private Integer ictGr3Num = 0;
	private Integer ictDatnNum = 0;
	private Integer vnGr1Num = 0;
	private Integer vnGr2Num = 0;
	private Integer vnGr3Num = 0;
	private Integer vnDatnNum = 0;
	private Integer sieDatnNum = 0;
	private Integer tcDatnNum = 0;
	private Integer cdDatnNum = 0;
	private Integer ks2DatnNum = 0;
	
	@Ignore private Integer posIndex = 0;
	
	public Statistics() {}
	
	public Statistics(String statId, Long teacherId, int scholarYear, int semester) {
		this.id = statId;
		this.teacherId = teacherId;
		this.scholarYear = scholarYear;
		this.semester = semester;
	}

	public void reset() {
		project1Num = 0;
		project2Num = 0;
		project3Num = 0;
		cnDatnNum = 0;
		ksDatnNum = 0;
		kstnProject2Num = 0;
		kstnDatnNum = 0;
		clcTtcnNum = 0;
		clcTttnNum = 0;
		clcDatnNum = 0;
		ictGr1Num = 0;
		ictGr2Num = 0;
		ictGr3Num = 0;
		ictDatnNum = 0;
		vnGr1Num = 0;
		vnGr2Num = 0;
		vnGr3Num = 0;
		vnDatnNum = 0;
		sieDatnNum = 0;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public Integer getScholarYear() {
		return scholarYear;
	}

	public void setScholarYear(Integer scholarYear) {
		this.scholarYear = scholarYear;
	}

	public Integer getSemester() {
		return semester;
	}

	public void setSemester(Integer semester) {
		this.semester = semester;
	}

	public Integer getCourse1ANum() {
		return course1ANum;
	}

	public void setCourse1ANum(Integer course1aNum) {
		course1ANum = course1aNum;
	}
	
	public void addCourse1ANum(int num) {
		setCourse1ANum(course1ANum + num);
		setTotalCourseNum(totalCourseNum + num);
	}

	public Integer getCourse1BNum() {
		return course1BNum;
	}

	public void setCourse1BNum(Integer course1bNum) {
		course1BNum = course1bNum;
	}

	public void addCourse1BNum(int num) {
		setCourse1BNum(course1BNum + num);
		setTotalCourseNum(totalCourseNum + num);
	}
	
	public Integer getTotalCourseNum() {
		return totalCourseNum;
	}

	public void setTotalCourseNum(Integer totalCourseNum) {
		this.totalCourseNum = totalCourseNum;
	}
	
	public void addPublication(int num) {
		setPublicationNum(publicationNum + num);
	}

	public Integer getPublicationNum() {
		return publicationNum;
	}

	public void setPublicationNum(Integer publicationNum) {
		this.publicationNum = publicationNum;
	}

	public Integer getStudentNum() {
		return studentNum;
	}

	public void setStudentNum(Integer studentNum) {
		this.studentNum = studentNum;
	}

	public void addStudent(int num) {
		setStudentNum(studentNum + num);
	}
	
	public Integer getPosIndex() {
		return posIndex;
	}

	public void setPosIndex(Integer posIndex) {
		this.posIndex = posIndex;
	}

	public Integer getTopicNum() {
		return topicNum;
	}

	public void setTopicNum(Integer topicNum) {
		this.topicNum = topicNum;
	}
	
	public void addTopic(int num) {
		setTopicNum(topicNum + num);
	}

	public Integer getProject1Num() {
		return project1Num;
	}

	public void setProject1Num(Integer project1Num) {
		this.project1Num = project1Num;
	}

	public Integer getProject2Num() {
		return project2Num;
	}

	public void setProject2Num(Integer project2Num) {
		this.project2Num = project2Num;
	}

	public Integer getProject3Num() {
		return project3Num;
	}

	public void setProject3Num(Integer project3Num) {
		this.project3Num = project3Num;
	}

	public Integer getKsDatnNum() {
		return ksDatnNum;
	}

	public void setKsDatnNum(Integer datnKsNum) {
		this.ksDatnNum = datnKsNum;
	}

	public Integer getKstnProject2Num() {
		return kstnProject2Num;
	}

	public void setKstnProject2Num(Integer kstnProject2Num) {
		this.kstnProject2Num = kstnProject2Num;
	}

	public Integer getKstnDatnNum() {
		return kstnDatnNum;
	}

	public void setKstnDatnNum(Integer kstnDatnNum) {
		this.kstnDatnNum = kstnDatnNum;
	}

	public Integer getClcTtcnNum() {
		return clcTtcnNum;
	}

	public void setClcTtcnNum(Integer clcTtcnNum) {
		this.clcTtcnNum = clcTtcnNum;
	}

	public Integer getClcTttnNum() {
		return clcTttnNum;
	}

	public void setClcTttnNum(Integer clcTttnNum) {
		this.clcTttnNum = clcTttnNum;
	}

	public Integer getClcDatnNum() {
		return clcDatnNum;
	}

	public void setClcDatnNum(Integer clcDatnNum) {
		this.clcDatnNum = clcDatnNum;
	}

	public Integer getIctGr1Num() {
		return ictGr1Num;
	}

	public void setIctGr1Num(Integer ictGr1Num) {
		this.ictGr1Num = ictGr1Num;
	}

	public Integer getIctGr2Num() {
		return ictGr2Num;
	}

	public void setIctGr2Num(Integer ictGr2Num) {
		this.ictGr2Num = ictGr2Num;
	}

	public Integer getIctGr3Num() {
		return ictGr3Num;
	}

	public void setIctGr3Num(Integer ictGr3Num) {
		this.ictGr3Num = ictGr3Num;
	}

	public Integer getIctDatnNum() {
		return ictDatnNum;
	}

	public void setIctDatnNum(Integer ictDatnNum) {
		this.ictDatnNum = ictDatnNum;
	}

	public Integer getVnGr1Num() {
		return vnGr1Num;
	}

	public void setVnGr1Num(Integer vnGr1Num) {
		this.vnGr1Num = vnGr1Num;
	}

	public Integer getVnGr2Num() {
		return vnGr2Num;
	}

	public void setVnGr2Num(Integer vnGr2Num) {
		this.vnGr2Num = vnGr2Num;
	}

	public Integer getVnGr3Num() {
		return vnGr3Num;
	}

	public void setVnGr3Num(Integer vnGr3Num) {
		this.vnGr3Num = vnGr3Num;
	}

	public Integer getVnDatnNum() {
		return vnDatnNum;
	}

	public void setVnDatnNum(Integer vnDatnNum) {
		this.vnDatnNum = vnDatnNum;
	}

	public Integer getSieDatnNum() {
		return sieDatnNum;
	}

	public void setSieDatnNum(Integer sieDatnNum) {
		this.sieDatnNum = sieDatnNum;
	}

	public List<Long> getKsClassIds() {
		return ksClassIds;
	}

	public void setKsClassIds(List<Long> cqClassIds) {
		this.ksClassIds = cqClassIds;
	}

	public List<Long> getClcClassIds() {
		return clcClassIds;
	}

	public void setClcClassIds(List<Long> clcClassIds) {
		this.clcClassIds = clcClassIds;
	}

	public List<Long> getKstnClassIds() {
		return kstnClassIds;
	}

	public void setKstnClassIds(List<Long> tnClassIds) {
		this.kstnClassIds = tnClassIds;
	}

	public List<Long> getIctClassIds() {
		return ictClassIds;
	}

	public void setIctClassIds(List<Long> ictClassIds) {
		this.ictClassIds = ictClassIds;
	}

	public List<Long> getVnClassIds() {
		return vnClassIds;
	}

	public void setVnClassIds(List<Long> vnClassIds) {
		this.vnClassIds = vnClassIds;
	}

	public List<Long> getCtttClassIds() {
		return ctttClassIds;
	}

	public void setCtttClassIds(List<Long> ctttClassIds) {
		this.ctttClassIds = ctttClassIds;
	}

	public List<Long> getSdhClassIds() {
		return sdhClassIds;
	}

	public void setSdhClassIds(List<Long> sdhClassIds) {
		this.sdhClassIds = sdhClassIds;
	}

	public List<Long> getSieClassIds() {
		return sieClassIds;
	}

	public void setSieClassIds(List<Long> sieClassIds) {
		this.sieClassIds = sieClassIds;
	}

	public List<Long> getTcClassIds() {
		return tcClassIds;
	}

	public void setTcClassIds(List<Long> tcClassIds) {
		this.tcClassIds = tcClassIds;
	}

	public List<Long> getCdClassIds() {
		return cdClassIds;
	}

	public void setCdClassIds(List<Long> cdClassIds) {
		this.cdClassIds = cdClassIds;
	}

	public List<Long> getKs2ClassIds() {
		return ks2ClassIds;
	}

	public void setKs2ClassIds(List<Long> ks2ClassIds) {
		this.ks2ClassIds = ks2ClassIds;
	}

	public List<Long> getCncnClassIds() {
		return cncnClassIds;
	}

	public void setCncnClassIds(List<Long> cncnClassIds) {
		this.cncnClassIds = cncnClassIds;
	}

	public List<Long> getDhlkClassIds() {
		return dhlkClassIds;
	}

	public void setDhlkClassIds(List<Long> dhlkClassIds) {
		this.dhlkClassIds = dhlkClassIds;
	}

	public Integer getCnDatnNum() {
		return cnDatnNum;
	}

	public void setCnDatnNum(Integer cnDatnNum) {
		this.cnDatnNum = cnDatnNum;
	}

	public Integer getKsProject1Num() {
		return ksProject1Num;
	}

	public void setKsProject1Num(Integer ksProject1Num) {
		this.ksProject1Num = ksProject1Num;
	}

	public Integer getKsProject2Num() {
		return ksProject2Num;
	}

	public void setKsProject2Num(Integer ksProject2Num) {
		this.ksProject2Num = ksProject2Num;
	}

	public Integer getKsProject3Num() {
		return ksProject3Num;
	}

	public void setKsProject3Num(Integer ksProject3Num) {
		this.ksProject3Num = ksProject3Num;
	}

	public Integer getTcDatnNum() {
		return tcDatnNum;
	}

	public void setTcDatnNum(Integer tcDatnNum) {
		this.tcDatnNum = tcDatnNum;
	}

	public Integer getProject1ANum() {
		return project1ANum;
	}

	public void setProject1ANum(Integer project1aNum) {
		project1ANum = project1aNum;
	}

	public Integer getProject1BNum() {
		return project1BNum;
	}

	public void setProject1BNum(Integer project1bNum) {
		project1BNum = project1bNum;
	}

	public Integer getCdDatnNum() {
		return cdDatnNum;
	}

	public void setCdDatnNum(Integer cdDatnNum) {
		this.cdDatnNum = cdDatnNum;
	}

	public Integer getKs2DatnNum() {
		return ks2DatnNum;
	}

	public void setKs2DatnNum(Integer ks2DatnNum) {
		this.ks2DatnNum = ks2DatnNum;
	}
}