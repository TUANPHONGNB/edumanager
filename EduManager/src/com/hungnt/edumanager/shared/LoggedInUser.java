package com.hungnt.edumanager.shared;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;

@Entity
public class LoggedInUser implements IBasic {

	@Id private String id;
	private Long userId;
	@Ignore Date date = null;
	
	public LoggedInUser() {}

	public LoggedInUser(String id, Long userId) {
		this.id = id;
		this.userId = userId;
		date = new Date();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}