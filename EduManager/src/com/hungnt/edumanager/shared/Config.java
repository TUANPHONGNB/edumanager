package com.hungnt.edumanager.shared;

public class Config {
	public static final Long NULL_ID = -1L;
	public static final String NULL_TXT = "";
	public static final String DEFAULT_PASSWORD = "Cnpm@601B1";
	public static final String DEFAULT_WEB = "http://soict.hust.edu.vn/";
	public static final String ADMIN_EMAIL = "se.soict.hust@gmail.com";
	public static final int currentScholarYear = 5;			
	public static final int currentSemester = 1;
	public static final int DEFAULT_EXPIRED_TIME = 10;
	public static final int DEFAULT_LOGIN_EXPIRED_TIME = 10;
	public static final int LONG_EXPIRED_TIME = 20;
	public static final String LOGIN_NAME_SPACE = "loginsession";
	public static final String EXPORT_TOKEN = "etoken";
	public static final String HTML_BREAK_LINE = "<br/>";
	
	public static final int DATA_TEACHER = 1;
	public static final int DATA_SUBJECT = 2;
	public static final int DATA_COURSE = 3;
	public static final int DATA_PUBLICATION = 4;
	public static final int DATA_TOPIC = 4;
	
	public static final int CATEGORY = 0;
	public static final int MATERIAL = 1;
	public static final String FOLDER_DOCUMENT = "documents";
	public static final int USER_TYPE_ADMIN = 1;
	
	
	public static final int STUDENT_TYPE_DHCQ = 0;
	public static final int STUDENT_TYPE_CLC = 1;
	public static final int STUDENT_TYPE_KSTN = 2;
	public static final int STUDENT_TYPE_HEDSPI = 3;
	public static final int STUDENT_TYPE_CTTT = 4;
	public static final int STUDENT_TYPE_SDH = 5;
	public static final int STUDENT_TYPE_CNCN = 6;
	public static final int STUDENT_TYPE_SIE = 10;
	public static final int STUDENT_TYPE_TC = 11;
	public static final int STUDENT_TYPE_CD_B = 12;
	public static final int STUDENT_TYPE_VB2 = 13;
	public static final int STUDENT_TYPE_DHLK = 14;
	
	
	public static final int COURSE_TYPE_LTBT = 0;
	public static final int COURSE_TYPE_LT = 1;
	public static final int COURSE_TYPE_BT = 2;
	public static final int COURSE_TYPE_DA = 3;
	public static final int COURSE_TYPE_TN = 4;				//Thi Nghiem
	public static final int COURSE_TYPE_TT = 5;				//Thuc Tap
	public static final int COURSE_TYPE_LVKH = 6;
	public static final int COURSE_TYPE_LVKT = 7;
	public static final int COURSE_TYPE_LATS = 8;
	public static final int COURSE_TYPE_CDNCS = 9;
	public static final int COURSE_TYPE_HPTS = 10;
	public static final int COURSE_TYPE_BS = 11;			//Bo sung
	public static final int COURSE_TYPE_PROJECT1 = 12;
	public static final int COURSE_TYPE_PROJECT2 = 13;
	public static final int COURSE_TYPE_PROJECT3 = 14;
	public static final int COURSE_TYPE_GR1 = 15;
	public static final int COURSE_TYPE_GR2 = 16;
	public static final int COURSE_TYPE_GR3 = 17;
	
	public static final int TYPE_EVENT = 0;
	public static final int TYPE_TASK = 1;
	
	public static final byte[] GWT_DES_KEY = new byte[]{
		(byte)1,(byte)7,(byte)12,(byte)10,(byte)15,(byte)17,(byte)16,(byte)22,
		(byte)2,(byte)4,(byte)11,(byte)9,(byte)13,(byte)14,(byte)20,(byte)23,
		(byte)3,(byte)6,(byte)5,(byte)8,(byte)18,(byte)19,(byte)21,(byte)24};

	public static String getEduType(int type) {
		if (type == 1)
			return "PGS";
		else if (type == 2)
			return "GS";
		return "";
	}
	public static String getTeachingType(int type) {
		if (type == 0)
			return "GV";
		if (type == 1)
			return "GVC";
		else if (type == 2)
			return "Cán bộ giảng dạy";
		else if (type == 3)
			return "Nghiên Cứu Sinh";
		return "";
	}
	public static String getDegreeType(int type) {
		if (type == 1)
			return "ThS";
		else if (type == 2)
			return "TS";
		return "KS";
	}
	
	public static String getSemesterType(int type) {
		if (type == 2)
			return "A";
		else if (type == 3)
			return "B";
		else
			return "AB";
	}

	public static String getStudentTypeName(int type) {
		switch (type) {
			case STUDENT_TYPE_DHCQ:
				return "DHCQ";
			case STUDENT_TYPE_CLC:
				return "KSCLC";
			case STUDENT_TYPE_KSTN:
				return "KSTN";
			case STUDENT_TYPE_HEDSPI:
				return "HEDSPI-A";
			case STUDENT_TYPE_CTTT:
				return "CTTT";
			case STUDENT_TYPE_SDH:
				return "SDH";
			case STUDENT_TYPE_CNCN:
				return "Cử nhân CN";
			case STUDENT_TYPE_SIE:
				return "SIE";
			case STUDENT_TYPE_TC:
				return "TC";
			case STUDENT_TYPE_CD_B:
				return "CD-B";
			case STUDENT_TYPE_VB2:
				return "VB2";
			case STUDENT_TYPE_DHLK:
				return "DHLK";
			default:
				return "";
		}
	}
	
	public static String getCourseType(int type) {
		switch (type) {
			case COURSE_TYPE_LTBT:
				return "LT+BT";
			case COURSE_TYPE_LT:
				return "LT";
			case COURSE_TYPE_BT:
				return "BT";
			case COURSE_TYPE_DA:
				return "Đồ Án";
			case COURSE_TYPE_TN:
				return "Thí Nghiệm";
			case COURSE_TYPE_TT:
				return "Thực Tập";
			case COURSE_TYPE_LVKH:
				return "Luận Văn KH";
			case COURSE_TYPE_LVKT:
				return "Luận Văn KT";
			case COURSE_TYPE_LATS:
				return "Luận Án TS";
			case COURSE_TYPE_CDNCS:
				return "Chuyên Đề NCS";
			case COURSE_TYPE_HPTS:
				return "Học Phần TS";
			case COURSE_TYPE_BS:
				return "Bổ Sung";
			case COURSE_TYPE_PROJECT1:
				return "Project 1";
			case COURSE_TYPE_PROJECT2:
				return "Project 2";
			case COURSE_TYPE_PROJECT3:
				return "Project 3";
			case COURSE_TYPE_GR1:
				return "GR1";
			case COURSE_TYPE_GR2:
				return "GR2";				
			case COURSE_TYPE_GR3:
				return "GR3";				
			default:
				return "";
		}
	}
	
	public static String getTopicType(int type) {
		switch (type) {
			case 0:
				return "Đồ Án Tốt Nghiệp";
			case 1:
				return "Project 1/GR1";
			case 2:
				return "Project 2/GR2";
			case 3:
				return "Project 3/GR3";
			default:
				return "";
		}
	}
	
	public static String getStatus(int type) {
		switch (type) {
			case 0:
				return "Chưa bắt đầu";
			case 1:
				return "Đang dạy";
			case 2:
				return "Kết thúc dạy, chưa thi";
			case 6:
				return "Kết thúc";
			default:
				return "";
		}
	}
	
	public static String getNotification(int type) {
		switch (type) {
			case 1:
				return "Đã có bảng điểm quá trình";
			case 2:
				return "Đã nhận bảng điểm quá trình";
			case 3:
				return "Đã có bảng điểm cuối kỳ";
			case 4:
				return "Đã nhận bảng điểm cuối kỳ";
			case 5:
				return "Chưa ra đề thi";
			case 6:
				return "Đã nộp đề thi";
			case 7:
				return "Chưa nộp bảng điểm";
			case 8:
				return "Đã nộp bảng điểm";
			default:
				return "";
		}
	}
	
	public static String getTopicStatus(int type) {
		switch (type) {
			case 0:
				return "Chưa có sinh viên";
			case 1:
				return "Đã có sinh viên";
			case 2:
				return "Đã bảo vệ";
			default:
				return "";
		}
	}
	
	public static String getPaymentType(int paymentType) {
		if (paymentType == 0)
			return "Loại 1A";
		else
			return "Loại 1B";
	}
	
	public static String getMadeFrom(int type) {
		if (type == 1)
			return "China";
		else if (type == 2)
			return "Japan";
		else
			return "Vietnam";
	}
	
	public static String getFinanceBy(int type) {
		if (type == 0)
			return "Bộ môn";
		else if (type == 1)
			return "Trường";
		else if (type == 2)
			return "Dự án";
		else
			return "";
	}
	
	public static String getPrivacyValue(int type) {
		if (type == 0)
			return "Tất cả mọi người";
		else if (type == 1)
			return "Chỉ các thầy cô";
		else if (type == 2)
			return "Chỉ mình tôi";
		else
			return "";
	}
	
	public static String getDay(int day) {
		if (day >= 2 && day <= 7) {
			return "Thứ " + day;
		}
		else if (day == 1)
			return "Chủ nhật";
		else if (day == 8)
			return "Tất cả";
		return "";
	}
	
	public static String getScholorYear(int val) {
		int year = 2010 + val;
		return year + "-" + (year + 1);
	}
	
	public static String getPaperType(int type) {
		switch (type) {
		case 1:
			return "Hội nghị quốc tế";
		case 2:
			return "Tạp chí quốc tế";
		case 3:
			return "Hội nghị quốc gia";
		case 4:
			return "Tạp chí quốc gia";
		case 5:
			return "Sách";
		case 6:
			return "Báo cáo khoa học";
		default:
			return "";
		}
	}
}
