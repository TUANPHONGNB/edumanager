package com.hungnt.edumanager.shared;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Category implements IBasic {
	@Id private Long id;
	@Index private String name = Config.NULL_TXT;
	@Index private Long parentId = Config.NULL_ID;
	@Index private Long userId = Config.NULL_ID;
	@Index private Integer privacy = 0;								//0: public, 1: private;
	private List<Long> materialIds = new ArrayList<Long>();
	private List<Long> childrenIds = new ArrayList<Long>();
	@Ignore List<Material> materials = new ArrayList<Material>();
	@Ignore List<Category> children = new ArrayList<Category>();
	
	public Category() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public List<Long> getMaterialIds() {
		return materialIds;
	}

	public void setMaterialIds(List<Long> materialIds) {
		this.materialIds = materialIds;
	}

	public List<Long> getChildrenIds() {
		return childrenIds;
	}

	public void setChildrenIds(List<Long> childrenIds) {
		this.childrenIds = childrenIds;
	}

	public List<Material> getMaterials() {
		return materials;
	}

	public void setMaterials(List<Material> materials) {
		this.materials = materials;
	}

	public List<Category> getChildren() {
		return children;
	}

	public void setChildren(List<Category> children) {
		this.children = children;
	}
}
