package com.hungnt.edumanager.shared;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class ExamInfo implements IBasic {
	@Id private Long id;
	private String place;
	@Index private Date date;
	private Integer hour;
	private Integer minute;
	private Integer duration;
		
	public ExamInfo() {}
	public ExamInfo(Date date, int hour, int minute, int duration, String place) {
		this.setDate(date);
		this.setHour(hour);
		this.setMinute(minute);
		this.setPlace(place);
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Integer getHour() {
		return hour;
	}
	public void setHour(Integer hour) {
		this.hour = hour;
	}
	public Integer getMinute() {
		return minute;
	}
	public void setMinute(Integer minute) {
		this.minute = minute;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
}
