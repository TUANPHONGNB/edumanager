package com.hungnt.edumanager.shared;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class MeetingAbsent implements IBasic {
	@Id private Long id;
	@Index private Long teacherId = Config.NULL_ID;
	private String teacherName = Config.NULL_TXT;
	private String reason = Config.NULL_TXT;
	@Index private int absentType = 0;				//0: co phep, 1: khong phep 
	@Index private Long meetingId = Config.NULL_ID;	
	@Index private Date meetingDate = null;
	
	public MeetingAbsent() {
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Long getMeetingId() {
		return meetingId;
	}
	public void setMeetingId(Long meetingId) {
		this.meetingId = meetingId;
	}
	public Date getMeetingDate() {
		return meetingDate;
	}
	public void setMeetingDate(Date meetingDate) {
		this.meetingDate = meetingDate;
	}
	public int getAbsentType() {
		return absentType;
	}
	public void setAbsentType(int absentType) {
		this.absentType = absentType;
	}
}
