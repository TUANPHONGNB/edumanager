package com.hungnt.edumanager.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Teacher implements IBasic {
	@Id private Long id;
	@Index private String userName = Config.NULL_TXT;
	@Index private String password = Config.NULL_TXT;
	@Index private String firstName = Config.NULL_TXT;
	@Index private String lastName = Config.NULL_TXT;
	@Index private String teacherId = Config.NULL_TXT;
	@Index private Integer gender = 0;
	@Index private Integer degreeType = 0;			//0: KS, 1: Master, 2: Ph.D.
	@Index private Integer eduType = 0;				//0: None, 1: PGS, 2: GS
	@Index private Integer teachingType = 0;		//0: GV, 1: GVC, 2: CB phuc vu giang day
	@Index private Integer departmentId = 0;		//0: CNPM, 1: HTTT, 2: KHMT
	@Index private Double salaryType = 0.0;			//He so luong
	private int position;							//Head, Deputy Head, ...
	private String taxNumber = Config.NULL_TXT;
	private String idNumber = Config.NULL_TXT;
	private String bankAccount = Config.NULL_TXT;
	private String bankInfo = Config.NULL_TXT;
	private String website = Config.NULL_TXT;
	private Date birthDate = null;//new Date();
	private String workEmail = Config.NULL_TXT;
	private String personalEmail = Config.NULL_TXT;
	private String workPhoneNumber = Config.NULL_TXT;
	private String cellPhoneNumber = Config.NULL_TXT;
	private List<String> otherEmails = new ArrayList<String>();
	private List<String> otherPhoneNumbers = new ArrayList<String>();
	private String avatar = Config.NULL_TXT;
	private String workAddress = Config.NULL_TXT;
	private String homeAddress = Config.NULL_TXT;
	private String info = Config.NULL_TXT;
	@Index private Integer userType = 0;
	@Index private Integer currentCourseNum = 0;
	@Index private Integer currentCourse1ANum = 0;
	@Index private Integer currentCourse1BNum = 0;
	@Index private Integer publicationNum = 0;
	private List<Long> courseIds = new ArrayList<Long>();
	private List<Long> subjectIds = new ArrayList<Long>();
	private List<Long> currentCourseIds = new ArrayList<Long>();
	private List<Long> currentCourse1AIds = new ArrayList<Long>();
	private List<Long> currentCourse1BIds = new ArrayList<Long>();
	private List<Long> currentSubjectIds = new ArrayList<Long>();
	private List<Long> publicationIds = new ArrayList<Long>();
	private Integer activated = 0;
	private Integer status = 0;			//0: normal; 1: absent
	
	private List<Long> materialIds = new ArrayList<Long>();
	private List<Long> categoryIds = new ArrayList<Long>();
	private List<Long> topicIds = new ArrayList<Long>();
	@Ignore private List<Material> materials = new ArrayList<Material>();
	@Ignore private List<Category> categories = new ArrayList<Category>();
	@Ignore private List<InternTopic> topics = new ArrayList<InternTopic>();
	@Ignore private int posIndex = 0;
	@Ignore private String sessionId = Config.NULL_TXT;
	@Ignore private Map<Integer, Statistics> statMap = new HashMap<Integer, Statistics>();
	
	public Teacher() {}
	
	public Teacher(String firstName, String lastName, String teacherId,
			int gender, int degreeType, int eduType, String email, String phone) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.setTeacherId(teacherId);
		this.gender = gender;
		this.degreeType = degreeType;
		this.eduType = eduType;
		this.workEmail = email;
		this.workPhoneNumber = phone;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		if (teacherId != null && !teacherId.isEmpty())
			this.teacherId = teacherId;
	}
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	public Integer getDegreeType() {
		return degreeType;
	}
	public void setDegreeType(Integer degreeType) {
		this.degreeType = degreeType;
	}
	public Integer getEduType() {
		return eduType;
	}
	public void setEduType(Integer eduType) {
		this.eduType = eduType;
	}
	public Double getSalaryType() {
		return salaryType;
	}
	public void setSalaryType(Double salaryType) {
		this.salaryType = salaryType;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	public List<Long> getClassIds() {
		return courseIds;
	}
	public void setClassIds(List<Long> classIds) {
		this.courseIds = classIds;
	}
	public List<Long> getSubjectIds() {
		return subjectIds;
	}
	public void setSubjectIds(List<Long> subjectIds) {
		this.subjectIds = subjectIds;
	}
	public List<Long> getCurrentCourseIds() {
		return currentCourseIds;
	}
	public void setCurrentCourseIds(List<Long> currentCourseIds) {
		this.currentCourseIds = currentCourseIds;
	}
	public List<Long> getCurrentSubjectIds() {
		return currentSubjectIds;
	}
	public void setCurrentSubjectIds(List<Long> currentSubjectIds) {
		this.currentSubjectIds = currentSubjectIds;
	}
	
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public String getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getBankInfo() {
		return bankInfo;
	}

	public void setBankInfo(String bankInfo) {
		this.bankInfo = bankInfo;
	}
	
	public int getPosIndex() {
		return posIndex;
	}

	public void setPosIndex(int posIndex) {
		this.posIndex = posIndex;
	}
	
	public Integer getActivated() {
		return activated;
	}

	public void setActivated(Integer activated) {
		this.activated = activated;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullName() {
		return this.lastName + " " + this.firstName;
	}
	
	public String getFullTitle() {
		String title = "";
		if (this.eduType > 0)
			title += Config.getEduType(this.eduType) + ".";
		else if (this.teachingType < 2)
			title += Config.getTeachingType(this.teachingType) + ".";
		title += Config.getDegreeType(this.degreeType) + ". " + getFullName();
		return title;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Integer getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	public List<Long> getPublicationIds() {
		return publicationIds;
	}

	public void setPublicationIds(List<Long> publicationIds) {
		this.publicationIds = publicationIds;
	}

	public Integer getTeachingType() {
		return teachingType;
	}

	public void setTeachingType(Integer teachingType) {
		this.teachingType = teachingType;
	}

	public List<Long> getMaterialIds() {
		return materialIds;
	}

	public void setMaterialIds(List<Long> materialIds) {
		this.materialIds = materialIds;
	}

	public List<Long> getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(List<Long> categoryIds) {
		this.categoryIds = categoryIds;
	}

	public List<Material> getMaterials() {
		return materials;
	}

	public void setMaterials(List<Material> materials) {
		this.materials = materials;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public Integer getCurrentCourseNum() {
		return currentCourseNum;
	}

	public void setCurrentCourseNum(Integer currentCourseNum) {
		this.currentCourseNum = currentCourseNum;
	}

	public Integer getPublicationNum() {
		return publicationNum;
	}

	public void setPublicationNum(Integer publicationNum) {
		this.publicationNum = publicationNum;
	}

	public String getWorkEmail() {
		return workEmail;
	}

	public void setWorkEmail(String workEmail) {
		this.workEmail = workEmail;
	}

	public String getPersonalEmail() {
		return personalEmail;
	}

	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	public String getWorkPhoneNumber() {
		return workPhoneNumber;
	}

	public void setWorkPhoneNumber(String workPhoneNumber) {
		this.workPhoneNumber = workPhoneNumber;
	}

	public String getCellPhoneNumber() {
		return cellPhoneNumber;
	}

	public void setCellPhoneNumber(String cellPhoneNumber) {
		this.cellPhoneNumber = cellPhoneNumber;
	}

	public List<String> getOtherEmails() {
		return otherEmails;
	}

	public void setOtherEmails(List<String> otherEmails) {
		this.otherEmails = otherEmails;
	}

	public List<String> getOtherPhoneNumbers() {
		return otherPhoneNumbers;
	}

	public void setOtherPhoneNumbers(List<String> otherPhoneNumbers) {
		this.otherPhoneNumbers = otherPhoneNumbers;
	}

	public String getWorkAddress() {
		return workAddress;
	}

	public void setWorkAddress(String workAddress) {
		this.workAddress = workAddress;
	}

	public String getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	public boolean isAdmin() {
		return this.userType > 0 || this.workEmail.equalsIgnoreCase("hungnt@soict.hust.edu.vn");
	}

	public Integer getCurrentCourse1ANum() {
		return currentCourse1ANum;
	}

	public void setCurrentCourse1ANum(Integer currentCourse1ANum) {
		this.currentCourse1ANum = currentCourse1ANum;
	}

	public Integer getCurrentCourse1BNum() {
		return currentCourse1BNum;
	}

	public void setCurrentCourse1BNum(Integer currentCourse1BNum) {
		this.currentCourse1BNum = currentCourse1BNum;
	}

	public List<Long> getCourseIds() {
		return courseIds;
	}

	public void setCourseIds(List<Long> courseIds) {
		this.courseIds = courseIds;
	}

	public List<Long> getCurrentCourse1AIds() {
		return currentCourse1AIds;
	}

	public void setCurrentCourse1AIds(List<Long> currentCourse1AIds) {
		this.currentCourse1AIds = currentCourse1AIds;
	}

	public List<Long> getCurrentCourse1BIds() {
		return currentCourse1BIds;
	}

	public void setCurrentCourse1BIds(List<Long> currentCourse1BIds) {
		this.currentCourse1BIds = currentCourse1BIds;
	}

	public List<Long> getTopicIds() {
		return topicIds;
	}

	public void setTopicIds(List<Long> topicIds) {
		this.topicIds = topicIds;
	}

	public List<InternTopic> getTopics() {
		return topics;
	}

	public void setTopics(List<InternTopic> topics) {
		this.topics = topics;
	}

	public Map<Integer, Statistics> getStatMap() {
		return statMap;
	}

	public void setStatMap(Map<Integer, Statistics> statMap) {
		this.statMap = statMap;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
