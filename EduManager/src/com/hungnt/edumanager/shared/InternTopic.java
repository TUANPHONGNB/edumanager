package com.hungnt.edumanager.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class InternTopic implements IBasic {
	@Id private Long id;
	@Index private Long teacherId = Config.NULL_ID;
	@Index private String title = Config.NULL_TXT;
	private String description = Config.NULL_TXT;
	private String teacherName = Config.NULL_TXT;
	private String courseIdCode = Config.NULL_TXT;
	private String subjectIdCode = Config.NULL_TXT;
	@Index private Long reviewerId = Config.NULL_ID;
	@Index private Integer topicType = -1;
	private String reviewerName = Config.NULL_TXT;
	private String externalSupervisor = Config.NULL_TXT;
	@Index private Integer studentType = 0;
	@Index private Integer studentNum = 0;
	@Index private Integer semester = 0;
	@Index private String studentId = Config.NULL_TXT;
	@Index private Date submittedDate = null;
	private String studentName = Config.NULL_TXT;
	private String studentPhone = Config.NULL_TXT;
	private String studentEmail = Config.NULL_TXT;
	private List<Long> studentIds = new ArrayList<Long>();
	private List<String> studentNames = new ArrayList<String>();
	@Index private Integer scholarYear = Config.currentScholarYear;
	@Index private Integer status = 0;							//0: available, 1: taken, ...
	@Ignore private Integer posIndex = 0;
	
	public InternTopic() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getExternalSupervisor() {
		return externalSupervisor;
	}

	public void setExternalSupervisor(String externalSupervisor) {
		this.externalSupervisor = externalSupervisor;
	}

	public Integer getStudentNum() {
		return studentNum;
	}

	public void setStudentNum(Integer studentNum) {
		this.studentNum = studentNum;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public List<Long> getStudentIds() {
		return studentIds;
	}

	public void setStudentIds(List<Long> studentIds) {
		this.studentIds = studentIds;
	}

	public Integer getScholarYear() {
		return scholarYear;
	}

	public void setScholarYear(Integer scholarYear) {
		this.scholarYear = scholarYear;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getPosIndex() {
		return posIndex;
	}

	public void setPosIndex(Integer posIndex) {
		this.posIndex = posIndex;
	}

	public List<String> getStudentNames() {
		return studentNames;
	}

	public void setStudentNames(List<String> studentNames) {
		this.studentNames = studentNames;
	}

	public Integer getSemester() {
		return semester;
	}

	public void setSemester(Integer semester) {
		this.semester = semester;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public Long getReviewerId() {
		return reviewerId;
	}

	public void setReviewerId(Long reviewerId) {
		this.reviewerId = reviewerId;
	}

	public String getReviewerName() {
		return reviewerName;
	}

	public void setReviewerName(String reviewerName) {
		this.reviewerName = reviewerName;
	}

	public String getStudentPhone() {
		return studentPhone;
	}

	public void setStudentPhone(String studentPhone) {
		this.studentPhone = studentPhone;
	}

	public String getStudentEmail() {
		return studentEmail;
	}

	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}

	public Integer getStudentType() {
		return studentType;
	}

	public void setStudentType(Integer studentType) {
		this.studentType = studentType;
	}

	public Date getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}

	public String getCourseIdCode() {
		return courseIdCode;
	}

	public void setCourseIdCode(String courseIdCode) {
		this.courseIdCode = courseIdCode;
	}

	public Integer getTopicType() {
		return topicType;
	}

	public void setTopicType(Integer topicType) {
		this.topicType = topicType;
	}

	public String getSubjectIdCode() {
		return subjectIdCode;
	}

	public void setSubjectIdCode(String subjectIdCode) {
		this.subjectIdCode = subjectIdCode;
	}
}
