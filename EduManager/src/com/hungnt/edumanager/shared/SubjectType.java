package com.hungnt.edumanager.shared;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class SubjectType implements IBasic {
	private @Id Long id;
	private @Index Integer subjectTypeId = 0;
	private String subjectTypeName = Config.NULL_TXT;
	
	public SubjectType() {}
	
	public SubjectType(int subjectTypeId, String subjectTypeName) {
		this.subjectTypeId = subjectTypeId;
		this.subjectTypeName = subjectTypeName;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getSubjectTypeId() {
		return subjectTypeId;
	}

	public void setSubjectTypeId(int subjectTypeId) {
		this.subjectTypeId = subjectTypeId;
	}

	public String getSubjectTypeName() {
		return subjectTypeName;
	}

	public void setSubjectTypeName(String subjectTypeName) {
		this.subjectTypeName = subjectTypeName;
	}
}
