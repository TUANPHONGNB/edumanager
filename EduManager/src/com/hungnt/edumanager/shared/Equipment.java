package com.hungnt.edumanager.shared;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Equipment implements IBasic {
	@Id private Long id;
	@Index private Long userId = Config.NULL_ID;
	private String name = Config.NULL_TXT;
	private String serialNumber = Config.NULL_TXT;
	private String idCode = Config.NULL_TXT;
	private Integer madeFrom = 0;
	private Long price = 0L;	
	private Integer fromMonth = 0;
	private Integer fromYear = 0;
	private Integer financeBy = 0;
	private String description = Config.NULL_TXT;
	private String notes = Config.NULL_TXT;		
	@Index private Integer equipmentType = 0;			
	@Index private Integer status = 0;					
	@Index private Integer quantity = 0;				
	@Ignore private Integer posIndex = 0;
	@Ignore private List<EquipmentHistory> histories = new ArrayList<EquipmentHistory>();
	@Ignore private EquipmentHistory currentHistory = null;
	
	public Equipment() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getIdCode() {
		return idCode;
	}

	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}

	public Integer getMadeFrom() {
		return madeFrom;
	}

	public void setMadeFrom(Integer madeFrom) {
		this.madeFrom = madeFrom;
	}

	public Integer getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(Integer fromMonth) {
		this.fromMonth = fromMonth;
	}

	public Integer getFromYear() {
		return fromYear;
	}

	public void setFromYear(Integer fromYear) {
		this.fromYear = fromYear;
	}

	public Integer getFinanceBy() {
		return financeBy;
	}

	public void setFinanceBy(Integer financeBy) {
		this.financeBy = financeBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getEquipmentType() {
		return equipmentType;
	}

	public void setEquipmentType(Integer equipmentType) {
		this.equipmentType = equipmentType;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getPosIndex() {
		return posIndex;
	}

	public void setPosIndex(Integer posIndex) {
		this.posIndex = posIndex;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public List<EquipmentHistory> getHistories() {
		return histories;
	}

	public void setHistories(List<EquipmentHistory> histories) {
		this.histories = histories;
	}

	public EquipmentHistory getCurrentHistory() {
		return currentHistory;
	}

	public void setCurrentHistory(EquipmentHistory currentHistory) {
		this.currentHistory = currentHistory;
	}
}