package com.hungnt.edumanager.shared;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class EquipmentHistory implements IBasic {
	@Id private Long id;
	@Index private Long userId = Config.NULL_ID;
	private String userName = Config.NULL_TXT;
	@Index private Long equipmentId = Config.NULL_ID;
	@Index private Date fromDate = new Date();
	@Index private Integer fromHour = 0;
	@Index private Integer fromMinute = 0;
	@Index private Date toDate = new Date();
	@Index private Integer toHour = 0;
	@Index private Integer toMinute = 0;
	@Index private Integer status = 0;				//0: not yet, 1: occupying, 2: done
	private String reason = Config.NULL_TXT;
	private String place = Config.NULL_TXT;
	private String notes = Config.NULL_TXT;
	@Ignore private Integer posIndex = 0;
	
	public EquipmentHistory() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getEquipmentId() {
		return equipmentId;
	}

	public void setEquipmentId(Long equipmentId) {
		this.equipmentId = equipmentId;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Integer getFromHour() {
		return fromHour;
	}

	public void setFromHour(Integer fromHour) {
		this.fromHour = fromHour;
	}

	public Integer getFromMinute() {
		return fromMinute;
	}

	public void setFromMinute(Integer fromMinute) {
		this.fromMinute = fromMinute;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Integer getToHour() {
		return toHour;
	}

	public void setToHour(Integer toHour) {
		this.toHour = toHour;
	}

	public Integer getToMinute() {
		return toMinute;
	}

	public void setToMinute(Integer toMinute) {
		this.toMinute = toMinute;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getPosIndex() {
		return posIndex;
	}

	public void setPosIndex(Integer posIndex) {
		this.posIndex = posIndex;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return this.userName + " đặt từ " + fromDate.toString() 
				+ " đến " + toDate.toString() + " tại " + this.place;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}