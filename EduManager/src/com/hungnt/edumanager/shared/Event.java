package com.hungnt.edumanager.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormat.PredefinedFormat;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Event implements IBasic {
	@Id private Long id;
	@Index private Long userId = Config.NULL_ID;
	@Index private Integer eventType = 0;	//0: event, 1: task
	private String name = Config.NULL_TXT;
	private String description = Config.NULL_TXT;
	private String notes = Config.NULL_TXT;		
	private String place = Config.NULL_TXT;		
	private Date date = null;
	private Integer hour = 0;
	private Integer minute = 0;
	private List<Long> involvedUsers = new ArrayList<Long>();
	private List<String> involvedUserNames = new ArrayList<String>();
	@Index private Integer status = 0;
	@Index private Integer doneNotification = 0;				//Number of days in advance that notification has already been sent
	@Ignore Integer posIndex = 0;
	
	public Event() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getHour() {
		return hour;
	}

	public void setHour(Integer hour) {
		this.hour = hour;
	}

	public Integer getMinute() {
		return minute;
	}

	public void setMinute(Integer minute) {
		this.minute = minute;
	}

	public List<Long> getInvolvedUsers() {
		return involvedUsers;
	}

	public void setInvolvedUsers(List<Long> involvedUsers) {
		this.involvedUsers = involvedUsers;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getPosIndex() {
		return posIndex;
	}

	public void setPosIndex(Integer posIndex) {
		this.posIndex = posIndex;
	}

	public List<String> getInvolvedUserNames() {
		return involvedUserNames;
	}

	public void setInvolvedUserNames(List<String> involvedUserNames) {
		this.involvedUserNames = involvedUserNames;
	}
	
	public String getTime() {
		return DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT).format(date) + " lúc " + hour + "h:" + minute + "m";
	}

	public Integer getDoneNotification() {
		return doneNotification;
	}

	public void setDoneNotification(Integer doneNotification) {
		this.doneNotification = doneNotification;
	}

	public Integer getEventType() {
		return eventType;
	}

	public void setEventType(Integer eventType) {
		this.eventType = eventType;
	}
}