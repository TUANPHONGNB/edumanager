package com.hungnt.edumanager.shared;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Material implements IBasic {
	
	@Id private Long id;
	@Index private Long parentId = Config.NULL_ID;
	@Index private String name = Config.NULL_TXT;
	private String url = Config.NULL_TXT;
	private String description = Config.NULL_TXT;
	@Index private String fileType = Config.NULL_TXT;
	@Index private Long fileSize = 0L;
	@Index private Long userId = Config.NULL_ID;
	@Index private Integer privacy = 0;				//0: public, 1: private;
	
	public Material() {}
	
	public Material(Long userId, Long parentId, String name, String url, String description, String fileType) {
		this.parentId = parentId;
		this.name = name;
		this.url = url;
		this.userId = userId;
		this.fileType = fileType;
		this.description = description;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
}
