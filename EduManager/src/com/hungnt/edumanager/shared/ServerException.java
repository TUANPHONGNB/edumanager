package com.hungnt.edumanager.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ServerException extends Exception implements Serializable, IsSerializable {

  /**
	 * 
	 */
private static final long serialVersionUID = 1L;

private int exceptionType;

public ServerException() {
    super();
  }

  public ServerException(String message) {
    super(message);
  }
  
  public ServerException(int exceptionType) {
	  this.exceptionType = exceptionType;
  }

  public int getExceptionType() {
	  return this.exceptionType;
  }
}