package com.hungnt.edumanager.shared;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Subject implements IBasic {
	@Id private Long id;
	@Index private Long userId = Config.NULL_ID;
	@Index private String subjectIdCode = Config.NULL_TXT;
	@Index private String name = Config.NULL_TXT;
	private String description = Config.NULL_TXT;
	private String syllabus = Config.NULL_TXT;
	@Index private Integer credits = 0;
	private String creditInfo = Config.NULL_TXT;				//2-1-0-1
	private List<Long> teacherIds = new ArrayList<Long>();
	private String avatar = Config.NULL_TXT;
	@Index private Integer courseType = 1;						//1: Theory, 2: Practise, 3: Project
	@Index private Integer departmentId = 0;					//0: CNPM, 1: HTTT, 2: KHMT
	@Index private Integer privacy = 0;							//0: public, 1: private, ...
	@Index private Integer courseLanguage = 0;					//0: vi, 1: en
	private List<Long> materialIds = new ArrayList<Long>();
	private List<Long> categoryIds = new ArrayList<Long>();
	@Ignore private List<Material> materials = new ArrayList<Material>();
	@Ignore private List<Category> categories = new ArrayList<Category>();
	@Ignore private Integer posIndex = 0;
	
	public Subject() {}
	
	public Subject(String subjectId, String name, String description, String syllabus,
			int credits, String creditInfo) {
		this.subjectIdCode = subjectId;
		this.name = name;
		this.description = description;
		this.syllabus = syllabus;
		this.credits = credits;
		this.creditInfo = creditInfo;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSubjectIdCode() {
		return subjectIdCode;
	}
	public void setSubjectIdCode(String subjectId) {
		this.subjectIdCode = subjectId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getCredits() {
		return credits;
	}
	public void setCredits(Integer credits) {
		this.credits = credits;
	}
	public String getCreditInfo() {
		return creditInfo;
	}
	public void setCreditInfo(String creditInfo) {
		this.creditInfo = creditInfo;
	}
	public List<Long> getTeacherIds() {
		return teacherIds;
	}
	public void setTeacherIds(List<Long> teacherIds) {
		this.teacherIds = teacherIds;
	}

	public String getSyllabus() {
		return syllabus;
	}

	public void setSyllabus(String syllabus) {
		this.syllabus = syllabus;
	}

	public Integer getPosIndex() {
		return posIndex;
	}

	public void setPosIndex(Integer posIndex) {
		this.posIndex = posIndex;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Integer getCourseType() {
		return courseType;
	}

	public void setCourseType(Integer courseType) {
		this.courseType = courseType;
	}

	public List<Long> getMaterialIds() {
		return materialIds;
	}

	public void setMaterialIds(List<Long> materials) {
		this.materialIds = materials;
	}

	public Integer getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<Long> getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(List<Long> categoryIds) {
		this.categoryIds = categoryIds;
	}

	public List<Material> getMaterials() {
		return materials;
	}

	public void setMaterials(List<Material> materials) {
		this.materials = materials;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public Integer getCourseLanguage() {
		return courseLanguage;
	}

	public void setCourseLanguage(Integer courseLanguage) {
		this.courseLanguage = courseLanguage;
	}
}
