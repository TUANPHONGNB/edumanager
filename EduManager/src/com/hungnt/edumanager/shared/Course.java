package com.hungnt.edumanager.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.i18n.shared.DateTimeFormat;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Course implements IBasic {
	@Id private Long id;
	@Index private Long teacherId = Config.NULL_ID;
	@Index private Long subjectId = Config.NULL_ID;
	@Index private Long userId = Config.NULL_ID;
	@Index private String courseIdCode = Config.NULL_TXT;		//Mã lớp
	@Index private String name = Config.NULL_TXT;
	private String teacherName = Config.NULL_TXT;
	private String subjectName = Config.NULL_TXT;
	private String classType = Config.NULL_TXT;					//K58, KSTN, ...
	@Index private String subjectIdCode = Config.NULL_TXT;				//Mã HP
	private String info = Config.NULL_TXT;
	@Index private Integer studenType = 0;					//DHCQ, TC, SIE, ...
	@Index private Integer semesterType = 0;					//A, B, AB
	@Index private Integer semester = 0;						//1, 2, 3
	@Index private Integer courseType = 0;						//1: Theory, 2: Practise, 3: Project
	@Index private Integer paymentType = 0;						//A, B
	@Index private Integer status = 0;							//0: not yet, 1: on-going, 2: done
	@Index private Integer notification = 0;							//0: not yet, 1: on-going, 2: done
	@Index private Integer accepted = 0;						//0: accepted = 1: teacher has accepted this course
	@Index private int scholarYear = Config.currentScholarYear;		//0: 2010-2011, 1: 2011-2012, 2: 2012-2013, 3: 2013-2014, 4: 2014-2015
	@Index private Date startDate = null;
	@Index private Date endDate = null;
	@Index private String weekInfo;
	@Index private Integer credits = 0;
	@Index private Integer paid = 0;
	@Index private Integer departmentId = 0;	//0: CNPM, 1: HTTT, 2: KHMT
	@Index private Integer privacy = 0;							//0: public, 1: private, ...
	private String paidAmount = Config.NULL_TXT;
	private String creditInfo = Config.NULL_TXT;
	private List<Long> materialIds = new ArrayList<Long>();
	private List<Long> categoryIds = new ArrayList<Long>();
	@Index private Integer studentNum = 0;
	@Index private Integer courseLanguage = 0;					//0: vi, 1: en
	@Index private Integer doneNotification = 0;				//Number of days in advance that notification has already been sent
	private List<Long> timePlaceIds = new ArrayList<Long>();
	private List<Long> examIds = new ArrayList<Long>();
	@Ignore private List<ExamInfo> exams = new ArrayList<ExamInfo>();
	@Ignore private List<CourseInfo> timePlaces = new ArrayList<CourseInfo>();
	@Ignore private int posIndex = 0;
	@Ignore private List<Material> materials = new ArrayList<Material>();
	@Ignore private List<Category> categories = new ArrayList<Category>();

	public Course() {}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public Long getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}
	public String getCourseIdCode() {
		return courseIdCode;
	}
	public void setCourseIdCode(String courseId) {
		this.courseIdCode = courseId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public Integer getSemesterType() {
		return semesterType;
	}
	public void setSemesterType(Integer semesterType) {
		this.semesterType = semesterType;
	}
	public Integer getSemester() {
		return semester;
	}
	public void setSemester(Integer semester) {
		this.semester = semester;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getCredits() {
		return credits;
	}
	public void setCredits(Integer credits) {
		this.credits = credits;
	}
	public Integer getStudentNum() {
		return studentNum;
	}
	public void setStudentNum(Integer studentNum) {
		this.studentNum = studentNum;
	}
	public Integer getCourseLanguage() {
		return courseLanguage;
	}
	public void setCourseLanguage(Integer courseLanguage) {
		this.courseLanguage = courseLanguage;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public int getScholarYear() {
		return scholarYear;
	}

	public void setScholarYear(int scholarYear) {
		this.scholarYear = scholarYear;
	}

	public int getPosIndex() {
		return posIndex;
	}

	public void setPosIndex(int posIndex) {
		this.posIndex = posIndex;
	}

	public String getCreditInfo() {
		return creditInfo;
	}

	public void setCreditInfo(String creditInfo) {
		this.creditInfo = creditInfo;
	}

	public Integer getCourseType() {
		return courseType;
	}

	public void setCourseType(Integer courseType) {
		this.courseType = courseType;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	public String getClassType() {
		return classType;
	}

	public void setClassType(String classType) {
		this.classType = classType;
	}

	public List<Long> getTimePlaceIds() {
		return timePlaceIds;
	}

	public void setTimePlaceIds(List<Long> timePlaceIds) {
		this.timePlaceIds = timePlaceIds;
	}

	public List<CourseInfo> getTimePlaces() {
		return timePlaces;
	}

	public void setTimePlaces(List<CourseInfo> timePlaces) {
		this.timePlaces = timePlaces;
	}

	public Integer getPaid() {
		return paid;
	}

	public void setPaid(Integer paid) {
		this.paid = paid;
	}

	public String getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(String paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public String getPlaceTimeInfoHtml() {
		String str = "";
		int index = 0;
		for (CourseInfo info : timePlaces) {
			str += info.toHtml();
			if (index < timePlaces.size() - 1)
				str += "<br>";
			index ++;
		}
		if (this.startDate != null)
			str += "<br>" + DateTimeFormat.getFormat("dd/MM/yyyy").format(this.startDate);
		if (this.endDate != null)
			str += "-" + DateTimeFormat.getFormat("dd/MM/yyyy").format(this.endDate);
		return str;
	}
	
	public String getPlaceTimeInfo() {
		String str = "";
		int index = 0;
		for (CourseInfo info : timePlaces) {
			str += info.toString();
			if (index < timePlaces.size() - 1)
				str += " - ";
			index ++;
		}
		return str;
	}

	public Integer getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	public String getSubjectIdCode() {
		return subjectIdCode;
	}

	public void setSubjectIdCode(String subjectIdCode) {
		this.subjectIdCode = subjectIdCode;
	}

	public Integer getAccepted() {
		return accepted;
	}

	public void setAccepted(Integer accepted) {
		this.accepted = accepted;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public List<Long> getMaterialIds() {
		return materialIds;
	}

	public void setMaterialIds(List<Long> materialIds) {
		this.materialIds = materialIds;
	}

	public List<Long> getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(List<Long> categoryIds) {
		this.categoryIds = categoryIds;
	}

	public List<Material> getMaterials() {
		return materials;
	}

	public void setMaterials(List<Material> materials) {
		this.materials = materials;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public String getWeekInfo() {
		return weekInfo;
	}

	public void setWeekInfo(String weekInfo) {
		this.weekInfo = weekInfo;
	}

	public Integer getStudenType() {
		return studenType;
	}

	public void setStudenType(Integer studenType) {
		this.studenType = studenType;
	}

	public List<Long> getExamIds() {
		return examIds;
	}

	public void setExamIds(List<Long> examIds) {
		this.examIds = examIds;
	}

	public List<ExamInfo> getExams() {
		return exams;
	}

	public void setExams(List<ExamInfo> exams) {
		this.exams = exams;
	}

	public Integer getNotification() {
		return notification;
	}

	public void setNotification(Integer notification) {
		this.notification = notification;
	}

	public Integer getDoneNotification() {
		return doneNotification;
	}

	public void setDoneNotification(Integer doneNotification) {
		this.doneNotification = doneNotification;
	}
}
