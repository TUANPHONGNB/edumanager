package com.hungnt.edumanager.shared;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Student implements IBasic {
	@Id private Long id;
	@Index private String userName = Config.NULL_TXT;
	@Index private String password = Config.NULL_TXT;
	@Index private String firstName = Config.NULL_TXT;
	@Index private String lastName = Config.NULL_TXT;
	@Index private String studentId = Config.NULL_TXT;
	@Index private Integer gender = 0;
	@Index private Integer degreeType = 0;	//0: KS, 1: Master, 2: Ph.D.
	@Index private Integer departmentId = 0;	//0: CNPM, 1: HTTT, 2: KHMT
	private Date birthDate = null;//new Date();
	private String workEmail = Config.NULL_TXT;
	private String personalEmail = Config.NULL_TXT;
	private String workPhoneNumber = Config.NULL_TXT;
	private String cellPhoneNumber = Config.NULL_TXT;
	private String avatar = Config.NULL_TXT;
	private String workAddress = Config.NULL_TXT;
	private String personalAddress = Config.NULL_TXT;
	private String info = Config.NULL_TXT;
	@Index private Integer userType = 0;
	private List<Long> courseIds = new ArrayList<Long>();
	private List<Long> currentCourseIds = new ArrayList<Long>();
	private Integer activated = 0;
	
	private List<Long> materialIds = new ArrayList<Long>();
	private List<Long> categoryIds = new ArrayList<Long>();
	@Ignore private List<Material> materials = new ArrayList<Material>();
	@Ignore private List<Category> categories = new ArrayList<Category>();
	@Ignore private int posIndex = 0;
	
	public Student() {}
	
	public Student(String firstName, String lastName, String teacherId,
			int gender, int degreeType, int eduType, String email, String phone) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.setTeacherId(teacherId);
		this.gender = gender;
		this.degreeType = degreeType;
		this.workEmail = email;
		this.workPhoneNumber = phone;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTeacherId() {
		return studentId;
	}
	public void setTeacherId(String teacherId) {
		if (teacherId != null && !teacherId.isEmpty())
			this.studentId = teacherId;
	}
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	public Integer getDegreeType() {
		return degreeType;
	}
	public void setDegreeType(Integer degreeType) {
		this.degreeType = degreeType;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	public List<Long> getCourseIds() {
		return courseIds;
	}
	public void setCourseIds(List<Long> courseIds) {
		this.courseIds = courseIds;
	}
	public List<Long> getCurrentCourseIds() {
		return currentCourseIds;
	}
	public void setCurrentCourseIds(List<Long> currentCourseIds) {
		this.currentCourseIds = currentCourseIds;
	}
	
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public String getIdNumber() {
		return studentId;
	}

	public void setIdNumber(String idNumber) {
		this.studentId = idNumber;
	}

	public int getPosIndex() {
		return posIndex;
	}

	public void setPosIndex(int posIndex) {
		this.posIndex = posIndex;
	}
	
	public Integer getActivated() {
		return activated;
	}

	public void setActivated(Integer activated) {
		this.activated = activated;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullName() {
		return this.lastName + " " + this.firstName;
	}
	
	public Integer getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	public List<Long> getMaterialIds() {
		return materialIds;
	}

	public void setMaterialIds(List<Long> materialIds) {
		this.materialIds = materialIds;
	}

	public List<Long> getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(List<Long> categoryIds) {
		this.categoryIds = categoryIds;
	}

	public List<Material> getMaterials() {
		return materials;
	}

	public void setMaterials(List<Material> materials) {
		this.materials = materials;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public String getWorkEmail() {
		return workEmail;
	}

	public void setWorkEmail(String workEmail) {
		this.workEmail = workEmail;
	}

	public String getPersonalEmail() {
		return personalEmail;
	}

	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	public String getWorkPhoneNumber() {
		return workPhoneNumber;
	}

	public void setWorkPhoneNumber(String workPhoneNumber) {
		this.workPhoneNumber = workPhoneNumber;
	}

	public String getCellPhoneNumber() {
		return cellPhoneNumber;
	}

	public void setCellPhoneNumber(String cellPhoneNumber) {
		this.cellPhoneNumber = cellPhoneNumber;
	}

	public String getWorkAddress() {
		return workAddress;
	}

	public void setWorkAddress(String workAddress) {
		this.workAddress = workAddress;
	}

	public String getPersonalAddress() {
		return personalAddress;
	}

	public void setPersonalAddress(String personalAddress) {
		this.personalAddress = personalAddress;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
}
