package com.hungnt.edumanager.server;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
//import com.google.appengine.repackaged.com.google.common.util.Base64;

@SuppressWarnings("serial")
public class UploadServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(UploadServlet.class.getName());
	
    private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();    

    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {

        Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);
        BlobKey blobKey = blobs.get("image").get(0);
       
        if (blobKey == null) {

        } else {
        	ImagesService imagesService = ImagesServiceFactory.getImagesService();
        	ServingUrlOptions servingUrlOptions = ServingUrlOptions.Builder.withBlobKey(blobKey);
        	String imageUrl = imagesService.getServingUrl(servingUrlOptions);
//        	byte[] bytes = readBlobFully(blobKey);//blobstoreService.fetchData(blobKey, 0, blobstoreService.MAX_BLOB_FETCH_SIZE);
//        	System.out.println("Images blob size: " + bytes.length);
//        	String base64 = Base64.encode(bytes);
//        	System.out.println("Base64: " + Base64.encode(bytes));
//        	UploadedImage image = DataServiceImpl.uploadImage(blobKey.getKeyString(), imageUrl, base64);
            res.sendRedirect("/upload?uploadedImageKey=" + imageUrl);
        	//res.sendRedirect("/upload?uploadedImageKey=" + image.getId()); 
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    		throws ServletException, IOException {
    	
    	String uploadedImageKey = req.getParameter("uploadedImageKey");
    	resp.setHeader("Content-Type", "text/html");
    	
    	// This is a bit hacky, but it'll work. We'll use this key in an Async service to
    	// fetch the image and image information
    	resp.getWriter().println(uploadedImageKey);
    }
    
    public static byte[] readBlobFully(BlobKey blobKey){
    	  
    	 BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
    	 BlobInfo blobInfo = new BlobInfoFactory().loadBlobInfo(blobKey);
    	  
    	 if( blobInfo == null )
    	  return null;
    	  
    	 if( blobInfo.getSize() > Integer.MAX_VALUE )
    	  throw new RuntimeException("This method can only process blobs up to " + Integer.MAX_VALUE + " bytes");
    	  
    	 int blobSize = (int)blobInfo.getSize();
    	 int chunks = (int)Math.ceil(((double)blobSize / BlobstoreService.MAX_BLOB_FETCH_SIZE));
    	 int totalBytesRead = 0;
    	 int startPointer = 0;
    	 int endPointer;
    	 byte[] blobBytes = new byte[blobSize];
    	  
    	 for( int i = 0; i < chunks; i++ ){
    	   
    	  endPointer = Math.min(blobSize - 1, startPointer + BlobstoreService.MAX_BLOB_FETCH_SIZE - 1);
    	   
    	  byte[] bytes = blobstoreService.fetchData(blobKey, startPointer, endPointer);
    	   
    	  for( int j = 0; j < bytes.length; j++ )
    	   blobBytes[j + totalBytesRead] = bytes[j];
    	   
    	  startPointer = endPointer + 1;
    	  totalBytesRead += bytes.length;
    	 }
    	  
    	 return blobBytes;
    	}
    
//    private byte[] getImageBytes_v2(BlobData blobData) {
//        if (blobData == null || blobData.getKey() == null) {
//
//          return null;
//        }
//
//        BlobKey blobKey = new BlobKey(blobData.getKey());
//
//        
//        FileService fileService = FileServiceFactory.getFileService();
//        AppEngineFile file = null;
//        try {
//          file = fileService.getBlobFile(blobKey);
//        } catch (FileNotFoundException e) {
//          log.severe("getImageBytes_V2(): Error: fileService error " + e.toString());
//          e.printStackTrace();
//        }
//        
//        if (file == null) {
//          return null;
//        }
//        
//        FileReadChannel ch = null;
//        try {
//          ch = fileService.openReadChannel(file, false);
//        } catch (FileNotFoundException e) {
//          log.severe("getImageBytes_V2(): Error: file not found " + e.toString());
//          e.printStackTrace();
//        } catch (LockException e) {
//          log.severe("getImageBytes_V2(): Error: lock exception " + e.toString());
//          e.printStackTrace();
//        } catch (IOException e) {
//          log.severe("getImageBytes_V2(): Error: file read channel - io exception " + e.toString());
//          e.printStackTrace();
//        }
//
//        if (ch == null) {
//
//          return null;
//        }
//        
//        ByteArrayOutputStream out = new ByteArrayOutputStream();
//
//        byte[] array = new byte[1024];
//        ByteBuffer buf = ByteBuffer.wrap(array);
//        try {
//          while (ch.read(buf) != -1) {
//            buf.rewind();
//            byte[] a = buf.array();
//            try {
//              out.write(a);
//            } catch (Exception e) {
//              log.severe("getImageBytes_V2(): Error: buffered out error " + e.toString());
//              e.printStackTrace();
//            }
//            buf.clear();
//          }
//        } catch (IOException e) {
//          log.severe("getImageBytes_V2(): Error: io exception reading channel " + e.toString());
//          e.printStackTrace();
//        }
//
//        byte[] filebytes = out.toByteArray();
//        
//        return filebytes;
//      }
}