package com.hungnt.edumanager.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.google.appengine.api.appidentity.AppIdentityServiceFactory;

/**
 * Servlet implementation class UploadMp3Servlet
 */
public class GcsUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Hello");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=utf-8");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		final PrintWriter writer = response.getWriter();
		FileItemFactory fileItemFactory = new DiskFileItemFactory();
		ServletFileUpload servletFileUpload = new ServletFileUpload(fileItemFactory);
		servletFileUpload.setHeaderEncoding("UTF-8");
		String bucket = request.getParameter("bucket");
		if (bucket == null || bucket.isEmpty())
			bucket = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName();
		String folder = request.getParameter("folder");
		//Long categoryId = Long.parseLong(req.getParameter("categoryid"));
		try {
			FileItemIterator fileItemIterator = servletFileUpload.getItemIterator(request);
			while (fileItemIterator.hasNext()) {
				FileItemStream fileItemStream = fileItemIterator.next();
				System.out.println(fileItemStream.getContentType() + ":" + fileItemStream.getFieldName() + ":" + fileItemStream.getName());
				if (fileItemStream.isFormField()) {
					System.out.println("Got a form field: "
							+ fileItemStream.getFieldName() + " "
							+ Streams.asString(fileItemStream.openStream()));
				} else {
					InputStream is = fileItemStream.openStream();
					String fileName = fileItemStream.getName();
					if (fileName.contains("/"))
						fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
					fileName = new Date().getTime() + "_" + fileName;
					byte[] bytes = getBytes(is);
					//String url = AmazonS3Uploader.uploadFile(bytes, "sound/"+getRandomFileName(),bucket);
					if (folder != null && !folder.isEmpty())
						fileName = folder + "/" + fileName;
					String url = GoogleCloudService.createObject(bucket, fileName, bytes);
					System.out.println("Image url: " + url);
					writer.write(url);
				}
			}
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
	}
	
	public static byte[] getBytes(InputStream is) {

	    int len;
	    int size = 1024;
	    byte[] buf = null;
	    try {
		    if (is instanceof ByteArrayInputStream) {
		    
				size = is.available();
				  buf = new byte[size];
			      len = is.read(buf, 0, size);
			    } else {
			      ByteArrayOutputStream bos = new ByteArrayOutputStream();
			      buf = new byte[size];
			      while ((len = is.read(buf, 0, size)) != -1)
			        bos.write(buf, 0, len);
			      buf = bos.toByteArray();
			    }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    return buf;
	  }
}
