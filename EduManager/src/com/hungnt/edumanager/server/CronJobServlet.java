package com.hungnt.edumanager.server;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class CronJobServlet extends HttpServlet {
    private static final Logger log = Logger.getLogger(CronJobServlet.class.getName());
	
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
    	resp.getWriter().println("BlowJob is running from POST");
    	System.out.println("BlowJob is running from POST");
    	doGet(req, resp);
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    		throws ServletException, IOException {
    	System.out.println("BlowJob is running from GET");
    	resp.getWriter().println("BlowJob is running from GET");
    	DataServiceImpl dataService = new DataServiceImpl();
    	dataService.remind();
    }
}