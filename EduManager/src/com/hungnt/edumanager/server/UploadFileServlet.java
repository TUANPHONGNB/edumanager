package com.hungnt.edumanager.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.google.appengine.api.appidentity.AppIdentityServiceFactory;

/**
 * Servlet implementation class UploadMp3Servlet
 */
public class UploadFileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=utf-8");
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		final PrintWriter writer = response.getWriter();
		FileItemFactory fileItemFactory = new DiskFileItemFactory();
		ServletFileUpload servletFileUpload = new ServletFileUpload(fileItemFactory);
		String bucket = request.getParameter("bucket");
		if (bucket == null || bucket.isEmpty())
			bucket = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName();
		String folder = request.getParameter("folder");

		servletFileUpload.setHeaderEncoding("UTF-8");
		//Long categoryId = Long.parseLong(req.getParameter("categoryid"));
		try {
			FileItemIterator fileItemIterator = servletFileUpload.getItemIterator(request);
			while (fileItemIterator.hasNext()) {
				FileItemStream fileItemStream = fileItemIterator.next();

				if (fileItemStream.isFormField()) {
					System.out.println("Got a form field: "
							+ fileItemStream.getFieldName() + " "
							+ Streams.asString(fileItemStream.openStream()));
				} else {
					InputStream is = fileItemStream.openStream();
					String fileName = fileItemStream.getName();
					//String extension = fileName.contains(".") ? fileName.substring(fileName.lastIndexOf(".") + 1) : "";
					if (folder != null && !folder.isEmpty())
						fileName = folder + "/" + fileName;
					
					byte[] bytes = getBytes(is);
					String url = GoogleCloudService.createObject(bucket, fileName, bytes);
					System.out.println("File url: " + url);
					String json = "{\"url\":\"" + url + "\", \"filesize\":\"" + bytes.length + "\"}";
					System.out.println("File url: " + url);
					writer.write(json);
				}
			}
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
	}
	
	public static byte[] getBytes(InputStream is) {

	    int len;
	    int size = 1024;
	    byte[] buf = null;
	    try {
		    if (is instanceof ByteArrayInputStream) {
		    
				size = is.available();
				  buf = new byte[size];
			      len = is.read(buf, 0, size);
			    } else {
			      ByteArrayOutputStream bos = new ByteArrayOutputStream();
			      buf = new byte[size];
			      while ((len = is.read(buf, 0, size)) != -1)
			        bos.write(buf, 0, len);
			      buf = bos.toByteArray();
			    }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    return buf;
	  }
	
	public String getRandomFileName(String extension) {
		Long date = new Date().getTime();
		return date + "." + extension;
	}

}
