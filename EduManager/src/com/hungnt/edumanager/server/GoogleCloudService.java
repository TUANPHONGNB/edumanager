package com.hungnt.edumanager.server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;

public class GoogleCloudService {
	
	private static final GcsService gcsService = GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
	
	public static final Logger log = Logger.getLogger(GoogleCloudService.class.getName());

	public static String createObject(String bucket, String name, byte[] content) throws IOException {
		GcsFilename filename = new GcsFilename(bucket, name);
		return createObject(filename, content);
	}
	
	public static String createObject(GcsFilename fileName, byte[] content) throws IOException {
		GcsFileOptions options = new GcsFileOptions.Builder().acl("public-read").build();
		GcsOutputChannel outputChannel =
		        gcsService.createOrReplace(fileName, options);//GcsFileOptions.getDefaultInstance());
		    outputChannel.write(ByteBuffer.wrap(content));
		    outputChannel.close();
		return "http://storage.googleapis.com/" + fileName.getBucketName() + "/" + fileName.getObjectName();
	}
	
	public static void deleteObject(String bucket, String name) throws IOException {
		gcsService.delete(new GcsFilename(bucket, name));
	}
}
