package com.hungnt.edumanager.server;
import java.io.Serializable;

import com.google.appengine.api.memcache.Expiration; 
import com.google.appengine.api.memcache.MemcacheService; 
import com.google.appengine.api.memcache.MemcacheServiceException; 
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.api.memcache.Stats;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Teacher;

public class CacheSupport {
	
	private static MemcacheService cacheInit(String nameSpace) { 
		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService(nameSpace);
		return memcache;
	}
	private static Object cacheGet(String nameSpace, Object id){ 
		Object r = null; 
		MemcacheService memcache = cacheInit(nameSpace);
		try {
			r = memcache.get(id);
		} catch (MemcacheServiceException e) {
			// nothing can be done. 
		}
		return r; 
	}
	
	public static Stats getUserCacheStatistics() {
		MemcacheService memcache = cacheInit(Config.LOGIN_NAME_SPACE); 
		Stats stats = memcache.getStatistics();
		return stats;
	}
	
	private static void cacheDelete(String nameSpace, Object id){ 
		MemcacheService memcache = cacheInit(nameSpace); 
		memcache.delete(id);
	}

	private static void cachePutExp(String nameSpace, Object id, Serializable o, int exp) {
		MemcacheService memcache = cacheInit(nameSpace);
		try {
			if (exp>0) { 
				memcache.put(id, o, Expiration.byDeltaSeconds(exp));
			} else {
				memcache.put(id, o);
			} 
		}
		catch (MemcacheServiceException e) { // nothing can be done.
		}
	}
	private static void cachePut(String nameSpace, Object id, Serializable o) {
		int expiredSeconds = Config.DEFAULT_EXPIRED_TIME*24*60*60;
		cachePutExp(nameSpace, id, o, expiredSeconds);
	}
	
	private static void cachePutWithLongExpireDate(String nameSpace, Object id, Serializable o) {
		int expiredSeconds = Config.DEFAULT_LOGIN_EXPIRED_TIME*24*60*60;
		cachePutExp(nameSpace, id, o, expiredSeconds);
	}
	
	public static void reset(String nameSpace, String id) {
		if (id != null && !id.isEmpty()) {
			CacheSupport.cacheDelete(nameSpace, id);
		}
		else {
			MemcacheService memcache = cacheInit(nameSpace);
			memcache.clearAll();
		}
	}
	
	public static void reset() {
		MemcacheService memcache = cacheInit(Config.LOGIN_NAME_SPACE);
		memcache.clearAll();
	}
	
	/********************* LOGIN ********************/
	public static Teacher getLoginInfo(String sessionId) {
		Teacher loginInfo = (Teacher)CacheSupport.cacheGet(Config.LOGIN_NAME_SPACE, sessionId);
		return loginInfo;
	}
	
	public static void putLoginInfo(String sessionId, Teacher loginInfo, int exp) {
		CacheSupport.cachePutExp(Config.LOGIN_NAME_SPACE, sessionId, loginInfo, exp);
	}
	
	public static void putLoginInfo(String sessionId, Teacher loginInfo) {
		CacheSupport.cachePutExp(Config.LOGIN_NAME_SPACE, sessionId, loginInfo, 
				Config.LONG_EXPIRED_TIME*24*60*60*1000000);
	}
	
	public static void deleteLoginInfo(String sessionId) {
		CacheSupport.cacheDelete(Config.LOGIN_NAME_SPACE, sessionId);
	}
}