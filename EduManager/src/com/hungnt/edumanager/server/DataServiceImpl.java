package com.hungnt.edumanager.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.Query;
import com.hungnt.edumanager.client.DataService;
import com.hungnt.edumanager.shared.AccentRemover;
import com.hungnt.edumanager.shared.Category;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Course;
import com.hungnt.edumanager.shared.CourseInfo;
import com.hungnt.edumanager.shared.Equipment;
import com.hungnt.edumanager.shared.EquipmentHistory;
import com.hungnt.edumanager.shared.Event;
import com.hungnt.edumanager.shared.ExamInfo;
import com.hungnt.edumanager.shared.InternTopic;
import com.hungnt.edumanager.shared.LoggedInUser;
import com.hungnt.edumanager.shared.Material;
import com.hungnt.edumanager.shared.MeetingAbsent;
import com.hungnt.edumanager.shared.MeetingMinutes;
import com.hungnt.edumanager.shared.Publication;
import com.hungnt.edumanager.shared.ServerException;
import com.hungnt.edumanager.shared.Statistics;
import com.hungnt.edumanager.shared.Subject;
import com.hungnt.edumanager.shared.SubjectType;
import com.hungnt.edumanager.shared.Teacher;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class DataServiceImpl extends RemoteServiceServlet implements
		DataService {

	static {
 		ObjectifyService.register(Teacher.class);
		ObjectifyService.register(Subject.class);
		ObjectifyService.register(Course.class);
		ObjectifyService.register(CourseInfo.class);
		ObjectifyService.register(Publication.class);
		ObjectifyService.register(Material.class);
		ObjectifyService.register(Category.class);
		ObjectifyService.register(Equipment.class);
		ObjectifyService.register(ExamInfo.class);
		ObjectifyService.register(InternTopic.class);
		ObjectifyService.register(Statistics.class);
		ObjectifyService.register(EquipmentHistory.class);
		ObjectifyService.register(LoggedInUser.class);
		ObjectifyService.register(Event.class);
		ObjectifyService.register(SubjectType.class);
		ObjectifyService.register(MeetingMinutes.class);
		ObjectifyService.register(MeetingAbsent.class);
 	}
	
	@Override
	public Teacher login(String sessionId, String userName, String password) {
		List<Teacher> teachers = ofy().load().type(Teacher.class).filter("userName", userName).list();
		if (teachers.size() > 0) {
			Teacher teacher = teachers.get(0);
//			return teacher;
			String hashFromDB = teacher.getPassword();//(obtain hash from user's db entry)
			if (hashFromDB != null) {
				boolean valid = BCrypt.checkpw(password, hashFromDB);
				if (valid) {
					ofy().save().entity(new LoggedInUser(sessionId, teacher.getId()));
					teacher.setSessionId(sessionId);
					return teacher;
				}
			}
		}
		return null;
	}
	
	@Override
	public Teacher changePassword(String sessionId, Long id, String oldPassword, String newPassword) {
		Teacher teacher = ofy().load().type(Teacher.class).id(id).now();
		if (teacher != null) {
//			return teacher;
			String hashFromDB = teacher.getPassword();//(obtain hash from user's db entry)
			if (hashFromDB != null) {
				boolean valid = BCrypt.checkpw(oldPassword, hashFromDB);
				if (valid) {
					ofy().save().entity(new LoggedInUser(sessionId, teacher.getId())).now();
					teacher.setPassword(BCrypt.hashpw(newPassword, BCrypt.gensalt()));
					teacher.setSessionId(sessionId);
					ofy().save().entity(teacher).now();
					return teacher;
				}
			}
		}
		return null;
	}
	
	@Override
	public Teacher checkLogin(String sessionId) {
		LoggedInUser loggedIn = ofy().load().type(LoggedInUser.class).id(sessionId).now();
		if (loggedIn != null) {
			Teacher user = ofy().load().type(Teacher.class).id(loggedIn.getUserId()).now();
			user.setSessionId(sessionId);
			if (user.getWorkEmail().contains("hungnt@soict.hust.edu.vn")) {
			}
			return user;
		}
		return null;
	}
	
	@Override
	public void logout(String sessionId) {
		ofy().delete().type(LoggedInUser.class).id(sessionId).now();
	}
	
	@Override
	public List<Teacher> getTeachers(String userId, int scholarYear) {
		List<Teacher> teachers = new ArrayList<Teacher>(ofy().load().type(Teacher.class).list());
		System.out.println("Teacher num: " + teachers.size());
		for (Teacher teacher : teachers) {
			Map<Integer, Statistics> stats = new HashMap<Integer, Statistics>();
			for (int semester = 1; semester <= 3; semester ++) {
				String id = teacher.getId() + "_" + scholarYear + "_" + semester;
				Statistics stat = ofy().load().type(Statistics.class).id(id).now();
				if (stat != null) {
					stats.put(semester, stat);
				}
			}
			System.out.println("Found stats: " + stats.size());
			teacher.setStatMap(stats);
		}
		return teachers;
	}
	
	@Override
	public List<Equipment> getEquipments(String userId, Date now) {
		List<Equipment> list = new ArrayList<Equipment>(ofy().load().type(Equipment.class).list());
		for (Equipment e : list) {
			List<EquipmentHistory> history = new ArrayList<EquipmentHistory>(ofy().load().type(EquipmentHistory.class).filter("status <=", 2).list());
			if (history.size() > 0) {
				for (EquipmentHistory h : history) {
					if (h.getToDate().before(now)) {
						h.setStatus(2);
						ofy().save().entity(h).now();
					}
					else {
						if (h.getFromDate().before(now))
							e.setCurrentHistory(h);
						e.getHistories().add(h);
					}
				}
			}
		}
		return list;
	}
	
	@Override
	public List<Event> getEvents(String userId, Date now) {
		List<Event> list = new ArrayList<Event>(ofy().load().type(Event.class).list());
		for (Event event : list) {
			boolean changed = false;
			if (event.getStatus() == 0 && event.getDate() != null
					&& event.getDate().before(now)) {
				event.setStatus(1);
				changed = true;
			}
			if (changed)
				ofy().save().entity(event).now();
		}

		return list;
	}
	
	@Override
	public List<Publication> getPublications(String userId) {
		List<Publication> publications = new ArrayList<Publication>(ofy().load().type(Publication.class).list());
		for (Publication pub : publications) {
			if (pub.getMaterialIds().size() > 0) {
				pub.getMaterials().addAll(ofy().load().type(Material.class).ids(pub.getMaterialIds()).values());
			}
		}
		return publications;
	}
	
	@Override
	public List<Subject> getSubjects(String userId) {
		return new ArrayList<Subject>(ofy().load().type(Subject.class).list());
	}
	
	@Override
	public List<Course> getCourses(String userId, int scholarYear, int semester) {
		List<Course> courses = new ArrayList<Course>();
		Query<Course> query = ofy().load().type(Course.class);
		if (scholarYear >= 0) {
			query = query.filter("scholarYear", scholarYear);
		}
		if (semester > 0) {
			query = query.filter("semester", semester);
		}
		courses.addAll(query.list());
//			courses.addAll(ofy().load().type(Course.class).filter("scholarYear", scholarYear).list());
//		else
//			courses.addAll(ofy().load().type(Course.class).list());
		System.out.println("Course num: " + courses.size());
		for (Course course : courses) {
			if (course.getTimePlaceIds().size() > 0) {
				course.getTimePlaces().addAll(ofy().load().type(CourseInfo.class).ids(course.getTimePlaceIds()).values());
			}
			if (course.getExamIds().size() > 0) {
				course.getExams().addAll(ofy().load().type(ExamInfo.class).ids(course.getExamIds()).values());
			}
			Date now = new Date();
			boolean changed = false;
			if (course.getStatus() == 0 && course.getStartDate() != null
					&& course.getEndDate() != null 
					&& course.getStartDate().before(now)) {
				course.setStatus(1);
				changed = true;
			}
			if (course.getStatus() == 1 && course.getStartDate() != null
					&& course.getEndDate() != null 
					&& course.getEndDate().before(now)) {
				course.setStatus(2);
				changed = true;
			}
			if (changed)
				ofy().save().entity(course).now();
		}
		return courses;
	}
	
	@Override
	public List<InternTopic> getTopics(String userId, int scholarYear, int semester) {
		List<InternTopic> courses = new ArrayList<InternTopic>();
		Query<InternTopic> query = ofy().load().type(InternTopic.class);
		if (scholarYear >= 0) {
			query = query.filter("scholarYear", scholarYear);
		}
		if (semester > 0) {
			query = query.filter("semester", semester);
		}
		courses.addAll(query.list());
//		if (scholarYear >= 0)
//			courses.addAll(ofy().load().type(InternTopic.class).filter("scholarYear", scholarYear).list());
//		else
//			courses.addAll(ofy().load().type(InternTopic.class).list());
		return courses;
	}
	
	@Override
	public Equipment updateEquipment(String userId, Equipment object) {
		if (object.getId() == null || object.getId() <= 0) {
			ofy().save().entity(object).now();
		}
		else {
			//Update
			ofy().save().entity(object).now();
		}
		return object;
	}
	
	@Override
	public Event updateEvent(String userId, Event object) {
		if (object.getId() == null || object.getId() <= 0) {
			ofy().save().entity(object).now();
		}
		else {
			//Update
			ofy().save().entity(object).now();
		}
		return object;
	}
	
	@Override
	public Teacher updateTeacher(String userId, Teacher teacher) {
		if (teacher.getUserName() == null || teacher.getUserName().trim().isEmpty())
			teacher.setUserName(teacher.getWorkEmail());
		System.out.println("Teacher Id: " + teacher.getId());
		if (teacher.getId() == null || teacher.getId() <= 0) {
			teacher.setPassword(BCrypt.hashpw(teacher.getPassword(), BCrypt.gensalt()));
			ofy().save().entity(teacher).now();
		}
		else {
			//Update
			ofy().save().entity(teacher).now();
		}
		return teacher;
	}
	
	@Override
	public Subject updateSubject(String userId, Subject obj) throws ServerException {
		if (obj.getId() == null || obj.getId() <= 0) {
			List<Subject> existing = ofy().load().type(Subject.class).filter("subjectIdCode", obj.getSubjectIdCode()).list();
			if (existing.size() > 0) {
				throw new ServerException("Môn " + existing.get(0).getName() + " với mã học phần " 
						+ existing.get(0).getSubjectIdCode() + " đã tồn tại, hãy kiểm tra lại.");
			}
			ofy().save().entity(obj).now();
		}
		else {
			//Update
			ofy().save().entity(obj).now();
		}
		return obj;
	}
	
	@Override
	public Publication updatePublication(String userId, Publication obj) {
		if (obj.getId() == null || obj.getId() <= 0) {
			ofy().save().entity(obj).now();
		}
		else {
			//Update
			ofy().save().entity(obj).now();
		}
		if (obj.getMaterials().size() > 0) {
			for (Material mat : obj.getMaterials()) {
				mat.setParentId(obj.getId());
				if (mat.getId() == null || mat.getId() <= 0) {
					ofy().save().entity(mat).now();
					obj.getMaterialIds().add(mat.getId());
				}
			}
			ofy().save().entity(obj).now();
		}
		if (obj.getAuthorIds().size() > 0) {
			List<Teacher> teachers = new ArrayList<Teacher>(ofy().load().type(Teacher.class).ids(obj.getAuthorIds()).values());
			for (Teacher t : teachers) {
				if (!t.getPublicationIds().contains(obj.getId())) {
					t.getPublicationIds().add(obj.getId());
				}
			}
			ofy().save().entities(teachers);
		}
		System.out.println("AuthorList: " + obj.getAuthorIds());
		return obj;
	}
	
	@Override
	public Course updateCourse(String userId, Course obj) throws ServerException {
		if (obj.getId() == null || obj.getId() <= 0) {
			List<Course> existing = ofy().load().type(Course.class).filter("scholarYear", obj.getScholarYear()).filter("courseIdCode", obj.getCourseIdCode()).list();
			if (existing.size() > 0) {
				throw new ServerException("Môn với mã lớp " 
						+ existing.get(0).getCourseIdCode() + " đã tồn tại, hãy kiểm tra lại.");
			}
		}
		if (obj.getTimePlaces().size() > 0) {
			ofy().save().entities(obj.getTimePlaces()).now();
			obj.getTimePlaceIds().clear();
			for (CourseInfo info : obj.getTimePlaces()) {
				obj.getTimePlaceIds().add(info.getId());
			}
		}
		if (obj.getExams().size() > 0) {
			ofy().save().entities(obj.getExams()).now();
			obj.getExamIds().clear();
			for (ExamInfo info : obj.getExams()) {
				obj.getExamIds().add(info.getId());
			}
		}
		if (obj.getId() == null || obj.getId() <= 0) {
			ofy().save().entity(obj).now();
//			int aNum = obj.getPaymentType() == 0 ? 1 : 0;
//			int bNum = obj.getPaymentType() == 1 ? 1 : 0;
//			this.addToStatistic(obj.getTeacherId(), obj.getTeacherName(), obj.getScholarYear(), obj.getSemester(), aNum, bNum, 0, 0);
		}
		else {
			//Update
			ofy().save().entity(obj).now();
		}
		if (obj.getScholarYear() != Config.currentScholarYear
				|| obj.getSemester() != Config.currentSemester)
			return obj;
		Teacher teacher = ofy().load().type(Teacher.class).id(obj.getTeacherId()).now();
		boolean changed = false;
		if (!teacher.getCurrentCourseIds().contains(obj.getId())) {
			teacher.getCurrentCourseIds().add(obj.getId());
			teacher.setCurrentCourseNum(teacher.getCurrentCourseIds().size());
			changed = true;
		}
		List<Long> current1ABIds = obj.getPaymentType() == 0 ? teacher.getCurrentCourse1AIds() 
				: teacher.getCurrentCourse1BIds();
		if (!current1ABIds.contains(obj.getId())) {
			current1ABIds.add(obj.getId());
			changed = true;
		}
		if (changed) {
			teacher.setCurrentCourseNum(teacher.getCurrentCourseIds().size());
			teacher.setCurrentCourse1ANum(teacher.getCurrentCourse1AIds().size());
			teacher.setCurrentCourse1BNum(teacher.getCurrentCourse1BIds().size());
			ofy().save().entity(teacher).now();
		}
		return obj;
	}
	
	@Override
	public void deleteCourse(Long userId, Course obj) {
		List<CourseInfo> courseInfo = new ArrayList<CourseInfo>();
		List<ExamInfo> examInfo = new ArrayList<ExamInfo>();
		if (obj.getTimePlaceIds().size() > 0) {
			courseInfo.addAll(ofy().load().type(CourseInfo.class).ids(obj.getTimePlaceIds()).values());
			ofy().delete().entities(courseInfo).now();
		}
		if (obj.getExamIds().size() > 0) {
			examInfo.addAll(ofy().load().type(ExamInfo.class).ids(obj.getExamIds()).values());
			ofy().delete().entities(examInfo).now();
		}
//		int aNum = obj.getPaymentType() == 0 ? -1 : 0;
//		int bNum = obj.getPaymentType() == 1 ? -1 : 0;
//		this.addToStatistic(obj.getTeacherId(), obj.getTeacherName(), obj.getScholarYear(), obj.getSemester(), aNum, bNum, 0, 0);

		Teacher teacher = ofy().load().type(Teacher.class).id(obj.getTeacherId()).now();
		boolean changed = false;
		if (teacher.getCurrentCourseIds().contains(obj.getId())) {
			teacher.getCurrentCourseIds().remove(obj.getId());
			teacher.setCurrentCourseNum(teacher.getCurrentCourseIds().size());
			changed = true;
		}
		List<Long> current1ABIds = obj.getPaymentType() == 0 ? teacher.getCurrentCourse1AIds() 
				: teacher.getCurrentCourse1BIds();
		if (current1ABIds.contains(obj.getId())) {
			current1ABIds.remove(obj.getId());
			changed = true;
		}
		if (changed) {
			teacher.setCurrentCourseNum(teacher.getCurrentCourseIds().size());
			teacher.setCurrentCourse1ANum(teacher.getCurrentCourse1AIds().size());
			teacher.setCurrentCourse1BNum(teacher.getCurrentCourse1BIds().size());
			ofy().save().entity(teacher).now();
		}
		ofy().delete().entity(obj).now();
	}
	
	@Override
	public InternTopic updateTopic(String userId, InternTopic obj) throws ServerException {
		if (obj.getId() == null || obj.getId() <= 0) {
			ofy().save().entity(obj).now();
		}
		else {
			//Update
			ofy().save().entity(obj).now();
		}
		Teacher teacher = ofy().load().type(Teacher.class).id(obj.getTeacherId()).now();
		if (!teacher.getTopicIds().contains(obj.getId())) {
			teacher.getTopicIds().add(obj.getId());
			ofy().save().entity(teacher).now();
		}
		return obj;
	}

	@Override
	public void deleteTopic(String userId, InternTopic obj) {
		Teacher teacher = ofy().load().type(Teacher.class).id(obj.getTeacherId()).now();
		if (teacher != null && teacher.getTopicIds().contains(obj.getId())) {
			teacher.getTopicIds().remove(obj.getId());
			ofy().save().entity(teacher).now();
		}
		ofy().delete().entity(obj).now();
	}
	
	
	@Override
	public String getBlobstoreUploadUrl() {
		BlobstoreService blobstoreService = BlobstoreServiceFactory
				.getBlobstoreService();
		return blobstoreService.createUploadUrl("/upload");
	}
	
	@Override
	public String export(Long userId, Teacher teacher) {
		List<Course> courses = new ArrayList<Course>(ofy().load().type(Course.class).ids(teacher.getCurrentCourseIds()).values());
		HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(teacher.getFullName());
         
        Map<String, Object[]> data = new HashMap<String, Object[]>();
        data.put("1", new Object[] {"TT", "Name", "HK", "Hệ ĐT", "Loại giờ", "Mã HP", "Tên Học Phần", "Mã lớp"});
        for (int i = 1; i < courses.size(); i ++) {
        	Course course = courses.get(i);
            data.put("" + i, new Object[] {i, course.getTeacherName(), course.getSemester(), 
            		course.getClassType(), course.getCourseType(), course.getSubjectIdCode(), 
            		course.getSubjectName(), course.getCourseIdCode()});
        }
         
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if(obj instanceof Date) 
                    cell.setCellValue((Date)obj);
                else if(obj instanceof Boolean)
                    cell.setCellValue((Boolean)obj);
                else if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Double)
                    cell.setCellValue((Double)obj);
            }
        }
         
        try {
        	ByteArrayOutputStream output = new ByteArrayOutputStream();
            //FileOutputStream out = 
            //        new FileOutputStream(new File("C:\\new.xls"));
            workbook.write(output);
            //out.close();
            byte[] xls = output.toByteArray();
            String url = GoogleCloudService.createObject(AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName(), 
            		teacher.getFullName().replaceAll(" ", "_") + ".xls", xls);
            //TODO
            //workbook.close();
			System.out.println("Excel url: " + url);
            System.out.println("Excel written successfully..");
            this.getThreadLocalResponse().setContentType("application/vnd.ms-excel");//("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            this.getThreadLocalResponse().setHeader("Content-Disposition","attachment;filename=" + teacher.getFullName().replaceAll(" ", "_") + ".xls");
            this.getThreadLocalResponse().getOutputStream().write(xls);
            //this.getThreadLocalResponse().getOutputStream().close();
            return url;
             
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
	}
	
	public static Teacher getTeacher(Long teacherId) {
		return ofy().load().type(Teacher.class).id(teacherId).now();
	}
	
	public static List<Course> getCourses(Teacher teacher) {
		return new ArrayList<Course>(ofy().load().type(Course.class).filter("teacherId", teacher.getId()).filter("scholarYear", Config.currentScholarYear).list());
	}
	
//	public Statistics addToStatistic(long teacherId, String teacherName, int scholarYear, int semester, 
//			int aNum, int bNum, int student, int publication) {
//		String id = teacherId + "_" + scholarYear + "_" + semester;
//		Statistics stat = ofy().load().type(Statistics.class).id(id).now();
//		if (stat == null) {
//			stat = new Statistics();
//			stat.setId(id);
//		}
//		stat.setTeacherId(teacherId);
//		stat.setTeacherName(teacherName);
//		stat.setScholarYear(scholarYear);
//		stat.setSemester(semester);
//		stat.addCourse1ANum(aNum);
//		stat.addCourse1BNum(bNum);
//		stat.setPublicationNum(stat.getPublicationNum() + publication);
//		stat.setStudentNum(stat.getStudentNum() + student);
//		ofy().save().entity(stat).now();
//		return stat;
//	}
	
	@Override
	public void updateAB() {
		List<Teacher> teachers = new ArrayList<Teacher>(ofy().load().type(Teacher.class).list());
		for (Teacher teacher : teachers) {
			for (int semester = 1; semester <= 3; semester ++) {
				Statistics stat = new Statistics();
				stat.setId(teacher.getId() + "_" + Config.currentScholarYear + "_" + semester);
				List<Course> courses = ofy().load().type(Course.class).filter("teacherId", teacher.getId())
						.filter("scholarYear", Config.currentScholarYear).filter("semester", semester).list();
				if (courses.size() > 0) {
					int aNum = 0;
					int bNum = 0;
					for (Course course : courses) {
						if (course.getPaymentType() == 0)
							aNum ++;
						else
							bNum ++;
					}
					stat.addCourse1ANum(aNum);
					stat.addCourse1BNum(bNum);
					stat.setTeacherId(teacher.getId());
					stat.setSemester(semester);
					stat.setScholarYear(Config.currentScholarYear);
					stat.setTeacherName(teacher.getFullName());
					ofy().save().entity(stat).now();
				}
			}
		}
	}
	
	@Override
	public EquipmentHistory updateEquipmentHistory(EquipmentHistory history) throws ServerException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(history.getFromDate());
		calendar.set(Calendar.HOUR_OF_DAY, history.getFromHour());
		calendar.set(Calendar.MINUTE, history.getFromMinute());
		history.setFromDate(calendar.getTime());
		calendar.setTime(history.getToDate());
		calendar.set(Calendar.HOUR_OF_DAY, history.getToHour());
		calendar.set(Calendar.MINUTE, history.getToMinute());
		history.setToDate(calendar.getTime());
		
		List<EquipmentHistory> histories = new ArrayList<EquipmentHistory>(ofy().load().type(EquipmentHistory.class).filter("equipmentId", history.getEquipmentId()).filter("status <=", 2).list());
		if (histories.size() > 0) {
			for (EquipmentHistory h : histories) {
				if ((history.getFromDate().after(h.getFromDate()) && history.getFromDate().before(h.getToDate()))
						|| (history.getToDate().after(h.getFromDate()) && history.getToDate().before(h.getToDate()))) {
					throw new ServerException("Bạn bị trùng lịch với " + h.toString());
				}
			}
		}
		
		ofy().save().entity(history);
		return history;
	}
	
	/*****************Category**********************/
	@Override
	public Long doUpdateCategory(Category category) {
		saveCategory(category);
		Category parent = getCategoryById(category.getParentId());
		if (parent != null) {
			parent.getChildrenIds().add(category.getId());
			saveCategory(parent);
		}
		return category.getId();
	}
	
	private Long saveCategory(Category category){
		ofy().save().entity(category).now();
		return category.getId();
	}
	
	private Category getCategoryById(long id){
		Category category = ofy().load().type(Category.class).id(id).now();
		return category;
	}

	@Override
	public List<Category> getCategories(Long parentId, Long ownerId, Long viewerId) {
		List<Category> result = new ArrayList<Category>();
		result.addAll(ofy().load().type(Category.class).filter("parentId", parentId).filter("privacy", 0).list());
		
		if (viewerId != null && viewerId > 0) {
			result.addAll(ofy().load().type(Category.class).filter("parentId", parentId).filter("privacy", 1).list());
		}
		
		if (ownerId != null && ownerId > 0 && viewerId != null && viewerId > 0) {
			result.addAll(ofy().load().type(Category.class).filter("parentId", parentId).filter("userId", ownerId).filter("privacy", 2).list());
		}
		return result;
	}

	@Override
	public Category getChildCategoriesAndMaterials(Long parentId) {
		Category parent = getCategoryById(parentId);
		System.out.println("parentId: " + parentId + " and ChildIdNum: " + parent.getChildrenIds().size() + " and MatIdNum: " + parent.getMaterialIds().size());
		if (parent.getChildrenIds().size() > 0)
			parent.getChildren().addAll(ofy().load().type(Category.class).ids(parent.getChildrenIds()).values());
		if (parent.getMaterialIds().size() > 0)
			parent.getMaterials().addAll(ofy().load().type(Material.class).ids(parent.getMaterialIds()).values());
		System.out.println("ChildNum: " + parent.getChildren().size() + " and MatNum: " + parent.getMaterials().size());
		return parent;
	}
	
	/*********Material*****************/
	
	private Long saveMaterial(Material material){
		ofy().save().entity(material).now();
		return material.getId();
	}

	@Override
	public Long doUpdateMaterial(Material material) {
		saveMaterial(material);
		if(material.getParentId()!=Config.NULL_ID){
			Category parent = getCategoryById(material.getParentId());
			if (!parent.getMaterialIds().contains(material.getId())) {
				parent.getMaterialIds().add(material.getId());
				saveCategory(parent);
			}
		}
		return material.getId();
	}
	
	@Override
	public Long doUpdateMeeting(MeetingMinutes meeting) {
		ofy().save().entity(meeting).now();
		if (meeting.getAbsentReasons().size() > 0) {
			for (MeetingAbsent absent : meeting.getAbsentReasons()) {
				absent.setMeetingId(meeting.getId());
				absent.setMeetingDate(meeting.getDate());
			}
			ofy().save().entities(meeting.getAbsentReasons()).now();
		}
		String attached = "";
		if (meeting.getAttacheds().size() > 0) {
			attached = "<br>Các file đính kèm:<br>";
			for (String url : meeting.getAttacheds()) {
				attached += "<a href='" + url + "'>" + url + "</a><br>";
			}
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		sendMail(Config.ADMIN_EMAIL, "Bo mon CNPM", "hungnt@soict.hust.edu.vn", "Bien ban: " + AccentRemover.removeAccent(meeting.getTitle()),
				"Kính gửi các Thầy Cô biên bản <br>" 
						+ meeting.getTitle() + " - " 
				+ sdf.format(meeting.getDate()) + "</b><br>" +
				meeting.getContent() + attached + getEmailFooter(null));
		return meeting.getId();
	}
	
	
	@Override
	public List<MeetingMinutes> getMeetings(int scholarYear, int semester) {
		List<MeetingMinutes> list = new ArrayList<MeetingMinutes>();
		list.addAll(ofy().load().type(MeetingMinutes.class).filter("scholarYear", scholarYear).list());
		return list;
	}
	
	@Override
	public void delete(Long userId, Material material) {
		if(material.getParentId()!=Config.NULL_ID){
			Category parent = getCategoryById(material.getParentId());
			if (parent.getMaterialIds().contains(material.getId())) {
				parent.getMaterialIds().remove(material.getId());
				saveCategory(parent);
			}
		}
		ofy().delete().entity(material).now();
	}
	
	public void remind() {
		System.out.println("Checking for reminding");
    	List<Course> courses = new ArrayList<Course>();
    	courses.addAll(ofy().load().type(Course.class).filter("scholarYear", Config.currentScholarYear).filter("status", 0).list());
    	long now = new Date().getTime();
    	//List<Course> reminds = new ArrayList<Course>();
    	System.out.println("Courses to be reminded: " + courses.size());
    	long dayInMili = 86400000;
    	Integer days[] = {1, 7};
    	List<Course> toSave = new ArrayList<Course>();
    	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    	sdf.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
    	System.out.println("Now in GMT+7 timezone: " + sdf.format(new Date()));
    	List<Teacher> admins = ofy().load().type(Teacher.class).filter("userType", Config.USER_TYPE_ADMIN).list();
    	for (Course course : courses) {
    		for (Integer day : days) {
	    		if (course.getStartDate() != null && course.getStartDate().getTime() - now < dayInMili*day
	    				&& (course.getDoneNotification() == 0 || day < course.getDoneNotification())) {
	    			//reminds.add(course);
	    			course.setDoneNotification(day);
	    			if (!toSave.contains(course))
	    				toSave.add(course);
	    			if (course.getTimePlaceIds().size() > 0) {
	    				course.getTimePlaces().addAll(ofy().load().type(CourseInfo.class).ids(course.getTimePlaceIds()).values());
	    			}
	    			Teacher teacher = getTeacher(course.getTeacherId());
	    			sendMailForCourse(course, teacher, day, sdf);
//	    			for (Teacher admin : admins) {
//	    				sendMailForCourse(course, admin, day, sdf);
//	    			}
	    		}
    		}
    	}
    	for (Course course : courses) {
    		for (Integer day : days) {
	    		if (course.getStartDate() != null && course.getStartDate().getTime() - now < dayInMili*day
	    				&& (course.getDoneNotification() == 0 || day < course.getDoneNotification())) {
	    			//reminds.add(course);
	    			course.setDoneNotification(day);
	    			if (!toSave.contains(course))
	    				toSave.add(course);
	    			if (course.getTimePlaceIds().size() > 0) {
	    				course.getTimePlaces().addAll(ofy().load().type(CourseInfo.class).ids(course.getTimePlaceIds()).values());
	    			}
	    			Teacher teacher = getTeacher(course.getTeacherId());
	    			sendMailForCourse(course, teacher, day, sdf);
//	    			for (Teacher admin : admins) {
//	    				sendMailForCourse(course, admin, day, sdf);
//	    			}
	    		}
    		}
    	}

    	if (toSave.size() > 0) {
    		ofy().save().entities(toSave).now();
    	}

    	List<Event> events = new ArrayList<Event>();
    	events.addAll(ofy().load().type(Event.class).filter("status", 0).list());
    	System.out.println("Events to be reminded: " + events.size());
    	List<Event> toSaveEvents = new ArrayList<Event>();
    	//List<Event> remindEvents = new ArrayList<Event>();
    	for (Event event : events) {
    		for (int day : days) {
	    		if (event.getDate() != null && event.getDate().getTime() - now < dayInMili*day
	    				&& (event.getDoneNotification() == 0 || day < event.getDoneNotification())) {
	    			event.setDoneNotification(day);
	    			if (!toSaveEvents.contains(event))
	    				toSaveEvents.add(event);
	    			//remindEvents.add(event);
	    			List<Teacher> teachers = new ArrayList<Teacher>(ofy().load().type(Teacher.class).ids(event.getInvolvedUsers()).values());
	    			for (Teacher teacher : teachers) {
	    				sendMailForEvent(teacher, event, day, sdf);
	    			}
	    			for (Teacher admin : admins) {
	    				sendMailForEvent(admin, event, day, sdf);
	    			}
	    		}
    		}
    	}
    	if (toSaveEvents.size() > 0) {
    		ofy().save().entities(toSaveEvents).now();
    	}
	}
	
	private void sendMailForCourse(Course course, Teacher teacher, int day, SimpleDateFormat sdf) {
		String title = (teacher.getGender() == 0) ? "Cô" : "Thầy";
		String time = day == 1 ? "Tuần này" : day == 7 ? "Tuần tới" : "";
		String content = "Kính gửi " + title + " <b>" + teacher.getFullName() + "</b>,<br>"
				+ time + " " + title + " sẽ có lịch bắt đầu dạy một lớp học mới.<br>"
				+ "Thông tin chi tiết như sau:<br>"
				+ "   Tên môn học: <b>" + course.getSubjectName() + "</b><br>"
				+ "   Loại hình: <b>" + Config.getCourseType(course.getCourseType()) + "</b><br>"
				+ "   Hệ đào tạo: <b>" + Config.getStudentTypeName(course.getStudenType()) + "</b><br>"
				+ "   Khối lượng: <b>" + course.getCreditInfo() + "</b><br>"
				+ "   Thời gian/Địa điểm: <b>" + course.getPlaceTimeInfo();
		if (course.getStartDate() != null)
			content += " từ " + sdf.format(course.getStartDate());
		if (course.getEndDate() != null)
			content += " đến " + sdf.format(course.getEndDate());
		content += "</b><br>";
		content += "<br>Kính mong " + title + " lưu ý giúp về lớp học này.<br>";
		content += "Xin chân thành cảm ơn " + title + "!<br>";
		content += "<br>Kính thư<br>";
		content += "Bộ môn Công Nghệ Phần Mềm<br>";
		content += getEmailFooter(teacher.getWorkEmail());
		sendMail(Config.ADMIN_EMAIL, "Bo mon CNPM", teacher.getWorkEmail(), "V/v: Lich day mon " + AccentRemover.removeAccent(course.getSubjectName()), content);
	}
	
	private void sendMailForEvent(Teacher teacher, Event event, int day, SimpleDateFormat sdf) {
		String title = (teacher.getGender() == 0) ? "Cô" : "Thầy";
		String content = getEventEmailHeader(title, event, day, teacher.getFullName());
		content +=	"Thông tin chi tiết như sau:<br>";
		content += "     Nội dung: <b>" + event.getDescription() + "</b><br>";
		String time = sdf.format(event.getDate()) + " lúc " + event.getHour() + "h:" + event.getMinute() + "m";
		if (event.getEventType() == 0) {
			content += "     Thời gian: <b>" + time + " tại " + event.getPlace() + "</b><br>";
		}
		else {
			content += "     Deadline: <b>" + time + "</b><br>";
		}
    	String users = "";
    	for (String name : event.getInvolvedUserNames())
    		users += name + ", ";
    	content += "     Thầy cô tham gia: <b>" + users + "</b><br>";
		content += "<br>";
		content += "Kính mong " + title + " lưu ý giúp về thời gian công việc.<br>";
		content += "Xin chân thành cảm ơn " + title + "!<br>";
		content += "<br>Kính thư,<br>";
		content += "Bộ môn Công Nghệ Phần Mềm<br><br>";
		content += getEmailFooter(teacher.getWorkEmail());
		sendMail(Config.ADMIN_EMAIL, "Bo mon CNPM", teacher.getWorkEmail(), 
				"V/v: " + AccentRemover.removeAccent(event.getName() + " - " + time), content);
	}
	
	private String getEventEmailHeader(String title, Event event, int day, String fullName) {
		String nextTime = day == 1 ? "Ngày mai" : day == 7 ? "Tuần tới" : "";
		String content = "";
		if (title != null)
			content += "Kính gửi " + title + " <b>" + fullName + "</b>,<br>"
				+ nextTime + " " + title + " có công việc: <b>" + event.getName() + "</b>.<br>";
		else {
			content += "Kính gửi Admin,<br>"
					+ nextTime + " Bộ môn có công việc: <b>" + event.getName() + "</b>.<br>";
		}
		return content;
	}

	private String getEmailFooter(String email) {
		String content = "";
		content += "<br>Các thông tin khác, xin truy cập website http://www.se-soict.appspot.com<br>";
		if (email != null && !email.isEmpty()) {
			content += "Thông tin truy cập:<br>";
			content += "    Tên đăng nhập: <b>" + email + "</b><br>";
			content += "    Mật khẩu     : <b>" + Config.DEFAULT_PASSWORD + "</b> (Thầy Cô nên thay đổi mật khẩu sau khi đăng nhập)<br>";
		}
		content += "Email này được gửi tự động. Do trang web đang trong thời gian hoàn thiện, nếu có gì sơ xuất kính mong Thầy Cô thứ lỗi.";
		return content;
	}
	
	private void sendMail(String fromEmail, String fromNickName, String to,
			String subject, String content) {
				Properties props = new Properties();
				Session session = Session.getDefaultInstance(props, null);
				// Create a default MimeMessage object.
				try {
					MimeMessage message = new MimeMessage(session);// error is shown in
					// Set the RFC 822 "From" header field using the value of the
					// InternetAddress.getLocalAddress method.
					message.setFrom(new InternetAddress(fromEmail, fromNickName));
					// Add the given addresses to the specified recipient type.
					message.addRecipient(Message.RecipientType.TO, new InternetAddress(to, "You"));
					// Set the "Subject" header field.
					message.setSubject(subject);
					// Sets the given String as this part's content, with a MIME type of
					// "text/plain".
					//message.setText(content);
					message.setContent(content, "text/html; charset=utf-8");
					Transport.send(message);
				} catch (UnsupportedEncodingException e) {
					System.out.println("Failed to send mail: " + e.getMessage());
				} catch (AddressException e) {
					System.out.println("Failed to send mail: " + e.getMessage());
				} catch (MessagingException e) {
					System.out.println("Failed to send mail: " + e.getMessage());
				}		
	}
	
	@Override
	public void updateStatistics(long userId, int scholarYear, int semester) {
		List<Course> courses = ofy().load().type(Course.class).filter("scholarYear", scholarYear).filter("semester", semester).list();
		Map<Long, Statistics> stats = new HashMap<Long, Statistics>();
		for (Course course : courses) {
			Statistics stat = stats.get(course.getTeacherId());
			if (stat == null) {
				String statId = course.getTeacherId() + "_" + scholarYear + "_" + semester;
				stat = ofy().load().type(Statistics.class).id(statId).now();
				if (stat == null)
					stat = new Statistics(statId, course.getTeacherId(), scholarYear, semester);
				stat.reset();
				stats.put(course.getTeacherId(), stat);
			}
			if (course.getCourseType() == Config.COURSE_TYPE_LTBT
					|| course.getCourseType() == Config.COURSE_TYPE_BT
					|| course.getCourseType() == Config.COURSE_TYPE_LT) {
				if (course.getStudenType() == Config.STUDENT_TYPE_CD_B) {
					if (!stat.getCdClassIds().contains(course.getId()))
						stat.getCdClassIds().add(course.getId());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_CLC) {
					if (!stat.getClcClassIds().contains(course.getId()))
						stat.getClcClassIds().add(course.getId());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_CNCN) {
					if (!stat.getCncnClassIds().contains(course.getId()))
						stat.getCncnClassIds().add(course.getId());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_CTTT) {
					if (!stat.getCtttClassIds().contains(course.getId()))
						stat.getCtttClassIds().add(course.getId());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_DHCQ) {
					if (!stat.getKsClassIds().contains(course.getId()))
						stat.getKsClassIds().add(course.getId());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_DHLK) {
					if (!stat.getDhlkClassIds().contains(course.getId()))
						stat.getDhlkClassIds().add(course.getId());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_HEDSPI) {
					if (!stat.getVnClassIds().contains(course.getId()))
						stat.getVnClassIds().add(course.getId());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_KSTN) {
					if (!stat.getKstnClassIds().contains(course.getId()))
						stat.getKstnClassIds().add(course.getId());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_SDH) {
					if (!stat.getSdhClassIds().contains(course.getId()))
						stat.getSdhClassIds().add(course.getId());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_SIE) {
					if (!stat.getSieClassIds().contains(course.getId()))
						stat.getSieClassIds().add(course.getId());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_TC) {
					if (!stat.getTcClassIds().contains(course.getId()))
						stat.getTcClassIds().add(course.getId());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_VB2) {
					if (!stat.getKs2ClassIds().contains(course.getId()))
						stat.getKs2ClassIds().add(course.getId());
				}
			}
			else if (course.getCourseType() == Config.COURSE_TYPE_DA
					|| course.getCourseType() == Config.COURSE_TYPE_TT
					|| course.getCourseType() >= Config.COURSE_TYPE_PROJECT1) {
				if (course.getStudenType() == Config.STUDENT_TYPE_CD_B) {
					if (course.getCourseType() == Config.COURSE_TYPE_DA)
						stat.setCdDatnNum(course.getStudentNum());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_CLC) {
					if (course.getCourseType() == Config.COURSE_TYPE_DA)
						stat.setClcDatnNum(course.getStudentNum());
					else if (course.getCourseType() == Config.COURSE_TYPE_TT)
						stat.setClcTttnNum(course.getStudentNum());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_CNCN) {
					if (course.getCourseType() == Config.COURSE_TYPE_DA)
						stat.setClcDatnNum(course.getStudentNum());
					else if (course.getCourseType() == Config.COURSE_TYPE_TT)
						stat.setClcTttnNum(stat.getClcTttnNum());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_CTTT) {
					//TODO:
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_DHCQ) {
					if (course.getCourseType() == Config.COURSE_TYPE_DA)
						stat.setKsDatnNum(course.getStudentNum());
					else if (course.getCourseType() == Config.COURSE_TYPE_PROJECT1)
						stat.setKsProject1Num(course.getStudentNum());
					else if (course.getCourseType() == Config.COURSE_TYPE_PROJECT2)
						stat.setKsProject2Num(course.getStudentNum());
					else if (course.getCourseType() == Config.COURSE_TYPE_PROJECT3)
						stat.setKsProject3Num(course.getStudentNum());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_DHLK) {
					//TODO:
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_HEDSPI) {
					if (course.getCourseType() == Config.COURSE_TYPE_DA)
						stat.setVnDatnNum(course.getStudentNum());
					else if (course.getCourseType() == Config.COURSE_TYPE_GR1)
						stat.setVnGr1Num(course.getStudentNum());
					else if (course.getCourseType() == Config.COURSE_TYPE_GR2)
						stat.setVnGr2Num(course.getStudentNum());
					else if (course.getCourseType() == Config.COURSE_TYPE_GR3)
						stat.setVnGr3Num(course.getStudentNum());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_KSTN) {
					if (course.getCourseType() == Config.COURSE_TYPE_DA)
						stat.setKstnDatnNum(course.getStudentNum());
					else if (course.getCourseType() == Config.COURSE_TYPE_PROJECT2)
						stat.setKstnProject2Num(course.getStudentNum());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_SDH) {
					//TODO:
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_SIE) {
					if (course.getCourseType() == Config.COURSE_TYPE_DA)
						stat.setSieDatnNum(course.getStudentNum());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_TC) {
					if (course.getCourseType() == Config.COURSE_TYPE_DA)
						stat.setTcDatnNum(course.getStudentNum());
				}
				else if (course.getStudenType() == Config.STUDENT_TYPE_VB2) {
					if (course.getCourseType() == Config.COURSE_TYPE_DA)
						stat.setKs2DatnNum(course.getStudentNum());
				}
			}
		}
		for (Long key : stats.keySet()) {
			Statistics stat = stats.get(key);
			stat.setCourse1ANum(stat.getClcClassIds().size() + stat.getKsClassIds().size() + stat.getCtttClassIds().size() 
					+ stat.getVnClassIds().size() + stat.getKstnClassIds().size() + stat.getIctClassIds().size());
			stat.setCourse1BNum(stat.getSieClassIds().size() + stat.getTcClassIds().size() + stat.getKs2ClassIds().size());
			stat.setProject1ANum(stat.getKsDatnNum() + stat.getKsProject1Num() + stat.getKsProject2Num() + stat.getKsProject3Num()
					+ stat.getCnDatnNum() + stat.getClcDatnNum() + stat.getClcTtcnNum() + stat.getClcTttnNum() + stat.getClcTttnNum()
					+ stat.getKstnDatnNum() + stat.getKstnProject2Num()
					+ stat.getIctDatnNum() + stat.getIctGr1Num() + stat.getIctGr2Num() + stat.getIctGr3Num()
					+ stat.getVnDatnNum() + stat.getVnGr1Num() + stat.getVnGr2Num() + stat.getVnGr3Num());
			stat.setProject1BNum(stat.getSieDatnNum() + stat.getTcDatnNum() + stat.getKs2DatnNum() + stat.getCdDatnNum());
		}
		ofy().save().entities(stats.values());
	}
}