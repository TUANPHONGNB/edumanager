package com.hungnt.edumanager.server;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.hungnt.edumanager.shared.AccentRemover;
import com.hungnt.edumanager.shared.Config;
import com.hungnt.edumanager.shared.Course;
import com.hungnt.edumanager.shared.Teacher;

public class ExporterServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			// Initialize Http Response Headers
			String teacherIdStr = request.getParameter(Config.EXPORT_TOKEN);
			Long teacherId = Config.NULL_ID;
			if (teacherIdStr != null && !teacherIdStr.isEmpty())
				teacherId = Long.parseLong(teacherIdStr);
			Teacher teacher = DataServiceImpl.getTeacher(teacherId);
            String contentType = "application/vnd.ms-excel";
            //String contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            response.setContentType(contentType);
            String fileName = AccentRemover.removeAccent(teacher.getFullName().replaceAll(" ", "_"));
            response.setHeader("Content-Disposition","attachment; filename=" + fileName + ".xls");
            System.out.println("Excel written successfully: " + fileName + ".xls");
            //response.setHeader("Content-Type", contentType);
			//PrintWriter writer = response.getWriter();

			List<Course> courses = DataServiceImpl.getCourses(teacher);
			List<Course> courses1A = new ArrayList<Course>();
			List<Course> courses1B = new ArrayList<Course>();
			
			for (Course course : courses) {
				if (course.getCourseType() != Config.COURSE_TYPE_DA
						&& course.getCourseType() < Config.COURSE_TYPE_PROJECT1) {
					if (course.getPaymentType() == 0)
						courses1A.add(course);
					else
						courses1B.add(course);
				}
			}
			for (Course course : courses) {
				if (course.getCourseType() == Config.COURSE_TYPE_DA
						|| course.getCourseType() >= Config.COURSE_TYPE_PROJECT1) {
					if (course.getPaymentType() == 0)
						courses1A.add(course);
					else
						courses1B.add(course);
				}
			}
			
			HSSFWorkbook workbook = new HSSFWorkbook();
			
			this.writeExcel(workbook, "Tong hop 1A", courses1A);
			this.writeExcel(workbook, "Tong hop 1B", courses1B);
			
//	        HSSFSheet sheet = workbook.createSheet(teacher.getFullName());
//	         
//	        //Map<String, Object[]> data = new HashMap<String, Object[]>();
//	        List<Object[]> data = new ArrayList<Object[]>();
//	        data.add(new Object[] {"TT", "Họ tên cán bộ", "HK", "Hệ ĐT", "Loại giờ", "Mã học phần", 
//	        		"Tên Học Phần", "Mã lớp", "Số SV", "Số TC", "Số giờ", "kL, kT", "kC", "TT duyệt", "Giờ quy đổi"});
//	        data.add(new Object[] {"[1]", "[2]", "[3]", "[4]", "[5]", "[6]", 
//	        		"[7]", "[8]", "[9]", "[10]", "[11]", "[12]", "[13]", "[14]", "[15]"});
//	        for (int i = 0; i < courses.size(); i ++) {
//	        	Course course = courses.get(i);
//	            data.add(new Object[] {i + 1, course.getTeacherName(), course.getSemester(), 
//	            		Config.getStudentTypeName(course.getStudenType()), Config.getCourseType(course.getCourseType()), 
//	            		course.getSubjectIdCode(), course.getSubjectName(), 
//	            		course.getCourseIdCode(), course.getStudentNum(), course.getCredits(),
//	            		course.getCredits() * 15, 1, 1, 1, course.getCredits() * 15});
//	        }
//	         
//	        //Set<String> keyset = data.keySet();
//	        int rownum = 0;
//	        for (Object[] objArr : data) {
//	            Row row = sheet.createRow(rownum++);
////	            Object [] objArr = data.get(key);
//	            int cellnum = 0;
//	            for (Object obj : objArr) {
//	                Cell cell = row.createCell(cellnum++);
//	                if(obj instanceof Date) 
//	                    cell.setCellValue((Date)obj);
//	                else if(obj instanceof Boolean)
//	                    cell.setCellValue((Boolean)obj);
//	                else if(obj instanceof String)
//	                    cell.setCellValue((String)obj);
//	                else if(obj instanceof Double)
//	                    cell.setCellValue((Double)obj);
//	                else if(obj instanceof Integer)
//	                    cell.setCellValue((Integer)obj);
//	            }
//	        }
	         
	        try {
	        	//ByteArrayOutputStream output = new ByteArrayOutputStream();
	            //FileOutputStream out = 
	            //        new FileOutputStream(new File("C:\\new.xls"));
	            workbook.write(response.getOutputStream());
	            //out.close();
	            //byte[] xls = output.toByteArray();
	            //String url = GoogleCloudService.createObject(AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName(), 
	            //		teacher.getFullName().replaceAll(" ", "_") + ".xls", xls);
	            workbook.close();
				//System.out.println("Excel url: " + url);
	            //response.setContentLength((int)xls.length);
	            ServletOutputStream stream = response.getOutputStream();
	            //stream.write(xls);
	            stream.flush();
	            stream.close();
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		}
		catch (Exception e) {
	    	response.setContentType("text/plain");
	    	response.getWriter().print("An error as occured while exporting cardset:" + e.getMessage());
		}
	}
	
	private void writeExcel(HSSFWorkbook workbook, String tabName, List<Course> courses) {
        HSSFSheet sheet = workbook.createSheet(tabName);
        
        //Map<String, Object[]> data = new HashMap<String, Object[]>();
        List<Object[]> data = new ArrayList<Object[]>();
        data.add(new Object[] {"TT", "Họ tên cán bộ", "HK", "Hệ ĐT", "Loại giờ", "Mã học phần", 
        		"Tên Học Phần", "Mã lớp", "Số SV", "Số TC", "Số giờ", "kL, kT", "kC", "TT duyệt", "Giờ quy đổi"});
        data.add(new Object[] {"[1]", "[2]", "[3]", "[4]", "[5]", "[6]", 
        		"[7]", "[8]", "[9]", "[10]", "[11]", "[12]", "[13]", "[14]", "[15]"});
        for (int i = 0; i < courses.size(); i ++) {
        	Course course = courses.get(i);
            data.add(new Object[] {i + 1, course.getTeacherName(), course.getSemester(), 
            		Config.getStudentTypeName(course.getStudenType()), Config.getCourseType(course.getCourseType()), 
            		course.getSubjectIdCode(), course.getSubjectName(), 
            		course.getCourseIdCode(), course.getStudentNum(), course.getCredits(),
            		course.getCredits() * 15, 1, 1, 1, course.getCredits() * 15});
        }
         
        //Set<String> keyset = data.keySet();
        int rownum = 0;
        for (Object[] objArr : data) {
            Row row = sheet.createRow(rownum++);
//            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if(obj instanceof Date) 
                    cell.setCellValue((Date)obj);
                else if(obj instanceof Boolean)
                    cell.setCellValue((Boolean)obj);
                else if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Double)
                    cell.setCellValue((Double)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.getWriter().println("Hello from Get method");
	}
}